<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReviewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
            Schema::create('reviews', function (Blueprint $table) {
            $table->increments('review_id');
            $table->string('review_name');
            $table->string('review_image');
            $table->text('review_description');
            $table->integer('item_id')->unsigned();
            $table->foreign('item_id')->references('item_id')->on('items')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reviews');
    }
}
