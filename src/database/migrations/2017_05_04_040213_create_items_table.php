<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
          Schema::create('items', function (Blueprint $table) {
            $table->increments('item_id');
            $table->string('item_name');
            $table->text('item_description');
            $table->integer('item_price');
            $table->text('details');
            $table->enum('item_status',['active','dormant']);
            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('category_id')->on('categorys')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
