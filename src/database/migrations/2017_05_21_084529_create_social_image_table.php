<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSocialImageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('socialimages', function (Blueprint $table) {
            $table->increments('socialimage_id');
            $table->string('image_link');
            $table->string('image_caption');
            $table->integer('social_id')->unsigned();
            $table->foreign('social_id')->references('social_id')->on('socials')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('socialimages');
    }
}
