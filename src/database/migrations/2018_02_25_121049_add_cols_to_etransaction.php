<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToEtransaction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('e_gift_card_transactions', function (Blueprint $table) {
            $table->integer('giftCard_id')->unsigned()->nullable();
            $table->foreign('giftCard_id')->references('id')->on('gift_cards')->onDelete('cascade');
            $table->string('token')->unique()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('e_gift_card_transactions', function (Blueprint $table) {
            //
        });
    }
}
