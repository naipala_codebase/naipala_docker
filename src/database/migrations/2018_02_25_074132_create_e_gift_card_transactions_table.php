<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEGiftCardTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('e_gift_card_transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->enum('amount',[25,50,100,150,200]);
            $table->string('recipient_name');
            $table->string('sender_name');
            $table->text('message');
            $table->enum('status',['completed','pending'])->default('pending');
            $table->string('sender_email')->nullable();
            $table->string('recipient_contact')->nullable();
            $table->date('delivery_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('e_gift_card_transactions');
    }
}
