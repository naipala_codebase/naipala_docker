<?php

use Illuminate\Database\Seeder;

class Landing3Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'config_key' => 'Video_Image',
            'config_page'=>'Landing',
            'config_alias'=>'video-image'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Story_Image',
            'config_page'=>'Landing',
            'config_alias'=>'story-image'
        ]);
    }
}
