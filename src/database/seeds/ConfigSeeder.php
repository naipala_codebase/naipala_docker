<?php

use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('configs')->insert([
            'config_key' => 'Landing_Text',
            'config_page'=>'Landing',
            'config_alias'=>'home'
        ]);
    //    DB::table('configs')->insert([
    //         'config_key' => 'Terms and Agreements',
    //         'config_page'=>'Info',
    //         'config_alias'=>'terms-and-agreements'
    //     ]);
    //    DB::table('configs')->insert([
    //         'config_key' => 'Contact',
    //         'config_page'=>'Info',
    //         'config_alias'=>'contact'
    //     ]);
    //    DB::table('configs')->insert([
    //         'config_key' => 'About Us',
    //         'config_page'=>'Info',
    //         'config_alias'=>'about-us'
    //     ]);
    //    DB::table('configs')->insert([
    //         'config_key' => 'Others',
    //         'config_page'=>'Info',
    //         'config_alias'=>'others'
    //     ]);
        DB::table('configs')->insert([
            'config_key' => 'Contact',
            'config_page'=>'Contact',
            'config_alias'=>'contact',
            'config_value' => '1234567890'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Collection',
            'config_page'=>'Collection',
            'config_alias'=>'Collection',
            'config_value' => '<p>To do our DESIGNS justice we source quality materials that are assembled by select artisans. A sense of craft and use of traditional techniques lend the pieces a soft and decidedly HUMAN touch. Luxury Lifestyle Accessories With Down To Earth Aesthetics SINCE 2007.</p>'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'shipping_donation_text',
            'config_page'=>'Final_purchase',
            'config_alias'=>'Final_purchase',
            'config_value' => '<p></p>'
        ]); 
    }
}
