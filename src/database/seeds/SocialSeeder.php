<?php

use Illuminate\Database\Seeder;

class SocialSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'config_key' => 'Social',
            'config_page'=>'Social',
            'config_alias'=>'social'
        ]);
    }
}
