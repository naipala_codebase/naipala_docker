<?php

use Illuminate\Database\Seeder;

class CurrencySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'name' => 'Australian Dollar',
            'code'=>'AUD',
            'symbol'=>'$',
            'exchange_rate' => 1
        ]); 
        DB::table('countries')->insert([
            'name' => 'Australia',
            'code'=>'AU',
            'term_for_administrative_divisions'=>'States'
            
        ]);   
    }
}
