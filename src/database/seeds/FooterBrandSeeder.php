<?php

use Illuminate\Database\Seeder;

class FooterBrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'config_key' => 'Right Footer Title',
            'config_page'=>'footer',
            'config_value'=>'asd',
            'config_alias'=>'right_footer_title'
        ]);

        DB::table('configs')->insert([
            'config_key' => 'Left Footer Title',
            'config_page'=>'footer',
            'config_value'=>'asd',
            'config_alias'=>'left_footer_title'
        ]);
    }
}
