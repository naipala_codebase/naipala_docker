<?php

use Illuminate\Database\Seeder;

class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admin_users')->insert([
            'name' => 'safal',
            'email' => 'safaljoshie@gmail.com',
            'password' => bcrypt('Naipala@977{'),
        ]);

        DB::table('admin_users')->insert([
            'name' => 'sarthak',
            'email' => 'iamsarthakjoshi@gmail.com',
            'password' => bcrypt('Naipala@977{'),
        ]);
    }
}
