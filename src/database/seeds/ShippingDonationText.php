<?php

use Illuminate\Database\Seeder;

class ShippingDonationText extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'config_key' => 'shipping_donation_text',
            'config_page'=>'Final_purchase',
            'config_alias'=>'Final_purchase',
            'config_value' => '<p></p>'
        ]);
    }
}
