<?php

use Illuminate\Database\Seeder;

class Landing2Seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('configs')->insert([
            'config_key' => 'First_Featured_Title',
            'config_page'=>'Landing',
            'config_alias'=>'first_featured_title',
            'config_value' => '#'
        ]);       
        DB::table('configs')->insert([
            'config_key' => 'Second_Featured_Title',
            'config_page'=>'Landing',
            'config_alias'=>'second_featured_Title',
            'config_value' => '#'
        ]);       
        DB::table('configs')->insert([
            'config_key' => 'Third_Featured_Title',
            'config_page'=>'Landing',
            'config_alias'=>'third_featured_Title',
            'config_value' => '#'
        ]);   
        
        DB::table('configs')->insert([
            'config_key' => 'First_Featured_Subtitle',
            'config_page'=>'Landing',
            'config_alias'=>'first_featured_Subtitle',
            'config_value' => '#'
        ]);       
        DB::table('configs')->insert([
            'config_key' => 'Second_Featured_Subtitle',
            'config_page'=>'Landing',
            'config_alias'=>'second_featured_Subtitle',
            'config_value' => '#'
        ]);       
        DB::table('configs')->insert([
            'config_key' => 'Third_Featured_Subtitle',
            'config_page'=>'Landing',
            'config_alias'=>'third_featured_Subtitle',
            'config_value' => '#'
        ]); 

        DB::table('configs')->insert([
            'config_key' => 'First_Featured_Link',
            'config_page'=>'Landing',
            'config_alias'=>'first_featured_link',
            'config_value' => '#'
        ]);       
        DB::table('configs')->insert([
            'config_key' => 'Second_Featured_Link',
            'config_page'=>'Landing',
            'config_alias'=>'second_featured_link',
            'config_value' => '#'
        ]);       
        DB::table('configs')->insert([
            'config_key' => 'Third_Featured_Link',
            'config_page'=>'Landing',
            'config_alias'=>'third_featured_link',
            'config_value' => '#'
        ]); 
    }
}
