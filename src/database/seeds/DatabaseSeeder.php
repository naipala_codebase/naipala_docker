<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminUserSeeder::class);
        $this->call(ConfigSeeder::class);
        $this->call(ServiceCategorySeeder::class);
        $this->call(SocialSeeder::class);
        $this->call(LandingSeeder::class);
        $this->call(Landing2Seeder::class);
        $this->call(Landing3Seeder::class);
        $this->call(ShippingDonationText::class);  
        $this->call(CurrencySeeder::class);    
        $this->call(FooterBrandSeeder::class);       

    }

}
