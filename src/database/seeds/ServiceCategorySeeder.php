<?php

use Illuminate\Database\Seeder;

class ServiceCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('service_categories')->insert([
            'title' => 'Jobs',
        ]);
        DB::table('service_categories')->insert([
            'title' => 'Rents',
        ]);
    }
}
