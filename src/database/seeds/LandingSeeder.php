<?php

use Illuminate\Database\Seeder;

class LandingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('landingimages')->insert([
            'image_link' => '1499260328895.jpg',
            'redirect_link'=>'#',
        ]);
        DB::table('landingimages')->insert([
            'image_link' => '1499260350277.jpg',
            'redirect_link'=>'#',
        ]);

        DB::table('configs')->insert([
            'config_key' => 'Featured_Image',
            'config_page'=>'Landing',
            'config_alias'=>'featured_image'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'First_Featured_Image',
            'config_page'=>'Landing',
            'config_alias'=>'first_featured_image'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Second_Featured_Image',
            'config_page'=>'Landing',
            'config_alias'=>'second_featured_image'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Third_Featured_Image',
            'config_page'=>'Landing',
            'config_alias'=>'third_featured_image'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Video_Link',
            'config_page'=>'Landing',
            'config_alias'=>'video-link'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Trending_Title',
            'config_page'=>'Landing',
            'config_alias'=>'trending-title'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Trending_Description',
            'config_page'=>'Landing',
            'config_alias'=>'trending-description'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Story_Title',
            'config_page'=>'Landing',
            'config_alias'=>'story-title'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Story_Subtitle',
            'config_page'=>'Landing',
            'config_alias'=>'story-subtitle'
        ]);
        DB::table('configs')->insert([
            'config_key' => 'Story_Description',
            'config_page'=>'Landing',
            'config_alias'=>'story-description'
        ]);
    }
}
