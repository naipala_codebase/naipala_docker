var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var Redis = require('ioredis');
var redis = new Redis();

http.listen(6379, function(){
});

redis.subscribe('channel-notify', function(err, count) {
	console.log('Subscribed to channel-notify');
});

redis.subscribe('notify-admin', function(err, count) {
	console.log('Subscribed to notify-admin');
});

	redis.on('message', function(channel, message) {
		message = JSON.parse(message);

		if(message.event == 'ServiceBooked'){
			console.log("notification send to "+ message.data.userName);
			io.emit(channel + ':' + message.data.userId, message.data);	
		}

		else if(message.event=='CartCheckout'){
			console.log("notification send to admin for transaction- "+ message.data.transaction_id);
			io.emit(channel , message.data);
		}
});