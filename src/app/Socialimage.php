<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Socialimage extends Model
{
    public function social()
    {
    	return $this->belongsTo('App\Social','social_id','social_id');
    }
}
