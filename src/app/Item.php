<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{

    protected $primaryKey = "item_id";
    public $appends = ['display_price','display_old_price'];

    public function category()
    {
        return $this->belongsTo('App\Category','category_id','category_id');
    }
    public function item_images()
    {
        return $this->hasMany('App\Item_Image','item_id','item_id');
    }
    public function attributes()
    {
    	return $this->hasMany('App\Attribute','item_id','item_id');
    }
    public function reviews()
    {
        return $this->hasMany('App\Review','item_id','item_id');
    }

    public function getDisplayPriceAttribute()
    {
        return Currency::currency($this->item_price);
    }

    public function getDisplayOldPriceAttribute()
    {
        return Currency::currency($this->item_old_price);
    }
}

        