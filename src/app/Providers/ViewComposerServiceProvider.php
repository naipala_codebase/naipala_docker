<?php 
namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\View\Factory as ViewFactory;

use Illuminate\Support\Facades\View;
use Cart;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot(ViewFactory $view)
    {
        $view->composer('layouts.app', 'App\Http\ViewComposers\CategoryComposer');
        $view->composer('layouts.app', 'App\Http\ViewComposers\InfoComposer');
        $view->composer('layouts.app', 'App\Http\ViewComposers\NotificationComposer');
        $view->composer('layouts.admin', 'App\Http\ViewComposers\AdminNotificationComposer');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

}