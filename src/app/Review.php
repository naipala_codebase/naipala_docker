<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    public function item()
    {
    	return $this->belongsTo('App\Item','item_id','item_id');
    }
}
