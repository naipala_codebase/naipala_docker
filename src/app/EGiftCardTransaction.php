<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EGiftCardTransaction extends Model
{
    public function giftCard()
    {
        return $this->belongsTo('App\GiftCard','giftCard_id');
    }
}
