<?php

namespace App\Listeners;

use App\Events\CartCheckout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use LRedis;

class SendCartCheckoutNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CartCheckout  $event
     * @return void
     */
    public function handle(CartCheckout $event)
    {
        $data = [
            'event' => 'CartCheckout',
            'data' => [
                'transaction_id' => $event->transaction
            ]
        ];

        $redis = LRedis::connection();
        $redis->publish('notify-admin', json_encode($data));
    }
}
