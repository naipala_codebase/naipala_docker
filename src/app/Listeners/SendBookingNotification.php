<?php

namespace App\Listeners;

use App\Events\ServiceBooked;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use LRedis;
use Mail;
use App\Mail\ServiceBookEmail;

class SendBookingNotification
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ServiceBooked  $event
     * @return void
     */
    public function handle(ServiceBooked $event)
    {
        $data = [
            'event' => 'ServiceBooked',
            'data' => [
                'userId' => $event->user->id,
                'userName' => $event->user->name
            ]
        ];

        $redis = LRedis::connection();
        $redis->publish('channel-notify', json_encode($data));
    }
}
