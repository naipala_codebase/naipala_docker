<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ServiceBook extends Model
{
    public function service(){
    	return $this->belongsTo('App\Service');
    }

    public function service_bookable()
    {
        return $this->morphTo();
    }
}
