<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Transaction;
use App\EGiftCardTransaction;
use Session;
use App\Mail\OrderShipped;
use Mail;
use DateTime;
use VisitLog;
use App\User;
use App\Subscriber;
use Auth;
use Validator;

class AdminUserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        date_default_timezone_set('Australia/Sydney');
        $date = new DateTime();
        
        $date->setTime(0,0,0);

        $users = User::where('confirmed',1)->paginate(5, ['*'], 'users');
        $subscribers = Subscriber::paginate(5, ['*'], 'subscribers');
    
        $all=VisitLog::all();
        $totalvisitors=$all->count();
        $todaysvisitors=$all->where('updated_at','>',$date)->count();
        $sales = Transaction::where('status','=','completed')->orWhere('status','=','shipped')->sum('total');
        
        $active_users = [];
        $count = 0;
        foreach(User::where('confirmed',1)->get() as $user){
            if($user->isOnline()){
                if($count < 5)
                    $active_users []= $user;
                $count++;
            }
        }
        $active_users = collect($active_users);
        return view('admin.admin',compact('totalvisitors','users','todaysvisitors','sales','subscribers','active_users','count'));
    }

    public function allusers(){
        $users = User::where('confirmed',1)->get();
        return view('admin.allusers')->with('users',$users);
    }

    public function allsubscribers(){
        $subscribers = Subscriber::all();
        return view('admin.allsubscribers')->with('subscribers',$subscribers);
    }
    
    public function active_users(){
        $active_users = [];
        foreach(User::where('confirmed',1)->get() as $user){
            if($user->isOnline())
                $active_users []= $user;
        }
        $active_users = collect($active_users);
        return view('admin.active_users')->with('active_users',$active_users);
    }

    public function notifications(Request $request){
        if ($request->ajax()){
            $transactions = Transaction::where('status','=','completed')->orderBy('updated_at','desc')->limit(10)->get();

            $notifications = [];

            foreach ($transactions as $transaction) {
                $count = 0;

                $items = json_decode($transaction->details);

                foreach ($items as $item) {
                    $count += $item->quantity;
                }

                $notification = array(
                    'id' => $transaction->id,
                    'customerName' => $transaction->transactionable->name,
                    'agent_code'=>$transaction->agent_code,
                    'count' => $count,
                    
                );
                $notifications []= $notification;
            }

            return $notifications;
        }
        else
            abort(404);
    }

    public function notificationsAll(){
        $transactions = Transaction::where('status','=','completed')
                                    ->orWhere('status','=','shipped')
                                    ->orderBy('updated_at','desc')->paginate(10);

        $notifications = [];

        foreach ($transactions as $transaction) {
            $count = 0;

            $items = json_decode($transaction->details);

            foreach ($items as $item) {
                $count += $item->quantity;
            }

            $notification = array(
                'id' => $transaction->id,
                'customerName' => $transaction->transactionable->name,
                'status' => $transaction->status,
                'agent_code'=>$transaction->agent_code,
                'count' => $count,
                'updated_at' => $transaction->updated_at
            );
            $notifications []= $notification;
        }

        return view('admin.notifications.notifications')->with('notifications',$notifications)->with('transactions',$transactions);
    }

    public function notification($id){
        $transaction = Transaction::find($id);
        if(!$transaction) abort(404);

        return view('admin.notifications.notification')->with('transaction',$transaction);

    }

    public function confirmDelivery($id){

        $transaction = Transaction::find($id);
        if ($transaction->status == 'shipped')
            Session::flash('error','The customer has already been notified');
        else
        {
            $transaction->status = 'shipped';
            $transaction->save();
            Mail::to($transaction->transactionable->email)->send(new OrderShipped($transaction));
            Session::flash('success','The customer will be notified soon');
        }
        return redirect()->route('notification',$transaction->id);
    }

    public function getUnseen(Request $request){

        if ($request->ajax()){
            $unseen = Transaction::where('status','completed')->count();
            return $unseen;
        }

        abort(404);
    }

    public function changepassword(){
        
        return view('admin.changepassword');
    }

    public function changedpassword(Request $request){
        $me = Auth::guard('admin_user')->user();

        $validator = Validator::make($request->all(), [
            'oldPassword' => 'required',
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return redirect()->route('admin.changepassword')->withErrors($validator)->withInput();
        }

        if (Auth::attempt(['email' => $me->email, 'password' => $request->oldPassword]))
        {
            $me->password = bcrypt($request->password);
            $me->save();

            Session::flash('success','Password changed');
            return redirect()->route('admin.changepassword');
        }
        else{
            return redirect()->route('admin.changepassword')->withErrors(['oldPassword'=>'Incorrect password']);
        }
    }

    public function eCards(){
        $etransactions = EGiftCardTransaction::where('status','=','completed')
                        ->orderBy('updated_at','desc')->paginate(10);

        return view('admin.ecards')->with('etransactions',$etransactions);
    }
}
