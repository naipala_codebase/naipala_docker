<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Config;
use App\Landingimage;
use App\User;
use App\Subscriber;
use App\Service;
use App\Socialimage;
use App\Social;
use App\Mail\NewSubscriberWelcome;
use Mail;
use Validator;
use Session;
use App\Item;
use App\Featured;
use App\FeaturedBrand;
use App\Category;
use App\Donation;
use Auth;
use App\Transaction;
use App\Currency;
use App\Invitation;
use Cart;
use App\Socialcomment;
use GuzzleHttp\Client;

class HomeController extends Controller
{
    
    public function test(){
        // dd(VisitLog::save());
    }
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $landingimages=Landingimage::all();
        $categories = Category::where('category_name','!=','Tickets')->get();
        $fid=Featured::all()->pluck('item_id');
        $allitems=Item::with('item_images')->with('category')->get();
        $featureditems=[];
        $remainitems=[];
        foreach ($allitems as $f) {
            if($fid->contains($f->item_id))
            {
                $featureditems[]=$f;
            }
            else
            {
                $remainitems[]=$f;
            }
        }

        $final_featureditems = [];

        $index = [];
        if(count($featureditems) > 3){
            $i= rand(0,count($featureditems)-1);
            for($j=0;$j<3;$j++){
                while(in_array($i, $index)){
                    $i= rand(0,count($featureditems)-1);
                }
                $index []= $i;
            }

            foreach($index as $i){
                $final_featureditems []= $featureditems[$i];
            }
        }
        else
            $final_featureditems = $featureditems;
        $landingconfigs=Config::where('config_page','Landing')->get();
        // dd($landingimages);
        return view('landing.landing',compact('landingimages','final_featureditems','categories','landingconfigs'));
    }

    public function setCurrency(Request $request){
        $code = $request->input('currency');

        $currency = Currency::where('code',$code)->where('active',1)->first();

        if(!$currency) $currency = Currency::where('code','AUD')->first();

        $request->session()->put('currency', $currency);

        $items = Cart::content();
        foreach($items as $item){
            $price = Currency::currency_convert($item->options['price'], $currency);
            Cart::update($item->rowId, ['price' => $price]);
        }

        return redirect()->back();
    }

    public function info($slug='')
    {
        $info=Config::where('config_page','Info')->where('config_alias',$slug)->first();
        if(!$info) abort(404);

        return view('landing.about',compact('info'));
    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        
        if ($validator->fails()) {
            Session::flash('error','Please provide all information and Try again.');
            return redirect()->back();
        }
        
        if (! Subscriber::where('email', '=', $request->email)->exists()) {
            
            $grecaptchaToken = $request->input('g-recaptcha-response');
            if($grecaptchaToken) {
                $client = new Client();
                $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                    'form_params' => array(
                        'secret' => '6LcdNKoUAAAAAJ2NcXCw2GGPdaSnnA2Zs9Gv8ZqH',
                        'response' => $grecaptchaToken,
                    )
                ]);
                $results = json_decode($response->getBody()->getContents());
                if($results->success){
                    $subscriber = new Subscriber();
                    $subscriber->email = $request->email;
                    $subscriber->token = Str::random(60);
                    $subscriber->save();
                    Mail::to($subscriber->email)->send(new NewSubscriberWelcome($subscriber));
                    Session::flash('success','We have sent you an email to confirm your subscription. From now on you will be updated to Naipala.');
                } else {
                    Session::flash('error','Ops! Looks like you\'re a bot.');
                    return redirect()->back();
                }
            }
            else {
                Session::flash('error','To subscribe please tick the captcha box to verify that you\'re not a robot! :)');
                return redirect()->back();
            }
         }
        else
            Session::flash('error','Looks like you are already in our subscription list.');
        return redirect()->back();
    }

    public function confirmEmail($token){
        if(Auth::check())
            return redirect()->route('home');
        
        $user = User::where('confirmation_code','=',$token)->first();
        if(!$user)
            abort(404);
        
        if($user->confirmed == 1){
            return redirect()->route('home');
        }

        $user->confirmed = 1;
        $user->save();

        if (!Subscriber::where('email', '=', $user->email)->exists()) {
            $subscriber = new Subscriber();
            $subscriber->email = $user->email;
            $subscriber->token = Str::random(60);
            $subscriber->save();
        }
        Session::flash('success','Your email has been verified.');
        return redirect()->route('login');
    }

    public function unsubscribe($token){
        $subscriber = Subscriber::where('token','=',$token)->first();
        
        if(!$subscriber) abort(404);
        return view('landing.unsubscribe')->with('token',$token);
    }

    public function unsubscribed($token)
    {
        $subscriber = Subscriber::where('token','=',$token)->first();
        
        if(!$subscriber) abort(404);
        $subscriber->delete();
        Session::flash('success','You are no longer subscribed to us.');
        return redirect()->route('home');
    }
    
    public function social()
    {
        $socials=Social::with('socialimages')->orderBy('social_id','desc')->get();
        $sociallinks=Config::where('config_page','SocialLink')->get();
        return view('landing.social',compact('socials','sociallinks'));
    }

    public function featuredBrands()
    {
        $featured_brands = FeaturedBrand::all();
        return view('landing.featuredbrands')->with('featured_brands',$featured_brands);
    }

    public function contact_us(Request $request){

        $validator = Validator::make($request->contact, [
            'email' => 'required|email',
            'name' => 'required',
            'body'=>'required'
        ]);
        
        if ($validator->fails()) {
            Session::flash('error','Please provide all information and Try again.');
            return redirect()->back();
        }

        $captchaToken = $request->input('g-recaptcha-response');
        if($captchaToken) {
            $client = new Client();
            $response = $client->post('https://www.google.com/recaptcha/api/siteverify', [
                'form_params' => array(
                    'secret' => '6LcdNKoUAAAAAJ2NcXCw2GGPdaSnnA2Zs9Gv8ZqH',
                    'response' => $captchaToken,
                )
            ]);
            $results = json_decode($response->getBody()->getContents());
            if($results->success){
                Mail::send([], [], function($message) use ($request) {
                    $message->from('no-reply@naipala.com', 'New Contact Us Message');
                    $message->to('info@naipala.com');
                    $message->subject('New Contact Us Message');
                    $message->setBody($request->contact['body'].'<br><br>- '.$request->contact['name'].' ('.$request->contact['email'].')', 'text/html');
                });
            }
            Session::flash('success','Message sent.');
            return redirect()->back();
        } else {
            Session::flash('error','To contact  please tick the captcha box to verify that you\'re not a robot! :)');
            return redirect()->back();
        }
    }

    public function donation(){
        $donations = Donation::orderBy('created_at', 'desc')->paginate(5);

        return view('landing.donation',compact('donations'));
    }
    
    public function invite(Request $request){
        $validator = Validator::make($request->all(), [
            'email' => 'required|email'
        ]);
        if ($validator->fails()) {
            Session::flash('error','Please provide a valid email.');
            return redirect()->back();
        }

        if (!Invitation::where('email', '=', $request->email)->exists()) {

            $invitation = new Invitation;
            $invitation->email = $request->email;
            $invitation->save();
            Session::flash('success','We will send you an invitation soon.');
        }
        else
            Session::flash('error','Looks like you are already in our invitation list.');
        return redirect()->back();
    }
    public function likeSocial(Request $request)
    {
        // return $request->id;
        $social=Social::findOrFail($request->id);
        $social->increment('like_count');
         return $social->like_count;
    }
    public function socialComment(Request $request)
    {
        $comment = new Socialcomment;
        if(Auth::check())
        {
            $comment->name=Auth::user()->name;
            if(Auth::user()->image)
                $comment->photo="/profile_images/".Auth::user()->image;
            else
                $comment->photo="/frontend-assets/images/profile.png";

        }
        else    
        {
            $comment->name="Anonymous";
            $comment->photo="/frontend-assets/images/profile.png";

        }
        $comment->body=$request->body;
        $comment->social_id=$request->id;
        $comment->save();

        $view=view('landing.social_partial.comment',compact('comment'))->render();
        return $view;

    }
    public function getAllComments(Request $request)
    {
        $comments=Social::findOrFail($request->id)->comments;
        
        $view=view('landing.social_partial.comments',compact('comments'))->render();
        return $view;

    }
    public function getTopComments(Request $request)
    {
        $comments=Social::findOrFail($request->id)->comments->take(5);
        
        $view=view('landing.social_partial.comments',compact('comments'))->render();
        return $view;

    }
}
