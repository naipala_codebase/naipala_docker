<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Cart;
use App\Item;
use App\Item_Image;
use Auth;

class WishlistController extends Controller
{
    public function __construct()
    {

    }
    public function addItem(Request $request)
    {
        $item=Item::where('item_id',$request->id)->first();
        $options=(array)json_decode($item->details);
        $options['image']=Item_Image::where('item_id',$item->item_id)->first()->image_link;
        $options['alias']=$item->item_alias;
        Cart::instance('wishlist')->restore(Auth::user()->id);
        
        Cart::instance('wishlist')->add(['id'=>$item->item_id,'qty'=>1,'name'=>$item->item_name,'price'=>$item->item_price,'options'=>$options]);       
        
        Cart::instance('wishlist')->store(Auth::user()->id);
        return json_encode(Cart::instance('wishlist')->content());
    }
    public function removeItem(Request $request)
    {
        Cart::instance('wishlist')->restore(Auth::user()->id);
        Cart::instance('wishlist')->remove($request->rowId);
        Cart::instance('wishlist')->store(Auth::user()->id);
        
        return json_encode(Cart::content());
    }
    
}
