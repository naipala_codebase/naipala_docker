<?php

namespace App\Http\Controllers\FrontEnd;

use App\Category;
use App\Rating;
use App\Review;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Redirect;
use VisitLog;
use App\Config;
use App\Item;
use App\Ticket;

class TicketsController extends Controller
{
    public function __construct()
    {
        // VisitLog::save();
    }

    public function index()
    {
        $desc= "Ticket Deals";
        $items = $items=Item::join('categorys', 'items.category_id', '=', 'categorys.category_id')
            ->where('items.item_status','active')
            ->where('categorys.category_name','Tickets')
            ->with('item_images')
            ->get();
        return view('landing.tickets',compact('items','desc'));
    }

    public function category($slug)
    {
        $desc=Config::where('config_key','Collection')->first()->config_value;
        $ids=Category::where('category_alias',$slug)->first();
        if($ids=='')
            abort(404);
        $id=$ids->category_id;
        $items=Item::where('category_id',$id)->where('item_status','active')->with('item_images')->get()->shuffle();
        $categorys=Category::all();
        return view('landing.tickets',compact('items','categorys','desc'));
    }


    public function ticketPage($slug)
    {
        $item=Item::where('item_status','active')->where('item_alias',$slug)->first();

        if(!$item) abort(404);

        $images=$item->item_images;
        $img = $images ? $images->first()->image_link : '';
        $details = json_decode($item->details, true);
        $colors = [];

        foreach($details as $type){
            if($type['color']!=''){
                if(!array_key_exists($type['color'],$colors))
                    $colors[$type['color']] = ['sizes'=>[],'images'=>[],'availability'=>false];
                if( $type['size']!=''){
                    $s = ['availability'=> $type['quantity']>0 ? true:false,'size' =>$type['size']];
                    $colors[$type['color']]['sizes'] []= $s;
                }
                if(!$colors[$type['color']]['availability'])
                    $colors[$type['color']]['availability'] = $type['quantity']>0 ? true:false;
            }
            else{
                if(!array_key_exists(null,$colors))
                    $colors[null] = ['sizes'=>[],'images'=>[],'availability'=>false];
                if( $type['size']!=''){
                    $s = ['availability'=> $type['quantity']>0 ? true:false,'size' =>$type['size']];
                    $colors[null]['sizes'] []= $s;
                }
                if(!$colors[null]['availability'])
                    $colors[null]['availability'] = $type['quantity']>0 ? true:false;
            }
        }

        foreach($images as $image){
            if($image->color)
                $colors[$image->color]['images'] []= $image;
            else
                $colors[null]['images'] []= $image;
        }

        $id=$item->item_id;
        $reviews=Review::where('item_id',$id)->take(5)->get();
        $rated="false";
        if(Auth::check())
        {
            if(Rating::where('user_id',Auth::user()->id)->where('item_id',$id)->exists())
            {
                $rated="true";
            }
        }

        return view('landing.ticketdetail',compact('item','colors','details','reviews','rated','img'));
    }
}