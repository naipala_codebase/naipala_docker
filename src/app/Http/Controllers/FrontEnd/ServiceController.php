<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Collection;
use App\Mail\ServiceBookEmail;
use Illuminate\Support\Facades\Validator;
use App\Service;
use App\ServiceCategory;
use App\ServiceBook;
use App\Guest;
use App\User;
use Auth;
use Session;
use Mail;

class ServiceController extends Controller
{

	public function index(Request $request)
	{
        $service_categories = ServiceCategory::has('services')->get();
  
        $final_service_types = [];

        if ($request->ajax()) {
            try{
                $final_service_type = $service_categories[$request->id]->services()->where('updated_at','<',$request->itemId)->orderBy('updated_at','desc')->limit(5)->get();
            
                $view = view('landing.service.service')->with('secatajx',$final_service_type)->with('id',$request->id)->render();
                return response()->json(['html'=>$view]);
            }
            catch (\Exception $e) {
                return response()->json('error',422);
            }
        }
        else{
            for ($i=0; $i < count($service_categories) ; $i++) { 
                $final_service_types[$i] = $service_categories[$i]->services()->orderBy('updated_at','desc')->paginate(5);
            }
        }

		return view('landing.services')->with('service_categories',$service_categories)->with('final_service_types',$final_service_types);
	}

	public function serviceBook(Request $request,$service_id=0)
    {

        if ($request->method()=='POST')
            $service_id = $request->service_id;

        $service_book = new ServiceBook();
        
        $service = Service::find($service_id);
        if(!$service) abort(404);

        $cat = $service->service_category->title;
        $message = (object) null;
        if(Auth::check()){
            $user = User::find(Auth::user()->id);
            if($service->user_id==$user->id){
                $message->type='error';
                $message->msg = 'You cant accept ur own service';
                if ($request->ajax()) return json_encode($message);
                else {
                    Session::flash($message->type,$message->msg);
                    return redirect('/services#'.str_replace(" ","",$cat));
                }
            }
            if(ServiceBook::where(function ($query) use ($user,$service_id) {
                    $query->where('service_bookable_id', $user->id)
                          ->where('service_bookable_type',get_class($user))
                          ->where('service_id',$service_id);
                })->first())
            {
                $message->type='error';
                $message->msg = 'You Have Already Applied';
                if ($request->ajax()) return json_encode($message);
                else {
                    Session::flash($message->type,$message->msg);
                    return redirect('/services#'.str_replace(" ","",$cat));
                }
            }
            $service_book->service_bookable_id = $user->id;
            $service_book->service_bookable_type = get_class($user);
        }
        else{
            if ($request->method()!='POST') return redirect('/login');
            
            $validator = Validator::make($request->all(), [
                'name' => 'required|max:191',
                'email' => 'required|email',
                'phone' => 'required'
            ]);
    
            if ($validator->fails()) {
                Session::flash('error','Please provide all the Information and Try Again');
                return redirect('/services#'.str_replace(" ","",$cat));
            }

            $guest = new Guest();
            $guest->name = $request->name;
            $guest->email = $request->email;
            $guest->phone = $request->phone;
            $guest->save();
            $service_book->service_bookable_id = $guest->id;
            $service_book->service_bookable_type = get_class($guest);
        }
        $service_book->service_id = $service_id;
        
        $service_book->save();
        $message->id = $service_book->id;
        $message->type='success';
        $message->msg = 'Applied Successfully';

        $mail_data = [
            'userName' => $service->user->name,
            'booker' => $service_book->service_bookable->name,
            'service' => $service->title
        ];

        Mail::to($service->user->email)->send(new ServiceBookEmail($mail_data));
        
        if ($request->ajax()) 
            return json_encode($message);
        else {
            Session::flash('book:'.$message->type,$service_book->id);
            return redirect('/services#'.str_replace(" ","",$cat));
        }
    }

    public function addMessage(Request $request)
    {
        $service_book = ServiceBook::find($request->book_id);
        if(!$service_book) abort(404);
        $service_book->message = $request->message;
        if($request->hasFile('cv')){
            $cv = $request->file('cv');
            if(in_array($cv->getClientOriginalExtension(),['doc','pdf','docx','rtf'])){
                $filename = time().rand(111,999) . '.' . $cv->getClientOriginalExtension();
                $path = public_path('cvs/');
                $cv->move($path,$filename);
                $service_book->cv = $filename;
            }
        }
        
        $service_book->save();
        if ($service_book->message!='' && $service_book->cv!='') $message= 'Message and CV Sent';
        else if ($service_book->message!='') $message= 'Message Sent';
        else if ($service_book->cv!='') $message= 'CV Sent';
        else $message = "";
        Session::flash('success',$message);
        if ($service_book->seen==1) {
            $service_book->seen=0;
            $service_book->save();
        }
        return redirect('/services#'.str_replace(" ","",$service_book->service->service_category->title));
    }
    
    public function search(Request $request)
    {
        $services = Service::where('title','LIKE', '%'.$request['key'].'%')->orderBy('updated_at','desc')->paginate(10);   
        return view('landing.servicesearch')->with('services',$services)->with('key',$request['key']);
    }
}