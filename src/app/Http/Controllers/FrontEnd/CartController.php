<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\History;
use App\GiftCard;
use Carbon\Carbon;
use App\Transaction;
use Illuminate\Support\Str;
use Mail;
use App\Mail\CustomerInvoiceMail;
use Auth;
use App\Guest;
use App\Item;
use App\Item_Image;
use Session;
use App\Subscriber;
use Cart;
use App\Agent;
use App\Currency;
use App\Country;

class CartController extends Controller
{

    public function addItem(Request $request)
    {
        //$item=Item::where('item_id',$request->id)->first();

        $item=Item::join('categorys', 'items.category_id', '=', 'categorys.category_id') // to handle tickets
                ->where('item_id',$request->id)->first();
                
        $details=json_decode($item->details);
        $qty = -1;

        $options=(array)json_decode($request->details);
        $options['image']=$request->image;
        $options['itemCat']=$item->category_name; // to handle tickets
        $qty_buy = $request->quantity ? (int)$request->quantity : 1;

        //checking if required product is out of stock
        foreach ($details as $d) {
            if($d->size==$options['size'] && $d->color==$options['color'])
                $qty = $d->quantity;
        }

        if($qty == -1){
            return response()->json(['errors' => 'Product unavailable'], 404);
        }

        if($qty==0 || $qty < $qty_buy)
        {
            return response()->json(['errors' => 'Out of Stock'], 404);
        }

        //checking if the added item quantity exceeds the remaining quantity
        foreach (Cart::content() as $c) {
             
             if($c->options->size==$options['size'] && $c->options->color==$options['color'] && $c->id==$request->id )
                {
                    if($c->qty+$qty_buy > $qty)
                        return response()->json(['errors' => 'Out of Stock'], 404);
                }
        }
        
        if($options['color']!=""){
            $itm = Item_Image::where('item_id',$item->item_id)->where('color',$options['color'])->first();
            $options['image'] = $itm ? $itm->image_link : '';
        }
        else{
            $itm = Item_Image::where('item_id',$item->item_id)->first();
            $options['image'] = $itm ? $itm->image_link : '';
        }
        $options['price'] = $item->item_price;

        $price = Currency::currency_convert($item->item_price, $request->session()->get('currency'));

        Cart::add(['id'=>$item->item_id,'name'=>$item->item_name,'qty'=>$qty_buy,'price'=>$price,'options'=>$options]);       

        return json_encode(Cart::content());
    }

    public function emptyCart()
    {
        Cart::destroy();
        return "Success";
    }

    public function updateQty(Request $request)
    {   
        $item=Cart::get($request->rowId);
        $details=json_decode(Item::where('item_id',$item->id)->first()->details);
        // return $item->options->color;
        $qty=0;
        if($request->qty<0)
        {
            return response()->json(['errors' => 'Invalid Entry'], 404);   
        }
        if($request->qty>10)
        {
             return response()->json(['errors' => 'Exceeds Purchase Limit'], 404);   
        }
        if(!is_numeric($request->qty))
        {
            return response()->json(['errors' => 'Please input number'], 404);   
        }
        if((int)$request->qty-$request->qty!=0)
        {
            return response()->json(['errors' => 'Input integer'], 404);   
        }
        foreach ($details as $d) {
             if($d->size==$item->options->size && $d->color==$item->options->color)
                $qty=$d->quantity;
        }
        if($request->qty<=$qty)
        {
            Cart::update($request->rowId, $request->qty); 
            return json_encode(Cart::content());
        }
        else
        {
            return response()->json(['errors' => 'Out of Stock'], 404);
        }
    }

    public function removeItem(Request $request)
    {
        Cart::remove($request->rowId);
        return json_encode(Cart::content());
    }

    public function cart()
    {   
        return view('cart.cart')->with('cartitems',json_encode(Cart::content()));
    }
    
    public function purchase(Request $request)
    {
        $cart = Cart::content();
        if(count($cart)==0){
            Session::flash('error','Your cart is empty');
            return redirect()->route('cart');
        }

        $total = 0;
        $purchases = [];

        foreach($cart as $c){
            $item = (object) null;
            $item->itemId = $c->id;
            $item->item = $c->name;
            $item->quantity = $c->qty;
            $item->price = $c->options['price'];
            $item->details = $c->options;
            $total += $item->price*$item->quantity;
            $purchases []= $item;
        }
        if(!self::CheckAvailability($purchases))
            return redirect()->route('cart');
        
        $transaction = new Transaction;
        $transaction->details = json_encode($purchases);
        $transaction->total = $total;
        $transaction->status = 'pending';
        $transaction->code = Str::random(20);
        $transaction->save();
        return redirect()->route('cart.payment',$transaction->code);
    }

    public function payment($code)
    {
        $transaction = Transaction::where('code','=',$code)->first();
        
        if(!$transaction)
            abort(404);
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }

        $countries = Country::orderBy('name')->get();

        return view('cart.payment')->with('transaction',$transaction)->with('countries',$countries);
    }

    public function discard($code){
        $transaction = Transaction::where('code','=',$code)->first();
        if(!$transaction)
            abort(404);
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }

        if($transaction->giftCard){
            $transaction->giftCard->status = 'unused';
            $transaction->giftCard->save();
            Session::flash('success','Transaction discarded. You can use the gift card again.');
        }
        else
            Session::flash('success','Transaction discarded');
        
        $transaction->delete();

        return redirect()->route('collection');
    }

    public function validateCode(Request $request,$code){
        $transaction = Transaction::where('code','=',$code)->first();

        if(!$transaction) abort(404);
        
        if($transaction->status!= 'pending'){
            return ['type'=>"error",'msg'=>'The transaction has already been completed.'];
        }
        
        if($transaction->giftCard){
            return ['type'=>"error",'msg'=>'You cannot use multiple gift cards for a single purchase.'];
        }
        
        $card = GiftCard::where('promocode','=',$request->code)->first();
        if(!$card)
            return ['type'=>"error",'msg'=>'Code is invalid'];
        if($card->status=='pending') return ['type'=>"error",'msg'=>"You cannot use this code again."];
        if($card->status=='used') return ['type'=>"error",'msg'=>"Card has already been used"];
        $today = Carbon::today();
        $expires_on = new Carbon($card->expires_on);
        if($expires_on < $today) return ['type'=>"error",'msg'=>"Card has already expired"];

        $total = $transaction->total - $transaction->donation;
        $total -= $card->discount;

        // if($total <= 0) return ['type'=>"error",'msg'=>"Your total is too low to use this code"];
        if($total <= 0) $total = 0;
        $transaction->total = $total + $transaction->donation;
        $transaction->giftCard_id = $card->id;
        $transaction->save();

        $card->status = 'pending';
        $card->save();

        //convert before send
        return ['type'=>"success",'msg'=>Currency::currency($card->discount),'total'=>Currency::currency($total)];
    }

    public function shippingInformation(Request $request,$code){
        $itemCat = $request['itemCat'];
        // if($request['itemCat'] == "Tickets") { // to handle tickets
        //     $this->validate($request,array(
        //         'fname' => 'sometimes|required|max:191',
        //         'lname' => 'sometimes|required|max:191',
        //         'email' => 'sometimes|required|email',
        //         'shipping_contact' => 'required'
        //     ));
        // } else {
        //     $this->validate($request,array(
        //     'fname' => 'sometimes|required|max:191',
        //     'lname' => 'sometimes|required|max:191',
        //     'email' => 'sometimes|required|email',
        //     'shipping_contact' => 'required',
        //     'shipping_address' => 'required|max:191',
        //     'apt_suit' => 'max:191',
        //     'suburb' => 'required|max:191',
        //     'country' => 'required|max:191|exists:countries,name',
        //     'state' => 'required|max:191',
        //     'postcode' => 'required|numeric',
        //     'city' => 'required'
        // ));
        // }
        
        $this->validate($request,array(
            'fname' => 'sometimes|required|max:191',
            'lname' => 'sometimes|required|max:191',
            'email' => 'sometimes|required|email',
            'shipping_contact' => 'required',
            'shipping_address' => 'required|max:191',
            'apt_suit' => 'max:191',
            'suburb' => 'required|max:191',
            'country' => 'required|max:191|exists:countries,name',
            'state' => 'required|max:191',
            'postcode' => 'required|numeric',
            'city' => 'required'
        ));
        
        $transaction = Transaction::where('code','=',$code)->first();
        if(!$transaction) abort(404);
        
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }
        
        if(Auth::check()){
            $transaction->transactionable_id = Auth::user()->id;
            $transaction->transactionable_type = get_class(Auth::user());
        }
        else{
            $guest = new Guest;
            $guest->name = $request['fname'] . ' ' . $request['lname'];
            $guest->email = $request['email'];
            $guest->phone = $request['shipping_contact'];
            $guest->save();
            $transaction->transactionable_id = $guest->id;
            $transaction->transactionable_type = get_class($guest);

            if($request->has('subscribeme')){
                if (!Subscriber::where('email', '=', $request->email)->exists()) {
                    $subscriber = new Subscriber();
                    $subscriber->email = $request->email;
                    $subscriber->token = Str::random(60);
                    $subscriber->save();
                }
            }
        }
        $transaction->shipping_contact = $request['shipping_contact'];
        $transaction->shipping_address = $request['shipping_address'];
        $transaction->companyName = $request['companyName'];
        $transaction->apt_suit = $request['apt_suit'];
        $transaction->city = $request['city'];
        $transaction->country = $request['country'];
        $transaction->state = $request['state'];
        $transaction->suburb = $request['suburb'];
        $transaction->postcode = $request['postcode'];
        if(isset($request['agentId']))
        {
            if(Agent::where('agent_code',$request['agentId'])->where('agent_status','active')->count()>0){
                $transaction->agent_code=$request['agentId'];
            }
        }
         
        $transaction->save();

        return redirect()->route('billingform',compact(['code', 'itemCat']));
    }

    public function CheckAvailability($purchases){
        foreach ($purchases as $purchase) { 
            
            $product = Item::find($purchase->itemId);
            
            if(!$product){
                Session::flash('error', $purchase->item . ' is no longer available.');
                return false;
            }
            
            $items = json_decode($product->details);
            $found = 0;
            foreach ($items as $item) {
                if($item->color == $purchase->details->color && $item->size == $purchase->details->size)
                {
                    $found = 1;
                    if($purchase->quantity > $item->quantity){
                        $size = '(' . $purchase->details->size . ')';
                        if($size=='()')
                            $size='';
                        $det = $purchase->item .'-'.$purchase->details->color . $size;
               
                        Session::flash('error', $det . ' only ' . $item->quantity . ' in stock');
                        return false;
                    }
                }
            }
            if($found == 0){
                $size = '(' . $purchase->details->size . ')';
                if($size=='()')
                    $size='';
                    
                $det = $purchase->item .'-'.$purchase->details->color . $size;
                Session::flash('error', $det . 'is no longer available.');
                return false;
            }
        }
        return true;
    }

    public function donate(Request $request,$code){
        $transaction = Transaction::where('code','=',$code)->first();

        if(!$transaction) abort(404);
        
        if($transaction->status!= 'pending'){
            return response()->json('error', 422);
        }
        
        //if($request->donated < 5)
        if($request->donated < 1)
            return response()->json('error', 422);

        $donated = $request->donated/$request->session()->get('currency')->exchange_rate;

        $old = $transaction->total;
        $transaction->increment('total',$donated);
        $transaction->increment('donation',$donated);
        return ['old'=>Currency::currency($old),'total'=>Currency::currency($transaction->total)];
    }

    public function paymentSuccess($code){
        $transaction = Transaction::where('code','=',$code)->first();

        if(!$transaction) abort(404);
        
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }
        
        if($transaction->total!=0){
            return view('cart.paywithpaypal')->with('transaction',$transaction);
        }
        
        $purchases = json_decode($transaction->details);

        if(!self::CheckAvailability($purchases))
            return redirect()->route('cart');
            
        //change gift cards status if exists
        if($transaction->giftCard_id!=''){
            $transaction->giftCard->status = 'used';
            $transaction->giftCard->save();
        }

        //update quantity

        foreach ($purchases as $purchase) { 
            $product = Item::find($purchase->itemId);
            $items = json_decode($product->details);
            
            // clear the cart item which is purchased
            // $carts = Cart::content();
            // $item_id = $purchase->itemId;
            // foreach ($carts as $c){
            //     if($c->id === $item_id){
            //         Cart::remove($c->rowId);
            //     }
            // }

            // clear the cart item which is purchased
            self::emptyCart();

            foreach ($items as $item) {
                if($item->color == $purchase->details->color && $item->size == $purchase->details->size)
                {
                    $item->quantity = $item->quantity - $purchase->quantity;
                    if($item->quantity<0) $item->quantity = 0;
                    $product->details = json_encode($items);
                    $product->save();
                    break;
                }
            }
        }
        $transaction->status = 'completed';
        $transaction->save();
        
        Mail::to($transaction->transactionable->email)->send(new CustomerInvoiceMail($transaction));
        Session::flash('success','Payment successful. Thank you for Ordering. Invoice has been sent to your email address.');
        return redirect()->route('collection');
    }
}
