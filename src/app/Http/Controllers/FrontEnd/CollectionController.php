<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Item;
use App\Item_Image;
use App\Review;
use App\Rating;
use Auth;
use Redirect;
use App\Category;
use App\Config;

class CollectionController extends Controller
{
    public function index()
    {
        $isSearch = false;
    	$desc=Config::where('config_key','Collection')->first()->config_value;
        $items=Item::join('categorys', 'items.category_id', '=', 'categorys.category_id')
            ->where('items.item_status','active')
            ->where('categorys.category_name','!=','Tickets')
            ->with('item_images')
            ->get()->shuffle();
        $categorys=Category::all();	
        return view('landing.collection',compact('items','categorys','desc','isSearch'));
    }

    public function category($slug)
    {
        $desc=Config::where('config_key','Collection')->first()->config_value;
        $ids=Category::where('category_alias',$slug)->first();
    	if($ids=='')
            abort(404);
        $id=$ids->category_id;      
        $items=Item::where('category_id',$id)->where('item_status','active')->with('item_images')->get()->shuffle();
        $categorys=Category::all();
        return view('landing.collection',compact('items','categorys','desc'));
    }

    public function getReview($id)
    {
        $reviews=Review::where('item_id',$id)->get();
        return $reviews;
    }

    public function addReview(Request $request)
    {
        $review = new Review;
        $review->item_id=$request['item_id'];
        $review->review_description=$request['post'];
        $review->review_name=$request['username'];
        $review->review_image=$request['image'];
        $review->save();
        return "success";
    }

    public function search(Request $request)
    {
        $desc=Config::where('config_key','Collection')->first()->config_value;
        $param = "";
        $isSearch = true;
        //$items=Item::where('item_status','active')->where('item_name', 'LIKE', '%'.$request['q'].'%')->with('item_images')->paginate(4);
        
        $items = $items=Item::join('categorys', 'items.category_id', '=', 'categorys.category_id')
            ->where('items.item_status','active')
            ->where('items.item_name', 'LIKE', '%'.$request['q'].'%')
            ->orWhere('categorys.category_name', 'LIKE', '%'.$request['q'].'%')
            ->with('item_images')
            ->get();
            
        if($request['q'] == 'tickets')
            $param = 'tickets';
        else if($request['q'] == 'ticket') 
            $param = 'tickets';
        else if($request['q'] == 'Tickets')
            $param = 'tickets';
        else if($request['q'] == 'Ticket')
            $param = 'tickets';
        else 
            $param = 'collection';
        
        $viewPage = 'landing.'.$param;
        
        //$categorys=Category::all(); 
        //return view('landing.collection',compact('items','categorys','desc'));
        
        return view($viewPage, compact('items', 'desc', 'isSearch'));
    }

    public function itemPage($slug)
    {
        $item=Item::where('item_status','active')->where('item_alias',$slug)->first();

        if(!$item) abort(404);

        $images=$item->item_images;
        $img = $images->count()>0 ? $images->first()->image_link : '';
        $details = json_decode($item->details, true);
        $colors = [];
        
        foreach($details as $type){
            if($type['color']!=''){
                if(!array_key_exists($type['color'],$colors))
                    $colors[$type['color']] = ['sizes'=>[],'images'=>[],'availability'=>false];
                if( $type['size']!=''){
                    $s = ['availability'=> $type['quantity']>0 ? true:false,'size' =>$type['size']];
                    $colors[$type['color']]['sizes'] []= $s;
                }
                if(!$colors[$type['color']]['availability'])
                    $colors[$type['color']]['availability'] = $type['quantity']>0 ? true:false;
            }
            else{
                if(!array_key_exists(null,$colors))
                    $colors[null] = ['sizes'=>[],'images'=>[],'availability'=>false];
                if( $type['size']!=''){
                    $s = ['availability'=> $type['quantity']>0 ? true:false,'size' =>$type['size']];
                    $colors[null]['sizes'] []= $s;
                }
                if(!$colors[null]['availability'])
                    $colors[null]['availability'] = $type['quantity']>0 ? true:false;
            }
        }
        
        foreach($images as $image){
            if($image->color)
                $colors[$image->color]['images'] []= $image;
            else
                $colors[null]['images'] []= $image;
        }

        $id=$item->item_id;
        $reviews=Review::where('item_id',$id)->take(5)->get();
        $rated="false";
        if(Auth::check())
        {
            if(Rating::where('user_id',Auth::user()->id)->where('item_id',$id)->exists())
            {
                $rated="true";
            }
        }
        
        return view('landing.itemdetail',compact('item','colors','details','reviews','rated','img'));
    }

    public function rating(Request $request)
    {
        if($request->ajax()){
            $item = Item::find($request['item_id']);
            Item::where('item_id',$request['item_id'])->increment('item_ratings',$request['rate']);
            Item::where('item_id',$request['item_id'])->increment('item_raters');
            $rating = new Rating;
            $rating->user_id=Auth::user()->id;
            $rating->item_id=$request['item_id'];
            $rating->save();

            return;
        }
        abort(404);
     
    }

    public function rating1($itemid,$rate)
    {
        $item = Item::find($itemid);
        if(!$item) abort(404);
        if(Auth::check()){
            if(Rating::where('user_id',Auth::user()->id)->where('item_id',$itemid)->exists())
            {
            
            }
            else{    
                Item::where('item_id',$itemid)->increment('item_ratings',$rate);
                Item::where('item_id',$itemid)->increment('item_raters');
                $rating = new Rating;
                $rating->user_id=Auth::user()->id;
                $rating->item_id=$itemid;
                $rating->save();
            }
        }
        return Redirect::to('collection/item/'.$item->item_alias);
    }
}
