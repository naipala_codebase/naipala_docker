<?php

namespace App\Http\Controllers\Frontend;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use URL;
use Session;
use Redirect;
use Response;
use Input;
use App\Transaction;
use App\EGiftCardTransaction;
use App\GiftCard;
use Mail;
use App\Mail\CustomerInvoiceMail;
use App\Mail\GiftCardUsed;
use App\Mail\ECardPurchased;
use App\Mail\ECardGifted;
use App\Item;
use App\Config;
use Carbon\Carbon;
/** All Paypal Details class **/
use PayPal\Rest\ApiContext;
use PayPal\Api\Amount;
use PayPal\Api\Details;
use PayPalItem;
use PayPal\Api\ItemList;
use PayPal\Api\ShippingAddress;
use PayPal\Api\Payer;
use PayPal\Api\Payment;
use PayPalTransaction;
use PayPal\Api\RedirectUrls;
use PayPal\Api\ExecutePayment;
use PayPal\Api\PaymentExecution;
use PayPal\Auth\OAuthTokenCredential;
use App\Currency;
use App\Country;

class PaypalController extends Controller
{
    private $_api_context;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // setup PayPal api context
        $paypal_conf = config('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential($paypal_conf['client_id'], $paypal_conf['secret']));
        $this->_api_context->setConfig($paypal_conf['settings']);
    }
    /**
     * Show the application paywith paypalpage.
     *
     * @return \Illuminate\Http\Response
     */
    public function payWithPaypal($code)
    {
        $transaction = Transaction::where('code','=',$code)->first();
        
        if(!$transaction)
            abort(404);
        if($transaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect('/collection');
        }

        if(!$transaction->transactionable){
            return redirect()->route('cart.payment',$code);
        }

        $shipping_donation_text = Config::where('config_key','shipping_donation_text')->first()->config_value;

        return view('cart.paywithpaypal')->with('transaction',$transaction)->with('shipping_donation_text',$shipping_donation_text);
    }
    /**
     * Store a details of payment with paypal.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function postPaymentWithpaypal(Request $request,$code)
    {   
        $transaction = Transaction::where('code','=',$code)->first();
        if(!$transaction)
            return Response::json('error', 404);
        $items = json_decode($transaction->details);
        
        //but check availability first
        if(!self::CheckAvailability($items)){
            if($transaction->giftCard){
                $transaction->giftCard->status = 'unused';
                $transaction->giftCard->save();

                Session::flash('error',Session::get('error').'. Your giftcard has been reset and can be used again');     
            }
            $transaction->delete();
            return Response::json(['url'=>route('cart')], 200);
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        
        $current = Session::get('currency');

        $itemcost = 0;
        $itemlist = [];
        foreach($items as $item){
            $i = new PayPalItem();
            $i->setName($item->item) /** item name **/
                ->setDescription(($item->details->color =='') ? $item->details->size : ($item->details->color.($item->details->size =='' ? '':'('. $item->details->size.')'))) 
                ->setCurrency($current->code)
                ->setQuantity($item->quantity)
                ->setPrice(Currency::currency_convert($item->price,$current)); /** unit price **/
            $itemlist []= $i;
            $itemcost += $item->price*$item->quantity;
        }
        if($transaction->giftCard){
            $discount = $transaction->giftCard->discount;
            
            $item = new PayPalItem();
            $item->setName('GiftCard Discount ($'.$discount.')')
                ->setCurrency($current->code)
                ->setQuantity(1);
            if($transaction->total-$transaction->donation == 0){
                $item->setPrice(Currency::currency_convert($itemcost,$current)*(-1));
            }
            else{
                $item->setPrice(Currency::currency_convert($discount,$current)*(-1));   
            }
            $itemlist []= $item;
        }

        if($transaction->donation!=0){
            $item = new PayPalItem();
            $item->setName('Donation')
                ->setCurrency($current->code)
                ->setQuantity(1)
                ->setPrice(Currency::currency_convert($transaction->donation,$current));
            $itemlist []= $item;
        }

        $address = [
            "recipient_name" => $transaction->transactionable->name,
            "line1" => $transaction->shipping_address,
            "line2" => $transaction->apt_suit,
            "city" => $transaction->suburb.', '.$transaction->city,
            "country_code" => Country::where('name',$transaction->country)->first()->code,
            "postal_code" => $transaction->postcode,
            "state" => $transaction->state,
            // "phone" => $transaction->shipping_contact
        ];

        $item_list = new ItemList();
        $item_list->setItems($itemlist)
                  ->setShippingAddress($address);

        $amount = new Amount();
        $amount->setCurrency($current->code)
                ->setTotal(Currency::currency_convert($transaction->total,$current));

        $Paypaltransaction = new PayPalTransaction();
        $Paypaltransaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Order No: '. $transaction->id);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('payment.status',$transaction->code)) /** Specify return URL **/
            ->setCancelUrl(URL::route('billingform',$transaction->code));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($Paypaltransaction));
        
        try {
            // $payment->create($this->_api_context);
            $response = $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                Session::flash('error','Connection timeout');
                return Response::json('error', 422);
            } else {
                Session::flash('error','Some error occured, sorry for the inconvenience');
                return Response::json('error', 422);
            }
        }catch(\PayPal\Exception\PayPalConnectionException $e){
            $error = json_decode($e->getData());
            $errors = '';
            foreach($error->details as $er){
                $errors .= $er->issue;
                break;
            }
            Session::flash('error',$errors);
            return Response::json('error', 422);
        }
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        //edit
        return $response;

    }

    public function getPaymentStatus(Request $request, $code)
    {
        $transaction = Transaction::where('code','=',$code)->first();
        if(!$transaction)
            return Response::json('error', 404);
        /** Get the payment ID before session clear **/
        $payment_id = $request->get('PaymentID');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        
        if (empty($request->get('PayerID')) || empty($request->get('token'))) {
            Session::flash('error','Payment failed');
            return Response::json('error', 422);
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId($request->get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') { 
            
            /** it's all right **/
            //change gift cards status if exists
            if($transaction->giftCard_id!=''){
                $transaction->giftCard->status = 'used';
                $transaction->giftCard->save();
            }

            //update quantity
            $purchases = json_decode($transaction->details);

            foreach ($purchases as $purchase) { 

                $product = Item::find($purchase->itemId);
                $items = json_decode($product->details);
                
                foreach ($items as $item) {
                    if($item->color == $purchase->details->color && $item->size == $purchase->details->size)
                    {
                        $item->quantity = $item->quantity - $purchase->quantity;
                        if($item->quantity<0) $item->quantity = 0;
                        $product->details = json_encode($items);
                        $product->save();
                        break;
                    }
                }
            }
            $transaction->status = 'completed';
            $transaction->save();

            Mail::to($transaction->transactionable->email)->send(new CustomerInvoiceMail($transaction));
            Session::flash('success','Payment successful. Thank you for Ordering. Invoice has been sent to your email address.');
            return Response::json('success', 200);
        }

        Session::flash('error','Payment failed');
        return Response::json('error', 422);
    }

    public function CheckAvailability($purchases){
        foreach ($purchases as $purchase) { 
            
            $product = Item::find($purchase->itemId);
            
            if(!$product){
                Session::flash('error', $purchase->item . ' is no longer available.');
                return false;
            }
            $items = json_decode($product->details);
            $found = 0;
            
            foreach ($items as $item) {
                if($item->color == $purchase->details->color && $item->size == $purchase->details->size)
                {
                    $found = 1;
                    if($purchase->quantity > $item->quantity){
                        $size = '(' . $purchase->details->size . ')';
                        if($size=='()')
                            $size='';
                        $det = $purchase->item .'-'.$purchase->details->color . $size;
                        
                        if($item->quantity == 0)
                            Session::flash('error', $det . ' is no longer in stock');
                        else
                            Session::flash('error', $det . ' only ' . $item->quantity . ' left in stock');
                        return false;
                    }
                }
            }
            if($found == 0){
                $size = '(' . $purchase->details->size . ')';
                if($size=='()')
                    $size='';
                $det = $purchase->item .'-'.$purchase->details->color . $size;
                Session::flash('error', $det . ' is no longer available.');
                return false;
            }
        }
        return true;
    }

    public function postecardPaymentWithpaypal(Request $request,$code)
    {   
        $etransaction = EGiftCardTransaction::where('token','=',$code)->first();
        if(!$etransaction)
            return Response::json('error', 404);

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');

        $current = Session::get('currency');

        $i = new PayPalItem();
        $i->setName('Gift Card ('.Currency::currency($etransaction->amount).')')
            ->setDescription('Naipala E-Gift Card ('.Currency::currency($etransaction->amount).')') 
            ->setCurrency($current->code)
            ->setQuantity(1)
            ->setPrice(Currency::currency_convert($etransaction->amount,$current));

        $item_list = new ItemList();
        $item_list->setItems(array($i));

        $amount = new Amount();
        $amount->setCurrency($current->code)
                ->setTotal(Currency::currency_convert($etransaction->amount,$current));

        $Paypaltransaction = new PayPalTransaction();
        $Paypaltransaction->setAmount($amount)
            ->setItemList($item_list)
            ->setDescription('Transaction ID: '. $etransaction->id);

        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(URL::route('ecards.status',$etransaction->token)) /** Specify return URL **/
            ->setCancelUrl(URL::route('ecards.payment',$etransaction->token));

        $payment = new Payment();
        $payment->setIntent('Sale')
            ->setPayer($payer)
            ->setRedirectUrls($redirect_urls)
            ->setTransactions(array($Paypaltransaction));
        
        try {
            $response = $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            if (\Config::get('app.debug')) {
                Session::flash('error','Connection timeout');
                return Response::json('error', 422);
            } else {
                Session::flash('error','Some error occured, sorry for inconvenient');
                return Response::json('error', 422);
            }
        }
        foreach($payment->getLinks() as $link) {
            if($link->getRel() == 'approval_url') {
                $redirect_url = $link->getHref();
                break;
            }
        }
        /** add payment ID to session **/
        Session::put('paypal_payment_id', $payment->getId());
        //edit
        return $response;
    }

    public function getecardPaymentStatus(Request $request, $code)
    {
        $etransaction = EGiftCardTransaction::where('token','=',$code)->first();
        if(!$etransaction)
            return Response::json('error', 404);
        /** Get the payment ID before session clear **/
        $payment_id = $request->get('PaymentID');
        /** clear the session payment ID **/
        Session::forget('paypal_payment_id');
        
        if (empty($request->get('PayerID')) || empty($request->get('token'))) {
            Session::flash('error','Payment failed');
            return Response::json('error', 422);
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        /** PaymentExecution object includes information necessary **/
        /** to execute a PayPal account payment. **/
        /** The payer_id is added to the request query parameters **/
        /** when the user is redirected from paypal back to your site **/
        $execution = new PaymentExecution();
        $execution->setPayerId($request->get('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);
        /** dd($result);exit; /** DEBUG RESULT, remove it later **/
        if ($result->getState() == 'approved') { 
            
            $ecard = new GiftCard;
            $ecard->batch = 0;
            $ecard->discount = $etransaction->amount;

            $date = new Carbon($etransaction->delivery_date);
            $ecard->expires_on = $date->addMonths(6);

            $pcode = $this->generateCode();
            while(GiftCard::where('promocode', '=', $pcode)->exists()){
                $pcode = $this->generateCode();
            }
            $ecard->promocode = $pcode;

            $ecard->type = 'ecard';
            $ecard->save();

            $etransaction->giftCard_id = $ecard->id;
            $etransaction->status = 'completed';
            $etransaction->save();

            Mail::to($etransaction->sender_email)->send(new ECardPurchased($etransaction));
            if(new Carbon($etransaction->delivery_date) == Carbon::today() || new Carbon($etransaction->delivery_date) == Carbon::yesterday())
                Mail::to($etransaction->recipient_contact)->send(new ECardGifted($etransaction));
            
            Session::flash('success','Payment successful. Thank you for Ordering. Invoice has been sent to your email address.');
            return Response::json('success', 200);
        }

        Session::flash('error','Payment failed');
        return Response::json('error', 422);
    }

    private function generateCode()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = rand(8,10);
        $charactersLength = strlen($characters);
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $code .= $characters[rand(0, $charactersLength - 1)];
        }
        return $code;
    }
}
