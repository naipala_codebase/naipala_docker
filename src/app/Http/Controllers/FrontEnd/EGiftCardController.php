<?php

namespace App\Http\Controllers\FrontEnd;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\EGiftCardTransaction;
use App\GiftCard;
use App\Mail\ECardGifted;
use Validator;
use Illuminate\Support\Str;
use Carbon\Carbon;
use Session;
use Mail;

class EGiftCardController extends Controller
{
    public function index(){
        return view('landing.ecards.index');
    }

    public function initial(Request $request){
        $validator = Validator::make($request->all(), [
            'amount' => 'required|in:25,50,100,150,200',
            'recipientName' => 'required|max:191',
            'senderName' => 'required|max:191l',
            'message' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator)->withInput();         
        }

        $etransaction = new EGiftCardTransaction;
        $etransaction->amount = $request->amount;
        $etransaction->recipient_name = $request->recipientName;
        $etransaction->sender_name = $request->senderName;
        $etransaction->message = $request->message;
        $etransaction->token = Str::random(20);
        $etransaction->save();

        return redirect()->route('ecards.checkoutform',$etransaction->token);
    }

    public function checkoutform($code){
        $etransaction = EGiftCardTransaction::where('token',$code)->first();
        if(!$etransaction) abort(404);
        
        if($etransaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect()->route('ecards.portallogin');
        }
        return view('landing.ecards.checkout')->with('etransaction',$etransaction);
    }

    public function checkout(Request $request,$code){
        $etransaction = EGiftCardTransaction::where('token',$code)->first();
        
        if(!$etransaction) abort(404);
        
        if($etransaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect()->route('ecards.portallogin');
        }
        
        $this->validate($request,array(
            'senderEmail' => 'required|email',
            'recipientEmail' => 'required|email|confirmed',
            'deliveryDate' => 'required|date|after_or_equal:yesterday'
        ));

        $etransaction->sender_email = $request->senderEmail;
        $etransaction->recipient_contact = $request->recipientEmail; 
        $etransaction->delivery_date = new Carbon($request->deliveryDate);
        $etransaction->save();

        return redirect()->route('ecards.payment',$etransaction->token);
    }

    public function payment($code){
        $etransaction = EGiftCardTransaction::where('token',$code)->first();
        if(!$etransaction) abort(404);

        if($etransaction->status!= 'pending'){
            Session::flash('success', 'The transaction has already been completed');
            return redirect()->route('ecards.portallogin');
        }

        if(!$etransaction->sender_email)
            return redirect()->route('ecards.checkout',$etransaction->token);
        
        return view('landing.ecards.payment')->with('etransaction',$etransaction);
    }

    public function portallogin(){
        Session::put('token', null);
        return view('landing.ecards.portallogin');
    }

    public function login(Request $request){
        $etransaction = EGiftCardTransaction::where('sender_email',$request->email)->where('token',$request->token)->first();
        
        if(!$etransaction){
            return redirect()->back()->withErrors(['incorrect','The credentials are incorrect.'])->withInput();
        }
        if($etransaction->status == 'pending')
            return redirect()->route('ecards.checkoutform',$etransaction->token);

        Session::put('token', $etransaction->token);

        return redirect()->route('ecards.portal');
    }

    public function portal(){
        if(!Session::get('token')){
            return redirect()->route('ecards.portallogin');
        }
        $etransaction = EGiftCardTransaction::where('token',Session::get('token'))->first();

        return view('landing.ecards.portal')->with('etransaction',$etransaction);
    }

    public function resend(){
        if(!Session::get('token')){
            return redirect()->route('ecards.portallogin');
        }

        $etransaction = EGiftCardTransaction::where('token',Session::get('token'))->first();
        if(new Carbon($etransaction->delivery_date) <= Carbon::today() && $etransaction->giftCard->status=='unused')
            Mail::to($etransaction->recipient_contact)->send(new ECardGifted($etransaction));
        Session::flash('success','Email has been sent to '.$etransaction->recipient_name);
        return redirect()->route('ecards.portal');
    }
}
