<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Image;
use App\User;
use App\Service;
use App\ServiceCategory;
use App\ServiceBook;
use App\Country;
use Auth;
use DB;
use Schema;
use Session;
use Response;

class UserController extends Controller
{
    private $user;
    private $servicetitles=[];

    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {

            $this->user= Auth::user();
            return $next($request);
        });
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $histories=[];

        $transactions = $this->user->transactions()->orderBy('updated_at','desc')->where('status','!=','pending')->paginate(5);
        foreach ($transactions as $items) {
            foreach (json_decode($items->details) as $item) {
                $history  = array('item' => $item,'purchased_date' => $items->updated_at,'slug' => str_replace(' ','-',$item->item .'-'. $item->itemId));
                $histories []= $history;
            }
            
        }

        $services = $this->user->services()->orderBy('updated_at','desc')->paginate(5);
        $service_categories = ServiceCategory::all();
        if ($request->ajax()) {
            if($request->type=='History'){
                $view = view('user.partials.history',compact('histories'))->render();
                return response()->json(['html'=>$view]);
            }
            else if($request->type=='Service'){
                $view = view('user.partials.service',compact('services'))->render();
                return response()->json(['html'=>$view]);
            }
        }

        $notifications = [];
        $all=ServiceBook::with('service')->get();
        $books=collect();
        $all2=collect();
        foreach ($all as $b) {
            if($b->service->user_id==$this->user->id)
            {
                $books->prepend($b);
            }
        }
        $sids=collect();
        foreach ($books as $a) {
            # code...
            $sids->prepend($a->service->id);
        }
        $sids=$sids->unique();
        
        foreach ($sids as $s) {
            $a=$books;
            $b=$a->splice(5);
            $all2=$all2->union($a);
        }

        foreach ($all2 as $b) {
            $finalbook = (object) null;
            $booker = ServiceBook::find($b->id)->service_bookable;
            $finalbook->id = $b->id;
            $finalbook->service_id=$b->service_id;
            $finalbook->bookerName = $booker->name;
            $finalbook->bookerEmail = $booker->email;
            $finalbook->bookerPhone = $booker->phone;
            if(Schema::hasColumn($booker->getTable(), 'image'))
                $finalbook->bookerImage = $booker->image;
            else
                $finalbook->bookerImage = '';
            $finalbook->category = ServiceCategory::find($b->service->service_category_id)->title;
            $finalbook->title = $b->service->title;
            $finalbook->service_id=$b->service_id;
            $finalbook->created_at = $b->created_at;
            $finalbook->seen = $b->seen;
            $finalbook->bookerMessage = $b->message;
            $finalbook->bookerCV = $b->cv;
            $notifications[] = $finalbook;
            $this->servicetitles[$b->service_id] =$b->service->title; 
        }

        $countries = Country::orderBy('name')->get();

        return view('user.home')->with('countries',$countries)->with('histories',$histories)->with('services',$services)->with('service_categories',$service_categories)->with('notifications',$notifications)->with('servicetitles',$this->servicetitles);

    }

    public function profileEdit(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:191',
            'phone' => 'required|numeric',
            'address' => 'required|max:191',
            'apt_suit' => 'max:191',
            'suburb' => 'required|max:191',
            'country' => 'required|max:191',
            'state' => 'required|max:191',
            'postcode' => 'required|numeric',
            'city' => 'required',
            'dob' => 'required|date'
        ]);
        if ($validator->fails()) {
            return redirect()->back()->withErrors($validator,'profileedit')->withInput();
        }

        $user = User::find($this->user->id);
        $user->name = $request->input('name');
        $user->address = $request->input('address');
        $user->phone = $request->input('phone');
        $user->apt_suit = $request->input('apt_suit');
        $user->country = $request->input('country');
        $user->state = $request->input('state');
        $user->suburb = $request->input('suburb');
        $user->postcode = $request->input('postcode');
        $user->city = $request->input('city');
        $user->dob = $request->input('dob');
        $user->save();

        Session::flash('success','Profile Edited Successfully');
        return redirect()->route('profile');
    }

    public function addImage(Request $request)
    { 
        
        if($request->hasFile('image')){
            $image = $request->file('image');
            
            if($image->getMimeType() == 'image/webp'){
                $im = imagecreatefromwebp($image->getRealPath());
                imagejpeg($im,$image->getRealPath(), 100);
                imagedestroy($im);
            }
            
            $validator = Validator::make($request->all(),array(
                'image'=>'mimes:jpeg,png'
            ));
            if($validator->fails()){
                Session::flash('error','Image must be either of jpg or png format.');
                 return redirect()->route('profile');
            }
            $user = User::find(Auth::user()->id);
            if($user->image)
                unlink(public_path('profile_images/'.$user->image));

            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('profile_images/' . $filename);
            Image::make($image)->resize(160,160)->save($location);

            $user->image= $filename;
            
            $user->save();
            Session::flash('success','Image Uploaded Successfully');
        }

        return redirect()->route('profile');
    }

    public function servicePost(Request $request){
        $this->validate($request,array(
            'title' => 'required|max:191',
            'service_category_id' => 'required|exists:service_categories,id',
            'description' => 'required'
        ));

        $service = new Service();
        $service->title = $request->title;
        $service->description = $request->description;
        $service->service_category_id = $request->service_category_id;
        $service->user_id = $this->user->id;
        $service->save();
        $services = $this->user->services()->orderBy('updated_at','desc')->paginate(3);
        $view = view('user.partials.service',compact('services'))->render();
        return response()->json(['html'=>$view]);
    }
    
    public function serviceEdit(Request $request){

        $this->validate($request,array(
            'title' => 'required|max:191',
            'service_category_id' => 'required|exists:service_categories,id',
            'description' => 'required'
        ));

        $service = $this->user->services()->find($request['id']);
        if(!$service){
            Session::flash('error','Invalid Action');
            abort(404);
        }

        $service->title = $request['title'];
        $service->description = $request['description'];
        $service->service_category_id = $request['service_category_id'];
        $service->save();
        $services = $this->user->services()->orderBy('updated_at','desc')->paginate(3);
        $view = view('user.partials.service',compact('services'))->render();
        return response()->json(['html'=>$view]);
    }

    public function serviceDelete($id){
        $service = $this->user->services()->find($id);
        
        if(!$service){
            Session::flash('error','Invalid Action');
            abort(404);
        }
        $service->delete();
        $services = $this->user->services()->orderBy('updated_at','desc')->offset(0)->limit(3)->get();
        $view = view('user.partials.service',compact('services'))->render();
        return response()->json(['html'=>$view]);
    }

    public function notificationRead($id){
        $service_book = ServiceBook::find($id);
        
        if(!$service_book || !$this->user->services()->where('id',$service_book->service_id)->exists()){
            Session::flash('error','Invalid Action');
            abort(404); 
        }

        if($service_book->seen == 1) 
            return 1;
        else 
            $service_book->seen = 1;
        $service_book->save();
        return 0;
    }

    public function allApplicants(Request $request)
    {
        $applicants=[];
        $all=ServiceBook::with('service')->orderBy('created_at','desc')->get();
        $books=[];
        foreach ($all as $b) {
            if($b->service->user_id==$this->user->id && $b->service->id==$request['service_id'])
            {
                $books[]=$b;
            }
        }

        foreach ($books as $b) {
            $finalbook = (object) null;
            $booker = ServiceBook::find($b->id)->service_bookable;
            $finalbook->id = $b->id;
            $finalbook->service_id=$b->service_id;
            $finalbook->bookerName = $booker->name;
            $finalbook->bookerEmail = $booker->email;
            $finalbook->bookerPhone = $booker->phone;
            if(Schema::hasColumn($booker->getTable(), 'image'))
                $finalbook->bookerImage = $booker->image;
            else
                $finalbook->bookerImage = '';
            $finalbook->category = ServiceCategory::find($b->service->service_category_id)->title;
            $finalbook->title = $b->service->title;
            $finalbook->service_id=$b->service_id;
            $finalbook->created_at = $b->created_at;
            $finalbook->seen = $b->seen;
            $finalbook->bookerMessage = $b->message;
            $finalbook->bookerCV = $b->cv;
            $applicants[] = $finalbook;       
        }       
        $view = view('user.partials.applicants',compact('applicants'))->render(); 
        return $view;
    }
}
