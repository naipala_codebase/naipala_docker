<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\User;
use Illuminate\Http\Request;
use Socialite;
use Session;
use Illuminate\Support\Str;
use Image;
use App\Subscriber;
use Validator;
use Cache;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->middleware('guest', ['except' => 'logout']);
    }
    
    public function showLoginForm(Request $request)
    {
        if ($request->query('next')){
            Session::put('url.intended',$request->query('next'));     
        }
        else
            Session::put('url.intended',route('profile'));     

        return view('auth.login')->with('next',Session::get('url.intended'));
    }

    public function login(Request $request)
    {
        if ($request->query('next')){
            Session::put('url.intended',$request->query('next'));     
        }

        $this->validateLogin($request);

        $user = User::where('email','=',$request->email)->first();
        
        if($user){
            $validator = Validator::make($user->toArray(), [
                'confirmed' => 'accepted'
            ]);
    
            if ($validator->fails()) {
                $validator->errors()->add('email', 'Your account is not verified yet. (<a href="'.route('resend_confirmation',array('next' => Session::get('url.intended'),'email' => $user["email"] )).'" style="color:#4545c8">Resend</a> verification email?)');
                return redirect()->to('/login?next='.Session::get('url.intended'))->withErrors($validator)->withInput();
            }
        }
        // If the class is using the ThrottlesLogins trait, we can automatically throttle
        // the login attempts for this application. We'll key this by the username and
        // the IP address of the client making these requests into this application.
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);

            return $this->sendLockoutResponse($request);
        }

        if ($this->attemptLogin($request)) {
            
            return $this->sendLoginResponse($request);
        }

        // If the login attempt was unsuccessful we will increment the number of attempts
        // to login and redirect the user back to the login form. Of course, when this
        // user surpasses their maximum number of attempts they will get locked out.
        $this->incrementLoginAttempts($request);

        return $this->sendFailedLoginResponse($request);
    }

    protected function sendFailedLoginResponse(Request $request)
    {

        $errors = [$this->username() => trans('auth.failed')];
        if ($request->expectsJson()) {
            return response()->json($errors, 422);
        }
        

        return redirect()->to('/login?next='.Session::get('url.intended'))
            ->withInput($request->only($this->username(), 'remember'))
            ->withErrors($errors);

    }

    public function redirectTo(){
        return Session::get('url.intended');
    }

    public function logout(Request $request)
    {
        Cache::forget('user-is-online-' .Auth::user()->id);
        
        $this->guard()->logout();

        $request->session()->flush();

        $request->session()->regenerate();

        return redirect(url()->previous());
    }

    public function redirectToProvider(Request $request)
    {
        if ($request->query('next')){
            Session::put('url.intended',$request->query('next'));     
        }
        
        return Socialite::driver('facebook')->redirect();
    }

    public function handleProviderCallback(Request $request)
    {
         // 1 check if the user exists in our database with facebook_id
        // 2 if not create a new user
        // 3 login this user into our application
        if (!$request->has('code') || $request->has('denied')) {
            return redirect('/login');
        }
        try
        {
            $socialUser = Socialite::driver('facebook')->user();
            
        }
        catch (Exception $e)
        {
            return redirect('/login');
        }

        $user = User::where('provider_id',$socialUser->getId())->first();
            
        if(!$user)
        {   
            if (!User::where('email', '=', $socialUser->getEmail())->exists()) {
                $u = new User();
                $u->provider_id = $socialUser->getId();
                $u->name = $socialUser->getName();
                $u->email = $socialUser->getEmail();
                $path = $socialUser->avatar_original;
                $fileName = time().rand(111,999).'.'.'jpg';
                Image::make($path)->resize(160,160)->save(public_path('profile_images/' . $fileName));
                            
                $u->image= $fileName;

                if(!$socialUser->getEmail()) 
                    return view('user.getEmail')->with('user',$u);
                $u->confirmed = 1;
                $u->save();
            }
            else{
                $u = User::where('email','=',$socialUser->getEmail())->first();
                $u->provider_id = $socialUser->getId();

                if(!$u->image){
                    $path = $socialUser->avatar_original;
                    $fileName = time().rand(111,999).'.'.'jpg';
                    Image::make($path)->resize(160,160)->save(public_path('profile_images/' . $fileName));
                                
                    $u->image= $fileName;
                }
                $u->confirmed = 1;
                $u->save();
            }

            $user = $u;
        }

        $validator = Validator::make($user->toArray(), [
            'confirmed' => 'accepted'
        ]);

        if ($validator->fails()) {
            $validator->errors()->add('email', 'Your account is not verified yet. (<a href="'.route('resend_confirmation',array('next' => Session::get('url.intended'),'email' => $user["email"] )).'" style="color:#4545c8">Resend</a> verification email?)');
            return redirect()->to('/login?next='.Session::get('url.intended'))->withErrors($validator)->withInput();
        }

        auth()->login($user);

        if (!Subscriber::where('email', '=', $user->email)->exists()) {
          $subscriber = new Subscriber();
          $subscriber->email = $user->email;
          $subscriber->token = Str::random(60);
          $subscriber->save();
        }
        return redirect($this->redirectTo());
    }  
}
