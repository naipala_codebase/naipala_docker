<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Session;
use App\Mail\NewSubscriberWelcome;
use Mail;
use App\Subscriber;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use App\Mail\ConfirmUserEmail;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm(Request $request)
    {
        if ($request->query('next')){
            Session::put('url.intended',$request->query('next'));     
        }
        else
            Session::put('url.intended',route('profile'));
            
        return view('auth.register');
    }

    public function redirectTo(){
        return Session::get('url.intended');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            
        ]);

        $user->confirmation_code = Str::random(60);
        $user->save();

        Mail::to($data['email'])->send(new ConfirmUserEmail($user));

        return $user;
    }

    public function register(Request $request)
    { 
        if ($request->query('next')){
            Session::put('url.intended',$request->query('next'));     
        }
        
        $validator = $this->validator($request->all());
        if ($validator->fails()) {
            $errors = $validator->messages()->messages();
            if ($request->expectsJson()) {
                return response()->json($errors, 422);
            }

            return redirect()->route('register',array('next' => Session::get('url.intended')))
                ->withErrors($errors)->withInput();
        }
        event(new Registered($user = $this->create($request->all())));

        Session::flash('success','Account successfully created. Please Check your email to verify your account.');
        $validator = Validator::make($user->toArray(), [
            'confirmed' => 'accepted'
        ]);

        if ($validator->fails()) {
            $validator->errors()->add('email', 'Please Check your email to verify your account. (<a href="'.route('resend_confirmation',array('next' => Session::get('url.intended'),'email' => $user->email )).'" style="color:#4545c8">Resend</a> verification email?)');
            return redirect()->route('login',array('next' => Session::get('url.intended')))->withErrors($validator)->withInput();
        }
        
        return redirect()->route('login',array('next' => Session::get('url.intended')))->withInput();
    }

    public function resendConfirmation(Request $request){
        if ($request->query('next')){
            $url = $request->query('next');  
        }
        else
            $url = route('profile');

        $email = $request->get('email');

        $user = User::where('email',$email)->first();
        if(!$user)
            abort(404);
        Mail::to($email)->send(new ConfirmUserEmail($user));
        Session::flash('success','Please Check your email to verify your account');
        return redirect()->route('login',array('next' => $url))->withInput();
    }
    
    public function setEmail(Request $request){

        $data = $request->all();
        $socialUser = json_decode($request->user);
        
        $validator = Validator::make($data, [
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            $errors = ['email'=>'Please provide a valid email.'];
            return view('user.getEmail')->with('errors',$errors)->with('user',$socialUser);
        }
        
        if (User::where('email', '=', $data['email'])->exists()) {
            $errors = ['email'=>'Email already taken'];
            return view('user.getEmail')->with('errors',$errors)->with('user',$socialUser);
        }

        $u = new User();
        $u->provider_id = $socialUser->provider_id;
        $u->name = $socialUser->name;
        $u->email = $data['email'];
        $u->image= $socialUser->image;
        $u->confirmation_code = Str::random(60);
        $u->save();

        Mail::to($data['email'])->send(new ConfirmUserEmail($u));

        $validator = Validator::make($u->toArray(), [
            'confirmed' => 'accepted'
        ]);

        if ($validator->fails()) {
            $validator->errors()->add('email', 'Please Check your email to verify your account. (<a href="'.route('resend_confirmation',array('next' => Session::get('url.intended'),'email' => $u->email )).'" style="color:#4545c8">Resend</a> verification email?)');
            
            return redirect()->route('login',array('next' => Session::get('url.intended')))->withErrors($validator)->withInput();
        }
        
    }
    
}
