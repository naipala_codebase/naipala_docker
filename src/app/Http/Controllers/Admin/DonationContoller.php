<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Config;
use App\Donation;
use Session;

class DonationContoller extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    
    public function newdonation(){
        return view('admin.donation.newdonation');
    }

    public function currentdonation(){
        $page = Donation::orderBy('created_at', 'desc')->first();

        if(!$page) return redirect()->route('alldonations');

        return view('admin.donation.currentdonation')->with('donation',$page);
    }

    public function updateDonationText(Request $request){
        $this->validate($request, [
            'donationText'=>'required'
        ]);

        $shipping_donation_text = Config::where('config_key','shipping_donation_text')->first();
        $shipping_donation_text->config_value = $request->donationText;
        $shipping_donation_text->save();

        Session::flash('success','Donation Text Successfully Updated');
        return redirect()->route('alldonations');
    }

    public function updateDonationPage(Request $request){
        $this->validate($request, [
            'title'=>'required',
            'image'=>'mimes:jpeg,bmp,png',
            'description'=>'required'
        ]);
        
        $page = Donation::orderBy('created_at', 'desc')->first();

        $page->title =  $request->title;

        if($request->hasFile('image')){
            unlink(public_path('uploads/'.$page->image));  
            $image = $request->file('image');
            $filename = time().rand(111,999).'.'.$image->getClientOriginalExtension();
            $location = public_path('uploads/');
            $image->move($location,$filename);
            $page->image = $filename;
        }

        $page->description = $request->description;
        $page->save();

        Session::flash('success','Donation Description Successfully Updated');
        return redirect()->route('alldonations');
    }

    public function index(){

        $shipping_donation_text = Config::where('config_key','shipping_donation_text')->first()->config_value;
        $l = 0;
        $latest = Donation::orderBy('created_at', 'desc')->first();
        if($latest) $l = $latest->id;
        $donations = Donation::orderBy('created_at', 'desc')->paginate(5);
        
        return view('admin.donation.alldonation')->with('latest',$l)->with('donations',$donations)->with('shipping_donation_text',$shipping_donation_text);
    
    }

    public function create(Request $request){
        $this->validate($request, [
            'title'=>'required',
            'image'=>'required|mimes:jpeg,bmp,png',
            'description'=>'required'
        ]);

        $page = new Donation;

        $page->title =  $request->title;

        if($request->hasFile('image')){
            $image = $request->file('image');
            $filename = time().rand(111,999).'.'.$image->getClientOriginalExtension();
            $location = public_path('uploads/');
            $image->move($location,$filename);
            $page->image = $filename;
        }

        $page->description = $request->description;
        $page->save();

        Session::flash('success','Donation Successfully Created');
        return redirect()->route('alldonations');
    }

    public function destroy(Request $request,$id){
        $donation = Donation::find($id);
        unlink(public_path('uploads/'.$donation->image));
        $donation->delete();
        
        Session::flash('success','Donation Successfully Deleted');
        return redirect()->route('alldonations');
    }
}
