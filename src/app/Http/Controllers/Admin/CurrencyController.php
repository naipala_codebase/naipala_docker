<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Currency;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $currencies = Currency::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.currencies.index')->with('currencies',$currencies);
    }

    public function create()
    {   
        return view('admin.currencies.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [          
            'code' => 'required',
            'name' => 'required',
            'symbol' => 'required',
            'exchange_rate' => 'required|numeric',
            'active' => 'required|in:1,0'
        ]);

        $currency = new Currency;
        $currency->name = $request->name;
        $currency->code = $request->code;
        $currency->symbol = $request->symbol;
        $currency->exchange_rate = $request->exchange_rate;
        $currency->active = $request->active;

        $currency->save();

        $request->session()->flash('success', 'Currency added.');        
        
        return redirect()->route('currencies.index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function changeStatus(Request $request, $id){
        $currency = Currency::findOrFail($id);

        if($currency->active)
            $currency->active = 0;
        else
            $currency->active = 1;
        $currency->save();

        $request->session()->flash('success', 'Currency updated.');        
        
        return redirect()->back();
    }
    
    public function edit($id)
    {
        $currency = Currency::findOrFail($id);

        return view('admin.currencies.edit',compact('currency'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [          
            'code' => 'required',
            'name' => 'required',
            'symbol' => 'required',
            'exchange_rate' => 'required|numeric',
            'active' => 'required|in:1,0'
        ]);

        $currency = Currency::findOrFail($id);
        $currency->name = $request->name;
        $currency->code = $request->code;
        $currency->symbol = $request->symbol;
        $currency->active = $request->active;
        $currency->exchange_rate = $request->exchange_rate;

        $currency->save();

        $request->session()->flash('success', 'Currency updated.');        
        
        return redirect()->route('currencies.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $currency = Currency::findOrFail($id);
        $currency->delete();

        $request->session()->flash('success', 'Currency deleted.');        
        
        return redirect()->back();
    }
}
