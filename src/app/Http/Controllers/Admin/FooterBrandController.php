<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FooterBrand;
use App\Config;
use Image;

class FooterBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    
    public function index()
    {
        $left = Config::where('config_alias','left_footer_title')->first()->config_value;
        $right = Config::where('config_alias','right_footer_title')->first()->config_value;
        $footer_brands = FooterBrand::orderBy('updated_at','desc')->get();
        return view('admin.footerBrands.index',compact('footer_brands','left','right'));
    }

    public function editCategory($id,Request $request)
    {
        $this->validate($request,array(
            'title'=>'required'
        ));

        if($id == 'left')
            $left = Config::where('config_alias','left_footer_title')->update(['config_value' => $request->title]);
        elseif($id == 'right')
            $left = Config::where('config_alias','right_footer_title')->update(['config_value' => $request->title]);
        else
            abort(404);
        return redirect()->route('footerBrands.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $left = Config::where('config_alias','left_footer_title')->first()->config_value;
        $right = Config::where('config_alias','right_footer_title')->first()->config_value;
        return view('admin.footerBrands.create',compact('left','right'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'category'=>'required|in:left,right',
            'link'=>'required|url',
            'logo' => 'required|mimes:jpeg,bmp,png'
        ));

        $footer_brand = new FooterBrand;
        $footer_brand->category = $request->category;
        $footer_brand->link = $request->link;

        if($request->hasFile('logo')){
            $image = $request->file('logo');
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('footer_brands/' . $filename);
            // resize the image to a height of 200 and constrain aspect ratio (auto width)
            
            Image::make($image)->resize(null, 80, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            $footer_brand->logo = $filename;
        }

        $footer_brand->save();

        $request->session()->flash('success', 'New Brand Created.');
        return redirect()->route('footerBrands.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $left = Config::where('config_alias','left_footer_title')->first()->config_value;
        $right = Config::where('config_alias','right_footer_title')->first()->config_value;
        $footer_brand = FooterBrand::findOrFail($id);

        return view('admin.footerBrands.edit',compact('left','right','footer_brand'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'category'=>'required|in:left,right',
            'link'=>'required|url',
            'logo' => 'mimes:jpeg,bmp,png'
        ));

        $footer_brand = FooterBrand::findOrFail($id);
        
        $footer_brand->category = $request->category;
        $footer_brand->link = $request->link;

        if($request->hasFile('logo')){
            unlink(public_path('footer_brands/'.$footer_brand->logo));            
            $image = $request->file('logo');
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('footer_brands/' . $filename);
            // resize the image to a height of 200 and constrain aspect ratio (auto width)
            
            Image::make($image)->resize(null, 80, function ($constraint) {
                $constraint->aspectRatio();
            })->save($location);
            
            $footer_brand->logo = $filename;
        }

        $footer_brand->save();

        $request->session()->flash('success', 'Brand Edited.');
        return redirect()->route('footerBrands.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $footer_brand = FooterBrand::findOrFail($id);
        unlink(public_path('footer_brands/'.$footer_brand->logo));
        $footer_brand->delete();

        $request->session()->flash('success', 'Brand Deleted.');
        return redirect()->route('footerBrands.index');
    }
}
