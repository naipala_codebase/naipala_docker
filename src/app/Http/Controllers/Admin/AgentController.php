<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agent;
use App\Transaction;
use Session;
use Redirect;

class AgentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    
    public function index()
    {
        $agents = Agent::orderBy('updated_at','desc')->get();
        return view('admin.agents.index')->with('agents',$agents);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.agents.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name'=>'required',
            'contact'=>'required',
            'email'=>'required',
            'agent_status' => 'required'
        ));

        $agent = new Agent;
        $agent->name = $request->name;
        $agent->contact = $request->contact;
        $agent->email = $request->email;
        $agent->agent_status=$request->agent_status;
        
        $agent->save();
        $agent->agent_code='AG-'.$agent->id;
        $agent->save();
        $request->session()->flash('success', 'New Agent Created.');
        return redirect()->route('agents.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $agent = Agent::find($id);
        
        if(!$agent) abort(404);

        return view('admin.agents.edit')->with('agent',$agent);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name'=>'required',
            'contact'=>'required',
            'email'=>'required',
            'agent_status' => 'required'
        ));

        $agent = Agent::find($id);
        if(!$agent) abort(404);
        
        $agent->name = $request->name;
        $agent->contact = $request->contact;
        $agent->email = $request->email;
        $agent->agent_status=$request->agent_status;

        $agent->save();

        $request->session()->flash('success', 'Agent Edited.');
        return redirect()->route('agents.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $agent = Agent::find($id);
        $agent->delete();

        $request->session()->flash('success', 'Agent Deleted.');
        return redirect()->route('agents.index');
    }
    public function changeStatus($id)
    {
        try {
    		$m=Agent::findOrFail($id);
            if($m->agent_status=="active")
                Agent::where('id',$id)->update(['agent_status'=>'dormant']);
            else
                Agent::where('id',$id)->update(['agent_status'=>'active']);
            
             Session::flash('success','SuccessFully changed the status');
        } 
        catch (Exception $e) {
               return $this->renderException($e);
        }
        return Redirect::to('/admin/agents');


    }

    public function showPurchases($id)
    {
        $agent=Agent::findOrFail($id);
        $transactions=Transaction::where('agent_code',$agent->agent_code)->where(function($q) {
                        $q->where('status','completed')->orWhere('status','shipped');
                    })->get();
      
        $purchases = [];

        foreach ($transactions as $transaction) {
            $count = 0;

            $items = json_decode($transaction->details);

            foreach ($items as $item) {
                $count += $item->quantity;
            }

            $purchase = array(
                'id' => $transaction->id,
                'customerName' => $transaction->transactionable->name,
                'status' => $transaction->status,
                'agent_code'=>$transaction->agent_code,
                'count' => $count
                );
            $purchases []= $purchase;
        }
      
        return view('admin.agents.purchaselist',compact('purchases','agent'));
    }
}
