<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Config;
use App\Landingimage;
use App\Support\Helper;
use App\Item;
use Redirect;
use Session;
use App\Featured;

class ConfigController extends Controller
{
    private $config;
    private $request;
    private $landingimage;
    
    function __construct(Request $request,Config $config,Landingimage $landingimage) 
    {
         $this->middleware('auth:admin_user');
    	$this->config=$config;
    	$this->request=$request;
    	$this->landingimage=$landingimage;
    }
    public function landingConfig()
    {
    	$images=$this->landingimage->all();
    	$fid=Featured::all()->pluck('item_id');
        $allitems=Item::with('item_images')->get();
        $featureditems=[];
        $remainitems=[];
        $landingconfigs=Config::where('config_page','Landing')->get();
        foreach ($allitems as $f) {
            if($fid->contains($f->item_id))
            {
                $featureditems[]=$f;
            }
            else
            {
                $remainitems[]=$f;
            }
        }
        // dd($featureditems);
        return view('config.landingconfig',compact('images','featureditems','remainitems','landingconfigs'));
    }

    public function addFeatured()
    {
        $f= new Featured;
        $f->item_id=$this->request['item_id'];
        $f->save();
        Session::flash('success','SuccessFully added Featured');
        return Redirect::to('admin/config/landing');
    }
    public function removeFeatured($id)
    {
        $f = new Featured;
        $f->where('item_id',$id)->delete();
        Session::flash('success','SuccessFully removed featured');
        return Redirect::to('admin/config/landing');
    }
    public function infoConfig()
    {
    	try{
    		$infos=$this->config->where('config_page','Info')->get();
    	}
    	catch (Exception $e) {
            return $this->renderException($e);
         }
    	return view('config.infoconfig',compact('infos'));
    }
    public function addInfo()
    {
    	$this->config->config_page='Info';
    	$this->config->config_key=$this->request['config_key'];
    	$this->config->config_value=$this->request['config_value'];
        $this->config->config_alias=$this->request['config_alias'];
    	try{
    		$this->config->save();
    	   Session::flash('success','SuccessFully added new Info');
        }
    	 catch (Exception $e) {
            return $this->renderException($e);
         }
    	return Redirect::to('admin/config/info');
    }
    public function editInfo()
    {
    	//dd($this->request->all());
    	$all=$this->request->all();
           
    	try{
    	foreach ($all as $key => $value) {
    		if($key !="_token")
    		{
                if(substr($key,0,2)=="id")
                {

                    $this->config->where('config_id',substr($key,2))->update(['config_key'=>$this->request[$key]]);
                }
                else if(substr($key,0,3)=='val')
                {
                    $this->config->where('config_id',substr($key,3))->update(['config_value'=>$this->request[$key]]);  
                }
                else if(substr($key,0,5)=='alias')
                {
                    $this->config->where('config_id',substr($key,5))->update(['config_alias'=>$this->request[$key]]);  
                }
            
    		
    		}
    	   }
           Session::flash('success','SuccessFully edited Info');
    	}
    	catch (Exception $e) {
            return $this->renderException($e);
         }
    	return Redirect::to('admin/config/info');

    }
    public function removeInfo($id)
    {
        
        try{
            $this->config->where('config_id',$id)->delete();
            Session::flash('success','SuccessFully Removed The Info');            
        }
        catch (Exception $e) {
            return $this->renderException($e);
         }
        return Redirect::to('admin/config/info');

    }
    public function addLandingImage()
    {
    	 $this->validate($this->request, [
            
            'file'=>'required|image',
        ]);
            try {

        $this->landingimage->image_link= Helper::uploadImage($this->request->file('file'));
        $this->landingimage->save();
        Session::flash('success','SuccessFully Uploaded!!!');
        } 
        catch (Exception $e) {
               return $this->renderException($e);
         }
         return Redirect::to('admin/config/landing');
    }
    public function removeLandingImage()
    {
    	try {
               $name= $this->landingimage->where('image_id',$this->request['image_id'])->first()->image_link;
       		 Helper::removeImage($name);
        		$this->landingimage->where('image_id',$this->request['image_id'])->delete();
                Session::flash('success','Image Removed Successfully');
     } 
        catch (Exception $e) {
               return $this->renderException($e);
         }
          return Redirect::to('admin/config/landing');
    
    }
    public function editLandingLink()
    {
        try {
            $this->landingimage->where('image_id',$this->request['id'])
                               ->update(['redirect_link'=>$this->request['link']]);
        } 
        catch (Exception $e) {
               return $this->renderException($e);
         }
         return Redirect::to('admin/config/landing');
    }
    public function contact()
    {
        $contact=$this->config->where('config_page','Contact')->first();
        
        return view('config.contactconfig',compact('contact'));
    }
    public function editContact()
    {
        $this->config->where('config_page','Contact')
        ->update(['config_value'=>$this->request['contactvalue']]);
        return Redirect::to('admin/config/contact');
    }
    public function addFeaturedImage()
    {
        if($this->request->hasFile('image')){
            $image = $this->request->file('image');
            
            $this->validate($this->request,array(
                'image'=>'mimes:jpeg,bmp,png'
            ));
            
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('/frontend-assets/images/config_images');
            $image->move($location,$filename);
            
            $prev=Config::where('config_key','Featured_Image')->first()->config_value;
            if($prev)
                unlink(public_path('/frontend-assets/images/config_images'.'/'.$prev));
            Config::where('config_key','Featured_Image')->update(['config_value'=>$filename]);
            Session::flash('success','Image Uploaded Successfully');
        }
        return Redirect::to('admin/config/landing');
    }
    public function addFirstFeaturedImage()
    {
        if($this->request->hasFile('image')){
            $image = $this->request->file('image');
            
            $this->validate($this->request,array(
                'image'=>'mimes:jpeg,bmp,png'
            ));
            
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('/frontend-assets/images/config_images');
            $image->move($location,$filename);
            
            $prev=Config::where('config_key','First_Featured_Image')->first()->config_value;
            if($prev)
                unlink(public_path('/frontend-assets/images/config_images'.'/'.$prev));
            Config::where('config_key','First_Featured_Image')->update(['config_value'=>$filename]);
            Session::flash('success','Image Uploaded Successfully');
        }
        return Redirect::to('admin/config/landing');
    }
    public function addSecondFeaturedImage()
    {
        if($this->request->hasFile('image')){
            $image = $this->request->file('image');
            
            $this->validate($this->request,array(
                'image'=>'mimes:jpeg,bmp,png'
            ));
            
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('/frontend-assets/images/config_images');
            $image->move($location,$filename);
            
            $prev=Config::where('config_key','Second_Featured_Image')->first()->config_value;
            if($prev)
                unlink(public_path('/frontend-assets/images/config_images'.'/'.$prev));
            Config::where('config_key','Second_Featured_Image')->update(['config_value'=>$filename]);
            Session::flash('success','Image Uploaded Successfully');
        }
        return Redirect::to('admin/config/landing');
    }
    public function addThirdFeaturedImage()
    {
        if($this->request->hasFile('image')){
            $image = $this->request->file('image');
            
            $this->validate($this->request,array(
                'image'=>'mimes:jpeg,bmp,png'
            ));
            
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('/frontend-assets/images/config_images');
            $image->move($location,$filename);
            
            $prev=Config::where('config_key','Third_Featured_Image')->first()->config_value;
            if($prev)
                unlink(public_path('/frontend-assets/images/config_images'.'/'.$prev));
            Config::where('config_key','Third_Featured_Image')->update(['config_value'=>$filename]);
            Session::flash('success','Image Uploaded Successfully');
        }
        return Redirect::to('admin/config/landing');
    }
    public function addFirstFeaturedContent()
    {
        Config::where('config_key','First_Featured_Title')->update(['config_value'=>$this->request->title]);
        Config::where('config_key','First_Featured_Subtitle')->update(['config_value'=>$this->request->subtitle]);
        Config::where('config_key','First_Featured_Link')->update(['config_value'=>$this->request->link]);
        Session::flash('success','Saved Successfully');
        return Redirect::to('admin/config/landing');
    }
    public function addSecondFeaturedContent()
    {
        Config::where('config_key','Second_Featured_Title')->update(['config_value'=>$this->request->title]);
        Config::where('config_key','Second_Featured_Subtitle')->update(['config_value'=>$this->request->subtitle]);
        Config::where('config_key','Second_Featured_Link')->update(['config_value'=>$this->request->link]);
        Session::flash('success','Saved Successfully');
        return Redirect::to('admin/config/landing');
    }
    public function addThirdFeaturedContent()
    {
        Config::where('config_key','Third_Featured_Title')->update(['config_value'=>$this->request->title]);
        Config::where('config_key','Third_Featured_Subtitle')->update(['config_value'=>$this->request->subtitle]);
        Config::where('config_key','Third_Featured_Link')->update(['config_value'=>$this->request->link]);
        Session::flash('success','Saved Successfully');
        return Redirect::to('admin/config/landing');
    }
    public function addLandingVideoLink()
    {
        Config::where('config_key','Video_Link')->update(['config_value'=>$this->request->link]);
        Session::flash('success','Saved Successfully');
        return Redirect::to('admin/config/landing');
    }
    public function addLandingVideoImage()
    {
        if($this->request->hasFile('image')){
            $image = $this->request->file('image');
            
            $this->validate($this->request,array(
                'image'=>'mimes:jpeg,bmp,png'
            ));
            
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('/frontend-assets/images/config_images');
            $image->move($location,$filename);
            
            $prev=Config::where('config_key','Video_Image')->first()->config_value;
            if($prev)
                unlink(public_path('/frontend-assets/images/config_images'.'/'.$prev));
            Config::where('config_key','Video_Image')->update(['config_value'=>$filename]);
            Session::flash('success','Image Uploaded Successfully');
        }
        return Redirect::to('admin/config/landing');
    }
    public function addStoryContent()
    {
        Config::where('config_key','Story_Title')->update(['config_value'=>$this->request->title]);
        Config::where('config_key','Story_Subtitle')->update(['config_value'=>$this->request->subtitle]);
        Config::where('config_key','Story_Description')->update(['config_value'=>$this->request->description]);
        Session::flash('success','Saved Successfully');
        return Redirect::to('admin/config/landing');
    }
    public function addTrendingContent()
    {
        Config::where('config_key','Trending_Title')->update(['config_value'=>$this->request->title]);
        Config::where('config_key','Trending_Description')->update(['config_value'=>$this->request->description]);
        Session::flash('success','Saved Successfully');
        return Redirect::to('admin/config/landing');
    }
    public function addStoryImage()
    {
        if($this->request->hasFile('image')){
            $image = $this->request->file('image');
            
            $this->validate($this->request,array(
                'image'=>'mimes:jpeg,bmp,png'
            ));
            
            $filename = time().rand(111,999) . '.' . $image->getClientOriginalExtension();
            $location = public_path('/frontend-assets/images/config_images');
            $image->move($location,$filename);
            
            $prev=Config::where('config_key','Story_Image')->first()->config_value;
            if($prev)
                unlink(public_path('/frontend-assets/images/config_images'.'/'.$prev));
            Config::where('config_key','Story_Image')->update(['config_value'=>$filename]);
            Session::flash('success','Image Uploaded Successfully');
        }
        return Redirect::to('admin/config/landing');
    }
    
}
