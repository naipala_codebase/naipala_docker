<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\GiftCard;
use Auth;
use Session;
use DB;

class GiftCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $cards = GiftCard::where('type','physical')->select('id','discount','status','expires_on','batch')->orderBy('batch','desc')->orderBy('updated_at','desc')->paginate(10);
        return view('admin.giftCards.index')->with('cards',$cards)->with('type','add');
    }

    public function showCode(Request $request,$id){
        $code = 'Incorrect';
        if (Auth::attempt(['email' => Auth::guard('admin_user')->user()->email, 'password' => $request->password]))
        {
            if($id=='all'){
                return response()->json(['type' => 'success', 'code' => 'next']);
            }
            $code = GiftCard::find($id)->promocode;
            return response()->json(['type' => 'success', 'code' => $code]);
        }
        else 
            return response()->json(['type' => 'error', 'code' => $code]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function viewAll(Request $request){
        $codes = [];
        $today = Carbon::today();
        $expires_on = new Carbon($request->expires_on);
        $cards =  GiftCard::where('type','physical')->orderBy('batch','desc')->get();
        foreach ($cards as $card){
            if(new Carbon($card->expires_on)>=$today){
                $code =  array('id'=>$card->id, 'batch'=>$card->batch,'discount'=>$card->discount,'promocode'=>$card->promocode,'expires_on'=>$card->expires_on, 'status'=>$card->status);
                $codes []= $code;
            }
        }
        return view('admin.giftCards.viewAll')->with('cards',$codes);
    }

    public function store(Request $request)
    {
        $this->validate($request,array(
            'batch'=>'required|min:1',
            'discount'=>'required|min:1',
            'expires_on' => 'required|date|after:today'
        ));

        $today = Carbon::today();
        $expires_on = new Carbon($request->expires_on);
        if($expires_on > $today)
        {
            $code = self::generateCode();
            $code = self::verifyCode($code);
            $card = new GiftCard();
            $card->batch = $request->batch;
            $card->discount = $request->discount;
            $card->expires_on = $request->expires_on;
            $card->promocode = $code;
            $card->save();
            Session::flash('success','Card added.');
        }
        else{
            Session::flash('error','Expired card cannot be added.');
        }

        return redirect()->route('giftCards.index');
    }

    public function generateCode()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $length = rand(8,10);
        $charactersLength = strlen($characters);
        $code = '';
        for ($i = 0; $i < $length; $i++) {
            $code .= $characters[rand(0, $charactersLength - 1)];
        }
        return $code;
    }

    public function verifyCode($code){
        if (!GiftCard::where('promocode', '=', $code)->exists()) return $code;
        $newCode = self::generateCode();
        $newCode = self::verifyCode($newCode);
        return $newCode;
    }


    public function destroy($id)
    {
        $card = GiftCard::where('type','physical')->find($id);
        $card->delete();

        Session::flash('success','Card deleted.');
        return redirect()->route('giftCards.index');
    }

    public function changeStatus($id)
    {
        try {
            $card = GiftCard::find($id);
            if($card->status == 'pending'){
                $card->status = 'unused';
                $card->save();
            }
            $card->transaction->delete();
            
            Session::flash('success','SuccessFully changed the status');
        } 
        catch (\Exception $e) {
            abort(404);
        }
        return redirect()->route('giftCards.index');
    }
}
