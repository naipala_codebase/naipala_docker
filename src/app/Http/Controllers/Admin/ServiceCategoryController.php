<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ServiceCategory;
use Session;

class ServiceCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
        $service_categories = ServiceCategory::all();
        return view('admin.serviceCategories.index')->with('service_categories',$service_categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'title'=>'required|max:255'
            ));
        $service_category = new ServiceCategory();
        $service_category->title = $request->title;
        $service_category->save();

        Session::flash('success','Category Succesfully added.');
        return redirect()->route('serviceCategories.index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'title'=>'required|max:255'
            ));
        $service_category = ServiceCategory::find($id);
        $title = preg_replace('/\s+/u',"",str_replace("&nbsp;",'',strip_tags($request->title)));
        if(strlen($title) > 0){
            $service_category->title = $title;
            $service_category->save();
        }
        return response()->json(['data'=>$title]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $service_category = ServiceCategory::find($id);
        $service_category->delete();
        Session::flash('success','Category Succesfully deleted.');
        return redirect()->route('serviceCategories.index');
    }
}
