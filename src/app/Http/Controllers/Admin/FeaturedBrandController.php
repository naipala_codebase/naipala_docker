<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\FeaturedBrand;

class FeaturedBrandController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    
    public function index()
    {
        $featured_brands = FeaturedBrand::orderBy('updated_at','desc')->get();
        return view('admin.featuredBrands.index')->with('featured_brands',$featured_brands);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.featuredBrands.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,array(
            'name'=>'required|max:191',
            'description'=>'required',
            'link'=>'required|url',
            'logo' => 'required|mimes:jpeg,bmp,png'
        ));

        $featured_brand = new FeaturedBrand;
        $featured_brand->name = $request->name;
        $featured_brand->description = $request->description;
        $featured_brand->link = $request->link;

        if($request->hasFile('logo')){
            $image = $request->file('logo');
            $filename = $image->getClientOriginalName();
            $location = public_path('featured_brands/');
            $image->move($location,$filename);
            $featured_brand->logo = $filename;
        }

        $featured_brand->save();

        $request->session()->flash('success', 'New Featured Brand Created.');
        return redirect()->route('featuredBrands.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $featured_brand = FeaturedBrand::find($id);
        
        if(!$featured_brand) abort(404);

        return view('admin.featuredBrands.edit')->with('featured_brand',$featured_brand);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request,array(
            'name'=>'required|max:191',
            'description'=>'required',
            'link'=>'required|url',
            'logo' => 'mimes:jpeg,bmp,png'
        ));

        $featured_brand = FeaturedBrand::find($id);
        if(!$featured_brand) abort(404);
        
        $featured_brand->name = $request->name;
        $featured_brand->description = $request->description;
        $featured_brand->link = $request->link;

        if($request->hasFile('logo')){
            unlink(public_path('featured_brands/'.$featured_brand->logo));            
            $image = $request->file('logo');
            $filename = $image->getClientOriginalName();
            $location = public_path('featured_brands/');
            $image->move($location,$filename);
            $featured_brand->logo = $filename;
        }

        $featured_brand->save();

        $request->session()->flash('success', 'Featured Brand Edited.');
        return redirect()->route('featuredBrands.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $featured_brand = FeaturedBrand::find($id);
        unlink(public_path('featured_brands/'.$featured_brand->logo));
        $featured_brand->delete();

        $request->session()->flash('success', 'Featured Brand Deleted.');
        return redirect()->route('featuredBrands.index');
    }
}
