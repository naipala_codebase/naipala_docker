<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Invitation;
use Session;

class InvitationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $emails = Invitation::paginate();
        $count = Invitation::count();
        return view('admin.invitations',compact('emails','count'));
    }

    public function destroy($id)
    {
        $email = Invitation::findorFail($id);
        $email->delete();
        Session::flash('success','Invitation Email Succesfully deleted.');
        return redirect()->route('invitations.index');
    }

    public function reset()
    {
        $email = Invitation::truncate();

        Session::flash('success','Invitation Email Succesfully reset.');
        return redirect()->route('invitations.index');
    }

    public function all(){
        $users = Invitation::orderBy('email','asc')->get();
        return view('admin.allinvites')->with('users',$users);
    }
}
