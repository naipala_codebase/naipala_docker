<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\Item;
use Redirect;
use App\Item_Image;
use App\Support\Helper;
use Session;
use App\Config;

class ItemController extends Controller
{
	private $category;
	private $item;
	private $itme_image;
	private $request;
    private $attrib;
    function  __construct(Category $category,Item $item,Request $request,Item_Image $item_image)
    {
        $this->middleware('auth:admin_user');
        $this->category=$category;
    	$this->item=$item;
    	$this->request=$request;
    	$this->item_image=$item_image;
    }

    public function index()
    {
    	try{
    		$category=$this->category->all();
         //   $items=$this->item->all();
    		$items=$this->item->join('categorys','categorys.category_id','=','items.category_id')    
    						->select('items.*','categorys.category_name')
    						->with('item_images')
                            ->get();
            $description = Config::where('config_key','Collection')->first()->config_value;
    	}
    	 catch (Exception $e) {
               return $this->renderException($e);
         }
       //  dd($items);
         return view('item.list',compact('category','items','description'));
    }

    public function updateDescription(Request $request){
        
        
        $this->validate($this->request, [
            'descriptionContent'=>'required'
  
          ]);
        $description = Config::where('config_key','Collection')->first();
        $description->config_value = $request->descriptionContent;
        $description->save();

        Session::flash('success','Collection Description Successfully Updated');
        return Redirect::to('admin/item');
    }

    public function create()
    {
        try {
            $category= Category::all()->pluck('category_name','category_id');
        } 
        catch (Exception $e) {
               return $this->renderException($e);
         }

        
    	return View('item.form',compact('category'));
    }

    public function store()
    {

         $this->validate($this->request, [
            'item_name'=>'required|max:75',
            'item_description'=>'required',
            'item_price'=>'required|numeric',
            'item_status'=>'required',
            'category_id'=>'required|exists:categorys,category_id',
            'details'=>'required',
            'item_old_price' => 'nullable|numeric'
        ]);
      
    	$this->item->item_name=$this->request['item_name'];
    	$this->item->item_description=$this->request['item_description'];
        $this->item->item_price=$this->request['item_price'];
        $this->item->item_old_price=$this->request['item_old_price'];
    	$this->item->item_status=$this->request['item_status'];
        $this->item->category_id=$this->request['category_id'];
        $this->item->details=$this->request['details'];
        $this->item->whats_the_story=$this->request['whats_the_story'];
        $this->item->what_sets_it_apart=$this->request['what_sets_it_apart'];        
        $this->item->item_alias="lll";

        try {
                $this->item->save();
                // $this->item->where('item_id',$this->item->item_id)->update(['item_alias'=>str_replace("-"," ",$this->item->item_name.'-'.$this->item->item_id)]);                
                $this->item->item_alias=str_replace(' ','-',$this->item->item_name.'-'.$this->item->item_id);
                $this->item->save();
                Session::flash('success','SuccessFully added the Item. Now add images');
            } 
            catch (Exception $e) {
                   return $this->renderException($e);
            }
    	return Redirect::to('admin/item#items');
    }

    public function edit($id)
    {
        try {
        $category= $this->category->all()->pluck('category_name','category_id');
        $item=$this->item->where('item_id',$id)->first();
        
        } 
        catch (Exception $e) {
               return $this->renderException($e);
         }

        return View('item.form',compact('item','category'));
    
    }

    public function update($id)
    {
        $this->validate($this->request, [
             'item_name'=>'required|max:75',
            'item_description'=>'required',
            'item_price'=>'required|numeric|min:1',
            'item_status'=>'required',
            'category_id'=>'required|exists:categorys,category_id',
            'details'=>'required',
            'item_old_price' => 'nullable|numeric'
        ]);

        
        try {

            $this->item->where('item_id',$id)->update([
                'item_name'=>$this->request['item_name'],
                'item_status'=>$this->request['item_status'],
                'item_description'=>$this->request['item_description'],
                'item_price'=>$this->request['item_price'],
                'item_old_price'=>$this->request['item_old_price'],
                'category_id'=>$this->request['category_id'],
                'details'=>$this->request['details'],
                'whats_the_story'=>$this->request['whats_the_story'],
                'what_sets_it_apart'=>$this->request['what_sets_it_apart'],
                'item_alias'=>str_replace(' ','-',$this->request['item_name'].'-'.$id)
            ]);
                
            $images=$this->item_image->where('item_id',$id)->get();
            $del = [];
            foreach ($images as $i) {
                $flag = 0;
                foreach(json_decode($this->request['details']) as $d){
                    if($i->color == $d->color ){
                        $flag = 1;
                    }
                    if(!$i->color && $d->color == ''){
                        $flag = 1;
                    }
                }
                if($flag==0 ){
                    $del []= $i;
                }
            }
            foreach($del as $d){
                Helper::removeImage($d->image_link);
                $this->item_image->where('image_id',$d->image_id)->delete();                
            }

            Session::flash('success','SuccessFully edited the item');
        } 
        catch (Exception $e) {
               return $this->renderException($e);
         }


        Session::flash('success','SuccessFully Updated The item');
        return Redirect::to('/admin/item#items');
    }

    public function changeStatus($id)
    {
        try {
    		$m=$this->item->where('item_id',$id)->first();
            if($m->item_status=="active")
                $this->item->where('item_id',$id)->update(['item_status'=>'dormant']);
            else
                $this->item->where('item_id',$id)->update(['item_status'=>'active']);
            
             Session::flash('success','SuccessFully changed the status');
        } 
        catch (Exception $e) {
               return $this->renderException($e);
        }
        return Redirect::to('/admin/item#items');


    }

    public function destroy($id)
    {
        try{
            $images=$this->item_image->where('item_id',$id)->get();
            foreach ($images as $i) {
                Helper::removeImage($i->image_link);
                // delete($i->image_link);
                
            
            }
           
            $this->item->where('item_id',$id)->delete();
             Session::flash('success','SuccessFully removed item');

        }
       catch (Exception $e) {
            return $this->renderException($e);
         }

        Session::flash('success','SuccessFully deleted The product');
        return Redirect::to('admin/item#items');
    }

    public function addCategory()
    {
        $this->validate($this->request, [
            'name'=>'required|max:75'
        ]);

    	try{
            $this->category->category_name=$this->request['name'];
            $this->category->category_alias="asdasd";
            $this->category->save();	
            $this->category->category_alias=str_replace(' ','-',$this->category->category_name.'-'.$this->category->category_id);
            $this->category->save();
            Session::flash('success','SuccessFully Updated The  Category');
    	}
    	catch (Exception $e) {

               return $this->renderException($e);
         }
         return Redirect::to('admin/item#categories');
    }

    public function editCategory()
    {
        $this->validate($this->request, [
            'name'=>'required|max:75'
        ]);
        try{
            $this->category->where('category_id',$this->request['id'])
                        ->update(['category_name'=>$this->request['name'],
                                'category_alias'=>$this->request['name'].'-'.$this->request['id']]);
                 Session::flash('success','SuccessFully Edited The  Category');
        }
        catch (Exception $e) {
               return $this->renderException($e);
         }
         return Redirect::to('admin/item#categories');
    }

    public function removeCategory()
    {
        
        try{
       
            $this->category->where('category_id',$this->request['id'])->delete();
            return "success";
                 Session::flash('success','SuccessFully Removed The  Category');
        
        }
        catch (Exception $e) {
               return $this->renderException($e);
         }
         return Redirect::to('admin/item#categories');
    }
    
    public function images($id)
    {
        $item_id=$id;
        $item_name=$this->item->where('item_id',$id)->first()->item_name;
        $images=$this->item_image->where('item_id',$id)->get();
        $types = json_decode($this->item->where('item_id',$id)->first()->details);
        $colors = [];
        
        foreach($types as $type){
            if($type->color!='')
                $colors[$type->color] = [];
            else
                $colors[null] = [];
        }
        
        foreach($images as $image){
            if($image->color)
                $colors[$image->color] []= $image;
            else
                $colors[null] []= $image;
        }
        // dd($colors);
        return view('item.images',compact('item_id','item_name','colors'));
    }

    public function addImage()
    {
       
        $this->validate($this->request, [
            'id'=>'required|exists:items,item_id',
            'files.*'=>'required|image',
        ]);
        
        try {
            $c = 0;
            foreach($this->request->file('files') as $file){
                $itemImage = new Item_Image;
                $itemImage->item_id=$this->request['id'];
                $itemImage->color=$this->request['color'];
                $itemImage->image_link= Helper::uploadImage($file,'uploads','I'.$c++);
                $itemImage->save();
            }
            Session::flash('success','SuccessFully uploaded the image');
        } 
        catch (Exception $e) {
               return $this->renderException($e);
        }
        return Redirect::to('admin/item/images'.'/'.$this->request['id']);

    }

    public function removeImage()
    {
        try {
                   $name= $this->item_image->where('image_id',$this->request['image_id'])->first()->image_link;
            Helper::removeImage($name);
            $this->item_image->where('image_id',$this->request['image_id'])->delete();
           Session::flash('success','SuccessFully removed the image');
         } 
        catch (Exception $e) {
            return $this->renderException($e);
        }
        return Redirect::to('admin/item/images'.'/'.$this->request['item_id']);
    }
}
