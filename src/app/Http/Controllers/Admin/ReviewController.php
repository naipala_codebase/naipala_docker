<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Review;
use App\Item;
use Session;
use Redirect;

class ReviewController extends Controller
{
	private $item;
	private $review;
	function __construct(Item $item,Review $review,Request $request)
	{

		$this->middleware('auth:admin_user');
		$this->request=$request;
		$this->item=$item;
		$this->review=$review;
	}
    public function index()
    {
    	$reviews=$this->review->with('item')->get();

    	return view('item.review',compact('reviews'));
    }
    public function destroy($id)
    {
    	try{
    		$this->review->where('review_id',$id)->delete();
    		Session::flash('success','SuccessFully removed the review');
    	}
    	catch(Exception $e)
    	{
    		  return $this->renderException($e);
    	}
    	return Redirect::to('admin/review');
    }
}
