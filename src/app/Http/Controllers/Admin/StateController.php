<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\State;
use App\Country;
use Validator;

class StateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index($country_id)
    {
        $states = State::where('country_id',$country_id)->orderBy('name')->get();
        $country = Country::findOrFail($country_id);
        
        return view('admin.states.index')->with('country',$country)->with('states',$states);
    }

    public function create($country_id)
    {   
        return view('admin.states.create')->with('country_id',$country_id);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $country_id)
    {
        $validator = Validator::make($request->states, [          
            '*.code' => 'required',
            '*.name' => 'required',
        ]);
        if($validator->fails()){
            $request->session()->flash('error', 'Please provide all information');
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $country = Country::findOrFail($country_id);
        $country->states()->createMany($request->states);

        $request->session()->flash('success', 'State added.');        
        
        return redirect()->route('states.index',$country_id);
    }

    
    public function edit($country_id,$id)
    {
        $state = State::where('country_id',$country_id)->findOrFail($id);

        return view('admin.states.edit',compact('state'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $country_id, $id)
    {
        $this->validate($request, [          
            'code' => 'required',
            'name' => 'required'
        ]);

        $state = State::where('country_id',$country_id)->findOrFail($id);
        $state->name = $request->name;
        $state->code = $request->code;
        $state->save();

        $request->session()->flash('success', 'State updated.');        
        
        return redirect()->route('states.index',$country_id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $country_id, $id)
    {
        $state = State::where('country_id',$country_id)->findOrFail($id);
        $state->delete();

        $request->session()->flash('success', 'State deleted.');        
        
        return redirect()->back();
    }
}
