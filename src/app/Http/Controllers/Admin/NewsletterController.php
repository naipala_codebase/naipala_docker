<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Subscriber;
use App\Mail\Newsletter;
use Session;
use Mail;

class NewsletterController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }

    public function index()
    {
    	return view('admin.newsletters.index');
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'content'=>'required'
        ]);
        
        $subscribers = Subscriber::all();
        // $subscriber = $subscribers->first();
        foreach ($subscribers as $subscriber) {
            Mail::to($subscriber->email)->queue(new Newsletter($subscriber,$request->content));
        }   
        // Mail::to('abaj2245@gmail.com')->queue(new Newsletter($subscriber,$request->content));
        // Mail::to('tintindean@yahoo.com')->queue(new Newsletter($subscriber,$request->content));
       
    	Session::flash('success','Sending emails.');
    	return redirect()->route('newsletters.index');
    }
}
