<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Country;

class CountryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin_user');
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $countries = Country::orderBy('updated_at','desc')->paginate(10);
        
        return view('admin.countries.index')->with('countries',$countries);
    }

    public function create()
    {   
        return view('admin.countries.create');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [          
            'code' => 'required',
            'name' => 'required',
            'term_for_administrative_divisions' => 'required'
        ]);

        $country = new Country;
        $country->name = $request->name;
        $country->code = $request->code;
        $country->term_for_administrative_divisions = $request->term_for_administrative_divisions;

        $country->save();

        $request->session()->flash('success', 'Country added.');        
        
        return redirect()->route('countries.index');
    }

    
    public function edit($id)
    {
        $country = Country::findOrFail($id);

        return view('admin.countries.edit',compact('country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [          
            'code' => 'required',
            'name' => 'required',
            'term_for_administrative_divisions' => 'required'
        ]);

        $country = Country::findOrFail($id);
        $country->name = $request->name;
        $country->code = $request->code;
        $country->term_for_administrative_divisions = $request->term_for_administrative_divisions;

        $country->save();

        $request->session()->flash('success', 'Country updated.');        
        
        return redirect()->route('countries.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        $country = Country::findOrFail($id);
        $country->delete();

        $request->session()->flash('success', 'Country deleted.');        
        
        return redirect()->back();
    }
}
