<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\Social;
use App\Config;
use App\Socialimage;
use App\Support\Helper;

class SocialController extends Controller
{
	private $socialimage;
	private $config;

	function __construct(Request $request, Config $config,Social $social, Socialimage $socialimage)
	{
        $this->middleware('auth:admin_user');
        $this->social=$social;
		$this->request=$request;
		$this->config=$config;
		$this->socialimage=$socialimage;
	}
    public function index()
    {
    	
      try{
        $socials=$this->social->where('social_id','>',0)->with('socialimages')->get();
        $sociallinks=$this->config->where('config_page','SocialLink')->get();
      }
      catch (Exception $e) {
            return $this->renderException($e);
         }
       //  dd($socials);
      return view('config.socialconfig',compact('socials','sociallinks'));
    }
    public function edit($id)
    {
      $this->validate($this->request, [
             'title'=>'required|max:75',
            'description'=>'required',
            ]);

    	$this->social->where('social_id',$id)
    				->update(['social_title'=>$this->request['title'],
    					'social_description'=>$this->request['description']]);
      Session::flash('success','SuccessFully edited social info!!!');
    	return Redirect::to('admin/social');
    }
    public function addSocial()
    {
      $this->validate($this->request, [
             'title'=>'required|max:75',
            'description'=>'required',
            ]);

      $this->social->social_title=$this->request['title'];
      $this->social->social_description=$this->request['description'];
      $this->social->save();
      Session::flash('success','SuccessFully added social info!!!');

      return Redirect::to('admin/social');
    }

    public function removeSocial($id)
    {
        
        try{
            $this->social->where('social_id',$id)->delete();
            Session::flash('success','SuccessFully Removed The Info');
        }
        catch (Exception $e) {
            return $this->renderException($e);
            
         }
        return Redirect::to('admin/social');

    }

      public function addSocialImage()
    {
       $this->validate($this->request, [
            
            'file'=>'required|image',
        ]);
    	
            try {

        $this->socialimage->image_link= Helper::uploadImage($this->request->file('file'));
        $this->socialimage->social_id=$this->request['social_id'];
        $this->socialimage->save();
        Session::flash('success','SuccessFully Uploaded!!!');
        } 
        catch (Exception $e) {
               return $this->renderException($e);
         }
         return Redirect::to('admin/social');
    }
    public function removeSocialImage()
    {
    	try {
               $name= $this->socialimage->where('socialimage_id',$this->request['image_id'])->first()->image_link;
       		 Helper::removeImage($name);
        		$this->socialimage->where('socialimage_id',$this->request['image_id'])->delete();
                Session::flash('success','Image Removed Successfully');
     } 
        catch (Exception $e) {
               return $this->renderException($e);
         }
          return Redirect::to('admin/social');
    
    }
    public function addCaption()
    {
        //dd($this->request->all());
        $this->socialimage->where('socialimage_id',$this->request['id'])
             ->update(['image_caption'=>$this->request['caption']]);
        return "Success";
    }
    public function addLinks()
    {
       // dd($this->request->all());
        $linkcount=$this->request['linkcount'];
        for($i=1;$i<=$linkcount;$i++)
        {
          $config= new Config;
          $config->config_page='SocialLink';
          $config->config_key=$this->request['linktitle'.$i];
          $config->config_value=$this->request['linkval'.$i];
          $config->save();
        }
        Session::flash('success','SuccessFully Link Added!!!');
        return Redirect::to('admin/social'); 
    }
    public function removeLink($id)
    {
        $this->config->where('config_id',$id)->delete();
        Session::flash('success','SuccessFully Link Deleted!!!');
        return Redirect::to('admin/social'); 
    }
}
