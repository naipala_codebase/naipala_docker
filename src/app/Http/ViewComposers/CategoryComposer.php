<?php 

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Repositories\MenuRepository;
use App\Category;
use Cart;
use Auth;

class CategoryComposer
 {
 
 	public function compose(View $view)
 	{
 		$category=Category::all();
		$cartitems=Cart::content();
		if(Auth::guest())
		{
			$wishlistitems="asd";
		}
		else
		{
			Cart::instance('wishlist')->restore(Auth::user()->id);
			$wishlistitems=Cart::instance('wishlist')->content();
			Cart::instance('wishlist')->store(Auth::user()->id);
		}
		$view->with('categories', $category)->with('cartitems',$cartitems)->with('wishlistitems',$wishlistitems);
 		
 	}

 }