<?php 

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use App\Repositories\MenuRepository;
use App\Config;
use App\ServiceCategory;
use App\FooterBrand;
use App\Currency;
use App\Country;
use Session;
use GeoIP;

class InfoComposer
 {
 
 	public function compose(View $view)
 	{
 		$infos=Config::where('config_page','Info')->get();
 		$contact=Config::where('config_page','Contact')->first();
 		$servicecategory=ServiceCategory::has('services')->get();
 		$left = Config::where('config_alias','left_footer_title')->first()->config_value;
		$right = Config::where('config_alias','right_footer_title')->first()->config_value;

 		$view->with('contact',$contact);
		$view->with('infos', $infos);
		
		$footer_brands = FooterBrand::all();

		$currencies = Currency::where('active',1)->get();

		$current = Session::get('currency');
		$country = Session::get('country');

        if(!$current || !$country){
			$location = GeoIP::getLocation();
			$current = $currencies->where('code', $location->currency)->first();
			$country = Country::where('code', $location->iso_code)->first();
			if(!$current) $current = $currencies->where('code','AUD')->first();
			if(!$country) $country = Country::where('code','AU')->first();

			Session::put('currency', $current);
			Session::put('country', $country->name);
		}

 		$view->with('servicecategory', $servicecategory)->with('current', $current);
		$view->with('left', $left)->with('right', $right)->with('footer_brands', $footer_brands)->with('currencies',$currencies);
 	}

 }