<?php 

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use Auth;
use DB;

class NotificationComposer
 {
 
 	public function compose(View $view)
 	{
 		$unseen = 0;
 		if(Auth::check()){
	 		$unseen = DB::table('service_books')
	                ->join('services', 'service_books.service_id', '=', 'services.id')
	                ->where('user_id',Auth::user()->id)
	                ->where('seen',0)
	                ->count();
	 		
 		}
 		$view->with('unseen', $unseen);
 	}

 }