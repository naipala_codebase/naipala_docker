<?php 

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;
use DB;

class AdminNotificationComposer
{
 
 	public function compose(View $view)
 	{
 		$unseen = 0;
 		$unseen = DB::table('transactions')
                ->where('status','completed')
                ->count();
	 		
 		
 		$view->with('unseen', $unseen);
 	}

}