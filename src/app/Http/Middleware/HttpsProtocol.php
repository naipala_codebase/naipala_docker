<?php



namespace App\Http\Middleware;



use Closure;
use App;



class HttpsProtocol {



    public function handle($request, Closure $next)

    {
            if (!App::isLocal() && !$request->secure()) {
            // if (!$request->secure()) {

                return redirect()->secure($request->getRequestUri());

            }
            return $next($request); 
    }

}



?>