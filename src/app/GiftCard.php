<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GiftCard extends Model
{
    public function transaction()
    {
        return $this->hasOne('App\Transaction','giftCard_id');
    }
}
