<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function service_category()
    {
    	return $this->belongsTo('App\ServiceCategory');
    }

    public function service_books()
    {
    	return $yhis->hasMany('App\ServiceBook');
    }
}
