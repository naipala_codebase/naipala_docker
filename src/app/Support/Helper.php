<?php namespace App\Support;


/**
* 	Cutom Helper Methodos
*	
*/
class Helper
{


	/*
	* Uploads File To Server 
	* @param $file 
	* @param $path
	* @return string
	*/
	static  function uploadImage($file,$path='uploads',$any='')
	{
		$extension = $file->getClientOriginalExtension();
		$fileName = time().rand(111,999).$any.'.'.$extension;
		$file->move($path,$fileName);
		return $fileName;
	}	

	/*
	* Removes File To Server 
	* @param $file 
	* @param $path
	* @return boolean
	*/
	static  function removeImage($file,$path='uploads')
	{

		return unlink($path.'/'.$file);
	}





}
