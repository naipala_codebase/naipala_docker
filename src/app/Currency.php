<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Money\Currencies\ISOCurrencies;
use Money\Currency as MCurrency;
use Session;
use GeoIP;

class Currency extends Model
{
    public static function currency($amount){
        $current = Session::get('currency');
        
        if(!$current){
            $location = GeoIP::getLocation();
            $current = Currency::where('code',$location->currency)->where('active',1)->first();
			
			if(!$current) $current = Currency::where('code','AUD')->first();

			Session::put('currency', $current);
        }

        $amount = self::currency_convert($amount, $current);
        return $current->symbol.' '.$amount;

    }

    public static function currency_convert($amount, $current)
    {
        
        $amount *= $current->exchange_rate;

        $iso_currencies = new ISOCurrencies();
        $currency = new MCurrency($current->code);

        if ($currency) {
            $decimals = $iso_currencies->subunitFor($currency);
            $amount = number_format($amount, $decimals, '.', '');
        }
        
        return $amount;
    }
}
