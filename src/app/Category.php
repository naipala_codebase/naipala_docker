<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table='categorys';
     protected $primaryKey = "category_id";
    public function items()
    {
        return $this->hasMany('App\Item','category_id','category_id');
    }
}
