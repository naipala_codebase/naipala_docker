<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\EGiftCardTransaction;

class ECardPurchased extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public $etransaction;

    public function __construct(EGiftCardTransaction $etransaction)
    {
        $this->etransaction = $etransaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Naipala E-Gift Card Purchased')->view('emails.egiftcardpurchased')->with('etransaction',$this->etransaction);
    }
}
