<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Subscriber;
class Newsletter extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $subscriber;
    protected $content;

    public function __construct(Subscriber $subscriber,$content)
    {
        $this->subscriber = $subscriber;
        $this->content = $content;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    
    public function build()
    {
        
        return $this->view('emails.newsletter')->with('subscriber',$this->subscriber)->with('content',$this->content);
    }
}
