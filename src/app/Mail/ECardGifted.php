<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\EGiftCardTransaction;

class ECardGifted extends Mailable
{
    use Queueable, SerializesModels;

    public $etransaction;

    public function __construct(EGiftCardTransaction $etransaction)
    {
        $this->etransaction = $etransaction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Naipala E-Gift Card Gifted')->view('emails.egiftcardgifted')->with('etransaction',$this->etransaction);
    }
}
