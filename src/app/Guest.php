<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guest extends Model
{
    public function service_books(){
        return $this->morphMany('App\ServiceBook');
    }

    public function transactions()
    {
        return $this->morphMany('App\Transaction','transactionable');
    }
}
