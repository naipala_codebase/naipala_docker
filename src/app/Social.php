<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Social extends Model
{
    protected $primaryKey='social_id';
    public function socialimages()
    {
    	return $this->hasMany('App\Socialimage','social_id','social_id');
    }
    public function comments()
    {
        return $this->hasMany('App\Socialcomment','social_id','social_id');
        
    }
}
