<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item_Image extends Model
{
    protected $table='item_images';
    protected $primaryKey = 'item_id';
    
    public function item()
    {
        return $this->belongsTo('App\Item','item_id','item_id');
    }
}
