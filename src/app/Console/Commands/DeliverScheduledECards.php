<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Mail\ECardGifted;
use App\EGiftCardTransaction;
use Carbon\Carbon;
use Mail;

class DeliverScheduledECards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeliverScheduledECards:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deliver Purchased E-Gift Cards to Reciever as per Scheduled';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $etransactions = EGiftCardTransaction::where('status','completed')->where('delivery_date',Carbon::today('Australia/Sydney'))->get();

        foreach($etransactions as $et){
            Mail::to($et->recipient_contact)->send(new ECardGifted($et));
        }
    }
}
