<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class queueRun extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'QueueEnsure:send';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'QueueEnsure:send';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $run_command = false;
        if (file_exists(__DIR__ . '/queue.pid')) {
            $pid = file_get_contents(__DIR__ . '/queue.pid');
            $result = exec("ps -p $pid --no-heading | awk '{print $1}'");
            if ($result == '') {
                $run_command = true;
            }
        } else {
            $run_command = true;
        }
        if($run_command)
        {
            $command = '/usr/local/bin/ea-php56 /home/naipalaweb/naipala/artisan queue:work --tries=3 > /dev/null & echo $!';
            $number = exec($command);
            file_put_contents(__DIR__ . '/queue.pid', $number);
        }
    }
}
