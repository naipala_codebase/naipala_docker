<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//landing for users
Auth::routes();
Route::get('/', 'HomeController@index')->name('home');
Route::get('/featuredbrands', 'HomeController@featuredBrands');
Route::post('/contact_us', 'HomeController@contact_us')->name('contact_us');
Route::get('/giftcard',function(){
	return view('landing.giftcard');
});

Route::get('/heartbeat', function() {
	dd("Asdf");
	return 'test';
});
// Route::get('/querytest', 'HomeController@test');

Route::post('/register/getEmail','Auth\RegisterController@setEmail')->name('setEmail');
Route::get('/user/confirm/{token}','HomeController@confirmEmail')->name('confirmEmail');
Route::get('/search','FrontEnd\CollectionController@search');
Route::get('/home', 'HomeController@index');
Route::get('/info', 'HomeController@info');
Route::get('/collection', 'FrontEnd\CollectionController@index')->name('collection');
Route::get('/collection/purchase','FrontEnd\CollectionController@purchase');
Route::get('/collection/rating','FrontEnd\CollectionController@rating');
Route::get('/collection/{itemid}/rating/{rate}','FrontEnd\CollectionController@rating1');
Route::get('/collection/item/{slug}','FrontEnd\CollectionController@itemPage')->name('collection.itemDetail');
Route::get('/collection/getreviews/{id}','FrontEnd\CollectionController@getReview');
Route::post('/collection/addreview','FrontEnd\CollectionController@addReview');
Route::get('/collection/getquickdetails','FrontEnd\CollectionController@getQuickDetails');
Route::get('/collection/{slug}', 'FrontEnd\CollectionController@category');

// Tickets
Route::get('/tickets','FrontEnd\TicketsController@index');
Route::get('/tickets/item/{slug}','FrontEnd\TicketsController@ticketPage')->name('tickets.ticketDetail');

Route::post('/wishlist/additem','FrontEnd\WishlistController@addItem');
Route::post('/wishlist/removeitem','FrontEnd\WishlistController@removeItem');


Route::post('/cart/additem','FrontEnd\CartController@addItem');
Route::post('/cart/removeitem','FrontEnd\CartController@removeItem');
Route::post('/cart/emptycart','FrontEnd\CartController@emptyCart');
Route::post('/cart/updateqty','FrontEnd\CartController@updateQty');

Route::get('/cart','FrontEnd\CartController@cart')->name('cart');
Route::post('cart/purchase/{code}/shippingInformation','FrontEnd\CartController@shippingInformation')->name('shippingInformation');
Route::post('/cart/purchase','FrontEnd\CartController@purchase')->name('cart.purchase');
Route::get('/cart/{code}/purchase/','FrontEnd\CartController@payment')->name('cart.payment');
Route::post('/cart/{code}/validateCode','FrontEnd\CartController@validateCode');
Route::post('/cart/{code}/donate','FrontEnd\CartController@donate');
Route::post('cart/{code}/discard','FrontEnd\CartController@discard')->name('cart.discard');

Route::get('cart/{code}/payment','FrontEnd\PaypalController@payWithPaypal')->name('billingform');
Route::post('cart/{code}/payment','FrontEnd\PaypalController@postPaymentWithpaypal')->name('postPaymentWithpaypal');
Route::post('cart/{code}/payment/status','FrontEnd\PaypalController@getPaymentStatus')->name('payment.status');

Route::post('cart/{code}/success','FrontEnd\CartController@paymentSuccess')->name('paymentSuccess');

Route::post('eCards/initial','FrontEnd\EGiftCardController@initial')->name('ecards.initial');
Route::get('eCards/{code}/checkout','FrontEnd\EGiftCardController@checkoutform')->name('ecards.checkoutform');
Route::post('eCards/{code}/checkout','FrontEnd\EGiftCardController@checkout')->name('ecards.checkout');
Route::get('eCards/{code}/payment','FrontEnd\EGiftCardController@payment')->name('ecards.payment');
Route::post('eCards/{code}/payment','FrontEnd\PaypalController@postecardPaymentWithpaypal')->name('postecardPaymentWithpaypal');
Route::post('eCards/{code}/payment/status','FrontEnd\PaypalController@getecardPaymentStatus')->name('ecards.status');
Route::get('eCards/portalform','FrontEnd\EGiftCardController@portallogin')->name('ecards.portallogin');
Route::post('eCards/portalform','FrontEnd\EGiftCardController@login');
Route::get('eCards/portal','FrontEnd\EGiftCardController@portal')->name('ecards.portal');
Route::post('eCards/portal/resend','FrontEnd\EGiftCardController@resend')->name('ecards.resend');
Route::get('eCards','FrontEnd\EGiftCardController@index')->name('ecards.index');

Route::post('/subscribe','HomeController@subscribe')->name('subscribe');
Route::get('unsubscribe/{token}','HomeController@unsubscribe')->name('unsubscribe');
Route::post('unsubscribe/{token}','HomeController@unsubscribed')->name('unsubscribed');

Route::post('invite','HomeController@invite')->name('invite');

Route::get('confirm/resend','Auth\RegisterController@resendConfirmation')->name('resend_confirmation');

Route::get('/services', 'FrontEnd\ServiceController@index')->name('services')->middleware('revalidate');
Route::get('/services/servicesearch', 'FrontEnd\ServiceController@search')->middleware('revalidate');
Route::get('/services/book/{service_id}','FrontEnd\ServiceController@serviceBook')->name('serviceBook');
Route::post('/services/book/guest','FrontEnd\ServiceController@serviceBook')->name('guest.serviceBook');
Route::post('/services/book/addMessage','FrontEnd\ServiceController@addMessage');

Route::get('/social', 'HomeController@social');
Route::post('/social/like', 'HomeController@likeSocial');
Route::post('/social/comment', 'HomeController@socialComment');
Route::get('/social/gettopcomments', 'HomeController@getTopComments');
Route::get('/social/getallcomments', 'HomeController@getAllComments');



Route::get('/notification','UserController@notification');
Route::post('/notification/{id}/read','UserController@notificationRead');

Route::post('/profile/service/create','UserController@servicePost')->name('services.create');
Route::post('/profile/service/edit','UserController@serviceEdit')->name('services.edit');
Route::get('/profile', 'UserController@index')->name('profile')->middleware('revalidate');
Route::post('/profile/edit', 'UserController@profileEdit')->name('profile.edit');
Route::post('/profile/image','UserController@addImage')->name('user.image');
Route::post('/profile/service/{id}/delete','UserController@serviceDelete')->name('services.destroy');
Route::get('/profile/viewallapplicants','UserController@allApplicants');

Route::get('/giftcard',function(){
	return view('landing.giftcard');
});

Route::get('/signin/facebook', 'Auth\LoginController@redirectToProvider');
Route::get('/signin/facebook/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('info/{slug}','HomeController@info');

Route::get('donation','HomeController@donation');

Route::get('/currency/set', 'HomeController@setCurrency');

Route::get('/asset/{source}/{img}/{h}/{w}',function($source,$img, $h, $w){
    try{
        $ext = explode(".",$img);
        $ext = end($ext);
        $source = str_replace('*','/',$source);
        if($w == 'auto'){
            return \Image::make(public_path($source."/".$img))->resize(null, $h, function ($constraint) {
                    $constraint->aspectRatio();
                })->response($ext);
        }
        return \Image::make(public_path($source."/".$img))->resize($h, $w)->response($ext);
    }catch(\Exception $e){
        abort(404);
    }
})->name('optimize');

//landing for admin
Route::prefix('admin')->group(function(){
	Route::get('/login','AdminAuth\LoginController@showLoginForm')->name('admin_login');
	Route::post('/login','AdminAuth\LoginController@login');
	Route::post('/logout','AdminAuth\LoginController@logout')->name('admin_logout');
	Route::post('/password/email','AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('admin_password.email');
	Route::post('/password/reset','AdminAuth\ResetPasswordController@reset');
	Route::get('/password/reset','AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('admin_password.request');
	Route::get('/password/reset/{token}','AdminAuth\ResetPasswordController@showResetForm')->name('admin_password.reset');
	
	Route::get('changepassword','AdminUserController@changepassword')->name('admin.changepassword');
	Route::post('changedpassword','AdminUserController@changedpassword')->name('admin.changedpassword');

	Route::get('/','AdminUserController@index')->name('admin_dashboard');

	Route::get('currencies/changestatus/{id}','Admin\CurrencyController@changeStatus');
	Route::resource('currencies','Admin\CurrencyController',['except'=>['show']]);

	Route::resource('countries','Admin\CountryController',['except'=>['show']]);
	Route::resource('countries/{country_id}/states','Admin\StateController',['except'=>['show']]);

	Route::get('/currentdonation','Admin\DonationContoller@currentdonation')->name('currentdonation');
	Route::get('/donations/add','Admin\DonationContoller@newdonation')->name('newdonation');
	Route::post('/donations/create','Admin\DonationContoller@create')->name('create');
	Route::delete('/donations/{id}/delete','Admin\DonationContoller@destroy')->name('destroy');
	Route::post('/updateDonationText','Admin\DonationContoller@updateDonationText')->name('updateDonationText');
	Route::post('/updateDonationPage','Admin\DonationContoller@updateDonationPage')->name('updateDonationPage');
	Route::get('donations','Admin\DonationContoller@index')->name('alldonations');

	Route::get('users/list', 'AdminUserController@allusers')->name('admin.users');
	Route::get('subscribers/list', 'AdminUserController@allsubscribers')->name('admin.subscribers');
	Route::get('activeUsers/list', 'AdminUserController@active_users')->name('admin.active_users');

	Route::get('/notifications','AdminUserController@notifications')->name('admin-notification');
	Route::get('/notifications/unseen','AdminUserController@getUnseen')->name('getUnseen');
	Route::get('/notifications/all','AdminUserController@notificationsAll');
	Route::get('/notifications/{id}','AdminUserController@notification')->name('notification');
	Route::post('/notifications/{id}/comfirm','AdminUserController@confirmDelivery')->name('confirmDelivery');
	Route::get('/item/addcategory','Admin\ItemController@addCategory');
	Route::get('/item/editcategory','Admin\ItemController@editCategory');
	Route::get('/item/removecategory','Admin\ItemController@removeCategory');
	Route::get('/item/changestatus/{id}','Admin\ItemController@changeStatus');
	Route::get('/item/images/{id}','Admin\ItemController@images');
	Route::post('/item/addimage','Admin\ItemController@addImage');
	Route::post('/item/removeimage','Admin\ItemController@removeImage');
	Route::post('/collection/updateDescription','Admin\ItemController@updateDescription');
	Route::resource('/item','Admin\ItemController');

	Route::get('config/landing','Admin\ConfigController@landingConfig');
	Route::get('config/info','Admin\ConfigController@infoConfig');
	Route::post('config/addfeatured','Admin\ConfigController@addFeatured');
	Route::post('config/removefeatured/{id}','Admin\ConfigController@removeFeatured');
	Route::get('config/removeinfo/{id}','Admin\ConfigController@removeInfo');
	Route::post('config/editinfo','Admin\ConfigController@editInfo');
	Route::post('config/addinfo','Admin\ConfigController@addInfo');

	Route::post('config/addlandingimage','Admin\ConfigController@addLandingImage');
	Route::post('config/removelandingimage','Admin\ConfigController@removeLandingImage');
	Route::get('config/editlandinglink','Admin\ConfigController@editLandingLink');
	
	Route::post('config/addfeaturedimage','Admin\ConfigController@addFeaturedImage');
	Route::post('config/addfirstfeaturedimage','Admin\ConfigController@addFirstFeaturedImage');
	Route::post('config/addsecondfeaturedimage','Admin\ConfigController@addSecondFeaturedImage');
	Route::post('config/addthirdfeaturedimage','Admin\ConfigController@addThirdFeaturedImage');
	Route::post('config/addfirstfeaturedcontent','Admin\ConfigController@addFirstFeaturedContent');
	Route::post('config/addsecondfeaturedcontent','Admin\ConfigController@addSecondFeaturedContent');
	Route::post('config/addthirdfeaturedcontent','Admin\ConfigController@addThirdFeaturedContent');
	Route::post('config/addlandingvideolink','Admin\ConfigController@addLandingVideoLink');
	Route::post('config/addlandingvideoimage','Admin\ConfigController@addLandingVideoImage');
	Route::post('config/addstorycontent','Admin\ConfigController@addStoryContent');
	Route::post('config/addstoryimage','Admin\ConfigController@addStoryImage');
	Route::post('config/addtrendingcontent','Admin\ConfigController@addTrendingContent');
	
	Route::get('config/contact','Admin\ConfigController@contact');
	Route::post('config/editcontact','Admin\ConfigController@editContact');

	Route::resource('serviceCategories','Admin\ServiceCategoryController',['except'=>['create','show','edit','update']]);
	Route::post('/serviceCategories/{id}/edit','Admin\ServiceCategoryController@update')->name('serviceCategories.update');


	Route::resource('review','Admin\ReviewController',['except'=>['create','show','update','edit']]);

	Route::get('social','Admin\SocialController@index');
	Route::post('social/edit/{id}','Admin\SocialController@edit');
	Route::post('social/addsocial','Admin\SocialController@addSocial');
	Route::get('social/removeSocial/{id}','Admin\SocialController@removeSocial');
	Route::post('social/addsocialimage','Admin\SocialController@addSocialImage');
	Route::post('social/removesocialimage','Admin\SocialController@removeSocialImage');
	Route::get('social/addcaption','Admin\SocialController@addCaption');
	Route::post('social/addlinks','Admin\SocialController@addLinks');
	Route::post('social/removelink/{id}','Admin\SocialController@removeLink');

	Route::resource('giftCards','Admin\GiftCardController',['except'=>['show','edit']]);
	Route::post('giftCards/showcode/{id}','Admin\GiftCardController@showCode');
	Route::post('giftCards/viewAll','Admin\GiftCardController@viewAll')->name('viewAll');
	
	Route::get('eCards','AdminUserController@eCards');

	Route::post('giftCards/{id}/changeStatus','Admin\GiftCardController@changeStatus')->name('gc.changeStatus');	

	Route::get('newsletter','Admin\NewsletterController@index')->name('newsletters.index');
	Route::post('newsletter/send','Admin\NewsletterController@send')->name('newsletters.send');

	Route::resource('featuredBrands', 'Admin\FeaturedBrandController',['except'=>['show']]);
	
	
	Route::post('footerBrands/{id}/edit','Admin\FooterBrandController@editCategory');
	Route::resource('footerBrands', 'Admin\FooterBrandController',['except'=>['show']]);
	
	Route::resource('agents', 'Admin\AgentController',['except'=>['show']]);
	Route::get('agents/changestatus/{id}','Admin\AgentController@changeStatus');
	Route::get('agents/{id}/purchases','Admin\AgentController@showPurchases');

	
	Route::get('invitations','Admin\InvitationController@index')->name('invitations.index');
	Route::delete('invitations/{id}/delete','Admin\InvitationController@destroy')->name('invitations.destroy');
	Route::post('invitations/reset','Admin\InvitationController@reset')->name('invitations.reset');
	Route::get('invitations/all','Admin\InvitationController@all')->name('invitations.all');

	Route::resource('agents', 'Admin\AgentController',['except'=>['show']]);
	Route::get('agents/changestatus/{id}','Admin\AgentController@changeStatus');
	Route::get('agents/{id}/purchases','Admin\AgentController@showPurchases');
	
});

