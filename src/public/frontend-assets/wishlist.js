var k=angular.module('wishlistapp',[]);
k.controller('WishlistController',function($scope,$http,$filter){
	$scope.wishlistItems=[];
	$scope.total=0;
	$scope.current = null;

	$scope.init=function(){
		if($('#wishlistitems').val()=='asd') return;
		var k=JSON.parse($('#wishlistitems').val());
		$scope.wishlistitems = $.map(k, function(el) { return el });
		$scope.total=0;
		for(var i=0;i<$scope.wishlistitems.length;i++)
		{
			$scope.total+=$scope.wishlistitems[i]['subtotal'];
		}
	}
	$scope.removeItem=function(rowId){
		$('#xloading'+rowId).removeAttr('hidden');
		$http({
		  method: 'POST',
		  url: '/wishlist/removeitem',
		  data:{rowId:rowId}
		}).then(function successCallback(response) {
		    // $('#emptywishlistloading').attr('hidden','true');
		    if(response.data==null)
		    	$scope.wishlistitems=[];
		    else{
			    // var k=JSON.parse(response.data);
				$scope.wishlistitems = $.map(response.data, function(el) { return el });
				$scope.total=0;
				for(var i=0;i<$scope.wishlistitems.length;i++)
				{
					$scope.total+=$scope.wishlistitems[i]['subtotal'];
				}
				}
				$('#xloading'+rowId).attr('hidden','true');
		  }, function errorCallback(response) {
		  	aalert('Something went wrong','','Oops..');
		  });
	}

	$scope.currency_convert = function(price){
		if(!$scope.current) return '$ '+price;

		return $scope.current.symbol+' '+price;
	}
});


angular.bootstrap(document.getElementById("wishlist"), ['wishlistapp']);