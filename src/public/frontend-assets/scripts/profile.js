var app = angular.module('Profile',[]);

app.controller('HistoryController',function($scope,$http){
	var page = 1;
	$scope.noMore = false;
    $('#style-2-h').scroll(function() {
        if($('#style-2-h').scrollTop() + $('#style-2-h').height() >= $('#history-data').height() && !$scope.noMore) {
            page++;
            loadMoreData(page);
        }
    });

    function loadMoreData(page){
      $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                data : { type : 'History'},
                beforeSend: function()
                {
                    $('#ajax-loadHistory').show();
                }
            })
            .done(function(data)
            {
                if((data.html == "")){
                    $('#ajax-loadHistory').html("No more records found");
                    $('#ajax-loadHistory').attr('style','background-color: rgba(0,0,0,.0001);display:block;');
                    $scope.noMore = true;
                    return;
                }
                $('#ajax-loadHistory').hide();
                $("#history-data").append(data.html);
            });
    }
});

app.controller('ServiceController',function($scope,$http,$compile){
	$scope.noMore = false;
	$scope._service ={};
    $scope.message = '';
    var _page=1;
    var page = 1;
    $scope.addError = false;
    $scope.editError = false;

    $('#style-2-s').scroll(function() {
        if($('#style-2-s').scrollTop() + $('#style-2-s').height() >= $('#service-data').height() && !$scope.noMore) {
            page++;
            loadMoreData(page);
        }
    });

    function loadMoreData(page){
        
      $.ajax(
            {
                url: '?page=' + page,
                type: "get",
                data : { type : 'Service'},
                beforeSend: function()
                {
                    $('#ajax-loadService').show();
                }
            })
            .done(function(data)
            {
                if((data.html == "")){
                    $('#ajax-loadService').html("No more records found");
                    $('#ajax-loadService').attr('style','background-color: rgba(0,0,0,.0001);display:block;');
                    $scope.noMore = true;
                    return;
                }
                $('#ajax-loadService').hide();
                var myEl = angular.element(document.querySelector('#service-data'));
                var $anchor = angular.element(data.html);
                myEl.append($compile($anchor)($scope));
            });
    }

    $scope.add = function(){

        $('#addingPost').html("Adding <img src='/img/loading.gif'  style='max-height:11px;max-width:16px;margin-bottom:2px;' >").prop('disabled',true);
        $scope.addError = false;
    	$http.post('/profile/service/create',{title:$scope._service.title,service_category_id:$scope._service.service_category_id,description:$scope._service.description}).success(function(data){
            $scope._service = {};
    		var myEl = angular.element(document.querySelector('#service-data'));
		    var $anchor = angular.element(data.html);
		    myEl.html($compile($anchor)($scope));
            $('#noservice').hide();
            $('#addPostModal').modal('hide');
            $scope.noMore = false;
            page = 1;
            aalert('Your Service Has Been Added.','','Success');
            $('#addingPost').html("Add Post").prop('disabled',false);

    	}).error(function(data, status){
            if(status == 422)
                $scope.addError = true;
            else
                window.location.reload();
            $('#addingPost').html("Add Post").prop('disabled',false);
        });
        
    }
    
     $scope.edit = function(){
        $('#editingPost').html("Editing <img src='/img/loading.gif'  style='max-height:11px;max-width:16px;margin-bottom:2px;' >").prop('disabled',true);
        $scope.editError = false;
        $http.post('/profile/service/edit',{title:$('#edittitle').val(), service_category_id:$('#eservice_cat').val(), description:$('#editdescription').val(), id:$('#editid').val()}).success(function(data){
            $scope._service = {};
            var myEl = angular.element(document.querySelector('#service-data'));
            var $anchor = angular.element(data.html);
            myEl.empty();
            myEl.append($compile($anchor)($scope));
            $('#editPostModal').modal('hide');
            $scope.noMore = false;
            page = 1;
            aalert('Your Service Has Been Edited.','','Success');
            $('#editingPost').html("Edit Post").prop('disabled',false);        

        }).error(function(data, status){
            if(status == 422)
                $scope.editError = true;
            else
                window.location.reload();
            $('#editingPost').html("Edit Post").prop('disabled',false);        

        });    
        
    }

    var del;
    
    $scope.delete = function(id){
        del = id;
        $('#doIt').removeAttr('hidden');
        aalert('You will not be able to undo it','',"Are you sure?");
    }

    $('#doIt').click(function(){
        if(del){
            closealert();
            $('#cross'+del).html("Deleting").prop('disabled',true);
            $http.post('/profile/service/' + del+'/delete').success(function(data){
                
                var myEl = angular.element(document.querySelector('#service-data'));
                var $anchor = angular.element(data.html);
                myEl.html($compile($anchor)($scope));
                page = 1;
                $scope.noMore = false;
                del = null;
                aalert('Your Service Has Been Deleted.','','Success');
                if ((data.html=="")){
                    $('#ajax-loadService').html('No Services');
                }
            }).error(function(data, status){
                window.location.reload();
            });
        }
    });

});

angular.bootstrap(document.getElementById("profileTabContent"), ['Profile']);