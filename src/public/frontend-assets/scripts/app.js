function detect_old_ie() {
    if (!/MSIE (\d+\.\d+);/.test(navigator.userAgent)) return !1;
    var o = new Number(RegExp.$1);
    return !(o >= 9) && (o >= 8 || (o >= 7 || (o >= 6 || (o >= 5 || void 0))))
}
! function(e) {
    var t, a, n, i, r, s, o = {
            cursorcolor: "255,255,255",
            opacity: .5,
            cursor: "crosshair",
            zindex: 2147483647,
            zoomviewsize: [580, 550],
            zoomviewposition: "right",
            zoomviewmargin: 1,
            zoomviewborder: "none",
            magnification: 2.925
        },
        l = {
            init: function(c) {
                $this = e(this), t = e(".imagezoom-cursor"), a = e(".imagezoom-view"), e(document).on("mouseenter", $this.selector, function(d) {
                    var u = e(this).data();
                    n = e.extend({}, o, c, u), s = e(this).offset(), i = e(this).width(), r = e(this).height(), cursorSize = [n.zoomviewsize[0] / n.magnification, n.zoomviewsize[1] / n.magnification], 1 == u.imagezoom ? imageSrc = e(this).attr("src") : imageSrc = e(this).get(0).getAttribute("data-imagezoom");
                    var v, p = d.pageX,
                        m = d.pageY;
                    e("body").prepend('<div class="imagezoom-cursor">&nbsp;</div><div class="imagezoom-view"><img src="' + imageSrc + '"></div>'), v = "right" == n.zoomviewposition ? s.left + i + n.zoomviewmargin : s.left - i - n.zoomviewmargin, e(a.selector).css({
                        position: "absolute",
                        left: v,
                        top: s.top,
                        width: cursorSize[0] * n.magnification,
                        height: cursorSize[1] * n.magnification,
                        background: "#000",
                        "z-index": 2147483647,
                        overflow: "hidden",
                        border: n.zoomviewborder
                    }), e(a.selector).children("img").css({
                        position: "absolute",
                        width: i * n.magnification,
                        height: r * n.magnification
                    }), e(t.selector).css({
                        position: "absolute",
                        width: cursorSize[0],
                        height: cursorSize[1],
                        "background-color": "rgb(" + n.cursorcolor + ")",
                        "z-index": n.zindex,
                        opacity: n.opacity,
                        cursor: n.cursor
                    }), e(t.selector).css({
                        top: m - cursorSize[1] / 2,
                        left: p
                    }), e(document).on("mousemove", document.body, l.cursorPos)
                })
            },
            cursorPos: function(o) {
                var l = o.pageX,
                    c = o.pageY;
                if (c < s.top || l < s.left || c > s.top + r || l > s.left + i) return e(t.selector).remove(), void e(a.selector).remove();
                l - cursorSize[0] / 2 < s.left ? l = s.left + cursorSize[0] / 2 : l + cursorSize[0] / 2 > s.left + i && (l = s.left + i - cursorSize[0] / 2), c - cursorSize[1] / 2 < s.top ? c = s.top + cursorSize[1] / 2 : c + cursorSize[1] / 2 > s.top + r && (c = s.top + r - cursorSize[1] / 2), e(t.selector).css({
                    top: c - cursorSize[1] / 2,
                    left: l - cursorSize[0] / 2
                }), e(a.selector).children("img").css({
                    top: (s.top - c + cursorSize[1] / 2) * n.magnification,
                    left: (s.left - l + cursorSize[0] / 2) * n.magnification
                }), e(t.selector).mouseleave(function() {
                    e(this).remove()
                })
            }
        };
   
}(jQuery),
function(e) {
    e.flexslider = function(t, a) {
        var n = e(t);
        n.vars = e.extend({}, e.flexslider.defaults, a);
        var i, r = n.vars.namespace,
            s = window.navigator && window.navigator.msPointerEnabled && window.MSGesture,
            o = ("ontouchstart" in window || s || window.DocumentTouch && document instanceof DocumentTouch) && n.vars.touch,
            l = "click touchend MSPointerUp keyup",
            c = "",
            d = "vertical" === n.vars.direction,
            u = n.vars.reverse,
            v = n.vars.itemWidth > 0,
            p = "fade" === n.vars.animation,
            m = "" !== n.vars.asNavFor,
            f = {};
        e.data(t, "flexslider", n), f = {
            init: function() {
                n.animating = !1, n.currentSlide = parseInt(n.vars.startAt ? n.vars.startAt : 0, 10), isNaN(n.currentSlide) && (n.currentSlide = 0), n.animatingTo = n.currentSlide, n.atEnd = 0 === n.currentSlide || n.currentSlide === n.last, n.containerSelector = n.vars.selector.substr(0, n.vars.selector.search(" ")), n.slides = e(n.vars.selector, n), n.container = e(n.containerSelector, n), n.count = n.slides.length, n.syncExists = e(n.vars.sync).length > 0, "slide" === n.vars.animation && (n.vars.animation = "swing"), n.prop = d ? "top" : "marginLeft", n.args = {}, n.manualPause = !1, n.stopped = !1, n.started = !1, n.startTimeout = null, n.transitions = !n.vars.video && !p && n.vars.useCSS && function() {
                    var e = document.createElement("div"),
                        t = ["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                    for (var a in t)
                        if (void 0 !== e.style[t[a]]) return n.pfx = t[a].replace("Perspective", "").toLowerCase(), n.prop = "-" + n.pfx + "-transform", !0;
                    return !1
                }(), n.ensureAnimationEnd = "", "" !== n.vars.controlsContainer && (n.controlsContainer = e(n.vars.controlsContainer).length > 0 && e(n.vars.controlsContainer)), "" !== n.vars.manualControls && (n.manualControls = e(n.vars.manualControls).length > 0 && e(n.vars.manualControls)), "" !== n.vars.customDirectionNav && (n.customDirectionNav = 2 === e(n.vars.customDirectionNav).length && e(n.vars.customDirectionNav)), n.vars.randomize && (n.slides.sort(function() {
                    return Math.round(Math.random()) - .5
                }), n.container.empty().append(n.slides)), n.doMath(), n.setup("init"), n.vars.controlNav && f.controlNav.setup(), n.vars.directionNav && f.directionNav.setup(), n.vars.keyboard && (1 === e(n.containerSelector).length || n.vars.multipleKeyboard) && e(document).bind("keyup", function(e) {
                    var t = e.keyCode;
                    if (!n.animating && (39 === t || 37 === t)) {
                        var a = 39 === t ? n.getTarget("next") : 37 === t && n.getTarget("prev");
                        n.flexAnimate(a, n.vars.pauseOnAction)
                    }
                }), n.vars.mousewheel && n.bind("mousewheel", function(e, t, a, i) {
                    e.preventDefault();
                    var r = t < 0 ? n.getTarget("next") : n.getTarget("prev");
                    n.flexAnimate(r, n.vars.pauseOnAction)
                }), n.vars.pausePlay && f.pausePlay.setup(), n.vars.slideshow && n.vars.pauseInvisible && f.pauseInvisible.init(), n.vars.slideshow && (n.vars.pauseOnHover && n.hover(function() {
                    n.manualPlay || n.manualPause || n.pause()
                }, function() {
                    n.manualPause || n.manualPlay || n.stopped || n.play()
                }), n.vars.pauseInvisible && f.pauseInvisible.isHidden() || (n.vars.initDelay > 0 ? n.startTimeout = setTimeout(n.play, n.vars.initDelay) : n.play())), m && f.asNav.setup(), o && n.vars.touch && f.touch(), (!p || p && n.vars.smoothHeight) && e(window).bind("resize orientationchange focus", f.resize), n.find("img").attr("draggable", "false"), setTimeout(function() {
                    n.vars.start(n)
                }, 200)
            },
            asNav: {
                setup: function() {
                    n.asNav = !0, n.animatingTo = Math.floor(n.currentSlide / n.move), n.currentItem = n.currentSlide, n.slides.removeClass(r + "active-slide").eq(n.currentItem).addClass(r + "active-slide"), s ? (t._slider = n, n.slides.each(function() {
                        var t = this;
                        t._gesture = new MSGesture, t._gesture.target = t, t.addEventListener("MSPointerDown", function(e) {
                            e.preventDefault(), e.currentTarget._gesture && e.currentTarget._gesture.addPointer(e.pointerId)
                        }, !1), t.addEventListener("MSGestureTap", function(t) {
                            t.preventDefault();
                            var a = e(this),
                                i = a.index();
                            e(n.vars.asNavFor).data("flexslider").animating || a.hasClass("active") || (n.direction = n.currentItem < i ? "next" : "prev", n.flexAnimate(i, n.vars.pauseOnAction, !1, !0, !0))
                        })
                    })) : n.slides.on(l, function(t) {
                        t.preventDefault();
                        var a = e(this),
                            i = a.index();
                        a.offset().left - e(n).scrollLeft() <= 0 && a.hasClass(r + "active-slide") ? n.flexAnimate(n.getTarget("prev"), !0) : e(n.vars.asNavFor).data("flexslider").animating || a.hasClass(r + "active-slide") || (n.direction = n.currentItem < i ? "next" : "prev", n.flexAnimate(i, n.vars.pauseOnAction, !1, !0, !0))
                    })
                }
            },
            controlNav: {
                setup: function() {
                    n.manualControls ? f.controlNav.setupManual() : f.controlNav.setupPaging()
                },
                setupPaging: function() {
                    
                    var t, a, i = "thumbnails" === n.vars.controlNav ? "control-thumbs" : "control-paging",
                        s = 1;
                    if (n.controlNavScaffold = e('<ol id="flexnav" style="opacity:0" class="' + r + "control-nav " + r + i + '"></ol>'), n.pagingCount >= 0)
                        for (var o = 0; o < n.pagingCount; o++) {
                            if (a = n.slides.eq(o), t = "thumbnails" === n.vars.controlNav ? '<img src="' + a.attr("data-thumb") + '"/>' : "<a>" + s + "</a>", "thumbnails" === n.vars.controlNav && !0 === n.vars.thumbCaptions) {
                                var d = a.attr("data-thumbcaption");
                                "" !== d && void 0 !== d && (t += '<span class="' + r + 'caption">' + d + "</span>")
                            }
                            n.controlNavScaffold.append("<li>" + t + "</li>"), s++
                        }
                        
                    n.controlsContainer ? e(n.controlsContainer).append(n.controlNavScaffold) : n.append(n.controlNavScaffold), f.controlNav.set(), f.controlNav.active(), n.controlNavScaffold.delegate("a, img", l, function(t) {
                        if (t.preventDefault(), "" === c || c === t.type) {
                            var a = e(this),
                                i = n.controlNav.index(a);
                            a.hasClass(r + "active") || (n.direction = i > n.currentSlide ? "next" : "prev", n.flexAnimate(i, n.vars.pauseOnAction))
                        }
                        "" === c && (c = t.type), f.setToClearWatchedEvent()
                    })
                    
                },
                setupManual: function() {
                    n.controlNav = n.manualControls, f.controlNav.active(), n.controlNav.bind(l, function(t) {
                        if (t.preventDefault(), "" === c || c === t.type) {
                            var a = e(this),
                                i = n.controlNav.index(a);
                            a.hasClass(r + "active") || (i > n.currentSlide ? n.direction = "next" : n.direction = "prev", n.flexAnimate(i, n.vars.pauseOnAction))
                        }
                        "" === c && (c = t.type), f.setToClearWatchedEvent()
                    })
                },
                set: function() {
                    var t = "thumbnails" === n.vars.controlNav ? "img" : "a";
                    n.controlNav = e("." + r + "control-nav li " + t, n.controlsContainer ? n.controlsContainer : n)
                },
                active: function() {
                    n.controlNav.removeClass(r + "active").eq(n.animatingTo).addClass(r + "active")
                },
                update: function(t, a) {
                    n.pagingCount > 1 && "add" === t ? n.controlNavScaffold.append(e("<li><a>" + n.count + "</a></li>")) : 1 === n.pagingCount ? n.controlNavScaffold.find("li").remove() : n.controlNav.eq(a).closest("li").remove(), f.controlNav.set(), n.pagingCount > 1 && n.pagingCount !== n.controlNav.length ? n.update(a, t) : f.controlNav.active()
                }
            },
            directionNav: {
                setup: function() {
                    var t = e('<ul class="' + r + 'direction-nav"><li class="' + r + 'nav-prev"><a class="' + r + 'prev" href="#">' + n.vars.prevText + '</a></li><li class="' + r + 'nav-next"><a class="' + r + 'next" href="#">' + n.vars.nextText + "</a></li></ul>");
                    n.customDirectionNav ? n.directionNav = n.customDirectionNav : n.controlsContainer ? (e(n.controlsContainer).append(t), n.directionNav = e("." + r + "direction-nav li a", n.controlsContainer)) : (n.append(t), n.directionNav = e("." + r + "direction-nav li a", n)), f.directionNav.update(), n.directionNav.bind(l, function(t) {
                        t.preventDefault();
                        var a;
                        "" !== c && c !== t.type || (a = e(this).hasClass(r + "next") ? n.getTarget("next") : n.getTarget("prev"), n.flexAnimate(a, n.vars.pauseOnAction)), "" === c && (c = t.type), f.setToClearWatchedEvent()
                    })
                },
                update: function() {
                    var e = r + "disabled";
                    1 === n.pagingCount ? n.directionNav.addClass(e).attr("tabindex", "-1") : n.vars.animationLoop ? n.directionNav.removeClass(e).removeAttr("tabindex") : 0 === n.animatingTo ? n.directionNav.removeClass(e).filter("." + r + "prev").addClass(e).attr("tabindex", "-1") : n.animatingTo === n.last ? n.directionNav.removeClass(e).filter("." + r + "next").addClass(e).attr("tabindex", "-1") : n.directionNav.removeClass(e).removeAttr("tabindex")
                }
            },
            pausePlay: {
                setup: function() {
                    var t = e('<div class="' + r + 'pauseplay"><a></a></div>');
                    n.controlsContainer ? (n.controlsContainer.append(t), n.pausePlay = e("." + r + "pauseplay a", n.controlsContainer)) : (n.append(t), n.pausePlay = e("." + r + "pauseplay a", n)), f.pausePlay.update(n.vars.slideshow ? r + "pause" : r + "play"), n.pausePlay.bind(l, function(t) {
                        t.preventDefault(), "" !== c && c !== t.type || (e(this).hasClass(r + "pause") ? (n.manualPause = !0, n.manualPlay = !1, n.pause()) : (n.manualPause = !1, n.manualPlay = !0, n.play())), "" === c && (c = t.type), f.setToClearWatchedEvent()
                    })
                },
                update: function(e) {
                    "play" === e ? n.pausePlay.removeClass(r + "pause").addClass(r + "play").html(n.vars.playText) : n.pausePlay.removeClass(r + "play").addClass(r + "pause").html(n.vars.pauseText)
                }
            },
            touch: function() {
                function e(e) {
                    e.stopPropagation(), n.animating ? e.preventDefault() : (n.pause(), t._gesture.addPointer(e.pointerId), x = 0, c = d ? n.h : n.w, f = Number(new Date), l = v && u && n.animatingTo === n.last ? 0 : v && u ? n.limit - (n.itemW + n.vars.itemMargin) * n.move * n.animatingTo : v && n.currentSlide === n.last ? n.limit : v ? (n.itemW + n.vars.itemMargin) * n.move * n.currentSlide : u ? (n.last - n.currentSlide + n.cloneOffset) * c : (n.currentSlide + n.cloneOffset) * c)
                }

                function a(e) {
                    e.stopPropagation();
                    var a = e.target._slider;
                    if (a) {
                        var n = -e.translationX,
                            i = -e.translationY;
                        if (x += d ? i : n, m = x, y = d ? Math.abs(x) < Math.abs(-n) : Math.abs(x) < Math.abs(-i), e.detail === e.MSGESTURE_FLAG_INERTIA) return void setImmediate(function() {
                            t._gesture.stop()
                        });
                        (!y || Number(new Date) - f > 500) && (e.preventDefault(), !p && a.transitions && (a.vars.animationLoop || (m = x / (0 === a.currentSlide && x < 0 || a.currentSlide === a.last && x > 0 ? Math.abs(x) / c + 2 : 1)), a.setProps(l + m, "setTouch")))
                    }
                }

                function i(e) {
                    e.stopPropagation();
                    var t = e.target._slider;
                    if (t) {
                        if (t.animatingTo === t.currentSlide && !y && null !== m) {
                            var a = u ? -m : m,
                                n = a > 0 ? t.getTarget("next") : t.getTarget("prev");
                            t.canAdvance(n) && (Number(new Date) - f < 550 && Math.abs(a) > 50 || Math.abs(a) > c / 2) ? t.flexAnimate(n, t.vars.pauseOnAction) : p || t.flexAnimate(t.currentSlide, t.vars.pauseOnAction, !0)
                        }
                        r = null, o = null, m = null, l = null, x = 0
                    }
                }
                var r, o, l, c, m, f, g, h, w, y = !1,
                    S = 0,
                    b = 0,
                    x = 0;
                s ? (t.style.msTouchAction = "none", t._gesture = new MSGesture, t._gesture.target = t, t.addEventListener("MSPointerDown", e, !1), t._slider = n, t.addEventListener("MSGestureChange", a, !1), t.addEventListener("MSGestureEnd", i, !1)) : (g = function(e) {
                    n.animating ? e.preventDefault() : (window.navigator.msPointerEnabled || 1 === e.touches.length) && (n.pause(), c = d ? n.h : n.w, f = Number(new Date), S = e.touches[0].pageX, b = e.touches[0].pageY, l = v && u && n.animatingTo === n.last ? 0 : v && u ? n.limit - (n.itemW + n.vars.itemMargin) * n.move * n.animatingTo : v && n.currentSlide === n.last ? n.limit : v ? (n.itemW + n.vars.itemMargin) * n.move * n.currentSlide : u ? (n.last - n.currentSlide + n.cloneOffset) * c : (n.currentSlide + n.cloneOffset) * c, r = d ? b : S, o = d ? S : b, t.addEventListener("touchmove", h, !1), t.addEventListener("touchend", w, !1))
                }, h = function(e) {
                    S = e.touches[0].pageX, b = e.touches[0].pageY, m = d ? r - b : r - S, y = d ? Math.abs(m) < Math.abs(S - o) : Math.abs(m) < Math.abs(b - o);
                    (!y || Number(new Date) - f > 500) && (e.preventDefault(), !p && n.transitions && (n.vars.animationLoop || (m /= 0 === n.currentSlide && m < 0 || n.currentSlide === n.last && m > 0 ? Math.abs(m) / c + 2 : 1), n.setProps(l + m, "setTouch")))
                }, w = function(e) {
                    if (t.removeEventListener("touchmove", h, !1), n.animatingTo === n.currentSlide && !y && null !== m) {
                        var a = u ? -m : m,
                            i = a > 0 ? n.getTarget("next") : n.getTarget("prev");
                        n.canAdvance(i) && (Number(new Date) - f < 550 && Math.abs(a) > 50 || Math.abs(a) > c / 2) ? n.flexAnimate(i, n.vars.pauseOnAction) : p || n.flexAnimate(n.currentSlide, n.vars.pauseOnAction, !0)
                    }
                    t.removeEventListener("touchend", w, !1), r = null, o = null, m = null, l = null
                }, t.addEventListener("touchstart", g, !1))
            },
            resize: function() {
                !n.animating && n.is(":visible") && (v || n.doMath(), p ? f.smoothHeight() : v ? (n.slides.width(n.computedW), n.update(n.pagingCount), n.setProps()) : d ? (n.viewport.height(n.h), n.setProps(n.h, "setTotal")) : (n.vars.smoothHeight && f.smoothHeight(), n.newSlides.width(n.computedW), n.setProps(n.computedW, "setTotal")))
            },
            smoothHeight: function(e) {
                if (!d || p) {
                    var t = p ? n : n.viewport;
                    e ? t.animate({
                        height: n.slides.eq(n.animatingTo).height()
                    }, e) : t.height(n.slides.eq(n.animatingTo).height())
                }
            },
            sync: function(t) {
                var a = e(n.vars.sync).data("flexslider"),
                    i = n.animatingTo;
                switch (t) {
                    case "animate":
                        a.flexAnimate(i, n.vars.pauseOnAction, !1, !0);
                        break;
                    case "play":
                        a.playing || a.asNav || a.play();
                        break;
                    case "pause":
                        a.pause()
                }
            },
            uniqueID: function(t) {
                return t.filter("[id]").add(t.find("[id]")).each(function() {
                    var t = e(this);
                    t.attr("id", t.attr("id") + "_clone")
                }), t
            },
            pauseInvisible: {
                visProp: null,
                init: function() {
                    var e = f.pauseInvisible.getHiddenProp();
                    if (e) {
                        var t = e.replace(/[H|h]idden/, "") + "visibilitychange";
                        document.addEventListener(t, function() {
                            f.pauseInvisible.isHidden() ? n.startTimeout ? clearTimeout(n.startTimeout) : n.pause() : n.started ? n.play() : n.vars.initDelay > 0 ? setTimeout(n.play, n.vars.initDelay) : n.play()
                        })
                    }
                },
                isHidden: function() {
                    var e = f.pauseInvisible.getHiddenProp();
                    return !!e && document[e]
                },
                getHiddenProp: function() {
                    var e = ["webkit", "moz", "ms", "o"];
                    if ("hidden" in document) return "hidden";
                    for (var t = 0; t < e.length; t++)
                        if (e[t] + "Hidden" in document) return e[t] + "Hidden";
                    return null
                }
            },
            setToClearWatchedEvent: function() {
                clearTimeout(i), i = setTimeout(function() {
                    c = ""
                }, 3e3)
            }
        }, n.flexAnimate = function(t, a, i, s, l) {
            if (n.vars.animationLoop || t === n.currentSlide || (n.direction = t > n.currentSlide ? "next" : "prev"), m && 1 === n.pagingCount && (n.direction = n.currentItem < t ? "next" : "prev"), !n.animating && (n.canAdvance(t, l) || i) && n.is(":visible")) {
                if (m && s) {
                    var c = e(n.vars.asNavFor).data("flexslider");
                    if (n.atEnd = 0 === t || t === n.count - 1, c.flexAnimate(t, !0, !1, !0, l), n.direction = n.currentItem < t ? "next" : "prev", c.direction = n.direction, Math.ceil((t + 1) / n.visible) - 1 === n.currentSlide || 0 === t) return n.currentItem = t, n.slides.removeClass(r + "active-slide").eq(t).addClass(r + "active-slide"), !1;
                    n.currentItem = t, n.slides.removeClass(r + "active-slide").eq(t).addClass(r + "active-slide"), t = Math.floor(t / n.visible)
                }
                if (n.animating = !0, n.animatingTo = t, a && n.pause(), n.vars.before(n), n.syncExists && !l && f.sync("animate"), n.vars.controlNav && f.controlNav.active(), v || n.slides.removeClass(r + "active-slide").eq(t).addClass(r + "active-slide"), n.atEnd = 0 === t || t === n.last, n.vars.directionNav && f.directionNav.update(), t === n.last && (n.vars.end(n), n.vars.animationLoop || n.pause()), p) o ? (n.slides.eq(n.currentSlide).css({
                    opacity: 0,
                    zIndex: 1
                }), n.slides.eq(t).css({
                    opacity: 1,
                    zIndex: 2
                }), n.wrapup(y)) : (n.slides.eq(n.currentSlide).css({
                    zIndex: 1
                }).animate({
                    opacity: 0
                }, n.vars.animationSpeed, n.vars.easing), n.slides.eq(t).css({
                    zIndex: 2
                }).animate({
                    opacity: 1
                }, n.vars.animationSpeed, n.vars.easing, n.wrapup));
                else {
                    var g, h, w, y = d ? n.slides.filter(":first").height() : n.computedW;
                    v ? (g = n.vars.itemMargin, w = (n.itemW + g) * n.move * n.animatingTo, h = w > n.limit && 1 !== n.visible ? n.limit : w) : h = 0 === n.currentSlide && t === n.count - 1 && n.vars.animationLoop && "next" !== n.direction ? u ? (n.count + n.cloneOffset) * y : 0 : n.currentSlide === n.last && 0 === t && n.vars.animationLoop && "prev" !== n.direction ? u ? 0 : (n.count + 1) * y : u ? (n.count - 1 - t + n.cloneOffset) * y : (t + n.cloneOffset) * y, n.setProps(h, "", n.vars.animationSpeed), n.transitions ? (n.vars.animationLoop && n.atEnd || (n.animating = !1, n.currentSlide = n.animatingTo), n.container.unbind("webkitTransitionEnd transitionend"), n.container.bind("webkitTransitionEnd transitionend", function() {
                        clearTimeout(n.ensureAnimationEnd), n.wrapup(y)
                    }), clearTimeout(n.ensureAnimationEnd), n.ensureAnimationEnd = setTimeout(function() {
                        n.wrapup(y)
                    }, n.vars.animationSpeed + 100)) : n.container.animate(n.args, n.vars.animationSpeed, n.vars.easing, function() {
                        n.wrapup(y)
                    })
                }
                n.vars.smoothHeight && f.smoothHeight(n.vars.animationSpeed)
            }
        }, n.wrapup = function(e) {
            p || v || (0 === n.currentSlide && n.animatingTo === n.last && n.vars.animationLoop ? n.setProps(e, "jumpEnd") : n.currentSlide === n.last && 0 === n.animatingTo && n.vars.animationLoop && n.setProps(e, "jumpStart")), n.animating = !1, n.currentSlide = n.animatingTo, n.vars.after(n)
        }, n.animateSlides = function() {
            n.animating || n.flexAnimate(n.getTarget("next"))
        }, n.pause = function() {
            clearInterval(n.animatedSlides), n.animatedSlides = null, n.playing = !1, n.vars.pausePlay && f.pausePlay.update("play"), n.syncExists && f.sync("pause")
        }, n.play = function() {
            n.playing && clearInterval(n.animatedSlides), n.animatedSlides = n.animatedSlides || setInterval(n.animateSlides, n.vars.slideshowSpeed), n.started = n.playing = !0, n.vars.pausePlay && f.pausePlay.update("pause"), n.syncExists && f.sync("play")
        }, n.stop = function() {
            n.pause(), n.stopped = !0
        }, n.canAdvance = function(e, t) {
            var a = m ? n.pagingCount - 1 : n.last;
            return !!t || (!(!m || n.currentItem !== n.count - 1 || 0 !== e || "prev" !== n.direction) || (!m || 0 !== n.currentItem || e !== n.pagingCount - 1 || "next" === n.direction) && (!(e === n.currentSlide && !m) && (!!n.vars.animationLoop || (!n.atEnd || 0 !== n.currentSlide || e !== a || "next" === n.direction) && (!n.atEnd || n.currentSlide !== a || 0 !== e || "next" !== n.direction))))
        }, n.getTarget = function(e) {
            return n.direction = e, "next" === e ? n.currentSlide === n.last ? 0 : n.currentSlide + 1 : 0 === n.currentSlide ? n.last : n.currentSlide - 1
        }, n.setProps = function(e, t, a) {
            var i = function() {
                var a = e || (n.itemW + n.vars.itemMargin) * n.move * n.animatingTo;
                return -1 * function() {
                    if (v) return "setTouch" === t ? e : u && n.animatingTo === n.last ? 0 : u ? n.limit - (n.itemW + n.vars.itemMargin) * n.move * n.animatingTo : n.animatingTo === n.last ? n.limit : a;
                    switch (t) {
                        case "setTotal":
                            return u ? (n.count - 1 - n.currentSlide + n.cloneOffset) * e : (n.currentSlide + n.cloneOffset) * e;
                        case "setTouch":
                            return e;
                        case "jumpEnd":
                            return u ? e : n.count * e;
                        case "jumpStart":
                            return u ? n.count * e : e;
                        default:
                            return e
                    }
                }() + "px"
            }();
            n.transitions && (i = d ? "translate3d(0," + i + ",0)" : "translate3d(" + i + ",0,0)", a = void 0 !== a ? a / 1e3 + "s" : "0s", n.container.css("-" + n.pfx + "-transition-duration", a), n.container.css("transition-duration", a)), n.args[n.prop] = i, (n.transitions || void 0 === a) && n.container.css(n.args), n.container.css("transform", i)
        }, n.setup = function(t) {
            if (p) n.slides.css({
                width: "100%",
                float: "left",
                marginRight: "-100%",
                position: "relative"
            }), "init" === t && (o ? n.slides.css({
                opacity: 0,
                display: "block",
                webkitTransition: "opacity " + n.vars.animationSpeed / 1e3 + "s ease",
                zIndex: 1
            }).eq(n.currentSlide).css({
                opacity: 1,
                zIndex: 2
            }) : 0 == n.vars.fadeFirstSlide ? n.slides.css({
                opacity: 0,
                display: "block",
                zIndex: 1
            }).eq(n.currentSlide).css({
                zIndex: 2
            }).css({
                opacity: 1
            }) : n.slides.css({
                opacity: 0,
                display: "block",
                zIndex: 1
            }).eq(n.currentSlide).css({
                zIndex: 2
            }).animate({
                opacity: 1
            }, n.vars.animationSpeed, n.vars.easing)), n.vars.smoothHeight && f.smoothHeight();
            else {
                var a, i;
                "init" === t && (n.viewport = e('<div class="' + r + 'viewport"></div>').css({
                    overflow: "hidden",
                    position: "relative"
                }).appendTo(n).append(n.container), n.cloneCount = 0, n.cloneOffset = 0, u && (i = e.makeArray(n.slides).reverse(), n.slides = e(i), n.container.empty().append(n.slides))), n.vars.animationLoop && !v && (n.cloneCount = 2, n.cloneOffset = 1, "init" !== t && n.container.find(".clone").remove(), n.container.append(f.uniqueID(n.slides.first().clone().addClass("clone")).attr("aria-hidden", "true")).prepend(f.uniqueID(n.slides.last().clone().addClass("clone")).attr("aria-hidden", "true"))), n.newSlides = e(n.vars.selector, n), a = u ? n.count - 1 - n.currentSlide + n.cloneOffset : n.currentSlide + n.cloneOffset, d && !v ? (n.container.height(200 * (n.count + n.cloneCount) + "%").css("position", "absolute").width("100%"), setTimeout(function() {
                    n.newSlides.css({
                        display: "block"
                    }), n.doMath(), n.viewport.height(n.h), n.setProps(a * n.h, "init")
                }, "init" === t ? 100 : 0)) : (n.container.width(200 * (n.count + n.cloneCount) + "%"), n.setProps(a * n.computedW, "init"), setTimeout(function() {
                    n.doMath(), n.newSlides.css({
                        width: n.computedW,
                        float: "left",
                        display: "block"
                    }), n.vars.smoothHeight && f.smoothHeight()
                }, "init" === t ? 100 : 0))
            }
            v || n.slides.removeClass(r + "active-slide").eq(n.currentSlide).addClass(r + "active-slide"), n.vars.init(n)
        }, n.doMath = function() {
            var e = n.slides.first(),
                t = n.vars.itemMargin,
                a = n.vars.minItems,
                i = n.vars.maxItems;
            n.w = void 0 === n.viewport ? n.width() : n.viewport.width(), n.h = e.height(), n.boxPadding = e.outerWidth() - e.width(), v ? (n.itemT = n.vars.itemWidth + t, n.minW = a ? a * n.itemT : n.w, n.maxW = i ? i * n.itemT - t : n.w, n.itemW = n.minW > n.w ? (n.w - t * (a - 1)) / a : n.maxW < n.w ? (n.w - t * (i - 1)) / i : n.vars.itemWidth > n.w ? n.w : n.vars.itemWidth, n.visible = Math.floor(n.w / n.itemW), n.move = n.vars.move > 0 && n.vars.move < n.visible ? n.vars.move : n.visible, n.pagingCount = Math.ceil((n.count - n.visible) / n.move + 1), n.last = n.pagingCount - 1, n.limit = 1 === n.pagingCount ? 0 : n.vars.itemWidth > n.w ? n.itemW * (n.count - 1) + t * (n.count - 1) : (n.itemW + t) * n.count - n.w - t) : (n.itemW = n.w, n.pagingCount = n.count, n.last = n.count - 1), n.computedW = n.itemW - n.boxPadding
        }, n.update = function(e, t) {
            n.doMath(), v || (e < n.currentSlide ? n.currentSlide += 1 : e <= n.currentSlide && 0 !== e && (n.currentSlide -= 1), n.animatingTo = n.currentSlide), n.vars.controlNav && !n.manualControls && ("add" === t && !v || n.pagingCount > n.controlNav.length ? f.controlNav.update("add") : ("remove" === t && !v || n.pagingCount < n.controlNav.length) && (v && n.currentSlide > n.last && (n.currentSlide -= 1, n.animatingTo -= 1), f.controlNav.update("remove", n.last))), n.vars.directionNav && f.directionNav.update()
        }, n.addSlide = function(t, a) {
            var i = e(t);
            n.count += 1, n.last = n.count - 1, d && u ? void 0 !== a ? n.slides.eq(n.count - a).after(i) : n.container.prepend(i) : void 0 !== a ? n.slides.eq(a).before(i) : n.container.append(i), n.update(a, "add"), n.slides = e(n.vars.selector + ":not(.clone)", n), n.setup(), n.vars.added(n)
        }, n.removeSlide = function(t) {
            var a = isNaN(t) ? n.slides.index(e(t)) : t;
            n.count -= 1, n.last = n.count - 1, isNaN(t) ? e(t, n.slides).remove() : d && u ? n.slides.eq(n.last).remove() : n.slides.eq(t).remove(), n.doMath(), n.update(a, "remove"), n.slides = e(n.vars.selector + ":not(.clone)", n), n.setup(), n.vars.removed(n)
        }, f.init()
    }, e(window).blur(function(e) {
        focused = !1
    }).focus(function(e) {
        focused = !0
    }), e.flexslider.defaults = {
        namespace: "flex-",
        selector: ".slides > li",
        animation: "fade",
        easing: "swing",
        direction: "horizontal",
        reverse: !1,
        animationLoop: !0,
        smoothHeight: !1,
        startAt: 0,
        slideshow: !0,
        slideshowSpeed: 7e3,
        animationSpeed: 600,
        initDelay: 0,
        randomize: !1,
        fadeFirstSlide: !0,
        thumbCaptions: !1,
        pauseOnAction: !0,
        pauseOnHover: !1,
        pauseInvisible: !0,
        useCSS: !0,
        touch: !0,
        video: !1,
        controlNav: !0,
        directionNav: !0,
        prevText: "Previous",
        nextText: "Next",
        keyboard: !0,
        multipleKeyboard: !1,
        mousewheel: !1,
        pausePlay: !1,
        pauseText: "Pause",
        playText: "Play",
        controlsContainer: "",
        manualControls: "",
        customDirectionNav: "",
        sync: "",
        asNavFor: "",
        itemWidth: 0,
        itemMargin: 0,
        minItems: 1,
        maxItems: 0,
        move: 0,
        allowOneSlide: !0,
        start: function() {},
        before: function() {},
        after: function() {},
        end: function() {},
        added: function() {},
        removed: function() {},
        init: function() {}
    }, e.fn.flexslider = function(t) {
        if (void 0 === t && (t = {}), "object" == typeof t) return this.each(function() {
            var a = e(this),
                n = t.selector ? t.selector : ".slides > li",
                i = a.find(n);
            1 === i.length && !0 === t.allowOneSlide || 0 === i.length ? (i.fadeIn(400), t.start && t.start(a)) : void 0 === a.data("flexslider") && new e.flexslider(this, t)
        });
        var a = e(this).data("flexslider");
        switch (t) {
            case "play":
                a.play();
                break;
            case "pause":
                a.pause();
                break;
            case "stop":
                a.stop();
                break;
            case "next":
                a.flexAnimate(a.getTarget("next"), !0);
                break;
            case "prev":
            case "previous":
                a.flexAnimate(a.getTarget("prev"), !0);
                break;
            default:
                "number" == typeof t && a.flexAnimate(t, !0)
        }
    }
}(jQuery), 
    

    $(document).ready(function() {
        $(".drawer-left").drawer(), $(".drawer-cart").drawer({
            class: {
                nav: "drawer-nav-cart", toggle: "drawer-toggle-cart"
            },
            iscroll: {
                mouseWheel: !0,
                preventDefault: !1
            }
        }), $(".drawerclose").click(function() {
            $(".drawer-left").drawer("close")
        }), $(".closecart").click(function() {
            $(".drawer-cart").drawer("close")
        }), $(".navToggle").click(function() {
                $('.navToggle').not(this).each(function(){
                    $(this).parent().siblings(".sub-menu").slideUp("fast");
                    $(this).children('i.fa.fa-minus').attr('class',"fa fa-chevron-down");
                });
                $(this).parent().siblings(".sub-menu").slideToggle("fast");
                if($(this).find('i').attr('class')=='fa fa-chevron-down')
                    $(this).find('i').attr('class',"fa fa-minus");
                else if($(this).find('i').attr('class')=='fa fa-minus')
                    $(this).find('i').attr('class',"fa fa-chevron-down");
        }), $(".carousel").carousel({
            interval: 4e3,
            pause: !1
        }), $(".trending .product").mouseenter(function() {
            $(this).children(".product-overlay").css({
                visibility: "visible"
            }), $(this).children(".product-image").css({
                opacity: .5
            })
        }), $(".trending .product").mouseleave(function() {
            $(".trending .product .product-overlay").css({
                visibility: "hidden"
            }), $(".trending .product .product-image").css({
                opacity: 1
            })
        }), $(window).scroll(function() {
            $(window).scrollTop() > 60 ? $("#mainNav").css("background", "rgba(70, 65, 65, 0.35)") : $("#mainNav").css("background", "transparent")
        }), $("#searchBtn").click(function() {
            $("#searchInput").is(":visible") || $(".searchBox").slideDown("fast");
        }), $("#closeSearch").click(function() {
            $("#searchInput").is(":visible") && $(".searchBox").slideUp("fast")
        }), $(".xzoom").xzoom()
    }), window.requestAnimFrame = function() {
        return window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(o) {
            window.setTimeout(o, 20)
        }
    }(),
    function(o) {
        function t(t, e) {
            function i() {
                var o = document.documentElement;
                return {
                    left: (window.pageXOffset || o.scrollLeft) - (o.clientLeft || 0),
                    top: (window.pageYOffset || o.scrollTop) - (o.clientTop || 0)
                }
            }

            function s() {
                var e = t.offset();
                if (g = "auto" == co.options.zoomWidth ? ao : co.options.zoomWidth, m = "auto" == co.options.zoomHeight ? po : co.options.zoomHeight, "#" == co.options.position.substr(0, 1) ? ho = o(co.options.position) : ho.length = 0, 0 != ho.length) return !0;
                switch (ro) {
                    case "lens":
                    case "inside":
                        return !0;
                    case "top":
                        x = e.top, b = e.left, z = x - m, y = b;
                        break;
                    case "left":
                        x = e.top, b = e.left, z = x, y = b - g;
                        break;
                    case "bottom":
                        x = e.top, b = e.left, z = x + po, y = b;
                        break;
                    case "right":
                    default:
                        x = e.top, b = e.left, z = x, y = b + ao
                }
                return !(y + g > eo || y < 0)
            }

            function n() {
                if ("circle" == co.options.lensShape && "lens" == co.options.position) {
                    R = q = Math.max(R, q);
                    var o = (R + 2 * Math.max(Z, L)) / 2;
                    X.css({
                        "-moz-border-radius": o,
                        "-webkit-border-radius": o,
                        "border-radius": o
                    })
                }
            }

            function a(o, t, e, i) {
                "lens" == co.options.position ? (F.css({
                    top: -(t - x) * B + q / 2,
                    left: -(o - b) * _ + R / 2
                }), co.options.bg && (X.css({
                    "background-image": "url(" + F.attr("src") + ")",
                    "background-repeat": "no-repeat",
                    "background-position": -(o - b) * _ + R / 2 + "px " + (-(t - x) * B + q / 2) + "px"
                }), e && i && X.css({
                    "background-size": e + "px " + i + "px"
                }))) : F.css({
                    top: -D * B,
                    left: -E * _
                })
            }

            function p(o, t) {
                if (go < -1 && (go = -1), go > 1 && (go = 1), N < j) var e = N - (N - 1) * go,
                    i = g * e,
                    s = i / Q;
                else var e = j - (j - 1) * go,
                    s = m * e,
                    i = s * Q;
                J ? (mo = o, wo = t, xo = i, bo = s) : (J || (zo = xo = i, yo = bo = s), _ = i / u, B = s / v, R = g / _, q = m / B, n(), l(o, t), F.width(i), F.height(s), X.width(R), X.height(q), X.css({
                    top: D - Z,
                    left: E - L
                }), Y.css({
                    top: -D,
                    left: -E
                }), a(o, t, i, s))
            }

            function r() {
                var o = ko,
                    t = Co,
                    e = $o,
                    i = Oo,
                    s = zo,
                    p = yo;
                o += (mo - o) / co.options.smoothLensMove, t += (wo - t) / co.options.smoothLensMove, e += (mo - e) / co.options.smoothZoomMove, i += (wo - i) / co.options.smoothZoomMove, s += (xo - s) / co.options.smoothScale, p += (bo - p) / co.options.smoothScale, _ = s / u, B = p / v, R = g / _, q = m / B, n(), l(o, t), F.width(s), F.height(p), X.width(R), X.height(q), X.css({
                    top: D - Z,
                    left: E - L
                }), Y.css({
                    top: -D,
                    left: -E
                }), l(e, i), a(o, t, s, p), ko = o, Co = t, $o = e, Oo = i, zo = s, yo = p, J && requestAnimFrame(r)
            }

            function l(o, t) {
                o -= b, t -= x, E = o - R / 2, D = t - q / 2, "lens" != co.options.position && (E < 0 && (E = 0), E > u - R && (E = u - R), D < 0 && (D = 0), D > v - q && (D = v - q))
            }

            function c() {
                void 0 !== M && M.remove(), void 0 !== I && I.remove(), void 0 !== to && to.remove()
            }

            function d(e, s) {
                switch ("fullscreen" == co.options.position ? (u = o(window).width(), v = o(window).height()) : (u = t.width(), v = t.height()), T.css({
                    top: v / 2 - T.height() / 2,
                    left: u / 2 - T.width() / 2
                }), w = co.options.rootOutput || "fullscreen" == co.options.position ? t.offset() : t.position(), w.top = Math.round(w.top), w.left = Math.round(w.left), co.options.position) {
                    case "fullscreen":
                        x = i().top, b = i().left, z = 0, y = 0;
                        break;
                    case "inside":
                        x = w.top, b = w.left, z = 0, y = 0;
                        break;
                    case "top":
                        x = w.top, b = w.left, z = x - m, y = b;
                        break;
                    case "left":
                        x = w.top, b = w.left, z = x, y = b - g;
                        break;
                    case "bottom":
                        x = w.top, b = w.left, z = x + v, y = b;
                        break;
                    case "right":
                    default:
                        x = w.top, b = w.left, z = x, y = b + u
                }
                x -= M.outerHeight() / 2, b -= M.outerWidth() / 2, "#" == co.options.position.substr(0, 1) ? ho = o(co.options.position) : ho.length = 0, 0 == ho.length && "inside" != co.options.position && "fullscreen" != co.options.position ? (co.options.adaptive && so && no || (so = u, no = v), g = "auto" == co.options.zoomWidth ? u : co.options.zoomWidth, m = "auto" == co.options.zoomHeight ? v : co.options.zoomHeight, z += co.options.Yoffset, y += co.options.Xoffset, I.css({
                    width: g + "px",
                    height: m + "px",
                    top: z,
                    left: y
                }), "lens" != co.options.position && f.append(I)) : "inside" == co.options.position || "fullscreen" == co.options.position ? (g = u, m = v, I.css({
                    width: g + "px",
                    height: m + "px"
                }), M.append(I)) : (g = ho.width(), m = ho.height(), co.options.rootOutput ? (z = ho.offset().top, y = ho.offset().left, f.append(I)) : (z = ho.position().top, y = ho.position().left, ho.parent().append(I)), z += (ho.outerHeight() - m - I.outerHeight()) / 2, y += (ho.outerWidth() - g - I.outerWidth()) / 2, I.css({
                    width: g + "px",
                    height: m + "px",
                    top: z,
                    left: y
                })), co.options.title && "" != Io && ("inside" == co.options.position || "lens" == co.options.position || "fullscreen" == co.options.position ? (k = z, C = y, M.append(to)) : (k = z + (I.outerHeight() - m) / 2, C = y + (I.outerWidth() - g) / 2, f.append(to)), to.css({
                    width: g + "px",
                    height: m + "px",
                    top: k,
                    left: C
                })), M.css({
                    width: u + "px",
                    height: v + "px",
                    top: x,
                    left: b
                }), A.css({
                    width: u + "px",
                    height: v + "px"
                }), co.options.tint && "inside" != co.options.position && "fullscreen" != co.options.position ? A.css("background-color", co.options.tint) : Mo && A.css({
                    "background-image": "url(" + t.attr("src") + ")",
                    "background-color": "#fff"
                }), S = new Image;
                var n = "";
                switch (Ao && (n = "?r=" + (new Date).getTime()), S.src = t.attr("xoriginal") + n, F = o(S), F.css("position", "absolute"), S = new Image, S.src = t.attr("src"), Y = o(S), Y.css("position", "absolute"), Y.width(u), co.options.position) {
                    case "fullscreen":
                    case "inside":
                        I.append(F);
                        break;
                    case "lens":
                        X.append(F), co.options.bg && F.css({
                            display: "none"
                        });
                        break;
                    default:
                        I.append(F), X.append(Y)
                }
            }

            function h(o) {
                var t = o.attr("title"),
                    e = o.attr("xtitle");
                return e || (t || "")
            }
            this.xzoom = !0;
            var f, u, v, g, m, w, x, b, z, y, k, C, $, O, M, A, I, T, W, H, S, F, X, Y, R, q, E, D, L, Z, _, B, N, j, Q, U, G, J, K, P, V, oo, to, eo, io, so, no, ao, po, ro, lo, co = this,
                ho = {},
                fo = (new Array, new Array),
                uo = 0,
                vo = 0,
                go = 0,
                mo = 0,
                wo = 0,
                xo = 0,
                bo = 0,
                zo = 0,
                yo = 0,
                ko = 0,
                Co = 0,
                $o = 0,
                Oo = 0,
                Mo = detect_old_ie(),
                Ao = /MSIE (\d+\.\d+);/.test(navigator.userAgent),
                Io = "";
            this.adaptive = function() {
                0 != so && 0 != no || (t.css("width", ""), t.css("height", ""), so = t.width(), no = t.height()), c(), eo = o(window).width(), io = o(window).height(), ao = t.width(), po = t.height();
                var e = !1;
                (so > eo || no > io) && (e = !0), ao > so && (ao = so), po > no && (po = no), e ? t.width("100%") : 0 != so && t.width(so), "fullscreen" != ro && (s() ? co.options.position = ro : co.options.position = co.options.mposition), co.options.lensReverse || (lo = co.options.adaptiveReverse && co.options.position == co.options.mposition)
            }, this.xscroll = function(o) {
                if ($ = o.pageX || o.originalEvent.pageX, O = o.pageY || o.originalEvent.pageY, o.preventDefault(), o.xscale) go = o.xscale, p($, O);
                else {
                    var t = -o.originalEvent.detail || o.originalEvent.wheelDelta || o.xdelta,
                        e = $,
                        i = O;
                    Mo && (e = K, i = P), t = t > 0 ? -.05 : .05, go += t, p(e, i)
                }
            }, this.openzoom = function(t) {
                switch ($ = t.pageX, O = t.pageY, co.options.adaptive && co.adaptive(), go = co.options.defaultScale, J = !1, M = o("<div></div>"), "" != co.options.sourceClass && M.addClass(co.options.sourceClass), M.css("position", "absolute"), T = o("<div></div>"), "" != co.options.loadingClass && T.addClass(co.options.loadingClass), T.css("position", "absolute"), A = o('<div style="position: absolute; top: 0; left: 0;"></div>'), M.append(T), I = o("<div></div>"), "" != co.options.zoomClass && "fullscreen" != co.options.position && I.addClass(co.options.zoomClass), I.css({
                    position: "absolute",
                    overflow: "hidden",
                    opacity: 1
                }), co.options.title && "" != Io && (to = o("<div></div>"), oo = o("<div></div>"), to.css({
                    position: "absolute",
                    opacity: 1
                }), co.options.titleClass && oo.addClass(co.options.titleClass), oo.html("<span>" + Io + "</span>"), to.append(oo), co.options.fadeIn && to.css({
                    opacity: 0
                })), X = o("<div></div>"), "" != co.options.lensClass && X.addClass(co.options.lensClass), X.css({
                    position: "absolute",
                    overflow: "hidden"
                }), co.options.lens && (lenstint = o("<div></div>"), lenstint.css({
                    position: "absolute",
                    background: co.options.lens,
                    opacity: co.options.lensOpacity,
                    width: "100%",
                    height: "100%",
                    top: 0,
                    left: 0,
                    "z-index": 9999
                }), X.append(lenstint)), "inside" != co.options.position && "fullscreen" != co.options.position ? ((co.options.tint || Mo) && M.append(A), co.options.fadeIn && (A.css({
                    opacity: 0
                }), X.css({
                    opacity: 0
                }), I.css({
                    opacity: 0
                })), f.append(M)) : (co.options.fadeIn && I.css({
                    opacity: 0
                }), f.append(M)), co.eventmove(M), co.eventleave(M), d($, O), co.options.position) {
                    case "inside":
                        z -= (I.outerHeight() - I.height()) / 2, y -= (I.outerWidth() - I.width()) / 2;
                        break;
                    case "top":
                        z -= I.outerHeight() - I.height(), y -= (I.outerWidth() - I.width()) / 2;
                        break;
                    case "left":
                        z -= (I.outerHeight() - I.height()) / 2, y -= I.outerWidth() - I.width();
                        break;
                    case "bottom":
                        y -= (I.outerWidth() - I.width()) / 2;
                        break;
                    case "right":
                        z -= (I.outerHeight() - I.height()) / 2
                }
                I.css({
                    top: z,
                    left: y
                }), F.xon("load", function() {
                    T.remove(), co.options.scroll && co.eventscroll(M), "inside" != co.options.position && "fullscreen" != co.options.position ? (M.append(X), co.options.fadeIn ? (A.fadeTo(300, co.options.tintOpacity), X.fadeTo(300, 1), I.fadeTo(300, 1)) : (A.css({
                        opacity: co.options.tintOpacity
                    }), X.css({
                        opacity: 1
                    }), I.css({
                        opacity: 1
                    }))) : co.options.fadeIn ? I.fadeTo(300, 1) : I.css({
                        opacity: 1
                    }), co.options.title && "" != Io && (co.options.fadeIn ? to.fadeTo(300, 1) : to.css({
                        opacity: 1
                    })), U = F.width(), G = F.height(), co.options.adaptive && (u < so || v < no) && (Y.width(u), Y.height(v), U *= u / so, G *= v / no, F.width(U), F.height(G)), zo = xo = U, yo = bo = G, Q = U / G, N = U / g, j = G / m;
                    var o, t = ["padding-", "border-"];
                    Z = L = 0;
                    for (var e = 0; e < t.length; e++) o = parseFloat(X.css(t[e] + "top-width")), Z += o !== o ? 0 : o, o = parseFloat(X.css(t[e] + "bottom-width")), Z += o !== o ? 0 : o, o = parseFloat(X.css(t[e] + "left-width")), L += o !== o ? 0 : o, o = parseFloat(X.css(t[e] + "right-width")), L += o !== o ? 0 : o;
                    Z /= 2, L /= 2, $o = ko = mo = $, Oo = Co = wo = O, p($, O), co.options.smooth && (J = !0, requestAnimFrame(r)), co.eventclick(M)
                })
            }, this.movezoom = function(o) {
                $ = o.pageX, O = o.pageY, Mo && (K = $, P = O);
                var t = $ - b,
                    e = O - x;
                lo && (o.pageX -= 2 * (t - u / 2), o.pageY -= 2 * (e - v / 2)), (t < 0 || t > u || e < 0 || e > v) && M.trigger("mouseleave"), co.options.smooth ? (mo = o.pageX, wo = o.pageY) : (n(), l(o.pageX, o.pageY), X.css({
                    top: D - Z,
                    left: E - L
                }), Y.css({
                    top: -D,
                    left: -E
                }), a(o.pageX, o.pageY, 0, 0))
            }, this.eventdefault = function() {
                co.eventopen = function(o) {
                    o.xon("mouseenter", co.openzoom)
                }, co.eventleave = function(o) {
                    o.xon("mouseleave", co.closezoom)
                }, co.eventmove = function(o) {
                    o.xon("mousemove", co.movezoom)
                }, co.eventscroll = function(o) {
                    o.xon("mousewheel DOMMouseScroll", co.xscroll)
                }, co.eventclick = function(o) {
                    o.xon("click", function(o) {
                        t.trigger("click")
                    })
                }
            }, this.eventunbind = function() {
                t.xoff("mouseenter"), co.eventopen = function(o) {}, co.eventleave = function(o) {}, co.eventmove = function(o) {}, co.eventscroll = function(o) {}, co.eventclick = function(o) {}
            }, this.init = function(e) {
                co.options = o.extend({}, o.fn.xzoom.defaults, e), f = co.options.rootOutput ? o("body") : t.parent(), ro = co.options.position, lo = co.options.lensReverse && "inside" == co.options.position, co.options.smoothZoomMove < 1 && (co.options.smoothZoomMove = 1), co.options.smoothLensMove < 1 && (co.options.smoothLensMove = 1), co.options.smoothScale < 1 && (co.options.smoothScale = 1), co.options.adaptive && o(window).xon("load", function() {
                    so = t.width(), no = t.height(), co.adaptive(), o(window).resize(co.adaptive)
                }), co.eventdefault(), co.eventopen(t)
            }, this.destroy = function() {
                co.eventunbind(), delete co
            }, this.closezoom = function() {
                J = !1, co.options.fadeOut ? (co.options.title && "" != Io && to.fadeOut(299), "inside" != co.options.position || "fullscreen" != co.options.position ? (I.fadeOut(299), M.fadeOut(300, function() {
                    c()
                })) : M.fadeOut(300, function() {
                    c()
                })) : c()
            }, this.gallery = function() {
                var o, t = new Array,
                    e = 0;
                for (o = vo; o < fo.length; o++) t[e] = fo[o], e++;
                for (o = 0; o < vo; o++) t[e] = fo[o], e++;
                return {
                    index: vo,
                    ogallery: fo,
                    cgallery: t
                }
            }, this.xappend = function(e) {
                function i(i) {
                    c(), i.preventDefault(), co.options.activeClass && (V.removeClass(co.options.activeClass), V = e, V.addClass(co.options.activeClass)), vo = o(this).data("xindex"), co.options.fadeTrans && (H = new Image, H.src = t.attr("src"), W = o(H), W.css({
                        position: "absolute",
                        top: t.offset().top,
                        left: t.offset().left,
                        width: t.width(),
                        height: t.height()
                    }), o(document.body).append(W), W.fadeOut(200, function() {
                        W.remove()
                    }));
                    var n = s.attr("href"),
                        a = e.attr("xpreview") || e.attr("src");
                    Io = h(e), e.attr("title") && t.attr("title", e.attr("title")), t.attr("xoriginal", n), t.removeAttr("style"), t.attr("src", a), co.options.adaptive && (so = t.width(), no = t.height())
                }
                var s = e.parent();
                fo[uo] = s.attr("href"), s.data("xindex", uo), 0 == uo && co.options.activeClass && (V = e, V.addClass(co.options.activeClass)), 0 == uo && co.options.title && (Io = h(e)), uo++, co.options.hover && s.xon("mouseenter", s, i), s.xon("click", s, i)
            }, this.init(e)
        }
        o.fn.xon = o.fn.on || o.fn.bind, o.fn.xoff = o.fn.off || o.fn.bind, o.fn.xzoom = function(e) {
            var i, s;
            if (this.selector) {
                var n = this.selector.split(",");
                for (var a in n) n[a] = o.trim(n[a]);
                this.each(function(a) {
                    if (1 == n.length)
                        if (0 == a) {
                            if (i = o(this), void 0 !== i.data("xzoom")) return i.data("xzoom");
                            i.x = new t(i, e)
                        } else void 0 !== i.x && (s = o(this), i.x.xappend(s));
                    else if (o(this).is(n[0]) && 0 == a) {
                        if (i = o(this), void 0 !== i.data("xzoom")) return i.data("xzoom");
                        i.x = new t(i, e)
                    } else void 0 === i.x || o(this).is(n[0]) || (s = o(this), i.x.xappend(s))
                })
            } else this.each(function(n) {
                if (0 == n) {
                    if (i = o(this), void 0 !== i.data("xzoom")) return i.data("xzoom");
                    i.x = new t(i, e)
                } else void 0 !== i.x && (s = o(this), i.x.xappend(s))
            });
            return void 0 !== i && (i.data("xzoom", i.x), o(i).trigger("xzoom_ready"), i.x)
        }, o.fn.xzoom.defaults = {
            position: "right",
            mposition: "inside",
            rootOutput: !0,
            Xoffset: 0,
            Yoffset: 0,
            fadeIn: !0,
            fadeTrans: !0,
            fadeOut: !1,
            smooth: !0,
            smoothZoomMove: 3,
            smoothLensMove: 1,
            smoothScale: 6,
            defaultScale: 0,
            scroll: !0,
            tint: !1,
            tintOpacity: .5,
            lens: !1,
            lensOpacity: .5,
            lensShape: "box",
            zoomWidth: "auto",
            zoomHeight: "auto",
            sourceClass: "xzoom-source",
            loadingClass: "xzoom-loading",
            lensClass: "xzoom-lens",
            zoomClass: "xzoom-preview",
            activeClass: "xactive",
            hover: !1,
            adaptive: !0,
            lensReverse: !1,
            adaptiveReverse: !1,
            title: !1,
            titleClass: "xzoom-caption",
            bg: !1
        }
    }(jQuery);
