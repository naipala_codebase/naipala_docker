var k=angular.module('cartapp',[]);
k.controller('CartController',function($scope,$http,$filter){
	$scope.cartItems=[];
	$scope.total=0;
	$scope.current = null;

	$scope.init=function(){
		var k=JSON.parse($('#cartitems').val());
		$scope.cartitems = $.map(k, function(el) { return el });
		
		$scope.total=0;
		for(var i=0;i<$scope.cartitems.length;i++)
		{
			$scope.total+=$scope.cartitems[i]['subtotal'];
		}
		
		$('#cartcount').text($scope.cartitems.length);
		if($scope.cartitems.length > 0)  $('#cartcount').removeAttr('hidden');
		else $('#cartcount').attr('hidden','hidden');
	}
	
	$scope.emptyCart=function(){
		$('#emptycartloading').removeAttr('hidden');
		$http({
		  method: 'POST',
		  url: '/cart/emptycart'
		}).then(function successCallback(response) {
		    $('#emptycartloading').attr('hidden','true');
			$scope.cartitems = [];
			$('#cartcount').text($scope.cartitems.length);
			if($scope.cartitems.length > 0)  $('#cartcount').removeAttr('hidden');
    	    else $('#cartcount').attr('hidden','hidden');
		  }, function errorCallback(response) {
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		  });
		$scope.total=0;
	}
	$scope.removeItem=function(rowId){
		$('#xloading'+rowId).removeAttr('hidden');
		$http({
		  method: 'POST',
		  url: '/cart/removeitem',
		  data:{rowId:rowId}
		}).then(function successCallback(response) {
		    // $('#emptycartloading').attr('hidden','true');
		    if(response.data==null)
		    	$scope.cartitems=[];
		    else{
			    // var k=JSON.parse(response.data);
				$scope.cartitems = $.map(response.data, function(el) { return el });
				$scope.total=0;
				for(var i=0;i<$scope.cartitems.length;i++)
				{
					$scope.total+=$scope.cartitems[i]['subtotal'];
				}
				}
				$('#xloading'+rowId).attr('hidden','true');
				$('#cartcount').text($scope.cartitems.length);
				if($scope.cartitems.length > 0)  $('#cartcount').removeAttr('hidden');
        	    else $('#cartcount').attr('hidden','hidden');
		  }, function errorCallback(response) {
		  	aalert('Something went wrong','','Oops..');
	        
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		  });
	}
	$scope.updateQuantity=function(rowId)
	{
		var qty=$('#qty'+rowId).val();
	    $('#updloading'+rowId).removeAttr('hidden');
		$http({
		  method: 'POST',
		  url: '/cart/updateqty',
		  data:{rowId:rowId, qty:qty}
		}).then(function successCallback(response) {
		    // $('#emptycartloading').attr('hidden','true');
		    
		    if(response.data==null)
		    	$scope.cartitems=[];
		    else{
			    // var k=JSON.parse(response.data);
				$scope.cartitems = $.map(response.data, function(el) { return el });
				$scope.total=0;
				for(var i=0;i<$scope.cartitems.length;i++)
				{
					$scope.total+=$scope.cartitems[i]['subtotal'];
				}
				}
			$('#upd'+rowId).attr('hidden','true');
			$('#updloading'+rowId).attr('hidden','true');
			$('#cartcount').text($scope.cartitems.length);
			if($scope.cartitems.length > 0)  $('#cartcount').removeAttr('hidden');
    	    else $('#cartcount').attr('hidden','hidden');
		  }, function errorCallback(response) {
		    aalert(response.data.errors,'','Oops..');
	         $('#updloading'+rowId).attr('hidden','true'); 
		    // called asynchronously if an error occurs
		    // or server returns response with an error status.
		  });

	}
	$scope.qtyChange=function(rowId){
		$('#upd'+rowId).removeAttr('hidden');
	}

	$scope.currency_convert = function(price){
		if(!$scope.current) return '$ '+price;

		return $scope.current.symbol+' '+price;
	}
});