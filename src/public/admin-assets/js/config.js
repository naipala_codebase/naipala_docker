 CKEDITOR.editorConfig = function( config ) {
        	config.toolbarGroups = [
        		{ name: 'document', groups: [ 'mode', 'document', 'doctools' ] },
        		{ name: 'clipboard', groups: [ 'clipboard', 'undo' ] },
        		{ name: 'editing', groups: [ 'find', 'selection', 'spellchecker', 'editing' ] },
        		{ name: 'forms', groups: [ 'forms' ] },
        		'/',
        		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
        		{ name: 'paragraph', groups: [ 'list', 'indent', 'blocks', 'align', 'bidi', 'paragraph' ] },
        		{ name: 'links', groups: [ 'links' ] },
        		{ name: 'insert', groups: [ 'insert' ] },
        		'/',
        		{ name: 'styles', groups: [ 'styles' ] },
        		{ name: 'colors', groups: [ 'colors' ] },
        		{ name: 'tools', groups: [ 'tools' ] },
        		{ name: 'others', groups: [ 'others' ] },
        		{ name: 'about', groups: [ 'about' ] }
        	];
        
        	config.removeButtons = 'Source,Save,Templates,Cut,Undo,SelectAll,Scayt,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Redo,Copy,Paste,Preview,NewPage,Print,PasteFromWord,PasteText,Replace,CopyFormatting,RemoveFormat,Anchor,Styles,Format,Font,FontSize,TextColor,BGColor,ShowBlocks,Maximize,About,Flash,Smiley,PageBreak,Iframe,BidiLtr,BidiRtl,Language,JustifyRight,JustifyCenter,Blockquote,Outdent,Indent,Italic,Strike,Superscript,Subscript,Find';
        };