var k= angular.module('itemapp',[]);
k.controller('itemController',function($scope){
	$scope.editClick=function(id){
		$("#btn"+id).attr("type","button");
		$("#txt"+id).attr("type","text");
		$("#sbtn"+id).attr("type","submit");
		console.log(id);
	}
	$scope.cancelEdit=function(id){
		$("#btn"+id).attr("type","hidden");
		$("#txt"+id).attr("type","hidden");
		$("#sbtn"+id).attr("type","hidden");
	}
	$scope.delcategory=function(id)
	{
		console.log(id);
		swal({
  				title: 'Are you sure?',
  				text: 'Deleting category will delete all items under this category',
  				type: 'info',
  				showCloseButton: true,
 				showCancelButton: true,
  				confirmButtonText: 'Delete',
  				cancelButtonText: 'Cancel',
    			confirmButtonClass: 'btn btn-success',
  				cancelButtonClass: 'btn btn-danger',
		}).then(function(){
		
				$.get('/admin/item/removecategory',{id:id},function(data){
					if(data=="success"){
						 swal(
   							 'Deleted!',
   							 'Category has been deleted with all items under it.',
   							 'success'
 							 ).then(function(){
 							 	location.reload();
 							 })
					}
				});
		});
	}

});