var l = angular.module('attribapp',[]);
l.controller('attribcontroller',function($scope){
	$scope.details=[{id:1,size:'',color:'',quantity:1}];
	$scope.count=1;
	$scope.getDetails = function(details){
		$scope.details = [];
		$scope.details = details;
		$scope.count = $scope.details[$scope.details.length-1].id;
	}

	$scope.addDetail= function()
	{
		$scope.count++;
		
		var k= { id:$scope.count ,size:'',color:'',quantity:1};
		$scope.details.push(k);
	};
	$scope.removeDetail= function(id)
	{
		for (var i=0; i<$scope.details.length ;i++)
		{
			if($scope.details[i].id==id)
			{
				$scope.details.splice(i,1);
			}
		}
	};
});