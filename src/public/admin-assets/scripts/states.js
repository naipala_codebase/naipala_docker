var l = angular.module('states',[]);
l.controller('StateController',function($scope){
	$scope.states=[{code:'',name:''}];
	$scope.count=1;

	$scope.addState= function()
	{
		$scope.count++;
		var k= {code:'',name:''};
		$scope.states.push(k);
    };
    
	$scope.removeState= function(index)
	{
        $scope.states.splice(index,1);
	};
});