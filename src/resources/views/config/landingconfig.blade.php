@extends('layouts.admin')
@section('body')
<link href="{{asset('/admin-assets/sweetprompt/lib/sweet-alert.css')}}" rel="stylesheet" type="text/css" media="all" />
<link href="{{asset('/admin-assets/sweetprompt/lib/sweet-prompt.css')}}" rel="stylesheet" type="text/css" media="all" />

<div class="content">
  <div class="container-fluid">
    <h3>Home Configuration</h3>
    <div class="card ">
      <div class="header">
        <h4 class="title">Landing Slider Images</h4>
        <p class="category">Add or remove images</p>

      </div>


      <div class="content">
        <div class="row">
          @foreach($images as $i)
          <div class="col-md-3 col-sm-12 col-xs-12">
            <div height="200px" width="200px">
              <div class="panel-body">
                <img src="{{asset('uploads'.'/'.$i->image_link)}}" height="200px" width="200px"/>
              </div>




            </div>
            <form action="{{URL::to('admin/config/removelandingimage')}}" method="POST">
              <input type="hidden" value="{{$i->image_id}}" name="image_id"/>

              {{ csrf_field() }}

              &nbsp;&nbsp;&nbsp; <input type="button" id="deleteLandingImage" value="Remove" class="btn btn-danger btn-fill btn-xs"/>
              <input type="hidden" id="imgcpn{{$i->image_id}}" value="{{$i->redirect_link}}"/>
              <a id="addCaption{{$i->image_id}}" class="btn btn-success btn-fill btn-xs" imgid="{{$i->image_id}}">Edit Link</a>
            </form>
            <br>
            <br>
          </div>
          @endforeach
        </div>

        <br>
        <br>
        {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addlandingimage','files'=>'true'])!!}


        <input type="file" name="file" class="btn btn-info btn-xs" value="Add Images" required border="0px"/>

        <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
        <span>Click to Upload</span>

        {!! Form::close() !!}
      </div>  
    </div>
    <div class="card">
        <div class="header">
            <h4 class="title">Main Featured image</h4>
            <p class="category">Upload featured image</p>
    
          </div>
          <div class="content">
            <div class="container-fluid">
              
              <h4>Current Image</h4>
              <img style="width:800px; height:400px;" src="{{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Featured_Image')->first()->config_value)}}"/>
              <br>
              <br>
                {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addfeaturedimage','files'=>'true'])!!}


                <input type="file" name="image" class="btn btn-info btn-xs" value="Add Images" required border="0px"/>
        
                <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
                <span>Click to Upload</span>
        
                {!! Form::close() !!}
                
            </div>
          </div>
          
    </div>
    <div class="card">
      <div class="header">
          <h4 class="title">Featured contents</h4>
          <p class="category">Upload contents</p>
  
        </div>
        <div class="content">
          <div class="container-fluid">
            <div class="row">
              <div class="col-lg-4">
                <h4>Current Image</h4>
                <img style="width:300px; height:300px;" src="{{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','First_Featured_Image')->first()->config_value)}}"/>
                <br>
                <br>
                  {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addfirstfeaturedimage','files'=>'true'])!!}


                  <input type="file" name="image"  class="btn btn-info btn-xs" value="Add Images" required border="0px"/>
          
                  <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
                  <span>Click to Upload</span>
          
                  {!! Form::close() !!}
                  <form action="{{URL::to('/admin/config/addfirstfeaturedcontent')}}" method="POST">
                  <br>
                    {{csrf_field()}}
                    <label>Title</label>
                    <input type="text" name="title" placeholder="title" class="form-control" value="{{$landingconfigs->where('config_key','First_Featured_Title')->first()->config_value}}"/>
                    <label>SubTitle</label>
                    <input type="text" name="subtitle" placeholder="sub-title" class="form-control" value="{{$landingconfigs->where('config_key','First_Featured_Subtitle')->first()->config_value}}"/>
                    <label>Link</label>
                    <input type="text" name="link" placeholder="Link" class="form-control" value="{{$landingconfigs->where('config_key','First_Featured_Link')->first()->config_value}}"/>
                    
                    <br>
                    <input type="submit" value="Save" class="btn btn-fill btn-success"/>
                  </form>
              </div>
              <div class="col-lg-4">
                <h4>Current Image</h4>
                <img style="width:300px; height:300px;" src="{{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Second_Featured_Image')->first()->config_value)}}"/>
                <br>
                <br>
                  {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addsecondfeaturedimage','files'=>'true'])!!}


                  <input type="file" name="image" class="btn btn-info btn-xs" value="Add Images" required border="0px"/>
          
                  <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
                  <span>Click to Upload</span>
          
                  {!! Form::close() !!}
                  <form action="{{URL::to('/admin/config/addsecondfeaturedcontent')}}" method="POST">
                      <br>
                        {{csrf_field()}}
                        <label>Title</label>
                        <input type="text" name="title" placeholder="title" class="form-control" value="{{$landingconfigs->where('config_key','Second_Featured_Title')->first()->config_value}}"/>
                        <label>SubTitle</label>
                        <input type="text" name="subtitle" placeholder="sub-title" class="form-control" value="{{$landingconfigs->where('config_key','Second_Featured_Subtitle')->first()->config_value}}"/>
                        <label>Link</label>
                        <input type="text" name="link" placeholder="Link" class="form-control" value="{{$landingconfigs->where('config_key','Second_Featured_Link')->first()->config_value}}"/>
                    
                        <br>
                        <input type="submit" value="Save" class="btn btn-fill btn-success"/>
                      </form>
              </div>
              <div class="col-lg-4">
                <h4>Current Image</h4>
                <img style="width:300px; height:300px;" src="{{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Third_Featured_Image')->first()->config_value)}}"/>
                <br>
                <br>
                  {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addthirdfeaturedimage','files'=>'true'])!!}


                  <input type="file" name="image" class="btn btn-info btn-xs" value="Add Images" required border="0px"/>
          
                  <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
                  <span>Click to Upload</span>
          
                  {!! Form::close() !!}
                  <form action="{{URL::to('/admin/config/addthirdfeaturedcontent')}}" method="POST">
                      <br>
                        {{csrf_field()}}
                        <label>Title</label>
                        <input type="text" name="title" placeholder="title" class="form-control" value="{{$landingconfigs->where('config_key','Third_Featured_Title')->first()->config_value}}"/>
                        <label>SubTitle</label>
                        <input type="text" name="subtitle" placeholder="sub-title" class="form-control" value="{{$landingconfigs->where('config_key','Third_Featured_Subtitle')->first()->config_value}}"/>
                        <label>Link</label>
                        <input type="text" name="link" placeholder="Link" class="form-control" value="{{$landingconfigs->where('config_key','Third_Featured_Link')->first()->config_value}}"/>
                    
                        <br>
                        <input type="submit" value="Save" class="btn btn-fill btn-success"/>
                      </form>
              </div>
            </div>
          </div>
        </div>
        
  </div>
   

    <div class="card">
        <div class="header">
            <h4 class="title">Featured Video</h4>
            <p class="category">video link</p>
    
          </div>
          <div class="content">
            <div class="container-fluid">
              
              <h4>Current Video</h4>
              <iframe src="{{$landingconfigs->where('config_key','Video_Link')->first()->config_value}}" alt="{{$landingconfigs->where('config_key','Video_Link')->first()->config_value}}" frameborder="0" gesture="media" allowfullscreen=""></iframe>
              <br>
              <br>
                {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addlandingvideolink','files'=>'true'])!!}

              <label>Enter Embeded link</label>
                <input type="text" name="link" class="form-control" placeholder="Enter FULL link(eg: https://www.youtube.com/embed/gKM15TaKLUI)" required border="0px"/>
              <br>
                <input type="submit" class="btn btn-success btn-fill btn-sm" value="Save" />
                
        
                {!! Form::close() !!}
                
            </div>
          </div>
          
    </div>
    <div class="card">
      <div class="header">
          <h4 class="title">Video Background Image</h4>
          <p class="category">Upload background image for video section</p>
  
        </div>
        <div class="content">
          <div class="container-fluid">
            
            <h4>Current Image</h4>
            <img style="width:800px; height:400px;" src="{{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Video_Image')->first()->config_value)}}"/>
            <br>
            <br>
              {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addlandingvideoimage','files'=>'true'])!!}


              <input type="file" name="image" class="btn btn-info btn-xs" value="Add Images" required border="0px"/>
      
              <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
              <span>Click to Upload</span>
      
              {!! Form::close() !!}
              
          </div>
        </div>
        
  </div>

    <div class="card">
      <div class="header">
        <h4 class="title">Trending Section</h4>
        <p class="category">Add or remove trending Items</p>

      </div>
      <div class="content">
        <div class="container-fluid">
          <form action="{{URL::to('/admin/config/addtrendingcontent')}}" method="POST">
              {{csrf_field()}}
                <label>Trending Title</label>
                <input type="text" class="form-control" placeholder="Trending Title" name="title" value="{{$landingconfigs->where('config_key','Trending_Title')->first()->config_value}}"/>
                <label>Trending Details</label>
                <input type="text" class="form-control" placeholder="Trending Description" name="description" value="{{$landingconfigs->where('config_key','Trending_Description')->first()->config_value}}"/>
                <br>
                <input type="submit" class="btn btn-fill btn-success" value="Save"/>
                <br>
          </form>
          <br>
          <br>
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <th>Name</th>
                <th>Price</th>
                <th>Details</th>
                <th>Images</th>
                <th>Actions</th>
              </thead>
              <tbody>
                @foreach($featureditems as $i)
                <tr>
                  <td>{{$i->item_name}}</td>
                  <td>{{$i->item_price}}</td>
                  <td><?php $details=json_decode($i->details,true);?>
                    @foreach($details as $detail)
                    <span {{ $detail['size'] ==''? 'hidden':''}}><b>Size</b>: {{ $detail['size'] }},</span>
                    <span {{ $detail['color'] ==''? 'hidden':''}}><b>Color</b>: {{ $detail['color'] }},</span>
                    <span><b>Quantity</b>: {{ $detail['quantity'] }}</span>
                    <br>
                    @endforeach

                  </td>
                  <td>
                      <?php $current = '-1'; ?>
                      @foreach($i->item_images as $g)
                        @if($current == $g->color) @continue @endif
                        <?php $current = $g->color; ?>
                        <img src="{{asset('uploads'.'/'.$g->image_link)}}" height="50px" width="50px" alt="img"/>
                      @endforeach
                    </td>
                  <td>
                
                    <form action="{{URL::to('admin/config/removefeatured'.'/'.$i->item_id)}}" method="POST">
                      {{ csrf_field() }}

                      <button class="badge" id="deleteFeatured" type="submit" border="1px"> 
                        remove
                      </button>
                    </form>

                  </td>
                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
        <br>
     <h4>Add Trending Items</h4>   
    <form action="{{URL::to('admin/config/addfeatured')}}" method="POST">
      <input type="hidden" name="_token" value="{{csrf_token()}}"/>
      <select name="item_id" class="form-control">
          @foreach($remainitems as $r)
          <option value="{{$r->item_id}}">{{$r->item_name}}</option>
          @endforeach
      </select>
      <br>
      <input type="submit" class="btn btn-fill btn-success" value="Add"/>
    </form>

        </div>
      </div>
    </div>
    <div class="card">
        <div class="header">
            <h4 class="title">Story Section</h4>
            <p class="category">Edit story</p>
    
          </div>
          <div class="content">
            <div class="container-fluid">
              <form action="{{URL::to('/admin/config/addstorycontent')}}" method="POST">
                {{csrf_field()}}
                <label>Title</label>
                <input type="text" class="form-control" placeholder="Title" name="title" value="{{$landingconfigs->where('config_key','Story_Title')->first()->config_value}}"/>
                <label>Sub-Title</label>
                <input type="text" class="form-control" placeholder="Sub-title" name="subtitle" value="{{$landingconfigs->where('config_key','Story_Subtitle')->first()->config_value}}"/>
                <label>Story</label>
                <textarea class="form-control" name="description" >
                    {{$landingconfigs->where('config_key','Story_Description')->first()->config_value}}
                </textarea>
                <br>
                <input type="submit" class="btn btn-fill btn-success" value="Save"/>
                <br>
              </form>
            </div>
          </div>
    </div>
    <div class="card">
      <div class="header">
          <h4 class="title">Story Background Image</h4>
          <p class="category">Upload background image for story section</p>
  
        </div>
        <div class="content">
          <div class="container-fluid">
            
            <h4>Current Image</h4>
            <img style="width:800px; height:400px;" src="{{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Story_Image')->first()->config_value)}}"/>
            <br>
            <br>
              {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addstoryimage','files'=>'true'])!!}


              <input type="file" name="image" class="btn btn-info btn-xs" value="Add Images" required border="0px"/>
      
              <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
              <span>Click to Upload</span>
      
              {!! Form::close() !!}
              
          </div>
        </div>
        
  </div>



  </div>

</div>
@endsection
@section('admin-js')
<script>
$(document).ready(function(){
  $('[id *= "addCaption"]').click(function(){
    var id = $(this).attr('imgid');
    var input= $('#imgcpn'+id).val();
    if(input == '') input = "http://";
    swal({
      title: "Edit Link",
      text: "Enter URL",
      input: 'text',
      inputValue: input,
      showCancelButton: true,
      confirmButtonText: 'Confirm',
      cancelButtonText: 'Cancel',
    }).then(function(response) {
      $.get('/admin/config/editlandinglink',{id: id , link:response },function(data,status){
        swal("Link", "Successfully edited link", "success");
      });
      $('#imgcpn'+id).val(response);
    }).catch(swal.noop);
  });
})
</script>

@stop