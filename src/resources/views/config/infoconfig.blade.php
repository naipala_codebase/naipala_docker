@extends('layouts.admin')
@section('body')

<div class="content">
  <div class="container-fluid">
     <div class="card">
        <div class="header">
            
              <h4 class="title">Edit Info:</h4>
        </div>
      <div class="content">
            {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/editinfo','files'=>'true'])!!}
              @foreach($infos as $i)
                <label>
                  {{$i->config_key}}

                  <input type="hidden" id="id" value="{{$i->config_id}}"/>

                  <a href="{{URL::to('admin/config/removeinfo'.'/'.$i->config_id)}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-s">
                    <i class="fa fa-times"></i>
                  </a>
                </label>
               <br>
                <label>Title</label>
                <input type="text" required class="form-control"  name= "id{{$i->config_id}}" value="{{$i->config_key}}" placeholder="title"/>
                <label>Alias</label>
                <input type="text" required class="form-control"  name="alias{{$i->config_id}}" value="{{$i->config_alias}}"/>
                 <label>Description</label>
                 <textarea class="form-control" id="ck{{$i->config_id}}" required name="val{{$i->config_id}}" cols="50" rows="10">{{$i->config_value}}</textarea>
               <script>
                  CKEDITOR.replace( 'ck{{$i->config_id}}' );
               </script>
              <br>
              <hr>
              @endforeach
              <br>
                <input type="submit" class="btn btn-success btn-fill btn-sm pull-right" value="Save"/>
              <br>
              <br>
            
            {!! Form::close() !!}
       </div>
      </div>
    <div class="card">
        <div class="header">
          
              <h4 class="title">Add New Info:</h4>
        </div>
         
          <div class="content">
            {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/addinfo','files'=>'true'])!!}
                   
                    <input name="config_key" required type="text" class="form-control" id="new" placeholder="Info Title"/>
                    <input name="config_alias" required type="text" class="form-control" id="newalias" placeholder="Alias"/>
                    <textarea class="form-control" name="config_value" placeholder="Info Value" ></textarea>
                    <br>
                    <input type="submit" class="btn btn-success btn-fill btn-sm" value="Add"/>
               {!! Form::close() !!}
          </div>
       </div>
    </div>
  </div>
@endsection
@section('admin-js')
<script type="text/javascript">

$(document).ready(function(){
  console.log('ready');
  

    $(document).on('keyup','[name^=id]',function(){
        var id=$(this).attr('name').substring(2);
        var k='[name=alias'+id+']';

        var name = $(this).val().toLowerCase().replace(/ /g,'-');
        console.log(name);
        $(k).val(name);
  });
       $(document).on('keyup','#new',function(){
        
        var name = $(this).val().toLowerCase().replace(/ /g,'-');
          
        $("#newalias").val(name);
  });
})
</script>
  
  @stop

