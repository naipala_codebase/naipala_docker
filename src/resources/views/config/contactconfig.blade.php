@extends('layouts.admin')
@section('body')

<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="header">

        <h4 class="title">Edit Contact:</h4>
      </div>
      <div class="content">
        {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/config/editcontact','files'=>'true'])!!}    		
          <label><h4>Contact Information:</h4></label>
          <input type="hidden" id="getvalue" value="{{$contact->config_value}}"/>
          <div class="card" style="padding: 10px;" id="value"></div> 

          <input type="button" id="edit" class="btn btn-success btn-fill btn-sm" value="Edit"/>
          <br>
          <div id="editdiv" hidden>
            <br>
            <textarea name="contactvalue"  class="form-control" required id="contact" value="{{$contact->config_value}}"></textarea>

            <br>
            <input type="submit" class="btn btn-success btn-fill btn-sm pull-right" value="Save"/>
          </div>
        {!!Form::close()!!}
            <br>
            <br>

      </div>
    </div>
  </div>
</div>
@endsection
@section('admin-js')
 <script>
      		  	$(document).ready(function(){
      		  		$('#value').html($('#getvalue').val());
      		  		$('#edit').click(function(){
      		  			$('#editdiv').removeAttr('hidden');
      		  		});
      		  	})
                  CKEDITOR.replace( 'contact' );
               		$(document)
               </script>
@endsection