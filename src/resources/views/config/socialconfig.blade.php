@extends('layouts.admin')
@section('body')

<div class="content">
  <div class="container-fluid">
    <div class="card">
      <div class="header">

        <h4 class="title">Edit Social:</h4>
      </div>

      <div class="content">
        @foreach($socials as $s)
        {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/social/edit'.'/'.$s->social_id,'files'=>'true'])!!}

        <label>
          {{$s->social_title}}


          <a href="{{URL::to('admin/social/removeSocial'.'/'.$s->social_id)}}" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-s">
            <i class="fa fa-times"></i>
          </a>

        </label>
        <br>
        <label>Title</label>
        <input type="text" required class="form-control"  name= "title" value="{{$s->social_title}}" placeholder="title"/>
        <label>Description</label>
        <textarea class="form-control" id="ck{{$s->social_id}}" required name="description" cols="50" rows="10">{{$s->social_description}}</textarea>
        <script>
        CKEDITOR.replace( 'ck{{$s->social_id}}' );
        </script>
        <br>
        <input type="submit" class="btn btn-success btn-fill btn-md pull-right" value="Save"/>
        <br>
        <br>
        {!! Form::close() !!}
        <h4 class="title">Add images for {{$s->social_title}}:</h4>
        <div class="row">

          @foreach($s->socialimages as $i)


          <div class="col-md-3 col-sm-12 col-xs-12">
            <div height="200px" width="200px">
              <div class="panel-body">
                <img src="{{asset('uploads'.'/'.$i->image_link)}}" height="200px" width="200px"/>
              </div>
            </div>
            <form action="{{URL::to('admin/social/removesocialimage')}}" method="POST">

              <input type="hidden" value="{{$i->socialimage_id}}" name="image_id"/>

              {{ csrf_field() }}
              &nbsp;&nbsp;&nbsp; 
              <input type="button" id="deleteSocialImage" value="Remove" class="btn btn-danger btn-fill btn-xs"/>
              <input type="hidden" id="imgcpn{{$i->socialimage_id}}" value="{{$i->image_caption}}"/>
              <a id="addCaption{{$i->socialimage_id}}" class="btn btn-success btn-fill btn-xs" imgid="{{$i->socialimage_id}}">Edit Caption</a>
            </form>
            <br>
            <br>
          </div>
          @endforeach
        </div>

        <br>
        <br>

        {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/social/addsocialimage','files'=>'true'])!!}


        <input type="file" name="file" class="btn btn-info btn-xs" value="Add Images" required border="0px"/>
        <input type="hidden" value="{{$s->social_id}}" name="social_id"/>
        <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
        <span>Click to Upload</span>

        {!! Form::close() !!}
        <hr>
        <hr>
        @endforeach  
      </div>
    </div>
    <div class="card">
      <div class="header">

        <h4 class="title">Add New social:</h4>
      </div>

      <div class="content">
        {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/social/addsocial','files'=>'true'])!!}
        <label>Title</label>
        <input type="text" class="form-control" name="title" placeholder="Enter Title" required/>
        <label>Description</label>
        <textarea class="form-control" id="newsocial" required name="description" cols="50" rows="10" placeholder="Enter Description"></textarea>
        <br>
        <input type="submit"  class="btn btn-success btn-fill btn-md pull-right" value="Add"/>
        <br>
        <br>
        <script>
        CKEDITOR.replace( 'newsocial' );
        </script>
        {!! Form::close() !!}
      </div>
    </div>

    <div class="card">
      <div class="header">

        <h4 class="title">Social Links: </h4>

      </div>

      <div class="content">
        <table>
          <thead>
            <th></th>
            <th width="400px"></th>
            <th></th>
          </thead>
          <tbody>
          
         
            
            @foreach($sociallinks as $s)
            <tr>

                <td><input type="text" disabled class="form-control" value="{{$s->config_key}}"/></td>
                <td><input type="text" disabled class="form-control" value="{{$s->config_value}}" /></td>
                <td>
                  
                  <form action="{{URL::to('admin/social/removelink'.'/'.$s->config_id)}}" method="POST">
                    {{ csrf_field() }}
                    <button type="button" rel="tooltip" id="deleteSocialLink" title="Remove" class="btn btn-danger btn-simple btn-md">
                        <i class="fa fa-times"></i>
                    </button>
                  </form>
                </td>
            </tr>
            
            @endforeach
          
          
            </tbody>
        </table>
       <br>
          <input type="button" id="Addlink" class="btn btn-success btn-fill btn-sm" value="Add"/>
         <br>
         <br>
          <form action="{{URL::to('admin/social/addlinks')}}" method="POST">
          <br>
          <input type="hidden" name="_token" value="{{csrf_token()}}"/>
         
          <table>
            <thead>
              <th></th>
                <th></th>
                  <th></th>
            </thead>
          <tbody>
            <div id="linkrow">
            </div>  
          </tbody>
          </table>
          <input type="hidden" id="linkcount" name="linkcount"/>
          <div id="linksubmit" hidden>
          <input type="button" id="clearlink" class="btn btn-danger btn-fill btn-sm pull-right" value="Clear"/>
          <input type="submit" class="btn btn-success btn-fill btn-sm pull-right" value="Save"/>
          </div>
          <br>
          <br>
        </form>
                
      </div>
    </div>
  </div>
</div>
@endsection

@section('admin-js')
  <script type="text/javascript">

    $(document).ready(function(){
      $(document).on('keyup','[name^=id]',function(){
        var id=$(this).attr('name').substring(2);
        var k='[name=alias'+id+']';

        var name = $(this).val().toLowerCase().replace(/ /g,'-');
        $(k).val(name);
      });
      $(document).on('keyup','#new',function(){
      
        var name = $(this).val().toLowerCase().replace(/ /g,'-');
          
        $("#newalias").val(name);
      });

      $('[id *= "addCaption"]').click(function(){
        
        var id = $(this).attr('imgid');
        var input= $('#imgcpn'+id).val();

        swal({
          title: "Edit Caption",
          text: "Enter Some Caption",
          input: 'text',
          inputValue: input,
          showCancelButton: true,
          confirmButtonText: 'Confirm',
          cancelButtonText: 'Cancel',
        }).then(function(response) {
          $.get('/admin/social/addcaption',{id: id , caption:response },function(data,status){
              swal("Caption", "Successfully edited caption", "success");
          });
          $('#imgcpn'+id).val(response);
        }).catch(swal.noop);
      });
      
      var linkcount=0;
      $('#Addlink').click(function(){
        linkcount++;
        $('#linkcount').val(linkcount);
        $('#linksubmit').removeAttr('hidden');
        var td1='<input type="text" placeholder="Title" required name="linktitle'+linkcount+'" class="form-control"/>';
        var td2='<input type="text" placeholder="http://" value="http://" required name="linkval'+linkcount+'" class="form-control"/>';
        var td3= '';
        var k='<tr><td>'+td1+'</td><td width="400px">'+td2+'</td><td>'+td3+'</td></tr>';
        $('#linkrow').append(k);
      });
      $('#clearlink').click(function(){
        linkcount=0;
        console.log('clear');
        $('#linkrow').empty();
        $('#linksubmit').attr('hidden','true');
      });
    });
  </script>
@endsection
