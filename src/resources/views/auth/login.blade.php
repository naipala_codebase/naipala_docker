@extends('layouts.app')

@section('body')
<div id="login">
    <section class="container my-5" style="max-width:900px">
            {{--  <h4 class="text-center"><b>Feel different?</b></h4>  --}}
        <div class="row">
           
            <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                <hr>
                <h4 class=""><b>Been here before?</b></h4>
                <br>
                <form style="padding-left: 60px;padding-right: 60px;" method="POST" action="{{ route('login',array('next'=>$next)) }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <input type="email" class="form-control {{ $errors->has('email') ? 'border-error' : '' }}"  style="background:#e8e8e8;"  placeholder="email@example.com" name="email" value="{{ old('email') }}" required autofocus>
                        @if ($errors->has('email'))
                            <p style="margin-top: 10px;">
                                <strong>{!! $errors->first('email') !!}</strong>
                            </p>
                        @endif
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="password" class="form-control {{ $errors->has('password') ? 'border-error' : '' }}" style="background:#e8e8e8;" placeholder="Password" name="password" required>
                        @if ($errors->has('password'))
                            <p style="margin-top: 10px;">
                                <strong>{{ $errors->first('password') }}</strong>
                            </p>
                        @endif
                    </div>
                    <div class="form-group flex-container justify-content-between align-items-stretch my-4">
                        <div class="form-check login-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox" value="None" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                            </label>
                        </div>
                        <div class="login-forgot">
                            <a href="{{ route('password.request') }}">Forgot Password?</a>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn custom-btn" type="submit">Login</button>
                    </div>
                    <hr>
                </form>
            </div>
            <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                <hr>
                <h4 class=""><b>First time?</b></h4>
              
                <h4 class=""><b>Create your own Milieu.</b></h4>
                <br>
                <div class="text-center">
                    <a href="{{ URL::to('register').'?next='.$next }}" class="btn custom-btn">Create</a>
                </div>
               <br>

                <div class="social-login">
                    <div class="connect-with my-5">
                        <span>OR CONNECT WITH</span>
                        <hr>
                    </div>
                    <a href="{{ url('/signin/facebook').'?next='.$next }}" class="btn custom-btn facebook-btn">
                        <i class="fa fa-facebook"></i>
                        FACEBOOK
                    </a>
                </div>
                <hr>
            </div>
        </div>
    </section>
</div>

@endsection
