
<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Naipala - Feel Different</title>

    <meta name="description" content="Naipala is a lifestyle brand and we market our products and services to personify the interests, attitudes, and opinions of Nepalese culture and people. We as a lifestyle brand seek to encourage, monitor and persuade people with the purpose of their products contributing to the definition of the consumer’s way of life.">
	
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
        <link rel="shortcut icon" href="https://naipala.com/img/logo.jpg">
    <link rel="apple-touch-icon" href="https://naipala.com/img/logo.jpg">

    <link rel="stylesheet" href="https://naipala.com/frontend-assets/bootstrap.min.css">
    <link rel="stylesheet" href="https://naipala.com/frontend-assets/font-awesome.min.css">

    <!-- drawer.css -->
    <link rel="stylesheet" href="https://naipala.com/frontend-assets/drawer.min.css">
    <link rel="stylesheet" href="https://naipala.com/frontend-assets/styles/styles.css">
    <style>
        .border-error{
            border-color: #de1111bd;
        }

        input[type="text"], input[type="password"], input[type="email"], input[type="number"], select,  textarea{
            background:#e8e8e8!important;
        }
        

    </style>
    
    <style>
        .carousel-fade .carousel-item {
            display: block;
            position: relative;
            opacity: 0;
            transition: opacity .75s ease-in-out;
             
          } 
          .carousel-fade .carousel-item.active {
            opacity: 1;
            display: block;
               
          }
          .carousel {
            position: relative;
            top: 0;
            left: 0;
           
          }
          .carousel-inner {
                height:100vh;
          }
          .carousel-item {
            opacity .5;
          }
          a.carousel-control-next:link, a.carousel-control-prev:link {
            background-color: transparent;
            text-decoration: none;
            opacity: .5;
          }
          a.carousel-control-next:hover, a.carousel-control-prev:hover  {
            background-color: transparent;
            text-decoration: none;
            opacity: 1;
          }
          .carousel-control-next-icon, .carousel-control-prev-icon {
            position:relative;
            background-image: none;
          }    
          .carousel-control-next-icon:before {
            top:0;
            left:-5px;
            padding-right:10px;
           }
          .carousel-control-prev-icon:before {
            top:0;
            left:-5px;
            padding-right:10px;
          }
          .sidebar {
            background-color: #e1e1e1;
          }
          
          .naipala-story {
          
        }
        @font-face {
            font-family: sketchblock;
            src: url(Sketch_Block.ttf);
        }
        @font-face {
            font-family: futuramedium;
            src: url(Futura_Medium.otf);
        }
        @media  screen and (min-width: 922px){
        .naipala-caption{
            top:20%;
            right:15%;
            left:15%;
            margin:0;
            margin-bottom:4rem;
            background:transparent;
            color:#d0d5db;
        }
        .naipala-countdown-card{
            margin: 1rem 2rem;
            background: #bdbcbab0;
            border-radius: 12px;
        }
    
        .naipala-countdown-time{
            line-height: 5rem;
            font-size: 4rem;
            margin: 0 2rem;
            color: #23272bb8;
            font-weight: 600;
            font-family: futuramedium;
            margin-top: 1rem;
        }
        .naipala-countdown-span{
            color: #23272bb8;
            margin-bottom: 0.5rem;
            font-weight:600;
            font-family: futuramedium;
            font-size:1rem;
        }
        .naipala-countdown-text{
            font-family: sketchblock;
            color:#c7c2bf;
            font-size:5rem !important;
        }
    }

    @media  screen and (min-width:600px) and (max-width:922px){
        .naipala-caption{
        top:20%;
        right:15%;
        left:15%;
        margin:0;
        margin-bottom:4rem;
        background:transparent;
        color:#d0d5db;
    }
    .naipala-countdown-card{
        margin: 1rem 2rem;
        background: #bdbcbab0;
        border-radius: 12px;
        height: 6.5rem;
        width: 7rem;
    }
    
        .naipala-countdown-time{
            line-height: 5rem;
            font-size: 2.5rem;
            /* margin: 0 2rem; */
            color: #23272bb8;
            font-weight: 600;
            font-family: futuramedium;
            /* margin-top: 1rem; */
        }
        .naipala-countdown-span{
            color: #23272bb8;
            /* margin-bottom: 0.5rem; */
            font-weight: 600;
            font-family: futuramedium;
            font-size: 1.2rem;
            line-height: 0.1rem;
        }
        .naipala-countdown-text{
            font-weight: 600;
            font-family: sketchblock;
            color:#c7c2bf;
            font-size:5rem !important;
        }    
    }
     

    @media  screen and (min-width:200px) and (max-width:600px){
        .naipala-caption{
        top:20%;
        right:15%;
        left:15%;
        margin:0;
        margin-bottom:4rem;
        background:transparent;
        color:#d0d5db;
    }
    .naipala-countdown-card{
        margin: 0rem 6px;
        background: #bdbcbab0;
        border-radius: 12px;
        height: 3.5rem;
        width: 5rem;
    }
    
        .naipala-countdown-time{
            line-height: 1.6rem;
            font-size: 1.3rem;
            /* margin: 0 2rem; */
            color: #23272bb8;
            font-weight: 600;
            font-family: futuramedium;
            margin-top: 0.3rem;
        }
        .naipala-countdown-span{
            color: #23272bb8;
            /* margin-bottom: 0.5rem; */
            font-weight: 600;
            font-family: futuramedium;
            font-size: 0.6rem;
            line-height: 1.2rem;
        }
        .naipala-countdown-text{
            font-weight: 600;
            font-family: sketchblock;
            color:#c7c2bf;
            font-size:2.5rem !important;
        }    
    }
    
    
        
</style>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=https://www.googletagmanager.com/gtag/js?id=UA-120474093-1></script>
    <script>
     window.dataLayer = window.dataLayer || [];
     function gtag(){dataLayer.push(arguments);}
     gtag('js', new Date());
    
     gtag('config', 'UA-120474093-1');
    </script>
    
</head>
<body>
    <div id="wrap">
        <div id="main"><style>
    @media(min-width:200px){
        .logoscroll{
            width:0px; 
            height:0px;  
            padding-left:0px;
            display:none
        }
    }
    
    @media(min-width:575px){
        .logoscroll{
            width:150px; 
            height:60px;  
            padding-left:12px;
            display:block;
        }
    }
</style>
                                                
<header class="drawer-left drawer--left">
    <!--search bar-->
    <section class="searchBox">
        <form method="GET" action="https://naipala.com/search">
            <input id="searchInput" name="q" type="search" placeholder="Search Collection">
        </form>
        <span id="closeSearch">x</span>
    </section>
    <!--search bar end-->

    <!--Main Navigation-->
    <nav class="navbar navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand mobile-brand" href="https://naipala.com">
            <img src="https://naipala.com/frontend-assets/images/naipala_logo.png" alt="naipala logo">
        </a>
        <div class="left-nav-block">
            <ul class="navbar-nav">
                <li class="nav-item drawer-toggle" id="openLeft"><i class="fa fa-bars" style="margin-right: 5px;"></i>MENU</li>
            </ul>
        </div>
        <a class="navbar-brand center-nav-block" href="https://naipala.com">
            <img src="https://naipala.com/frontend-assets/images/naipala_logo.png" alt="" id="logoscroll" class="logoscroll">
        </a>

        <div id="leftnav" class="right-nav-block">
            <ul class="navbar-nav d-flex flex-row mr-3">
                
                                    <li class="nav-item">
                        <a class="nav-link" style="cursor: pointer;"  data-toggle="modal" data-target="#loginModal"><i class="fa fa-user-circle-o"></i></a>
                    </li>
                                <li class="nav-item ">
                    <button id="searchBtn"><i class="fa fa-search"></i></button>
                </li>
                <li class="nav-item drawer-toggle-cart">
                    <a class="nav-link" id="carticon" style="cursor: pointer;" ><i class="fa fa-shopping-cart"></i></a>
                </li>
            </ul>
        </div>

        <!-- Main Menu -->
        <div class="drawer-nav" role="navigation">
            <div class="clearfix drawer-nav-brand">
                <a href="https://naipala.com" class="text-center">
                    <img src="https://naipala.com/frontend-assets/images/naipala_logo.png" alt="Naipala" width="150">
                </a>
                <span class="drawerclose">
                    <i class="fa fa-times"></i>
                </span>
            </div>
            <hr class="custom-hr" style="width: 100%;">
            <!--navigation-->
            <ul class="drawer-menu">
                <li>
                    <a class="drawer-menu-item right-menu" href="https://naipala.com">home</a>
                </li>
                                <li>
                    <div class="flex-container">
                        <a class="drawer-menu-item right-menu" href="https://naipala.com/collection">collection</a>
                        <div class="navToggle">
                            <i class="fa fa-chevron-down"></i>
                        </div>
                    </div>
                    <ul class="sub-menu">
                                                <li><a class="drawer-menu-item right-menu" href="https://naipala.com/collection/Apparels-1">APPARELS</a></li>
                                                <li><a class="drawer-menu-item right-menu" href="https://naipala.com/collection/Decores-2">DECORES</a></li>
                                                <li><a class="drawer-menu-item right-menu" href="https://naipala.com/collection/Accessories-3">ACCESSORIES</a></li>
                                                <li><a class="drawer-menu-item right-menu" href="https://naipala.com/collection/Gift-cards-4">GIFT CARDS</a></li>
                                            </ul>
                </li>
                
                <li>
                    <div class="flex-container">
                        <a class="drawer-menu-item right-menu" href="https://naipala.com/services">services</a>
                        <div class="navToggle">
                            <i class="fa fa-chevron-down"></i>
                        </div>
                    </div>
                    <ul class="sub-menu">
                                             </ul>
                </li>
                
                <li>
                    <div class="flex-container">
                        <a class="drawer-menu-item right-menu" id="optioninfo" style="cursor: pointer;">info</a>
                        <div class="navToggle">
                            <i class="fa fa-chevron-down" id="infoclick"></i>
                        </div>
                    </div>
                    <ul class="sub-menu">
                                                <li><a class="drawer-menu-item right-menu" href="https://naipala.com/info/about-us">ABOUT US</a></li>
                                                <li><a class="drawer-menu-item right-menu" href="https://naipala.com/info/terms-and-condition">TERMS AND CONDITION</a></li>
                                                <li><a class="drawer-menu-item right-menu" href="https://naipala.com/giftcard">gift card</a></li>
                        <li><a class="drawer-menu-item right-menu" href="https://naipala.com/social">social</a></li>
                        <li><a style="cursor: pointer;" class="drawer-menu-item right-menu" data-toggle="modal"
                               data-target="#newsletterModal">newsletter</a></li>
                                                    <li><a style="cursor: pointer;" class="drawer-menu-item right-menu" data-toggle="modal" data-target="#loginModal">login</a></li>
                                                <li><a style="cursor: pointer;" class="drawer-menu-item right-menu" data-toggle="modal" data-target="#contactModal">Quick
                            Contact</a>

                    </ul>
                </li>
                
                <li>
                    <a class="drawer-menu-item right-menu" href="https://naipala.com/eCards">E-GIFT CARDS</a>
                </li>
                
                <li>
                    <a class="drawer-menu-item right-menu" href="https://naipala.com/featuredbrands">featured brands</a>
                </li>
            </ul>
            <!--navigation end-->

            <div class="share-options text-center">
                <ul>
                    <li><a href="https://www.facebook.com/naipala.com.au" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com/naipala.com.au" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Cart Menu -->
            <div id="cartapp" ng-app="cartapp" ng-controller="CartController" ng-init="init()"  class="drawer-cart drawer-right">
            <div class="drawer-nav-cart" role="navigation">
                <section>
                    <div class="clearfix drawer-nav-brand">
                        <a class="drawer-brand text-center" href="#">My Cart <i class="fa fa-shopping-cart"></i></a>
                        <span class="closecart">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                    <hr class="custom-hr" style="width: 100%;">
                    <ul class="cart-list">
                        
                    </ul>
                    <div class="clearfix drawer-cart-action">
                        <div class="text-center"><h5>Total : 0</h5></div>
                        <div class="text-center">
                            <a href="https://naipala.com/cart" class="btn custom-btn">Checkout</a>
                            <button ng-click="emptyCart()" class="btn custom-btn black-btn ">Empty Cart &nbsp; <img hidden src="https://naipala.com/img/loading.gif" style="text-align:center" id="emptycartloading" ></img></button>
                        </div>
                    </div>
                </section>
            </div>
            <input type="hidden" value="[]" id="cartitems" ng-click="init()"/>

        </div> <!-- /.drawer cart -->
    
</header>

<div id="wrapper-landing">
    <!-- homeslider -->
    <section id="landing-home">

        <div id="carousel" class="carousel carousel-fade" data-ride="carousel">
                <div class="carousel-inner" role="listbox" >
                    <div class="carousel-item active" link="">
                        <img style= "width: 100%; height: 100vh;-o-object-fit: cover;object-fit: cover;"  src="https://naipala.com/asset/uploads/1542718863164.jpg/1359/800" alt="First slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption">
                            <div class="text-center">
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;">
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days">00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button class="btn custom-btn black-btn" data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 1 -->
                                        <div class="carousel-item" link="" style="position: absolute; top:0px;">
                        <img  style="width: 100%; height: 100vh;-o-object-fit: cover;object-fit: cover;" src="https://naipala.com/asset/uploads/1542719060432.png/1359/800" alt="Second slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption">
                              <div class="text-center">
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;">
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days" >00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button class="btn custom-btn black-btn" data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 2 -->
                                        <div class="carousel-item" link="" style="position: absolute; top:0px;">
                        <img  style="width: 100%; height: 100vh;-o-object-fit: cover;object-fit: cover;" src="https://naipala.com/asset/uploads/1542719083988.jpeg/1359/800" alt="Second slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption">
                              <div class="text-center">
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;">
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days" >00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button class="btn custom-btn black-btn" data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 2 -->
                                        <div class="carousel-item" link="" style="position: absolute; top:0px;">
                        <img  style="width: 100%; height: 100vh;-o-object-fit: cover;object-fit: cover;" src="https://naipala.com/asset/uploads/1542719095272.jpg/1359/800" alt="Second slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption">
                              <div class="text-center">
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;">
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days" >00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button class="btn custom-btn black-btn" data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 2 -->
                                        <div class="carousel-item" link="" style="position: absolute; top:0px;">
                        <img  style="width: 100%; height: 100vh;-o-object-fit: cover;object-fit: cover;" src="https://naipala.com/asset/uploads/1542719106615.jpg/1359/800" alt="Second slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption">
                              <div class="text-center">
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;">
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days" >00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button class="btn custom-btn black-btn" data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 2 -->
                                        <div class="carousel-item" link="https://www.instagram.com/pravat_moms_son_/" style="position: absolute; top:0px;">
                        <img  style="width: 100%; height: 100vh;-o-object-fit: cover;object-fit: cover;" src="https://naipala.com/asset/uploads/1542719119308.jpg/1359/800" alt="Second slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption">
                              <div class="text-center">
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;">
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days" >00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button class="btn custom-btn black-btn" data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 2 -->
                                    </div> <!-- close carousel-inner -->
    
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
    </section>
    <section id="featured-collection" class="section-padding">
        <div class="container">
            <div class="main-heading heading-padding text-center" style="font-family:Courier New;">
                <h2 class="text-uppercase"><strong>Featured Collection</strong></h2>
              
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row mt-4">
                        <div class="col-12 my-2">

                            <img src="https://naipala.com/frontend-assets/images/config_images/1542173146952.jpeg" class="img-fluid" alt="" >
                        </div>
                    </div>
                    <div class="row my-3 featured-products">
                        <div class="col-md-4 col-sm-4 col-12 my-3">
                            <section>
                                <img src="https://naipala.com/frontend-assets/images/config_images/1542173146952.jpeg" style="min-width:"alt="" class="img-fluid">
                                <div class="">
                                    <h3 class="text-uppercase">SUMMER APPAREL</h3>
                                    <p class="m-0">When summer is near, put on your gear</p>
                                    <a href="http://www.naipala.com/collection" class="">Shop Now
                                        <span> > </span>
                                    </a>

                                </div>
                            </section>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12 my-3">
                                <section>
                                    <img src="https://naipala.com/asset/frontend-assets*images*config_images/1519127435539.png/350/300" style="min-width:" alt="" class="img-fluid">
                                    <div class="">
                                        <h3 class="text-uppercase">NAIPALA GIFT CARD</h3>
                                        <p class="m-0">Get Give Change</p>
                                        <a href="http://www.naipala.com/eCards" class="">Shop Now
                                            <span> > </span>
                                        </a>

                                    </div>
                                </section>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12 my-3">
                            <section>
                                <img src="https://naipala.com/asset/frontend-assets*images*config_images/1542173819513.jpg/350/300" style="min-width:"alt="" class="img-fluid">
                                <div class="">
                                    <h3 class="text-uppercase">NAIPALA APPAREL</h3>
                                    <p class="m-0">Feel different with Naipala Apparel</p>
                                    <a href="http://www.naipala.com/collection" class="">Shop Now
                                        <span> > </span>
                                    </a>

                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 
    <!-- youtube video -->
    <section class="youtube-section" style=" background: linear-gradient(to bottom, rgba(255, 255, 255, .6), rgba(255, 255, 255, .6)), url(https://naipala.com/frontend-assets/images/config_images/1519138230387.jpg) center/cover no-repeat fixed;">
        <div class="container" style="padding: 0 1%">
            <video id="ytplayer" src="https://www.youtube.com/embed/XhkYPkzwoe0" frameborder="0" gesture="media"
                    allowfullscreen></video>
            <div class="youtube-details d-flex justify-content-between pt-4">
                <div class="container text-center">
                    <a href="https://www.youtube.com/channel/UCPOUYMY61Aq4dGM-KEY5tWg" target="_blank">
                        <span><i class="fa fa-youtube-play"></i></span>
                        More Videos
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Product section -->
    <div>
        <section class="products-section" style="padding-top:50px; padding-bottom:50px; padding-left:20px;padding-right:20px;">
            <div class="container-fluid">
                <div class="row trending">
                    <div class="col-lg-4 px-0">
                        <div class="card p-4">
                            <h2 class="card-title text-uppercase text-center">
                                <small>Trending Now</small>
                                <br> SUMMER APPARELS
                            </h2>
                            <p class="card-text text-center mb-4 d-none d-lg-block">Let&#039;s make this summer a Naipala Summer. Feel Different this summer. Cheers!</p>
                            <a href="https://naipala.com/collection" class="btn btn-custom-outline d-none d-lg-block">Shop now</a>
                        </div>
                    </div>
                    <div class="col-lg-8 d-flex align-items-center">
                        <div class="row">
                                                        <div class="col-md-4">
                                <a href="https://naipala.com/collection/item/Naipala-Summer-Cap-Muscles-56">
                                    <div class="product">
                                        <div class="product-image">
                                                                                        <img class="card-img-top" src="https://naipala.com/asset/uploads/1541920091733I0.png/290/365" alt="Naipala Naipala Summer Cap Muscles">
                                                                                    </div>
                                        <div class="product-overlay text-center">
                                            <p style="color:black"> <strong>Naipala Summer Cap Muscles</strong><br>
                                            $30</p>
                                            
                                        </div>
                                    </div>
                                </a>
                            </div>
                                                        <div class="col-md-4">
                                <a href="https://naipala.com/collection/item/Naipala-Basic-Logo-T-shirt-58">
                                    <div class="product">
                                        <div class="product-image">
                                                                                        <img class="card-img-top" src="https://naipala.com/asset/uploads/1541920392721I0.png/290/365" alt="Naipala Naipala Basic Logo T-shirt">
                                                                                    </div>
                                        <div class="product-overlay text-center">
                                            <p style="color:black"> <strong>Naipala Basic Logo T-shirt</strong><br>
                                            $35</p>
                                            
                                        </div>
                                    </div>
                                </a>
                            </div>
                                                        <div class="col-md-4">
                                <a href="https://naipala.com/collection/item/Naipala-KTM-Skyline-59">
                                    <div class="product">
                                        <div class="product-image">
                                                                                        <img class="card-img-top" src="https://naipala.com/asset/uploads/1541920519744I0.png/290/365" alt="Naipala Naipala KTM Skyline">
                                                                                    </div>
                                        <div class="product-overlay text-center">
                                            <p style="color:black"> <strong>Naipala KTM Skyline</strong><br>
                                            $60</p>
                                            
                                        </div>
                                    </div>
                                </a>
                            </div>
                                                        
    
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Naipala Story -->
        <section class="naipala-story" style="  background: -webkit-gradient(linear, left top, left bottom, from(rgba(255, 255, 255, .6)), to(rgba(255, 255, 255, .6))), url(https://naipala.com/frontend-assets/images/config_images/1535627912831.jpg) center center fixed;
            background: -webkit-linear-gradient(top, rgba(255, 255, 255, .6), rgba(255, 255, 255, .6)), url(https://naipala.com/frontend-assets/images/config_images/1535627912831.jpg) center center fixed;
            background: linear-gradient(to bottom, rgba(255, 255, 255, .6), rgba(255, 255, 255, .6)), url(https://naipala.com/frontend-assets/images/config_images/1535627912831.jpg) center center fixed;
            background-size: cover;
            min-height: 400px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center" >
            <div class="container">
                <div class="naipala-story-details">
                    <div class="story-title">
                        <h1>The Beginning of Naipala</h1>
                        <h4 class="sub-title">A Lifestyle Brand and Everything Else</h4>
                    </div>
                    <p style="font-size: 21px;">Naipala is just an online platform we made up. Somehow, we came to believe that the purpose of this is divergent, open for every. Begins with you, finishes with the world, we will make you do things differently, think differently, see the world differently and help differently. We are all capable of it. All of us.

“Feel Different” 
                    </p>
                    <div class="cta-block">
                        <a href="https://naipala.com/info/about-us" class="btn btn-custom-outline">Learn More About Naipala</a>
                    </div>
                </div>
            </div>
        </section>
    </div>


<!-- Modal Contact-->
<style>
    @media(min-width:100px){
        .contact {
            z-index: 100;
            position: fixed;
            width: 250px !important;
            height:400px !important;
            bottom: 10px;
            right: 0.25rem;
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-transform: translateY(calc(100% - 18px));
            -moz-transform: translateY(calc(100% - 18px));
            -ms-transform: translateY(calc(100% - 18px));
            transform: translateY(calc(100% - 18px));
            overflow-y:hidden!important;
        }
        .contact__trigger {
            height: 30px !important; 
            width: 90px !important;
            margin: 0 auto !important;
            background: #23272b !important;
            display: block !important;
            line-height: 30px !important;
            color: #ffffff !important;
            font-size: 0.825rem !important;
            text-align: center !important;
            font-family: 'brandon-grotesque' !important;
            font-weight: normal !important;
            font-style: normal !important;
            letter-spacing: 1px !important;
            font-weight: 500 !important;
            text-decoration: none !important;
            margin-left: 150px !important;
            text-transform: uppercase !important;
        }
        .contact__trigger:hover {
            height: 30px !important;
            width: 90px !important;
            margin: 0 auto !important;
            background: #3b3e40 !important;
            display: block !important;
            line-height: 30px !important;
            color: #ffffff !important;
            font-size: 0.825rem !important;
            text-align: center !important;
            font-family: 'brandon-grotesque' !important;
            font-weight: normal !important;
            font-style: normal !important;
            letter-spacing: 1px !important;
            font-weight: 500 !important;
            text-decoration: none !important;
            margin-left: 150px !important;
            text-transform: uppercase !important;
        }
        .contact__content {
            overflow-y:auto !important;
            padding: 0.25rem;
            background: Url(https://naipala.com/frontend-assets/images/bodybackground.jpg);
        }
        .contact--is-open {
            -webkit-transform: translateY(0);
            -moz-transform: translateY(0);
            -ms-transform: translateY(0);
            transform: translateY(0);
        }
        .contact--poke {
            -webkit-transform: translateY(calc(100% - 28px));
            -moz-transform: translateY(calc(100% - 28px));
            -ms-transform: translateY(calc(100% - 28px));
            transform: translateY(calc(100% - 28px));
        }
    
        }
    @media(min-width:300px){
    .contact {
        z-index: 100;
        position: fixed;
        width: 300px !important;
        height:460px !important;
        bottom: 10px;
        right: 0.75rem;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        -webkit-transform: translateY(calc(100% - 18px));
        -moz-transform: translateY(calc(100% - 18px));
        -ms-transform: translateY(calc(100% - 18px));
        transform: translateY(calc(100% - 18px));
    }
    .contact__trigger {
        height: 30px !important;
        width: 130px !important;
        margin: 0 auto !important;
        background: #23272b !important;
        display: block !important;
        line-height: 30px !important;
        color: #ffffff !important;
        font-size: 0.825rem !important;
        text-align: center !important;
        font-family: 'brandon-grotesque' !important;
        font-weight: normal !important;
        font-style: normal !important;
        letter-spacing: 1px !important;
        font-weight: 500 !important;
        text-decoration: none !important;
        margin-left: 170px !important;
        text-transform: uppercase !important;
    }
    .contact__trigger:hover {
        height: 30px !important;
        width: 130px !important;
        margin: 0 auto !important;
        background: #3b3e40 !important;
        display: block !important;
        line-height: 30px !important;
        color: #ffffff !important;
        font-size: 0.825rem !important;
        text-align: center !important;
        font-family: 'brandon-grotesque' !important;
        font-weight: normal !important;
        font-style: normal !important;
        letter-spacing: 1px !important;
        font-weight: 500 !important;
        text-decoration: none !important;
        margin-left: 170px !important;
        text-transform: uppercase !important;
    }
    .contact__content {
        padding: 1.25rem;
        background: Url(https://naipala.com/frontend-assets/images/bodybackground.jpg);
    }
    .contact--is-open {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
    .contact--poke {
        -webkit-transform: translateY(calc(100% - 28px));
        -moz-transform: translateY(calc(100% - 28px));
        -ms-transform: translateY(calc(100% - 28px));
        transform: translateY(calc(100% - 28px));
    }

    }
</style>
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="contact-wrapper">
                    <div class="info-pg-heading text-center">
                        <h3 class="">Contact Us</h3>
                    </div>
                    <hr class="info-page-hr" style="width: 100px; border-color:#a0a0a0;">
                    <div class="contact-details" style="font-size: 1.1rem;">
                        <p><big>✉ info@naipala.com</big></p>

<p><big>☏ 0450419803&nbsp;|&nbsp;0435372002</big></p>

<p>&nbsp;</p>

<p><big>Ashfield, NSW 2131, Australia</big></p>

<p><big>naipala.com.au</big></p>
                        <div class="share-options">
                            <ul>
                                <li><a href="https://www.facebook.com/naipala.com.au" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/naipala.com.au" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal NewsLetter -->
<div class="modal fade" id="newsletterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="https://naipala.com/subscribe">            
                <div class="modal-body">
                    <h3 class="modal-body-title text-center">Sign Up for Newsletter !</h3>
                    <input type="hidden" name="_token" value="FhMbdayiX3tZrkBKQudsaTZQD9hk70VFZbf4ykGA">
                    <div class="form-row">
                        <div class="col-2"></div>
                        <div class="col-2">
                            <label class="col-sm-2 col-form-label">Email</label>
                        </div>
                        <div class="col-6">
                            <input type="email" class="form-control" placeholder="jone@doe.com" name="email" required>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn custom-btn black-btn">Subscribe</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal invitation -->
<div class="modal fade" id="invitationModal" tabindex="-1" role="dialog" aria-labelledby="invitationModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="https://naipala.com/invite">            
                <div class="modal-body">
                    <h3 class="modal-body-title text-center">Get your Invitation!</h3>
                    <input type="hidden" name="_token" value="FhMbdayiX3tZrkBKQudsaTZQD9hk70VFZbf4ykGA">
                    <div class="form-row">
                        <div class="col-2"></div>
                        <div class="col-2">
                            <label class="col-sm-2 col-form-label">Email</label>
                        </div>
                        <div class="col-6">
                            <input type="email" class="form-control" placeholder="jone@doe.com" name="email" required>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn custom-btn black-btn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Login-->
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body custom-tabs">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                           aria-controls="home" aria-selected="true">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                           aria-controls="profile" aria-selected="false">Register</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active text-center" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h4 class=""><b>Been here before?</b></h4>
                        <form method="POST" action="https://naipala.com/login?next=https%3A%2F%2Fnaipala.com" style="padding-left:40px;padding-right:40px">
                            <input type="hidden" name="_token" value="FhMbdayiX3tZrkBKQudsaTZQD9hk70VFZbf4ykGA">
                            <div class="form-group">
                                <input type="email" class="form-control" id="loginstaticEmail" placeholder="email@example.com" name="email" value="" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="logininputPassword" placeholder="Password" name="password" required>
                            </div>
                            <div class="form-group flex-container">
                                <div class="form-check login-check" style="margin-bottom: 0;">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="None" id="rememberme" name="remember" > Remember Me
                                    </label>
                                </div>
                                <div class="login-forgot">
                                    <a href="https://naipala.com/password/reset">Forgot Password?</a>
                                </div>
                            </div>
                                <button type="submit" class="btn custom-btn">Login</button>
                           
                        </form>

                        <div class="social-login">
                            <div class="connect-with">
                                <span>OR CONNECT WITH</span>
                                <hr>
                            </div>
                            <a href="https://naipala.com/signin/facebook?next=https://naipala.com" class="btn custom-btn facebook-btn">
                                <i class="fa fa-facebook"></i>
                                FACEBOOK
                            </a>
                        </div>
                    </div>

                    <div class="tab-pane fade text-center log" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <h4 class=""><b>First time here?</b></h4>
                        <style>
                               ::-webkit-input-placeholder {
                                    text-align: center;
                                 }
                                 
                        </style>
                        <h4 class=""><b>Create your different Milieu.</b></h4>
                        <form method="POST" action="https://naipala.com/register?next=https%3A%2F%2Fnaipala.com" style="padding-left:40px;padding-right:40px">
                            <input type="hidden" name="_token" value="FhMbdayiX3tZrkBKQudsaTZQD9hk70VFZbf4ykGA">
                            <div class="form-group">
                                <input type="text" class="form-control" id="registername" placeholder="Full Name" name="name" value="" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="registerstaticEmail" placeholder="email@example.com" name="email" value="" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="registerinputPassword" placeholder="Password" name="password" required>
                              </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="registerinputRePassword" placeholder="Confirm Password" name="password_confirmation" required>
                            </div>
                            <div class="text-center" style="padding-top: 10px;">
                                <button class="btn custom-btn" type="submit">Create</button>
                            </div>
                        </form>
                        <div class="social-login">
                            <div class="connect-with">
                                <span>OR CONNECT WITH</span>
                                <hr>
                            </div>
                            <a href="https://naipala.com/signin/facebook?next=https://naipala.com" class="btn custom-btn facebook-btn">
                                <i class="fa fa-facebook"></i>
                                FACEBOOK
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal Alert -->
<div class="modal fade" id="alertpopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="alerttype"></h5>
                <button type="button" class="close" onclick="closealert()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="modal-body-title text-center" id="alerttitle"></h3>
                <br><h4 id="alertcontent" style="text-align:center"></h4><br>
                <div class="text-center">
                    <button id="doIt" hidden type="button" class="btn custom-btn black-btn" >Confirm</button>                
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="closealert()" class="btn custom-btn black-btn">Close</button>
            </div>
        </div>
    </div>
</div>


<style>
    .contact {
        z-index: 100;
        position: fixed;
        width: 300px;
        bottom: 10px;
        right: 0.75rem;
        
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        -webkit-transform: translateY(calc(100% - 18px));
        -moz-transform: translateY(calc(100% - 18px));
        -ms-transform: translateY(calc(100% - 18px));
        transform: translateY(calc(100% - 18px));
        }
    .contact__trigger {
        height: 30px;
        width: 130px;
        margin: 0 auto;
        background: #23272b;
        display: block;
        line-height: 30px;
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        font-family: 'brandon-grotesque';
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 170px;
        text-transform: uppercase;
    }
    .contact__trigger:hover {
        height: 30px;
        width: 130px;
        margin: 0 auto;
        background: #3b3e40;
        display: block;
        line-height: 30px;
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        font-family: 'brandon-grotesque';
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 170px;
        text-transform: uppercase;
    }
    .contact__content {
        padding: 1.25rem;
        background: Url(https://naipala.com/frontend-assets/images/bodybackground.jpg);
        
        
    }
    .contact--is-open {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
    .contact--poke {
        -webkit-transform: translateY(calc(100% - 28px));
        -moz-transform: translateY(calc(100% - 28px));
        -ms-transform: translateY(calc(100% - 28px));
        transform: translateY(calc(100% - 28px));
    }

</style>
<style>
    .wishlist {
        z-index: 100;
        position: fixed;
        bottom: 10px;
        left: .25rem;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        -webkit-transform: translateY(calc(100% - 18px));
        -moz-transform: translateY(calc(100% - 18px));
        -ms-transform: translateY(calc(100% - 18px));
        transform: translateY(calc(100% - 18px));
    }
    .wishlist__trigger {
        height: 30px;
        width: 130px;
        margin: 0 auto;
        background: #23272b;
        display: block;
        line-height: 30px;
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        font-family: 'brandon-grotesque';
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 15px;
        text-transform: uppercase;
    }
    .wishlist__trigger:hover {
        height: 30px;
        width: 130px;
        margin: 0 auto;
        background: #3b3e40;
        display: block;
        line-height: 30px;
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        font-family: 'brandon-grotesque';
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 15px;
        text-transform: uppercase;
    }
    .wishlist__content {
        padding: 1.25rem;
        background: Url(https://naipala.com/frontend-assets/images/bodybackground.jpg);
        height: 60vh;
        overflow-y:auto;
    }
    .wishlist--is-open {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
    .wishlist--poke {
        -webkit-transform: translateY(calc(100% - 28px));
        -moz-transform: translateY(calc(100% - 28px));
        -ms-transform: translateY(calc(100% - 28px));
        transform: translateY(calc(100% - 28px));
    }
</style>
  
<div>
    <div id="hgcontact" class="contact">
        <a href="#" class="contact__trigger">Contact</a>
        <div class="contact__content">
        
            <form method="post" action="https://naipala.com/contact_us" id="contact_form" >
                <input type="hidden" name="_token" value="FhMbdayiX3tZrkBKQudsaTZQD9hk70VFZbf4ykGA">    
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" required="" type="text" id="contactFormName" name="contact[name]">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" required="" id="contactFormEmail" name="contact[email]">
                </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea class="form-control" required="" rows="5" id="contactFormMessage" name="contact[body]"></textarea>
                </div>
                <div class="form-group">
                    <button class="btn custom-btn" type="submit" id="contactFormSubmit">Send</button>
                    <button class="btn custom-btn" type="button" id="cancelContact">Cancel</button>
                </div>
            </form>
        </div>
    </div>

    <div id="wishlist" ng-controller="WishlistController" ng-init="init()" class="wishlist">
        <input type="hidden" value="asd" id="wishlistitems" ng-click="init()"/>
        <a href="#" class="wishlist__trigger" id="wishlisttrigger">Wishlist</a>
        <div class="wishlist__content" hidden>
            <img hidden src="https://naipala.com/img/loading.gif" style="text-align:center" id="wloading" ></img>
            <section>
                <hr class="custom-hr" style="width: 100%;">
                <ul class="cart-list" style="padding-right:10px;">
                    <li >
                        
                        
                    </li>
                </ul>
                
            </section> 
        </div>
    </div>
</div>
    
</div>
</div>
        <input type="hidden" id="logoval"/>
        <footer id="footer" class="">
            <div class="container">
                <div class="footer-brand text-center">
                    <img src="https://naipala.com/frontend-assets/images/naipala_logo.png" alt="Naipala">
                </div>
                <div class="social-block text-center">
                    <a href="https://www.facebook.com/naipala.com.au/" target="_blank"><i class="fa fa-facebook-official"></i></a>
                    <a href="https://www.instagram.com/naipala.com.au/" target="_blank"><i class="fa fa-instagram"></i></a>
                    <a href="https://mail.google.com/mail/?view=cm&fs=1&to=info@naipala.com" target="_blank"><i class="fa fa-envelope"></i></a>
                </div>
                <div class="home-contacts text-center">
                <b>Powered By <a href="http://www.incubeweb.com/" target="_blank"> <strong>inCube</strong></a></b>
                </div>
            </div>
        </footer>

        <script
        src="https://naipala.com/frontend-assets/jquery-3.2.1.min.js"></script>

        <script src="https://naipala.com/frontend-assets/popper.min.js"></script>
            
        <script src="https://naipala.com/frontend-assets/bootstrap.min.js"></script>

        <!-- iScroll.js -->
        <script src="https://naipala.com/frontend-assets/iscroll.min.js"></script>
        <!-- drawer.js -->
        <script src="https://naipala.com/frontend-assets/drawer.min.js"></script>
       
        <script src="https://naipala.com/frontend-assets/scripts/app.js"></script>
        <script>
             
            $(document).ready(function(){
                var h=$('#logoscroll').css('height');
                var w=$('#logoscroll').css('width');
                $('#logoval').attr('height',h);
                $('#logoval').attr('width',w);
            });
            $(window).scroll(function() {
                $(window).scrollTop() > 60 ? ($("#mainNav").css("background", "rgba(70, 65, 65, 0.35)"), $("#mainNav .navbar-brand > img").css({
                    width: "100px",
                    height: "40px"
                })) : ($("#mainNav").css("background", "transparent"), $("#mainNav .navbar-brand > img").css({
                    width: $('#logoval').attr('width'),
                    height:$('#logoval').attr('height')
                }));
            });
                $(".contact__trigger").click(function() {
                    var $contact = $('#hgcontact').eq(0);
                    $contact.toggleClass("contact--is-open");
                    $contact.removeClass("contact--poke");
                    return false;
                });
            
                $('#cancelContact').click(function(){
                    $('.contact__trigger').trigger('click');
                })
             
                $(".wishlist__trigger").click(function() {
                    var $contact = $(this).parents(".wishlist").eq(0);
                    var k =0
                    if($('.wishlist__content').attr('hidden'))
                    {    
                        k=1;
                        $('.wishlist__content').removeAttr('hidden');
                    }
                    $contact.toggleClass("wishlist--is-open");
                    if(k==0)
                    {
                        window.setTimeout(function(){$('.wishlist__content').attr('hidden','true');
                    }, 500);                        
                        
                    }
                        
                    $contact.removeClass("wishlist--poke");
                    return false;
                });
           $('#optioninfo').click(function(){
               
               $('#infoclick').trigger('click');
           });
           
            </script>
        <script type="text/javascript">
            if (window.location.hash && window.location.hash == '#_=_') {
                window.location.hash = '';
            }
        </script>
                <script src="https://naipala.com/admin-assets/js/angular.min.js"></script>

        <script type="text/javascript">
            

            function aalert(content,type,title){
                $('#alerttype').html(type);
                $('#alerttitle').html(title);
                $('#alertcontent').html(content);
                $('#alertpopup').modal('show');
            }

            function closealert(){
                $('#alerttitle').empty();
                $('#alertcontent').empty();
                $('#doIt').attr('hidden','true');
                $('#alertpopup').modal('hide');
            }

        </script>
        
        <script src="https://naipala.com/frontend-assets/cart.js" defer></script>
        <script src="https://naipala.com/frontend-assets/wishlist.js" defer></script>
            
    <script>
    
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  
        // Replace the 'ytplayer' element with an <iframe> and
        // YouTube player after the API code downloads.
        var player;
        var id=$('#ytplayer').attr('src').split('/')[4];
        function onYouTubePlayerAPIReady() {
          player = new YT.Player('ytplayer', {
            height: '390',
            width: '640',
            playerVars : {
                  autoplay : 0
              },
            videoId: id
          });
        }
  
        $(window).scroll(function() {
            $("#ytplayer").each( function() {
                if(($(window).scrollTop() > $(this).offset().top - 200) && ($(window).scrollTop() < $(this).offset().top + $('#ytplayer').height())) {
                    player.playVideo();
                } else {
                    player.pauseVideo();
                }
            }); 
        });
    </script>
    
    <script>
        $(document).ready(function(){
            var countDownDate = new Date("Dec 21, 2018 00:00:00").getTime();
            
            // Update the count down every 1 second
            var x = setInterval(function() {
            
            // Get todays date and time
            var now = new Date().getTime();
            
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            // Display the result in the element with id="demo"

            if(days<10)
                days='0' +days.toString();
            if(hours<10)
                hours='0'+hours.toString();
            if(minutes<10)
                minutes='0'+minutes.toString();
            
            $('.count_days').html(days);
            $('.count_hours').html(hours);
            $('.count_minutes').html(minutes);

            // document.getElementById("demo").innerHTML = days + "d " + hours + "h "
            // + minutes + "m " + seconds + "s ";
            
            // If the count down is finished, write some text 
            if (distance < 0) {
                clearInterval(x);
                // document.getElementById("demo").innerHTML = "EXPIRED";
                $('.timer_caption').attr('hidden','true');
            }
            }, 1000);
        })
            // Set the date we're counting down to
            
    </script>
        
       </body>
</html>
     
