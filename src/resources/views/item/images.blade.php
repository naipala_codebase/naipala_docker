@extends('layouts.admin')
@section('body')
<div class="content">
  <div class="container-fluid">
    <div class="card ">
      <div class="header">
        <a href="{{URL::to('admin/item#items')}}" class="btn btn-info btn-fill btn-md  pull-right" >Back</a>
        <h4 class="title">{{$item_name}}</h4>
        <p class="category">Add or remove images</p>

      </div>

     	
      <div class="content">
        @foreach($colors as $id=>$images)

          <h3>{{ $id }} {{$item_name}} - Images</h3>
          <div class="row">

            @foreach($images as $i)
              {{--  @if ($loop->first) @continue @endif  --}}
              <div class="col-md-3 col-sm-12 col-xs-12">
                <div height="200px" width="200px">
                    <div class="panel-body">
                      <img src="{{asset('uploads'.'/'.$i->image_link)}}" height="200px" width="200px"/>
                    </div>
                </div>
                <form action="{{URL::to('admin/item/removeimage')}}" method="POST">
                  <input type="hidden" value="{{$i->image_id}}" name="image_id"/>
                    <input type="hidden" value="{{$item_id}}" name="item_id"/>
                    {{ csrf_field() }}
                    &nbsp;&nbsp;&nbsp;	<input type="button" id="deleteImage" value="Remove" class="btn btn-danger btn-fill btn-xs"/>
                                        
                </form>
                                        <br>
                                        <br>
              </div>
            @endforeach
          </div>

          <br>
          <br>
          {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/item/addimage','files'=>'true'])!!}
              

              <input type="file" name="files[]" class="btn btn-info btn-xs" value="Add Images" required border="0px" multiple/>
              <input type="hidden" value="{{$item_id}}" name="id"/>
              <input type="hidden" value="{{$id}}" name="color"/>
              <input type="submit" class="btn btn-success btn-fill btn-sm" value="Upload" />
              <span>Click to Upload</span>
          {!! Form::close() !!}
          <br>
          <hr>
        @endforeach
      </div>	
    </div>
  </div>
</div>
@stop