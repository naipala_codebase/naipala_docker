@extends('layouts.admin')
@section('body')
<div class="content">
  <div class="container-fluid">
<div class="card ">
    <div class="header">
    	<h4 class="title">All reviews</h4>
    	<p class="category">Remove unwanted reviews</p>
    </div>
    <div class="content">
        <div class="table-full-width">
            <table class="table">
                <thead>
                	<th>Reviewer</th>
                	<th>Review</th>
                	<th>Item</th>
                	<th>Actions</th>
                </thead>
                <tbody>
                	@foreach($reviews as $r)
                    <tr>
                    	<td>{{$r->review_name}}</td>
                        <td>{{$r->review_description}}</td>
                        <td>{{$r->item->item_name}}</td>
                        <td class="td-actions">
                       
                        <form method="POST" action="/admin/review/{{ $r->review_id }}">
                            <input name="_method" type="hidden" value="DELETE">
                            {{ csrf_field() }}
                            <button type="button" rel="tooltip" id="deleteReview" title="Remove" class="btn btn-danger btn-simple btn-md">
                                <i class="fa fa-times"></i>
                            </button>
                        </form>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>

        <div class="footer">
            <hr>
            <div class="stats">
                <i class="fa fa-history"></i> Remove undesired reviews
            </div>
        </div>
    </div>
</div>
</div>
</div>
@endsection