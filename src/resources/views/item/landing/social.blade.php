@extends('layouts.app')

@section('body')
<div id="social-page">
    <section class="container">
        <div class="row">
            <div class="col-12 social-wrapper pg-top">
                <div class="info-pg-heading text-center">
                    <br>
                    <h2 style="font-family:Courier New;"><b>Social</b></h2>
                    <nav aria-label="breadcrumb" role="navigation">
                        @if(count($sociallinks) > 0)
                            <ol class="breadcrumb" style="margin-bottom: 0px;">
                                @foreach($sociallinks as $s)
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{$s->config_value}}" target="_blank">{{$s->config_key}}</a></li>
                                @endforeach
                               
                            </ol>
                        @endif
                    </nav>
                </div>
            </div>
        </div>
        <div class="row info-pg-container">
        	@foreach($socials as $s)
            <div class="col-12 social-wrapper pg-bottom">
                <div class="info-pg-heading">
                    <h1>{{$s->social_title}}</h1>
                </div>
                <div class="social-pg-details">
                    <div class="row info-img-container ">
                    	@foreach($s->socialimages as $i)
	                        <div class="col-sm-4 info-img"><img src="{{asset('/uploads'.'/'.$i->image_link)}}" alt=""></div>
                     	@endforeach
                     </div>
                    <div class="social-terms">
                        {!! $s->social_description !!}
                    </div>
                </div>
            </div>
            @endforeach
        </div>

    </section>

</div>

@endsection