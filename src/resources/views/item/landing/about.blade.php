@extends('layouts.app')

@section('body')
<div id="about-us">
    <section class="container info-pg-container">
        <div class="row">
            <div class="col-12 aboutUs-wrapper">
                <div class="info-pg-heading">
                    <h1>{{$info->config_key}}</h1>
                </div>
                <hr class="info-page-hr">
                <div class="aboutUs-details">
                    {!! $info->config_value !!}
                    
                    @if( $info->config_alias == 'terms-and-condition')
                        <span id="siteseal" style="float:right"><script async type="text/javascript" src="https://seal.godaddy.com/getSeal?sealID=mQ62pNLBgyssZNARSi58c96UezzxWT6AhBdAbtAc00Pjuw7j0puxZMCsMdGB"></script></span>
                    @endif
                </div>
            </div>
        </div>
    </section>
</div>

@endsection