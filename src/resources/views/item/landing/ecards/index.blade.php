@extends('layouts.app')

@section('body')
<style>
    
    /* The Modal (background) */
    .modal1 {
        display: none; /* Hidden by default */
        position: fixed; /* Stay in place */
        z-index: 1000000; /* Sit on top */
        left: 0;
        top: 0;
        padding-top:5%;
        width: 100%; /* Full width */
        height: 100%; /* Full height */
        overflow: auto; /* Enable scroll if needed */
        background-color: rgb(0,0,0); /* Fallback color */
        background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
    }
    
    /* modal1 Content (image) */
    .modal1-content {
        margin: auto;
        display: block;
        width: 80%;
        
        max-height: 531px;
        max-width: 700px;
    }
    
    /* Caption of modal1 Image */
    #caption {
        margin: auto;
        display: block;
        width: 80%;
        max-width: 700px;
        text-align: center;
        color: #ccc;
        padding: 10px 0;
        height: 150px;
    }
    
    /* Add Animation */
    .modal1-content, #caption {    
        -webkit-animation-name: zoom;
        -webkit-animation-duration: 0.6s;
        animation-name: zoom;
        animation-duration: 0.6s;
    }
    
    @-webkit-keyframes zoom {
        from {-webkit-transform:scale(0)} 
        to {-webkit-transform:scale(1)}
    }
    
    @keyframes zoom {
        from {transform:scale(0)} 
        to {transform:scale(1)}
    }
    
    /* The close1 Button */
    .close1 {
        position: absolute;
        top: 15px;
        right: 35px;
        color: #f1f1f1;
        font-size: 40px;
        font-weight: bold;
        transition: 0.3s;
    }
    
    .close1:hover,
    .close1:focus {
        color: #bbb;
        text-decoration: none;
        cursor: pointer;
    }
    
    /* 100% Image Width on Smaller Screens */
    @media only screen and (max-width: 700px){
        .modal1-content {
            width: 100%;
        }
    }

</style>
    <div id="single-product">
        <section class="container">
            <div class="row sg-product-section">
                <div class="col-lg-7 col-md-6 sg-main-image" id="flexdiv" style="padding: 31px;">

                    <!--flex-slider one-->
                    <div class="flexslider">
                        <ul class="slides">
                            <li data-thumb="{{ asset('frontend-assets/images/gc.png') }}">
                                <div class='thumb-image'>
                                    <img src="{{ asset('frontend-assets/images/gc.png') }}" class='img-responsive test' style='cursor:pointer'>
                                </div>
                            </li>
                            <li data-thumb="{{ asset('frontend-assets/images/gc1.png') }}">
                                <div class='thumb-image'>
                                    <img src="{{ asset('frontend-assets/images/gc1.png') }}" class='img-responsive test' style='cursor:pointer'>
                                </div>
                            </li>
                            <li data-thumb="{{ asset('frontend-assets/images/gc2.png') }}">
                                <div class='thumb-image'>
                                    <img src="{{ asset('frontend-assets/images/gc2.png') }}" class='img-responsive test' style='cursor:pointer'>
                                </div>
                            </li>
                            <li data-thumb="{{ asset('frontend-assets/images/gc3.png') }}">
                                <div class='thumb-image'>
                                    <img src="{{ asset('frontend-assets/images/gc3.png') }}" class='img-responsive test' style='cursor:pointer'>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <span>
                        <a target="_blank" href="{{ URL::to('giftcard') }}">E-GiftCard <i class="fa fa-info-circle"></i></a>
                    </span>                                                 
                </div>
                <div class="col-lg-5 col-md-6 sg-product-details">
                    <div class="sg-product-title">
                        <div class="sg-header">
                            <h1>Naipala E-Gift Card</h1>
                        </div>
                        <hr>
                    </div>

                    <div class="sg-product-info">
                        <form method="POST" action="{{ route('ecards.initial') }}">
                            {{ csrf_field() }}
                            
                            @if(count($errors)>0)
                                <p style="color:#c61616">Please Provide all the Information</p>
                            @endif
                            <div class="form-row">
                                <div class="form-group col-10">
                                    <label><strong>Amount</strong></label><br>
                                    <select class="custom-select custom-select-option {{ $errors->has('amount') ? 'border-error' : '' }}"  style="-webkit-appearance:menulist; -moz-appearance:menulist; appearance:menulist;{{ $errors->has('amount') ? 'border-color:#de1111bd' : 'border:0' }};" name="amount" required>
                                        <option selected disabled>Choose Amount</option>
                                        <option value="25">$25</option>
                                        <option value="50">$50</option>
                                        <option value="100">$100</option>
                                        <option value="150">$150</option>
                                        <option value="200">$200</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-10">
                                    <label><strong>Recipient Name</strong></label>
                                    <input type="text" class="form-control {{ $errors->has('recipientName') ? 'border-error' : '' }}" name="recipientName" value="{{ old('recipientName') }}" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-10">
                                    <label><strong>Sender Name</strong></label>
                                    <input type="text" class="form-control {{ $errors->has('senderName') ? 'border-error' : '' }}" name="senderName" value="{{ old('senderName') }}" required>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-10">
                                    <label><strong>Message</strong></label>
                                    <textarea class="form-control {{ $errors->has('message') ? 'border-error' : '' }}" name="message" rows="6" required>{{ old('message') }}</textarea>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-10">
                                    <button type="submit" class="btn custom-btn sg-addToCart">Proceed</button>
                                </div>
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="mymodal" class="modal1">
        <span class="close1">&times;</span>
        <img class="modal1-content" src ="" id="img01">
        <div id="caption"></div>
    </div>
@endsection

@section('js')

    <script>
        $(document).ready(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails",
                slideshow: false,
                start: function(){
                    $('#flexnav').attr('style','opacity:1');
                }
            });
        });

        $('.flexslider').on('click','img.test',function(){
            var image = $(this).attr('src');
            $('#img01').attr('src',image);
            
            $('.modal1').attr('style','display:block');
        });

        $('.close1').click(function(){
            $('#img01').attr('src','');
            $('.modal1').attr('style','display:none');
        });
        
    </script>
    
@endsection