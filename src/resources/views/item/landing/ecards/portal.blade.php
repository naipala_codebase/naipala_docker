@extends('layouts.app')

@section('body')
<div id="single-product">
        <section class="container">
            <div class="row sg-product-section">
                <div class="col-lg-5 col-md-6 sg-main-image">
                    <div style="text-align: center">
                        <img src="{{ asset('frontend-assets/images/gc.png') }}" class='img-responsive'>
                    </div>
                </div>
                <div class="col-lg-7 col-md-6 sg-product-details">
                    <div class="sg-product-title">
                        <div class="sg-header">
                            <h1>Naipala E-Gift Card</h1>
                        </div>
                        <hr>
                    </div>

                    <div class="sg-product-info">
                            <label><strong>Amount: </strong>${{ $etransaction->amount }}</label><br>
                            <label><strong>Sender Name: </strong>{{ $etransaction->sender_name }}</label><br>
                            <label><strong>Recipient Name: </strong>{{ $etransaction->recipient_name }}</label><br>
                            <label><strong>Recipient Email: </strong>{{ $etransaction->recipient_contact }}</label><br>
                            <label><strong>Message: </strong>
                            <text>{!! $etransaction->message !!}</text></label><br>
                            <label><strong>Delivery Date: </strong>{{ date('M j, Y', strtotime($etransaction->delivery_date)) }}</label><br>
                            <label style="text-transform: capitalize;"><strong>Gift Card Status: </strong>{{ $etransaction->giftCard->status }}</label><br>
                            
                            @if(new \Carbon\Carbon($etransaction->delivery_date) <= \Carbon\Carbon::today() && $etransaction->giftCard->status=='unused')
                                <form action="{{ route('ecards.resend') }}" method="POST">
                                    {{ csrf_field() }}
                                    <button type="submit" title="Resend the gift-card email to the receiver." class="btn custom-btn sg-addToCart">Resend Gift Card Email</button>
                                </form>
                            @endif
                            
                        </form>                              
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

@section('js')

@endsection
