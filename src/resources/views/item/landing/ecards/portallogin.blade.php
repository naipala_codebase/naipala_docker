@extends('layouts.app')

@section('body')
<div id="login">
    <section class="container my-5" style="max-width:900px">
        <h4 class="text-center"><b>E-Gift Card Portal</b></h4>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                <hr>
                <form style="padding-left: 60px;padding-right: 60px;" method="POST" action="{{ route('ecards.portallogin') }}">
                    {{ csrf_field() }}
                    
                    @if (count($errors->all())>0)
                        <p style="margin-top: 10px;">
                            <strong>The credentials are incorrect.</strong>
                        </p>
                    @endif
                    <div class="form-group">
                        <input type="email" class="form-control"  style="background:#e8e8e8;"  placeholder="Sender Email" name="email" value="{{ old('email') }}" required autofocus>
                    </div>
                    <br>
                    <div class="form-group">
                        <input type="text" class="form-control" style="background:#e8e8e8;" placeholder="Your Token" name="token" required>
                    </div>
                    <div class="text-center">
                        <button class="btn custom-btn" type="submit">Submit</button>
                    </div>
                    <hr>
                </form>
            </div>
            <div class="col-3"></div>
            {{--  <div class="col-lg-6 col-md-6 col-xs-12 text-center">
                <hr>
                <h4 class=""><b>Some info</b></h4>
              
                
                <hr>
            </div>  --}}
        </div>
    </section>
</div>

@endsection
