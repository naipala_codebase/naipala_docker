@extends('layouts.app')

@section('css')
<style>
        .carousel-fade .carousel-item {
            display: block;
            position: relative;
            opacity: 0;
            transition: opacity .75s ease-in-out;
             
          } 
          .carousel-fade .carousel-item.active {
            opacity: 1;
            display: block;
               
          }
          .carousel {
            position: relative;
            top: 0;
            left: 0;
           
          }
          .carousel-inner {
                height:100vh;
          }
          .carousel-item {
            opacity .5;
          }
          a.carousel-control-next:link, a.carousel-control-prev:link {
            background-color: transparent;
            text-decoration: none;
            opacity: .5;
          }
          a.carousel-control-next:hover, a.carousel-control-prev:hover  {
            background-color: transparent;
            text-decoration: none;
            opacity: 1;
          }
          .carousel-control-next-icon, .carousel-control-prev-icon {
            position:relative;
            background-image: none;
          }    
          .carousel-control-next-icon:before {
            top:0;
            left:-5px;
            padding-right:10px;
           }
          .carousel-control-prev-icon:before {
            top:0;
            left:-5px;
            padding-right:10px;
          }
          .sidebar {
            background-color: #e1e1e1;
          }
          
          .naipala-story {
          
        }
        @font-face {
            font-family: sketchblock;
            src: url(Sketch_Block.ttf);
        }
        @font-face {
            font-family: futuramedium;
            src: url(Futura_Medium.otf);
        }
        @media screen and (min-width: 922px){
        .naipala-caption{
            top:20%;
            right:15%;
            left:15%;
            margin:0;
            margin-bottom:4rem;
            background:transparent;
            color:#d0d5db;
        }
        .naipala-countdown-card{
            margin: 1rem 2rem;
            background: #bdbcbab0;
            border-radius: 12px;
        }
    
        .naipala-countdown-time{
            line-height: 5rem;
            font-size: 4rem;
            margin: 0 2rem;
            color: #23272bb8;
            font-weight: 600;
            font-family: futuramedium;
            margin-top: 1rem;
        }
        .naipala-countdown-span{
            color: #23272bb8;
            margin-bottom: 0.5rem;
            font-weight:600;
            font-family: futuramedium;
            font-size:1rem;
        }
        .naipala-countdown-text{
            font-family: sketchblock;
            color:#c7c2bf;
            font-size:5rem !important;
        }
        .slider_height{
            height:100vh;
        }
    }

    @media screen and (min-width:600px) and (max-width:922px){
        .naipala-caption{
        top:20%;
        right:15%;
        left:15%;
        margin:0;
        margin-bottom:4rem;
        background:transparent;
        color:#d0d5db;
    }
    .naipala-countdown-card{
        margin: 1rem 2rem;
        background: #bdbcbab0;
        border-radius: 12px;
        height: 6.5rem;
        width: 7rem;
    }
    
        .naipala-countdown-time{
            line-height: 5rem;
            font-size: 2.5rem;
            /* margin: 0 2rem; */
            color: #23272bb8;
            font-weight: 600;
            font-family: futuramedium;
            /* margin-top: 1rem; */
        }
        .naipala-countdown-span{
            color: #23272bb8;
            /* margin-bottom: 0.5rem; */
            font-weight: 600;
            font-family: futuramedium;
            font-size: 1.2rem;
            line-height: 0.1rem;
        }
        .naipala-countdown-text{
            font-weight: 600;
            font-family: sketchblock;
            color:#c7c2bf;
            font-size:5rem !important;
        }    
        .slider_height{
            height:20rem;
        }
    }
     

    @media screen and (min-width:200px) and (max-width:600px){
        .naipala-caption{
        top:20%;
        right:15%;
        left:15%;
        margin:0;
        margin-bottom:4rem;
        background:transparent;
        color:#d0d5db;
    }
    .naipala-countdown-card{
        margin: 0rem 6px;
        background: #bdbcbab0;
        border-radius: 12px;
        height: 3.5rem;
        width: 5rem;
    }
    
        .naipala-countdown-time{
            line-height: 1.6rem;
            font-size: 1.3rem;
            /* margin: 0 2rem; */
            color: #23272bb8;
            font-weight: 600;
            font-family: futuramedium;
            margin-top: 0.3rem;
        }
        .naipala-countdown-span{
            color: #23272bb8;
            /* margin-bottom: 0.5rem; */
            font-weight: 600;
            font-family: futuramedium;
            font-size: 0.6rem;
            line-height: 1.2rem;
        }
        .naipala-countdown-text{
            font-weight: 600;
            font-family: sketchblock;
            color:#c7c2bf;
            font-size:2.5rem !important;
        }   
        .slider_height{
            height:15rem;
        }
    }
    
    
        
</style>
@stop

@section('body')

<div id="wrapper-landing">
    <!-- homeslider -->
    <section id="landing-home">

        <div id="carousel" class="carousel carousel-fade slider_height" data-ride="carousel">
                <div class="carousel-inner" role="listbox" >
                    <div class="carousel-item active" link="{{$landingimages->first()->redirect_link}}">
                        <img style= "width: 100%; max-height: 100vh; -o-object-fit: cover;object-fit: cover;"  src="{{ route('optimize', ['uploads',$landingimages->first()->image_link,1360,800]) }}" alt="First slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption" >
                            <div class="text-center" hidden>
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;"  hidden>
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days">00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button class="btn custom-btn black-btn" hidden data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 1 -->
                    @foreach($landingimages->slice(1) as $l)
                    <div class="carousel-item" link="{{$l->redirect_link}}" style="position: absolute; top:0px;">
                        <img  style="width: 100%; max-height:100vh;-o-object-fit: cover;object-fit: cover;" src="{{ route('optimize', ['uploads',$l->image_link,1360,800]) }}" alt="Second slide">
                        <div class="carousel-caption naipala-caption d-md-block text-center timer_caption" hidden>
                              <div class="text-center" hidden>
                                <h3 class="naipala-countdown-text">Get Ready To</h3>
                                <h3 class="naipala-countdown-text"> Feel Different</h3>
                            </div>
                                <div class="container" style="display:flex;justify-content:center;padding:1rem 0rem;" hidden>
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_days" >00</span>
                                        <span class="naipala-countdown-span">Days</span>

                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_hours">00</span>
                                        <span class="naipala-countdown-span">Hours</span>
                                    </div>
                                
                                    <div class="card naipala-countdown-card">
                                        <span class="naipala-countdown-time count_minutes">00</span>
                                        <span class="naipala-countdown-span">Minutes</span>
                                    </div>
                                </div>
                                <button hidden class="btn custom-btn black-btn" data-toggle="modal" data-target="#invitationModal">Get Your Invitation</button>
                                       
                        </div>
                    </div> <!-- close carousel-item 2 -->
                    @endforeach
                </div> <!-- close carousel-inner -->
    
                <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
    </section>
    <section id="featured-collection" class="section-padding">
        <div class="container">
            <div class="main-heading heading-padding text-center" style="font-family:Courier New;">
                <h2 class="text-uppercase"><strong>Featured Collection</strong></h2>
              
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="row mt-4">
                        <div class="col-12 my-2">

                            <img src="{{ asset('frontend-assets/images/config_images/'.$landingconfigs->where('config_key','Featured_Image')->first()->config_value) }}" class="img-fluid" alt="" >
                        </div>
                    </div>
                    <div class="row my-3 featured-products">
                        <div class="col-md-4 col-sm-4 col-12 my-3">
                            <section>
                                <img src="{{ route('optimize', ['frontend-assets*images*config_images',$landingconfigs->where('config_key','First_Featured_Image')->first()->config_value,350,300]) }}" style="min-width:"alt="" class="img-fluid">
                                <div class="">
                                    <h3 class="text-uppercase">{{ $landingconfigs->where('config_key','First_Featured_Title')->first()->config_value }}</h3>
                                    <p class="m-0">{{ $landingconfigs->where('config_key','First_Featured_Subtitle')->first()->config_value }}</p>
                                    <a href="{{ $landingconfigs->where('config_key','First_Featured_Link')->first()->config_value }}" class="">Shop Now
                                        <span> > </span>
                                    </a>

                                </div>
                            </section>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12 my-3">
                                <section>
                                    <img src="{{ route('optimize', ['frontend-assets*images*config_images',$landingconfigs->where('config_key','Second_Featured_Image')->first()->config_value,350,300]) }}" style="min-width:" alt="" class="img-fluid">
                                    <div class="">
                                        <h3 class="text-uppercase">{{ $landingconfigs->where('config_key','Second_Featured_Title')->first()->config_value }}</h3>
                                        <p class="m-0">{{ $landingconfigs->where('config_key','Second_Featured_Subtitle')->first()->config_value }}</p>
                                        <a href="{{ $landingconfigs->where('config_key','Second_Featured_Link')->first()->config_value }}" class="">Shop Now
                                            <span> > </span>
                                        </a>

                                    </div>
                                </section>
                        </div>
                        <div class="col-md-4 col-sm-4 col-12 my-3">
                            <section>
                                <img src="{{ route('optimize', ['frontend-assets*images*config_images',$landingconfigs->where('config_key','Third_Featured_Image')->first()->config_value,350,300]) }}" style="min-width:"alt="" class="img-fluid">
                                <div class="">
                                    <h3 class="text-uppercase">{{ $landingconfigs->where('config_key','Third_Featured_Title')->first()->config_value }}</h3>
                                    <p class="m-0">{{ $landingconfigs->where('config_key','Third_Featured_Subtitle')->first()->config_value }}</p>
                                    <a href="{{ $landingconfigs->where('config_key','Third_Featured_Link')->first()->config_value }}" class="">Shop Now
                                        <span> > </span>
                                    </a>

                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
 
    <!-- youtube video -->
    <section class="youtube-section" style=" background: linear-gradient(to bottom, rgba(255, 255, 255, .6), rgba(255, 255, 255, .6)), url({{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Video_Image')->first()->config_value )}}) center/cover no-repeat fixed;">
        <div class="container" style="padding: 0 1%">
            <video id="ytplayer" src="{{ $landingconfigs->where('config_key','Video_Link')->first()->config_value }}" frameborder="0" gesture="media"
                    allowfullscreen></video>
            <div class="youtube-details d-flex justify-content-between pt-4">
                <div class="container text-center">
                    <a href="https://www.youtube.com/channel/UChPpeQFBtFbQPvd5029GC3Q?fbclid=IwAR1cEcOY0S9ukpt6i4iHUVj2pckI6ndx09K_Di9KR9UUoc54n9JwTkCDzA0" target="_blank">
                        <span><i class="fa fa-youtube-play"></i></span>
                        More Videos
                    </a>
                </div>
            </div>
        </div>
    </section>

    <!-- Product section -->
    <div>
        <section class="products-section" style="padding-top:50px; padding-bottom:50px; padding-left:20px;padding-right:20px;">
            <div class="container-fluid">
                <div class="row trending">
                    <div class="col-lg-4 px-0">
                        <div class="card p-4">
                            <h2 class="card-title text-uppercase text-center">
                                <small>Trending Now</small>
                                <br> {{ $landingconfigs->where('config_key','Trending_Title')->first()->config_value }}
                            </h2>
                            <p class="card-text text-center mb-4 d-none d-lg-block">{{ $landingconfigs->where('config_key','Trending_Description')->first()->config_value }}</p>
                            <a href="{{ URL::to('collection') }}" class="btn btn-custom-outline d-none d-lg-block">Shop now</a>
                        </div>
                    </div>
                    <div class="col-lg-8 align-items-center">
                        <div class="row">
                            @foreach($final_featureditems as $f)
                            <div class="col-md-4">
                                <a href="{{ route('collection.itemDetail',$f->item_alias) }}">
                                    <div class="product">
                                        <div class="product-image">
                                            @if($f->item_images->count()<=0)
                                            <img class="card-img-top" height="200px" src="{{asset('/frontend-assets/images/purse.png')}}" alt="Naipala {{$f->item_name}}">
                                            @else
                                            <img class="card-img-top" src="{{ route('optimize', ['uploads',$f->item_images->first()->image_link,290,290]) }}" alt="Naipala {{$f->item_name}}">
                                            @endif
                                        </div>
                                        <div class="product-overlay text-center">
                                            <p style="color:black"> <strong>{{$f->item_name}}</strong><br>
                                            ${{$f->item_price}}</p>
                                            
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                            
    
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- Naipala Story -->
        <section class="naipala-story" style="  background: -webkit-gradient(linear, left top, left bottom, from(rgba(255, 255, 255, .6)), to(rgba(255, 255, 255, .6))), url({{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Story_Image')->first()->config_value) }}) center center fixed;
            background: -webkit-linear-gradient(top, rgba(255, 255, 255, .6), rgba(255, 255, 255, .6)), url({{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Story_Image')->first()->config_value) }}) center center fixed;
            background: linear-gradient(to bottom, rgba(255, 255, 255, .6), rgba(255, 255, 255, .6)), url({{asset('/frontend-assets/images/config_images'.'/'.$landingconfigs->where('config_key','Story_Image')->first()->config_value) }}) center center fixed;
            background-size: cover;
            min-height: 400px;
            display: -webkit-box;
            display: -ms-flexbox;
            display: flex;
            -webkit-box-align: center;
            -ms-flex-align: center;
            align-items: center" >
            <div class="container">
                <div class="naipala-story-details">
                    <div class="story-title">
                        <h1>{{ $landingconfigs->where('config_key','Story_Title')->first()->config_value }}</h1>
                        <h4 class="sub-title">{{ $landingconfigs->where('config_key','Story_Subtitle')->first()->config_value }}</h4>
                    </div>
                    <p style="font-size: 21px;">{{ $landingconfigs->where('config_key','Story_Description')->first()->config_value }} 
                    </p>
                    <div class="cta-block">
                        <a href="{{ URL::to('info/about-us') }}" class="btn btn-custom-outline">Learn More About Naipala</a>
                    </div>
                </div>
            </div>
        </section>
    </div>


@stop


@section('js')
    {{--  <script>
        $('#carousel').click(function(){
            console.log($('.carousel-item.active').attr('link'));
            var link=$('.carousel-item.active').attr('link');
        
        window.open(link,'_blank');
        });
    </script>  --}}
    <script>
    
        var tag = document.createElement('script');
        tag.src = "https://www.youtube.com/player_api";
        var firstScriptTag = document.getElementsByTagName('script')[0];
        firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
  
        // Replace the 'ytplayer' element with an <iframe> and
        // YouTube player after the API code downloads.
        var player;
        var id=$('#ytplayer').attr('src').split('/')[4];
        function onYouTubePlayerAPIReady() {
          player = new YT.Player('ytplayer', {
            height: '390',
            width: '640',
            playerVars : {
                  autoplay : 0
              },
            videoId: id
          });
        }
  
        $(window).scroll(function() {
            $("#ytplayer").each( function() {
                if(($(window).scrollTop() > $(this).offset().top - 200) && ($(window).scrollTop() < $(this).offset().top + $('#ytplayer').height())) {
                    player.playVideo();
                } else {
                    player.pauseVideo();
                }
            }); 
        });
    </script>
    
    <script>
        $(document).ready(function(){
            var countDownDate = new Date("Dec 21, 2018 00:00:00").getTime();
            
            // Update the count down every 1 second
            var x = setInterval(function() {
            
            // Get todays date and time
            var now = new Date().getTime();
            
            // Find the distance between now and the count down date
            var distance = countDownDate - now;
            
            // Time calculations for days, hours, minutes and seconds
            var days = Math.floor(distance / (1000 * 60 * 60 * 24));
            var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
            var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
            var seconds = Math.floor((distance % (1000 * 60)) / 1000);
            
            // Display the result in the element with id="demo"

            if(days<10)
                days='0' +days.toString();
            if(hours<10)
                hours='0'+hours.toString();
            if(minutes<10)
                minutes='0'+minutes.toString();
            
            $('.count_days').html(days);
            $('.count_hours').html(hours);
            $('.count_minutes').html(minutes);

            // document.getElementById("demo").innerHTML = days + "d " + hours + "h "
            // + minutes + "m " + seconds + "s ";
            
            // If the count down is finished, write some text 
            if (distance < 0) {
                clearInterval(x);
                // document.getElementById("demo").innerHTML = "EXPIRED";
                $('.timer_caption').attr('hidden','true');
            }
            }, 1000);
        })
            // Set the date we're counting down to
            
    </script>
@endsection
