<!-- Modal Service Apply -->
<div class="modal fade" id="serviceApplyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> You are not signed in?</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="modal-body-title text-center">Enter your Details</h3>
                <form method='POST' action="{{ route('guest.serviceBook') }}">
                    {{ csrf_field() }}
                    <input type='hidden' name='service_id' id='GuestInfo_serviceId' value=''/>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <input type="text" class="form-control" id="name" placeholder="Enter Name" name="name" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <input type="email" class="form-control" id="email" placeholder="Enter Email" name="email" required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <input type="text" class="form-control" id="contact" placeholder="Enter Contact" name="phone" required>
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn custom-btn" type="submit">Submit</button>
                    </div>
                </form>
                <div class="social-login">
                    <div class="connect-with">
                        <span style="top:0;">OR</span>
                        <hr>
                        <br>
                        <button class="btn btn-custom-outline" onclick="loginToBook()">Have an account?</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="serviceExtraInfoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"> Provide Additional Info? </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form id="ServiceMsgForm" enctype="multipart/form-data" method='POST' action="/services/book/addMessage">
                    {{ csrf_field() }}
                    <input type='hidden' name='book_id' id='book_id' value=''/>
                    <div class="form-row">
                        <div class="form-group col-12">
                            
                            <textarea rows="6" id="message" class="form-control" placeholder="Leave a Message?" name="message" required></textarea>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-12">
                            <label for="cv">Leave your CV(pdf,doc,rtf)? (optional)</label>
                            <input type="file" style="background:#e8e8e8!important;" class="form-control" id="cv" placeholder="Upload CV" name="cv" accept="application/pdf,.doc, .docx,.rtf">
                        </div>
                    </div>
                    <div class="text-center">
                        <button class="btn custom-btn" type="submit">Submit</button>
                    </div>
                </form>
                <div class="social-login">
                    <div class="connect-with">
                        <span style="top:0;">OR</span>
                        <hr>
                        <br>
                        <button class="btn btn-custom-outline" id='nomessage'>Skip</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>