@extends('layouts.app')

@section('body')

<div id="services-page">
    <section class="container">
        <div class="row services-pg-container">
            <div class="services-header col-12">
                <h2 class="text-center" style="padding-left: 13px;">Services</h2>
                <section>
                    <form method="GET" action="{{URL::to('services/servicesearch')}}" id='servicesearch' class="sg-search-box">
                        {{--  <input name="category" hidden/>  --}}
                        <input type="search" class="form-control" placeholder="Search Service" aria-label="Search" aria-describedby="search-addon2" name="key" required>
                        <button type="submit" class="btn custom-btn"><i class="fa fa-search"></i></button>
                    </form>
                </section>
            </div>
            <div class="custom-tabs col-12">
                <ul class="nav nav-tabs" id="social-tab" role="tablist">
                    @foreach($service_categories as $sercat)
                        <?php $title=str_replace(" ","",$sercat->title); ?>
                        <li class="nav-item">
                            <a class="nav-link" id="{{$title}}s" data-toggle="tab" href="#{{$title}}Block" role="tab" aria-controls="{{$title}}s" aria-selected="true">{{ $sercat->title }}</a>
                        </li>
                    @endforeach
                    
                </ul>
                <div class="tab-content" id="servicesTabContent">
                    @forelse($final_service_types as $secat)
				        <?php $title=str_replace(" ","",$service_categories[$loop->iteration-1]->title); ?>
      
                        <div class="tab-pane fade" id="{{$title}}Block" role="tabpanel" aria-labelledby="{{$title}}-tab">
                            <section class="row marginZero">
                                <div class="col-12" id='services-{{ $loop->iteration-1 }}-data'>
                                    @foreach($secat->items() as $service)
                                        <div class="card services-info-list">
                                            <div class="card-header si-list-header">
                                                <section class="si-header-date">
                                                    <div>
                                                        <span>{{ date('M j, Y', strtotime($service->updated_at)) }}</span>
                                                    </div>

                                                </section>
                                                <section class="si-header-title">
                                                    <div>
                                                        <h3>{{ $service->title }}</h3>
                                                    </div>
                                                    <div>
                                                        <a href="javascript:void(0)" class="badge badge-dark">{{ $service_categories[$loop->parent->iteration-1]->title }}</a>
                                                    </div>
                                                </section>
                                            </div>
                                            <div class="card-body si-list-body">
                                                <p class="" style="white-space: pre-wrap;">{{ $service->description }}</p>
                                            </div>
                                            <div class="si-list-footer">
                                                <div class="user-avatar-block">
                                                    <div class="user-avatar">
                                                        @if( $service->user->image!=null)
                                                            <img src=" {{ asset('profile_images/' . $service->user->image) }}" alt="{{ $service->user->name }}">
                                                        @else
                                                            <img src="{{asset('frontend-assets/images/profile.png')}}" alt="{{ $service->user->name }}">
                                                        @endif
                                                    </div>
                                                    <div class="user-info">
                                                        <h5>{{ $service->user->name }}</h5>
                                                    </div>
                                                </div>
                                                @if(Auth::check())
                                                    <button class="btn btn-secondary btn-sm" onclick='bookService({{ $service->id }})' id="bookload{{$service->id}}" {{ $service->user->id==Auth::user()->id ? 'hidden':'' }} >
                                                        Apply
                                                    </button>
                                                @else
                                                    <button class="btn btn-secondary btn-sm" onclick="guestInfoBook({{$service->id}})">
                                                        Apply
                                                    </button>
                                                @endif
                                            </div>
                                        </div>
                                        @if($loop->iteration==count($secat->items()))
                                            <input type="hidden" id="lastItem{{$loop->parent->iteration -1}}" value="{{ $service->updated_at }}"/>
                                        @endif
                                    @endforeach
                                </div>
                            </section>
                            @if(count($secat->items())==5)
                                <div class="text-center" style="margin: 15px 0 30px 0;">
                                    <button class="btn btn-custom-outline" id='more-{{ $loop->iteration-1 }}' onclick='getMore({{ $loop->iteration-1 }})' >Load More&nbsp;&nbsp;<img src="{{ asset('img/loading.gif') }}" id='loader-{{ $loop->iteration-1 }}' style='margin-top:-2px;max-height:11px;max-width:16px;' hidden></button>
                                </div>
                            @endif
                        </div>
                    @empty
                        <div class='text-center'>
                            <h4 style="margin:100px">No services yet</h4>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </section>
</div>
@include('landing.service.serviceModals')
@stop

@section('js')
    <script>
        var page = [];
        var itemId;
        for(var i=0; i< {{ count($service_categories) }}; i++) {
            page.push(1);
        }
        
        var first = '{{ count($service_categories) > 0 ? str_replace(" ","",$service_categories[0]->title) : "" }}';


        var active;
        if(window.location.hash==''){
            active = first;
        }
        else {
            active = window.location.hash.split('#')[1];
            window.location.hash = '';
        }
        $('#'+active+'s').attr('class','nav-link active');
        $('#'+active+'Block').attr('class','tab-pane fade show active');

        $(window).on('hashchange', function() {
            if(window.location.hash!=''){
                 $('.nav-tabs li a.active').removeClass('active');
                $('.tab-content div.active').removeClass('active');
                active = window.location.hash.split('#')[1];
                $('#'+active+'s').attr('class','nav-link active');
                $('#'+active+'Block').attr('class','tab-pane fade show active');

                if(! $('.nav-tabs li a').hasClass("active")){
                    active = first;
                    $('#'+active+'s').attr('class','nav-link active');
                    $('#'+active+'Block').attr('class','tab-pane fade show active');
                }
                window.location.hash = '';
            }
        });

        function getMore(id){
            page[id]++;
            itemId = $('#lastItem'+id).val();
            $.ajax({
                url: '?page=' + page[id] ,
                type: "get",
                data : { id : id ,itemId: itemId},
                beforeSend: function()
                {
                    $('#more-'+id).attr('disabled',true);
                    $('#loader-'+id).removeAttr('hidden');
                }
            })
            .done(function(data)
            {
                if((data.html == "" )){
                    $('#more-'+id).html('No more services.').removeAttr('onclick');
                }
                $('#more-'+id).attr('disabled',false);
                $("#services-"+id+"-data").append(data.html);
                $('#loader-'+id).attr('hidden','true');

            })
            .fail(function(){
                $('#more-'+id).attr('disabled',false);
                $('#loader-'+id).attr('hidden','true');
                page[id]--;
                location.reload();
            });
        }
        
        function bookService(service_id) {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            $.ajax(
            {
                url: '{{ URL::to("services/book")."/" }}' + service_id,
                type: "get",
                beforeSend: function()
                {
                    $('#bookload'+service_id).text('Processing').attr('disabled',true);
                }
            })
            .done(function(data)
            {
                data = JSON.parse(data);
                if(data.type=='success'){
                    $('#book_id').val(data.id);
                    $('#serviceExtraInfoModal').modal('show');
                }
                else
                    aalert(data.msg,'','Oops...');
                $('#bookload'+service_id).text('Apply').attr('disabled',false);
            })
            .fail(function(){
                $('#bookload'+service_id).text('Apply').attr('disabled',false);
            });

           
        }
        
        $('#nomessage').click(function(){
            $('#serviceExtraInfoModal').modal('hide');
            aalert('Applied Succesfully','','Success');
        });
            
        
        function guestInfoBook(id){
            $('#GuestInfo_serviceId').val(id);
            $('#serviceApplyModal').modal('show');
        }
        
        function loginToBook(){
            var service_id = $('#GuestInfo_serviceId').val();
            var next="{{URL::to('services/book') }}"+ "/" + service_id;
            window.location.href="{{URL::to('login')}}"+"?next="+next;
        }

        $('#cv').on('change', function() {
            myfile= $(this).val();
            var ext = myfile.split('.').pop();
            if(ext=="pdf" || ext=="doc" || ext == 'docx' || ext == 'rtf'){
                
            } else{
                $(this).val(null);
                aalert('Please Choose a Valid File','','Oops');
                
            }
        });

        $(document).on('show.bs.modal', '.modal', function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });
    </script>
@endsection
