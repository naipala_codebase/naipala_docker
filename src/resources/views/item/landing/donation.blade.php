@extends('layouts.app')

@section('body')
<div id="social-page">
    <section class="container">
        <div class="row">
            <div class="col-12 social-wrapper pg-top">
                <div class="info-pg-heading text-center">
                    <br>
                    <h2 style="font-family:Courier New;"><b>In their shoes for a minute!</b></h2>
                </div>
            </div>
        </div>
        @foreach($donations as $donation)
        <div class="row info-pg-container">
            <div class="col-12 social-wrapper pg-bottom">
                    <div class="info-pg-heading">
                        <h1>{{$donation->title}}</h1>
                    </div>
                    <div class="social-pg-details">
                        <div class="row info-img-container ">
                            <div class="col-sm-12 info-img"><img src="{{asset('/uploads'.'/'.$donation->image)}}" alt=""></div>
                        </div>
                        <div class="social-terms">
                            {!! $donation->description !!}
                        </div>
                    </div>
            </div>
        </div>
        @endforeach
        <div class="row">
            <div class="col-12">
                <nav aria-label="Page navigation example">
                    {!! $donations->links('partial.user.pagination') !!}
                </nav>
            </div>
        </div>
    </section>

</div>


@endsection