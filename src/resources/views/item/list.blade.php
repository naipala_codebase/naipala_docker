
@extends('layouts.admin')

@section('body')
<div ng-app="itemapp" class='content' ng-controller="itemController">
  <div class="container-fluid">
    <div class="card">
      <div class="header">
        <h4 class="title">Collection Description</h4>
          <p class="category">Add or edit Description for Collection page</p>
      </div>
      <div class="content">

        <form id="descriptionform" class="form" method="POST" action="{{URL::to('admin/collection/updateDescription')}}">
          {{ csrf_field() }}
          <div class="editable">
              {!! $description !!}
          </div>
          <hr>
          <div style="text-align:right">
            <button type="button" id="descriptionsubmit" class="btn btn-info btn-fill" >Save</button>
          </div>
        </form>   
      </div>
    </div>

    <div class="card" id="categories">
      <div class="header">
        <h4 class="title">Category</h4>
          <p class="category">Add, edit or remove category</p>
      </div>
      <div class="content">
        <div class="table-full-width">
          <table class="table">
            <tbody>
              @foreach($category as $c) 
                <tr>
                   <td class="td-actions">
                    <div class="row">
                      <div class="col-sm-1">  
                          {{$c->category_name}}
                      </div>
                     <div class="col-sm-4" >
                         <form action="{{URL::to('admin/item/editcategory')}}">
                             <input type="hidden"  name="name" id="txt{{$c->category_id}}" class="form-control" required placeholder="enter new name"/> 
                             <input type="hidden" name="id" class="form-control" value="{{$c->category_id}}"/>
                             <input type="hidden" id="sbtn{{$c->category_id}}" class="btn btn-info btn-fill btn-sm"value="save"/>
                             <input type="hidden" id="btn{{$c->category_id}}" value="cancel" class="btn btn-info btn-fill btn-sm" ng-click="cancelEdit({{$c->category_id}})"/>
                        </form> 
                      </div>
                      <div class="col-sm-1">
                        <button type="button" rel="tooltip" title="Edit Category" id="btn{{$c->category_id}}" ng-click="editClick({{$c->category_id}})" class="btn btn-info btn-simple btn-s">
                           <i class="fa fa-edit"></i>
                         </button>
                       </div>
                       <div class="col-sm-2">
                         <form action="{{URL::to('admin/item/removecategory')}}">
                         <input type="hidden" name="cid" id="cid{{$c->category_id}}" class="form-control" value="{{$c->category_id}}"/>
                        <button type="button" ng-click="delcategory({{$c->category_id}})" rel="tooltip" title="Remove" class="btn btn-danger btn-simple btn-s">
                          <i class="fa fa-times"></i>
                        </button>
                      </form>
                        </div>
                         
                   </div>
                   </td>
                </tr>
              @endforeach        
            </tbody>
          </table>
        </div>

        <div class="footer">
          <hr>
            <div class="stats">
              <form action="{{URL::to('admin/item/addcategory')}}">
                  <div class="row">
                    <div class="col-md-6">          
                      <input name="name" required type="text" class="form-control" id="new" placeholder="Add new category"/>
                    </div>
                    <div class="col-md-6">
                      <input type="submit" class="btn btn-info btn-fill pull-right" value="Add Category"/>
                    </div>   
                 </div>             
              </form>   
            </div>
        </div>
      </div>
    </div>
           
    <div class="row">
        <div class="col-md-12">
            <h2 class="category">
                 Items <small>Add, edit or remove items</small>
            </h2>
         </div>
    </div>
    <div class="panel panel-default" id="items">
        <div class="panel-heading">
        
            <a href="{{URL::to('admin/item/create')}}" class="btn btn-success btn-fill btn-sm pull-right">Add Items</a>
               Item Collections
               <br>
               <br>  
        </div> 
        <div class="panel-body">
          <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover">
              <thead>
                <th>Item Id</th>
              	<th>Name</th>
              	<th>Price</th>
              	<th>Details</th>
              	<th>Category</th>
                <th>Images</th>
              	<th>Status</th>
              	<th>Actions</th>
              </thead>
              <tbody>
              	@foreach($items as $i)
              	<tr>
              		<td>{{$i->item_id}}</td>                  
              		<td>{{$i->item_name}}</td>
              		<td>{{$i->item_price}}</td>
              		<td><?php $details=json_decode($i->details,true);?>
                    @foreach($details as $detail)
                        <span {{ $detail['size'] ==''? 'hidden':''}}><b>Size</b>: {{ $detail['size'] }},</span>
                        <span {{ $detail['color'] ==''? 'hidden':''}}><b>Color</b>: {{ $detail['color'] }},</span>
                        <span><b>Quantity</b>: {{ $detail['quantity'] }}</span>
                        <br>
                    @endforeach
                    
                  </td>
              		<td>{{$i->category_name}}</td>
              		<td>
                    <a href="{{URL::to('admin/item/images'.'/'.$i->item_id)}}">
                    <?php $current = '-1'; ?>
                    @foreach($i->item_images as $g)
                      @if($current == $g->color) @continue @endif
                      <?php $current = $g->color; ?>
                      <img src="{{asset('uploads'.'/'.$g->image_link)}}" height="50px" width="50px" alt="img"/>
                    @endforeach
                    </a>
                  </td>
                  <td>
              			<a href="{{URL::to('admin/item/changestatus'.'/'.$i->item_id)}}"><span class="badge">{{$i->item_status}}</span></a>
              		</td>
              		<td>
              			<a href="{{URL::to('admin/item'.'/'.$i->item_id.'/'.'edit')}}"><span class="badge">edit</span></a>
                      <form method="POST" action="{{ URL::to('admin/item'.'/'.$i->item_id) }}">
                        <input name="_method" type="hidden" value="DELETE">
                        {{ csrf_field() }}
                        <button class="badge" type="button" id="deleteItem" border="1px"> 
                          delete
                        </button>
                      </form>
                      
                  
              			<a href="{{URL::to('admin/item/images'.'/'.$i->item_id)}}"><span class="badge">images</span></a>
              		</td>
              	</tr>
              	@endforeach
              </tbody>
            </table>
          </div>
        </div>
    </div>
  </div>
</div>   

@endsection
@section('admin-js')
<script src="{{asset('admin-assets/scripts/itemscript.js')}}"></script>
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
	<script>
		tinymce.init({
            selector: 'div.editable',
            inline: true,
            theme: "modern",
            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
            toolbar2: "print preview | forecolor backcolor emoticons"
        });

        $('#descriptionsubmit').click(function(e){
            param = $('#mce_1').html();
            param = "<textarea name='descriptionContent' hidden>" + param + "</textarea>";
            $('#descriptionform').append(param);
            this.form.submit();
        });
    </script>
@stop