@extends('layouts.admin')
@section('body')



<div class="content" ng-app="attribapp" ng-controller="attribcontroller">
  <div class="container-fluid">
    <div class="card">

      <div class="header">
        <a href="{{URL::to('admin/item#items')}}" class="btn btn-info btn-fill pull-right" >Back</a>
        <h4 class="title">Edit Item</h4>
      </div>
      <div class="content">

        @if(Request::segment('4') == 'edit')
          {!! Form::model($item,['url' => 'admin/item'.'/'.$item->item_id,'method'=>'PATCH','files'=>'true'])!!}
          <div ng-init="getDetails({{ $item->details }})"></div>
        @else
          {!! Form::open([ 'enctype'=>'multipart/form-data','url' => 'admin/item','files'=>'true'])!!}
        @endif
          <div class="form-group">
            <label>Name</label>
            {!! Form::text('item_name',null,['class'=>'form-control','id'=>'item_name','required'=>'true']) !!}

            <label>Category</label>
            {!! Form::select('category_id',$category,null,['class'=>'form-control','required'=>'true']) !!}

            <label>Description</label>
            {!! Form::textarea('item_description',null,['class'=>'form-control ckeditor','id'=>'item_description','required'=>'true']) !!}

            <label>What’s The Story</label>
            {!! Form::textarea('whats_the_story',null,['class'=>'form-control','id'=>'whats_the_story','required'=>'true']) !!}

            <label>What Sets It Apart</label>
            {!! Form::textarea('what_sets_it_apart',null,['class'=>'form-control','id'=>'what_sets_it_apart','required'=>'true']) !!}

            <label>Price($)</label>
            {!! Form::number('item_price',null,['class'=>'form-control','id'=>'item_price','required'=>'true','min'=>1]) !!}

            <label>Old Price($)</label>
            {!! Form::number('item_old_price',null,['class'=>'form-control','id'=>'item_old_price','min'=>1]) !!}
            <br>
            
            <label>Details (Color,Size,Quantity)</label>
            <button type="button" class="btn btn-success btn-simple btn-md" ng-click="addDetail()" rel="tooltip" title="add color"><i class="fa fa-plus"></i></button>

            <div ng-repeat="s in details">
              <div class="row input-group">
                <div class="col-md-4 ">
                  <input type="text" ng-model="s.color" placeholder="Color" class="form-control"/>
                </div>
                <div class="col-md-4 ">
                  <input type="text" ng-model="s.size" placeholder="Size" class="form-control"/>
                </div>
                <div class="col-md-4 ">
                  <input type="number" min='0' ng-model="s.quantity"  placeholder="Quantity" class="form-control" required="true"/>
                </div>
                <span class="input-group-btn" ng-hide="details.length==1"><button type="button" class="btn btn-danger btn-simple btn-md" ng-click="removeDetail(s.id)" rel="tooltip" title="remove"><i class="fa fa-remove"></i></button></span>

              </div>
            </div>
            <br><br>
            <label>Status</label>
            {!! Form::select('item_status',['dormant'=>'dormant','active'=>'active'],null,['class'=>'form-control','required'=>'true']) !!}
          </div>
          <input type="hidden" value="@{{details}}" name="details"/>
          <div class="form-group">
            <input type="submit" class="btn btn-info btn-fill" value="Submit"/>
          </div>

        {!! Form::close() !!}
      </div>
      
    </div>
  </div>
  <br>
  
</div>
@endsection
@section('admin-js')
<script src="{{asset('admin-assets/scripts/attribscript.js')}}"></script>
@stop