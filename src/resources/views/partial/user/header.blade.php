<!doctype html>
<html class="no-js" lang="">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Naipala - Feel Different</title>

    <!--<meta name="description" content="Naipala is a lifestyle brand and we market our products and services to personify the interests, attitudes, and opinions of Nepalese culture and people. We as a lifestyle brand seek to encourage, monitor and persuade people with the purpose of their products contributing to the definition of the consumer’s way of life.">-->
    <meta name="description" content="Naipala is a Nepalese lifestyle brand and we market Nepal made products to personify the interests, attitudes, and opinions of culture and people from Nepal.">
	<meta name="keywords" content="Nepali Products, Nepali Goodies, Naipala, Nepali Brand, Nepali Clothing Brand, Made in Nepal, Nepali Products, Nepal based products, Handicraft, Kaam Kotha, Jobs, Photography, Neapli Events, Nepali Movies in australia">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=0">
    
    @yield('pin')
    <link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ asset('img/favicon.ico') }}">

    <link rel="stylesheet" href="{{asset('frontend-assets/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('frontend-assets/font-awesome.min.css') }}">

    <!-- drawer.css -->
    <link rel="stylesheet" href="{{asset('frontend-assets/drawer.min.css') }}">
    <link rel="stylesheet" href="{{asset('frontend-assets/styles/styles.css')}}">
    
    <!-- Owl Carousel -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/assets/owl.carousel.min.css">
  
    <style>
        .border-error{
            border-color: #de1111bd;
        }

        input[type="text"], input[type="password"], input[type="email"], input[type="number"], select,  textarea{
            background:#e8e8e8!important;
        }
        

    </style>
    
    @yield('css')
    <script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    <!-- Configuration file for CKeditor -->
    <!--<script src="{{asset('/admin-assets/js/config.js')}}"></script>-->
    
    <!-- <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.css" /> -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.1.0/cookieconsent.min.js"></script> -->
    <!-- <script>
    window.addEventListener("load", function(){
    window.cookieconsent.initialise({
      "palette": {
        "popup": {
          "background": "#23272b",
          "text": "#cfcfe8"
        },
        "button": {
          "background": "#81302e",
          "type": "opt-out"
        }
      },
      "theme": "edgeless"
    })});
    </script> -->
    
    <!-- Facebook Pixel Code -->
    <script>
        !function(f,b,e,v,n,t,s)
        {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window,document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '1080571182331316'); 
        fbq('track', 'PageView');
    </script>
    <noscript>
         <img height="1" width="1" src="https://www.facebook.com/tr?id=1080571182331316&ev=PageView&noscript=1"/>
    </noscript>
    <!-- End Facebook Pixel Code -->
    
    <!--Start of Tawk.to Script-->
    <script type="text/javascript">
        var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
        (function(){
            var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
            s1.async=true;
            s1.src='https://embed.tawk.to/5d1851e17a48df6da2423767/default';
            s1.charset='UTF-8';
            s1.setAttribute('crossorigin','*');
            s0.parentNode.insertBefore(s1,s0);
        })();
    </script>
    <!--End of Tawk.to Script-->
    <script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
    <div id="wrap">
        <div id="main">