
@if (Session::has('book:success'))

 <script type="text/javascript">
	$(document).ready(function(){
	    $('#book_id').val({{Session::get('book:success')}});
        $('#serviceExtraInfoModal').modal('show');
	});
 	
 </script>
    
@endif
@if (Session::has('error'))
    <script type="text/javascript">
        aalert("{{ Session::get('error') }}",'','Oops...');
    </script>
@endif

@if (Session::has('success'))
    <script type="text/javascript">
        aalert("{{ Session::get('success') }}",'','Success');
    </script>
@endif