</div>
</div>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src=https://www.googletagmanager.com/gtag/js?id=UA-120474093-1></script>
    <script>
        /*// Lazy loading
        document.addEventListener("DOMContentLoaded", function() {
        var lazyImages = [].slice.call(document.querySelectorAll("img.lazy"));
        var observer = {
            threshold: 0.1
            }
        if (
            "IntersectionObserver" in window &&
            "IntersectionObserverEntry" in window &&
            "intersectionRatio" in window.IntersectionObserverEntry.prototype
        ) {
            let lazyImageObserver = new IntersectionObserver(function(
            entries,
            observer
            ) {
            entries.forEach(function(entry) {
                if (entry.isIntersecting) {
                let lazyImage = entry.target;
                lazyImage.src = lazyImage.dataset.src;
                lazyImage.srcset = lazyImage.dataset.srcset;
                lazyImage.classList.remove("lazy");
                lazyImageObserver.unobserve(lazyImage);
                }
            });
            });

            lazyImages.forEach(function(lazyImage) {
            lazyImageObserver.observe(lazyImage);
            });
        } else {    
            let active = false;

            const lazyLoad = function() {
            if (active === false) {
                active = true;

                setTimeout(function() {
                lazyImages.forEach(function(lazyImage) {
                    if (
                    lazyImage.getBoundingClientRect().top <= window.innerHeight &&
                    lazyImage.getBoundingClientRect().bottom >= 0 &&
                    getComputedStyle(lazyImage).display !== "none"
                    ) {
                    lazyImage.src = lazyImage.dataset.src;
                    lazyImage.srcset = lazyImage.dataset.srcset;
                    lazyImage.classList.remove("lazy");

                    lazyImages = lazyImages.filter(function(image) {
                        return image !== lazyImage;
                    });

                    if (lazyImages.length === 0) {
                        document.removeEventListener("scroll", lazyLoad);
                        window.removeEventListener("resize", lazyLoad);
                        window.removeEventListener("orientationchange", lazyLoad);
                    }
                    }
                });

                active = false;
                }, 200);
            }
            };

            document.addEventListener("scroll", lazyLoad);
            window.addEventListener("resize", lazyLoad);
            window.addEventListener("orientationchange", lazyLoad);
        }
        });*/
        // End of Lazy Loadin

        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());
        gtag('config', 'UA-120474093-1');
    </script>

    <style>
        #owl-demo .item{
            margin: 15px;
            text-align: center;
        }
        #owl-demo .item a img{
            display: block;
            width: auto;
            height: 80px;
        }
        #owl-demo2 .item{
            margin: 15px;
            text-align: center;
        }
        #owl-demo2 .item a img{
            display: block;
            width: auto;
            height: 80px;
        }
        .mobileview{
            display: none;
        }
        @media (max-width:767px) {
            .mobileview{
                display: block;
            }
        }
    </style>
        
        <input type="hidden" id="logoval"/>
        <footer id="footer" class="">
            <div class="">
                <div class="row">
                    <div class="col-md-5" style="    /*margin-top: -20px;*/">
                        <p style="text-align: center;margin-bottom: 0px;font-weight: 600;">{{ $left }}</p>
                        <div id="owl-demo" class="owl-carousel owl-theme" hidden>
                            @foreach($footer_brands->where('category','left') as $b)
                                <div class="item"><a href="{{ $b->link }}" target="_blank"><img src="{{ asset('footer_brands/'.$b->logo) }}" class="img-fluid" alt="{{ $b->link }}"></a></div>
                            @endforeach
                        </div>
                        <hr class="mobileview">
                    </div>
                    <div class="col-md-2" style="margin-top:25px">
                        <div class="footer-brand text-center">
                            <img src="{{asset('/frontend-assets/images/naipala_logo.png')}}" alt="Naipala">
                        </div>
                        <div class="social-block text-center">
                            <a href="https://www.facebook.com/naipala.au/" target="_blank"><i class="fa fa-facebook-official"></i></a>
                            <a href="https://www.instagram.com/naipala.au/" target="_blank"><i class="fa fa-instagram"></i></a>
                            <a href="https://mail.google.com/mail/?view=cm&fs=1&to=info@naipala.com" target="_blank"><i class="fa fa-envelope"></i></a>
                        </div>
                        <div class="home-contacts text-center" style="width: 100%;">
                            <b>Powered By <a href="https://www.incubeweb.com/" target="_blank"> <strong>inCube</strong></a></b>
                        </div>
                        <hr class="mobileview">
                    </div>
                    <div class="col-md-5" style="    /*margin-top: -20px;*/">
                        <p style="text-align: center;font-weight: 600;margin-bottom: 0px;">{{ $right }}</p>
                        <div id="owl-demo2" class="owl-carousel owl-theme" hidden>
                            @foreach($footer_brands->where('category','right') as $b)
                                <div class="item"><a href="{{ $b->link }}" target="_blank"><img src="{{ asset('footer_brands/'.$b->logo) }}" class="img-fluid" alt="{{ $b->link }}"></a></div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </footer>

        <script
        src="{{asset('frontend-assets/jquery-3.2.1.min.js') }}"></script>

        <script src="{{asset('frontend-assets/popper.min.js') }}"></script>
            
        <script src="{{asset('frontend-assets/bootstrap.min.js') }}"></script>

        <!-- iScroll.js -->
        <script src="{{asset('frontend-assets/iscroll.min.js') }}"></script>
        <!-- drawer.js -->
        <script src="{{asset('frontend-assets/drawer.min.js') }}"></script>
       
        <script src="{{asset('/frontend-assets/scripts/app.js')}}"></script>
        
        <!-- Owl Carousel -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.2.1/owl.carousel.min.js"></script>

        <script>
            $(document).ready(function(){
                var h=$('#logoscroll').css('height');
                var w=$('#logoscroll').css('width');
                $('#logoval').attr('height',h);
                $('#logoval').attr('width',w);
                
                $('#owl-demo').removeAttr('hidden');
                $('#owl-demo2').removeAttr('hidden');
                
                $("#owl-demo").owlCarousel({
                    autoPlay: 3000, //Set AutoPlay to 3 seconds
 
                    items : 4,
                });

                $("#owl-demo2").owlCarousel({
                    autoPlay: 3000,
                    items : 4,
                });
            });
            $(window).scroll(function() {
                $(window).scrollTop() > 60 ? ($("#mainNav").css("background", "rgba(70, 65, 65, 0.35)"), $("#mainNav .navbar-brand > img").css({
                    width: "100px",
                    height: "40px"
                })) : ($("#mainNav").css("background", "transparent"), $("#mainNav .navbar-brand > img").css({
                    width: $('#logoval').attr('width'),
                    height:$('#logoval').attr('height')
                }));
            });
            $(".contact__trigger").click(function() {
                var $contact = $('#hgcontact').eq(0);
                $contact.toggleClass("contact--is-open");
                $contact.removeClass("contact--poke");
                return false;
            });
        
            $('#cancelContact').click(function(){
                $('.contact__trigger').trigger('click');
            })
            
            $(".wishlist__trigger").click(function() {
                var $contact = $(this).parents(".wishlist").eq(0);
                var k =0
                if($('.wishlist__content').attr('hidden'))
                {    
                    k=1;
                    $('.wishlist__content').removeAttr('hidden');
                }
                $contact.toggleClass("wishlist--is-open");
                if(k==0)
                {
                    window.setTimeout(function(){$('.wishlist__content').attr('hidden','true');
                }, 500);                        
                    
                }
                    
                $contact.removeClass("wishlist--poke");
                return false;
            });

           $('#optioninfo').click(function(){
               
               $('#infoclick').trigger('click');
           });
        </script>

        <script type="text/javascript">
            if (window.location.hash && window.location.hash == '#_=_') {
                window.location.hash = '';
            }
        </script>

        @if($unseen > 0)
            <script type="text/javascript">document.title = "(" + $('#notification').text() + ") " + 'Naipala';</script>
        @endif

        <script src="{{asset('admin-assets/js/angular.min.js')}}"></script>

        <script type="text/javascript">
            $(document).ready(function(){
                $('#setCurrency').on('change',function(){
                    location.href="/currency/set?currency="+$('#setCurrency').val()
                });
            });

            function aalert(content,type,title){
                $('#alerttype').html(type);
                $('#alerttitle').html(title);
                $('#alertcontent').html(content);
                $('#alertpopup').modal('show');
            }

            function closealert(){
                $('#alerttitle').empty();
                $('#alertcontent').empty();
                $('#doIt').attr('hidden','true');
                $('#alertpopup').modal('hide');
            }
        </script>

        @include('partial.user.validation')

        <script src="{{asset('frontend-assets/cart.js')}}"></script>
        <script src="{{asset('frontend-assets/wishlist.js')}}"></script>
        @yield('js')
    </body>
</html>
