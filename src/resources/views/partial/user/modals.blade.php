

<!-- Modal Contact-->
<style>

    @media(min-width:100px){
        .contact {
            z-index: 100;
            position: fixed;
            width: 250px !important;
            height:460px !important;
            bottom: 10px;
            right: 0.25rem;
            display: none; /* Remove this line to show contact bar */
            -webkit-transition: all 0.3s ease-in-out;
            -moz-transition: all 0.3s ease-in-out;
            -o-transition: all 0.3s ease-in-out;
            transition: all 0.3s ease-in-out;
            -webkit-transform: translateY(calc(100% - 18px));
            -moz-transform: translateY(calc(100% - 18px));
            -ms-transform: translateY(calc(100% - 18px));
            transform: translateY(calc(100% - 18px));
            overflow-y:hidden!important;
        }
        .contact__trigger {
            height: 30px !important; 
            width: 90px !important;
            margin: 0 auto !important;
            background: #23272b !important;
            display: block !important;
            line-height: 30px !important;
            color: #ffffff !important;
            font-size: 0.825rem !important;
            text-align: center !important;
            font-family: 'brandon-grotesque' !important;
            font-weight: normal !important;
            font-style: normal !important;
            letter-spacing: 1px !important;
            font-weight: 500 !important;
            text-decoration: none !important;
            margin-left: 150px !important;
            text-transform: uppercase !important;
        }
        .contact__trigger:hover {
            height: 30px !important;
            width: 90px !important;
            margin: 0 auto !important;
            background: #3b3e40 !important;
            display: block !important;
            line-height: 30px !important;
            color: #ffffff !important;
            font-size: 0.825rem !important;
            text-align: center !important;
            font-family: 'brandon-grotesque' !important;
            font-weight: normal !important;
            font-style: normal !important;
            letter-spacing: 1px !important;
            font-weight: 500 !important;
            text-decoration: none !important;
            margin-left: 150px !important;
            text-transform: uppercase !important;
        }
        .contact__content {
            overflow-y:auto !important;
            padding: 0.25rem;
            background: Url({{asset('/frontend-assets/images/bodybackground.jpg')}});
        }
        .contact--is-open {
            -webkit-transform: translateY(0);
            -moz-transform: translateY(0);
            -ms-transform: translateY(0);
            transform: translateY(0);
        }
        .contact--poke {
            -webkit-transform: translateY(calc(100% - 28px));
            -moz-transform: translateY(calc(100% - 28px));
            -ms-transform: translateY(calc(100% - 28px));
            transform: translateY(calc(100% - 28px));
        }
        .g-recaptcha {transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;
        }
    @media(min-width:300px){
    .contact {
        z-index: 100;
        position: fixed;
        width: 300px !important;
        height:530px !important;
        bottom: 10px;
        right: 0.75rem;
        display: none; /* Remove this line to show contact bar */
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        -webkit-transform: translateY(calc(100% - 18px));
        -moz-transform: translateY(calc(100% - 18px));
        -ms-transform: translateY(calc(100% - 18px));
        transform: translateY(calc(100% - 18px));
    }
    .contact__trigger {
        height: 30px !important;
        width: 130px !important;
        margin: 0 auto !important;
        background: #23272b !important;
        display: block !important;
        line-height: 30px !important;
        color: #ffffff !important;
        font-size: 0.825rem !important;
        text-align: center !important;
        font-family: 'brandon-grotesque' !important;
        font-weight: normal !important;
        font-style: normal !important;
        letter-spacing: 1px !important;
        font-weight: 500 !important;
        text-decoration: none !important;
        margin-left: 170px !important;
        text-transform: uppercase !important;
    }
    .contact__trigger:hover {
        height: 30px !important;
        width: 130px !important;
        margin: 0 auto !important;
        background: #3b3e40 !important;
        display: block !important;
        line-height: 30px !important;
        color: #ffffff !important;
        font-size: 0.825rem !important;
        text-align: center !important;
        font-family: 'brandon-grotesque' !important;
        font-weight: normal !important;
        font-style: normal !important;
        letter-spacing: 1px !important;
        font-weight: 500 !important;
        text-decoration: none !important;
        margin-left: 170px !important;
        text-transform: uppercase !important;
    }
    .contact__content {
        padding: 1.25rem;
        background: Url({{asset('/frontend-assets/images/bodybackground.jpg')}});
    }
    .contact--is-open {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
    .contact--poke {
        -webkit-transform: translateY(calc(100% - 28px));
        -moz-transform: translateY(calc(100% - 28px));
        -ms-transform: translateY(calc(100% - 28px));
        transform: translateY(calc(100% - 28px));
    }
    .g-recaptcha {transform:scale(0.85);-webkit-transform:scale(0.85);transform-origin:0 0;-webkit-transform-origin:0 0;
    }
</style>
<div class="modal fade" id="contactModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="contact-wrapper">
                    <div class="info-pg-heading text-center">
                        <h3 class="">Contact Us</h3>
                    </div>
                    <hr class="info-page-hr" style="width: 100px; border-color:#a0a0a0;">
                    <div class="contact-details" style="font-size: 1.1rem;">
                        {!! $contact->config_value !!}
                        <div class="share-options">
                            <ul>
                                <li><a href="https://www.facebook.com/naipala.com.au" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.instagram.com/naipala.com.au" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-dark btn-sm" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal NewsLetter -->
<div class="modal fade" id="newsletterModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Sign Up for Newsletter !</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('subscribe') }}">            
                <div class="modal-body">
                    <!--<h3 class="modal-body-title text-center">Sign Up for Newsletter !</h3>-->
                    {{ csrf_field() }}
                    <div class="form-row">
                        <!--<div class="col-2"></div>-->
                        <!--<div class="col-2">-->
                        <!--    <label class="col-sm-2 col-form-label">Email</label>-->
                        <!--</div>-->
                        <!--<div class="col-6">-->
                        <!--    <input type="email" class="form-control" placeholder="example@gmail.com" name="email" required>-->
                        <!--</div>-->
                        <!--<div class="col-2"></div>-->
                        <div class="col-12" style="margin: 0 auto;">
                            <input type="email" class="form-control" placeholder="example@gmail.com" name="email" required>
                        </div>
                    </div>
                    <div style="padding-top: 1em;">
                        <div class="g-recaptcha" data-sitekey="6LcdNKoUAAAAAGHGnnFlTNyb4zvKo_1OOdUyetIN"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn custom-btn black-btn">Subscribe</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal invitation -->
<div class="modal fade" id="invitationModal" tabindex="-1" role="dialog" aria-labelledby="invitationModal"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('invite') }}">            
                <div class="modal-body">
                    <h3 class="modal-body-title text-center">Get your Invitation!</h3>
                    {{ csrf_field() }}
                    <div class="form-row">
                        <div class="col-2"></div>
                        <div class="col-2">
                            <label class="col-sm-2 col-form-label">Email</label>
                        </div>
                        <div class="col-6">
                            <input type="email" class="form-control" placeholder="jone@doe.com" name="email" required>
                        </div>
                        <div class="col-2"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn custom-btn black-btn">Submit</button>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal Login-->
<?php $modalnext = Request::get('next') ? Request::get('next') : url()->current() ?>
<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body custom-tabs">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab"
                           aria-controls="home" aria-selected="true">Login</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                           aria-controls="profile" aria-selected="false">Register</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active text-center" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <h4 class=""><b>Been here before?</b></h4>
                        <form method="POST" action="{{ route('login',array('next' => $modalnext)) }}" style="padding-left:40px;padding-right:40px">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="email" class="form-control" id="loginstaticEmail" placeholder="email@example.com" name="email" value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="logininputPassword" placeholder="Password" name="password" required>
                            </div>
                            <div class="form-group flex-container">
                                <div class="form-check login-check" style="margin-bottom: 0;">
                                    <label class="form-check-label">
                                        <input class="form-check-input" type="checkbox" value="None" id="rememberme" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                    </label>
                                </div>
                                <div class="login-forgot">
                                    <a href="{{ route('password.request') }}">Forgot Password?</a>
                                </div>
                            </div>
                                <button type="submit" class="btn custom-btn">Login</button>
                           
                        </form>

                        <div class="social-login">
                            <div class="connect-with">
                                <span>OR CONNECT WITH</span>
                                <hr>
                            </div>
                            <a href="{{ url('/signin/facebook').'?next='.$modalnext }}" class="btn custom-btn facebook-btn">
                                <i class="fa fa-facebook"></i>
                                FACEBOOK
                            </a>
                        </div>
                    </div>

                    <div class="tab-pane fade text-center log" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                        <h4 class=""><b>First time here?</b></h4>
                        <style>
                               ::-webkit-input-placeholder {
                                    text-align: center;
                                 }
                                 
                        </style>
                        <h4 class=""><b>Create your different Milieu.</b></h4>
                        <form method="POST" action="{{ route('register',array('next' => $modalnext)) }}" style="padding-left:40px;padding-right:40px">
                            {{ csrf_field() }}
                            <div class="form-group">
                                <input type="text" class="form-control" id="registername" placeholder="Full Name" name="name" value="{{ old('name') }}" required>
                            </div>
                            <div class="form-group">
                                <input type="email" class="form-control" id="registerstaticEmail" placeholder="email@example.com" name="email" value="{{ old('email') }}" required>
                            </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="registerinputPassword" placeholder="Password" name="password" required>
                              </div>
                            <div class="form-group">
                                <input type="password" class="form-control" id="registerinputRePassword" placeholder="Confirm Password" name="password_confirmation" required>
                            </div>
                            <div class="text-center" style="padding-top: 10px;">
                                <button class="btn custom-btn" type="submit">Create</button>
                            </div>
                        </form>
                        <div class="social-login">
                            <div class="connect-with">
                                <span>OR CONNECT WITH</span>
                                <hr>
                            </div>
                            <a href="{{ url('/signin/facebook').'?next='.$modalnext }}" class="btn custom-btn facebook-btn">
                                <i class="fa fa-facebook"></i>
                                FACEBOOK
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Modal Alert -->
<div class="modal fade" id="alertpopup" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="alerttype"></h5>
                <button type="button" class="close" onclick="closealert()" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="modal-body-title text-center" id="alerttitle"></h3>
                <br><h4 id="alertcontent" style="text-align:center"></h4><br>
                <div class="text-center">
                    <button id="doIt" hidden type="button" class="btn custom-btn black-btn" >Confirm</button>                
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" onclick="closealert()" class="btn custom-btn black-btn">Close</button>
            </div>
        </div>
    </div>
</div>


<style>
    .contact {
        z-index: 100;
        position: fixed;
        width: 300px;
        bottom: 10px;
        right: 0.75rem;
        display: none; /* Remove this line to show contact bar */
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        -webkit-transform: translateY(calc(100% - 18px));
        -moz-transform: translateY(calc(100% - 18px));
        -ms-transform: translateY(calc(100% - 18px));
        transform: translateY(calc(100% - 18px));
        }
    .contact__trigger {
        height: 30px;
        width: 130px;
        margin: 0 auto;
        background: #23272b;
        display: block;
        line-height: 30px;
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        font-family: 'brandon-grotesque';
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 170px;
        text-transform: uppercase;
    }
    .contact__trigger:hover {
        height: 30px;
        width: 130px;
        margin: 0 auto;
        background: #3b3e40;
        display: block;
        line-height: 30px;
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        font-family: 'brandon-grotesque';
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 170px;
        text-transform: uppercase;
    }
    .contact__content {
        padding: 1.25rem;
        background: Url({{asset('/frontend-assets/images/bodybackground.jpg')}});
        
        
    }
    .contact--is-open {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
    .contact--poke {
        -webkit-transform: translateY(calc(100% - 28px));
        -moz-transform: translateY(calc(100% - 28px));
        -ms-transform: translateY(calc(100% - 28px));
        transform: translateY(calc(100% - 28px));
    }

</style>
<style>
    .wishlist {
        z-index: 100;
        position: fixed;
        bottom: 20px; /* matching height with twak box */
        /* bottom: 10px; */ /* orginal height */
        left: .25rem;
        -webkit-transition: all 0.3s ease-in-out;
        -moz-transition: all 0.3s ease-in-out;
        -o-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out;
        -webkit-transform: translateY(calc(100% - 18px));
        -moz-transform: translateY(calc(100% - 18px));
        -ms-transform: translateY(calc(100% - 18px));
        transform: translateY(calc(100% - 18px));
    }
    .wishlist__trigger {
        height: 40px; /* matching height with twak box */
        /* height: 30px; */ /* orginal height */
        width: 130px;
        margin: 0 auto;
        background: #3C3533; /* matching bg color with twak box *
        /* background: #23272b; */ /* orginal bg color */
        display: block;
        line-height: 40px; /* matching height with twak box */
        /* line-height: 30px; */ /* orginal height */
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        /* font-family: 'brandon-grotesque'; */
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 15px;
        text-transform: uppercase;
    }
    .wishlist__trigger:hover {
        height: 40px; /* matching height with twak box */
        /* height: 30px; */ /* orginal height */
        width: 130px;
        margin: 0 auto;
        background: #3b3e40;
        display: block;
        line-height: 40px; /* matching height with twak box */
        /* line-height: 30px; */ /* orginal height */
        color: #ffffff;
        font-size: 0.825rem;
        text-align: center;
        /* font-family: 'brandon-grotesque'; */
        font-weight: normal;
        font-style: normal;
        letter-spacing: 1px;
        font-weight: 500;
        text-decoration: none;
        margin-left: 15px;
        text-transform: uppercase;
    }
    .wishlist__content {
        padding: 1.25rem;
        background: Url({{asset('/frontend-assets/images/bodybackground.jpg')}});
        height: 60vh;
        overflow-y:auto;
    }
    .wishlist--is-open {
        -webkit-transform: translateY(0);
        -moz-transform: translateY(0);
        -ms-transform: translateY(0);
        transform: translateY(0);
    }
    .wishlist--poke {
        -webkit-transform: translateY(calc(100% - 28px));
        -moz-transform: translateY(calc(100% - 28px));
        -ms-transform: translateY(calc(100% - 28px));
        transform: translateY(calc(100% - 28px));
    }
</style>
  
<div>
    <div id="hgcontact" class="contact">
        <a href="#" class="contact__trigger">Contact</a>
        <div class="contact__content">
        
            <form method="post" action="{{ route('contact_us') }}" id="contact_form" >
                {{ csrf_field() }}    
                <div class="form-group">
                    <label>Name</label>
                    <input class="form-control" required="" type="text" id="contactFormName" name="contact[name]">
                </div>
                <div class="form-group">
                    <label>Email</label>
                    <input class="form-control" type="email" required="" id="contactFormEmail" name="contact[email]">
                </div>
                <div class="form-group">
                    <label>Message</label>
                    <textarea class="form-control" required="" rows="3" id="contactFormMessage" name="contact[body]"></textarea>
                </div>
                <div class="form-group">
                    <div class="g-recaptcha" data-sitekey="6LcdNKoUAAAAAGHGnnFlTNyb4zvKo_1OOdUyetIN"></div>
                </div>
                <div class="form-group">
                    <button class="btn custom-btn" type="submit" id="contactFormSubmit">Send</button>
                    <button class="btn custom-btn" type="button" id="cancelContact">Cancel</button>
                </div>
            </form>
        </div>
    </div>

    <div id="wishlist" ng-controller="WishlistController" ng-init="current={{ $current }};init()" class="wishlist">
        <input type="hidden" value="{{$wishlistitems}}" id="wishlistitems" ng-click="init()"/>
        <a href="#" class="wishlist__trigger" id="wishlisttrigger">Wishlist</a>
        <div class="wishlist__content" hidden>
            <img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="wloading" ></img>
            <section>
                <hr class="custom-hr" style="width: 100%;">
                <ul class="cart-list" style="padding-right:10px;">
                    <li >
                        <div ng-repeat="c in wishlistitems" class="cart-item">
                            <a style="cursor: pointer;" class="removeFromCart" ng-click="removeItem(c.rowId)"><i class="fa fa-times"></i></a>
                            <div class="cart-img">
                                <a href="/collection/item/@{{ c.options['alias'] }}"><img ng-src="{{ URL::to('asset/uploads') }}/@{{ c.options['image'] }}/110/110"  alt="@{{c.name}}" nopin="nopin">
                                </a>
                            </div>
                            <div class="cart-details">
                                <ul>
                                    <li>@{{c.name}}</li>
                                    <li>Price: @{{ currency_convert(c.price) }}</li>
                                </ul>
                            </div>
                        </div>
                        
                    </li>
                </ul>
                
            </section> 
        </div>
    </div>
</div>
    
