
<style>
    @media(min-width:200px){
        .logoscroll{
            width:0px; 
            height:0px;  
            padding-left:0px;
            display:none
        }
    }
    
    @media(min-width:575px){
        .logoscroll{
            width:150px; 
            height:60px;  
            padding-left:12px;
            display:block;
        }
    }
</style>
                                                
<header class="drawer-left drawer--left">
    <!--search bar-->
    <section class="searchBox">
        <form method="GET" action="{{URL::to('/search')}}">
            <input id="searchInput" name="q" type="search" placeholder="Search Collection">
        </form>
        <span id="closeSearch">x</span>
    </section>
    <!--search bar end-->

    <!--Main Navigation-->
    <nav class="navbar navbar-light fixed-top" id="mainNav">
        <a class="navbar-brand mobile-brand" href="{{ route('home') }}">
            <img src="{{asset('/frontend-assets/images/naipala_logo.png')}}" alt="naipala logo">
        </a>
        <div class="left-nav-block">
            <ul class="navbar-nav">
                <li class="nav-item drawer-toggle" id="openLeft"><i class="fa fa-bars" style="margin-right: 5px;"></i>MENU</li>
            </ul>
        </div>
        <a class="navbar-brand center-nav-block" href="{{ route('home') }}">
            <img src="{{asset('/frontend-assets/images/naipala_logo.png')}}" alt="" id="logoscroll" class="logoscroll">
        </a>

        <div id="leftnav" class="right-nav-block">
            <ul class="navbar-nav d-flex flex-row mr-3">
                {{-- @if($currencies->count() > 0)
                    <li class="nav-item">
                        <ul id="languages-dropdown-2" class="list-unstyled u-shadow-v29 g-pos-abs g-bg-white g-width-160 g-pb-5 g-mt-19 g-z-index-2"
                            aria-labelledby="languages-dropdown-invoker-2">
                            @foreach($currencies as $currency)
                                <li style="color:{{ $current->code == $currency->code ? 'red' : 'white'}}">
                                    <a class="d-block g-color-black g-color-primary--hover g-text-underline--none--hover g-font-weight-400 g-py-5 g-px-20" href="/currency/set?currency={{ $currency->code }}">
                                            {{ $currency->symbol }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </li>
                @endif --}}
                @if($currencies->count() > 1)
                    <select class="custom-select custom-select-option " id="setCurrency" style="min-width:2rem;-webkit-appearance:menulist; -moz-appearance:menulist; appearance:menulist;border:0;" name="amount" required="">
                        {{-- <option selected="" disabled="">Choose Amount</option> --}}
                        @foreach($currencies as $currency)

                            <option value="{{$currency->code}}" {{$current->code==$currency->code?'selected':''}}>{{$currency->symbol.' '.$currency->code}}</option>
                        @endforeach
                    </select>
                @endif

                @if(Auth::check())
                    <li class="nav-item">
                        <div class="user-avatar-block">
                            <a href="{{ URL::to('profile') }}" class="user-avatar">
                                @if( Auth::user()->image!=null)
                                    <img src="{{ asset('profile_images/' . Auth::user()->image) }}"  alt="{{ Auth::user()->name }}" nopin="nopin">
                                @else
                                    <img src="{{asset('frontend-assets/images/profile.png')}}"  alt="{{ Auth::user()->name }}" nopin="nopin"/>
                                @endif
                            </a>
                        </div>
                    </li>
                    <li class="nav-item ">
                        <a class="nav-link" href="{{ URL::to('profile#applicant') }}"><i class="fa fa-bell"></i></a>
                        <span class="nav-badge" id='notification' {{$unseen==0 ? 'hidden':''}}>{{ $unseen }}</span>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" style="cursor: pointer;"  data-toggle="modal" data-target="#loginModal"><i class="fa fa-user-circle-o"></i></a>
                    </li>
                @endif
                <li class="nav-item ">
                    <button id="searchBtn"><i class="fa fa-search"></i></button>
                </li>
                <li class="nav-item drawer-toggle-cart">
                    <a class="nav-link" id="carticon" style="cursor: pointer;" ><i class="fa fa-shopping-cart"></i></a>
                    <span class="nav-badge" id="cartcount" hidden>0</span>
                </li>
            </ul>
        </div>

        <!-- Main Menu -->
        <div class="drawer-nav" role="navigation">
            <div class="clearfix drawer-nav-brand">
                <a href="{{ route('home') }}" class="text-center">
                    <img src="{{asset('/frontend-assets/images/naipala_logo.png')}}" alt="Naipala" width="150">
                </a>
                <span class="drawerclose">
                    <i class="fa fa-times"></i>
                </span>
            </div>
            <hr class="custom-hr" style="width: 100%;">
            <!--navigation-->
            <ul class="drawer-menu">
                <li>
                    <a class="drawer-menu-item right-menu" href="{{ route('home') }}">home</a>
                </li>
                @if(Auth::user())
                    <li>
                        <div class="flex-container">
                            <a class="drawer-menu-item right-menu" href="{{ URL::to('profile') }}">{{ Auth::user()->name }}</a>
                            <div class="navToggle">
                                <i class="fa fa-chevron-down"></i>
                            </div>
                        </div>
                        <ul class="sub-menu">
                            <li><a class="drawer-menu-item right-menu" href="{{ URL::to('profile') }}">MY PROFILE</a></li>
                            <li>
                                <a class="drawer-menu-item right-menu" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">LOGOUT</a>
                                <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </li>
                        </ul>
                    </li>
                @endif
                <li>
                    <div class="flex-container">
                        <a class="drawer-menu-item right-menu" href="{{ route('collection') }}">collection</a>
                        <div class="navToggle">
                            <i class="fa fa-chevron-down"></i>
                        </div>
                    </div>
                    <ul class="sub-menu">
                        @foreach($categories as $c)
                            <?php if($c->category_name != 'Tickets'): ?>
                                <li><a class="drawer-menu-item right-menu" href="{{URL::to('collection'.'/'.$c->category_alias)}}">{{strtoupper($c->category_name)}}</a></li>
                            <?php endif; ?> 
                        @endforeach
                    </ul>
                </li>
                
                <li>
                    <div class="flex-container">
                        <a class="drawer-menu-item right-menu" href="{{ route('services') }}">services</a>
                        <div class="navToggle">
                            <i class="fa fa-chevron-down"></i>
                        </div>
                    </div>
                    <ul class="sub-menu">
                         @foreach($servicecategory as $c)
                            <?php $title=str_replace(" ","",$c->title); ?>
           
                        <li><a class="drawer-menu-item right-menu" href="{{ route('services') }}#{{ $title }}">{{$c->title}}</a></li>
                        @endforeach
                    </ul>
                </li>
                
                <li>
                    <div class="flex-container">
                        <a class="drawer-menu-item right-menu" id="optioninfo" style="cursor: pointer;">info</a>
                        <div class="navToggle">
                            <i class="fa fa-chevron-down" id="infoclick"></i>
                        </div>
                    </div>
                    <ul class="sub-menu">
                        @foreach($infos as $i)
                        <li><a class="drawer-menu-item right-menu" href="{{URL::to('info'.'/'.$i->config_alias)}}">{{strtoupper($i->config_key)}}</a></li>
                        @endforeach
                        <li><a class="drawer-menu-item right-menu" href="{{URL::to('/giftcard')}}">gift card</a></li>
                        <li><a class="drawer-menu-item right-menu" href="{{URL::to('/social')}}">social</a></li>
                        <li><a style="cursor: pointer;" class="drawer-menu-item right-menu" data-toggle="modal"
                               data-target="#newsletterModal">newsletter</a></li>
                        @if(Auth::guest())
                            <li><a style="cursor: pointer;" class="drawer-menu-item right-menu" data-toggle="modal" data-target="#loginModal">login</a></li>
                        @endif
                        <li><a style="cursor: pointer;" class="drawer-menu-item right-menu" data-toggle="modal" data-target="#contactModal">Quick
                            Contact</a>

                    </ul>
                </li>
                
                <li>
                    <a class="drawer-menu-item right-menu" href="{{URL::to('/tickets')}}">Tickets</a>
                </li>
                
                <li>
                    <a class="drawer-menu-item right-menu" href="{{URL::to('/social')}}">social / blog</a>
                </li>
                
                <li>
                    <a class="drawer-menu-item right-menu" href="{{URL::to('eCards')}}">{{ strtoupper('E-Gift Cards') }}</a>
                </li>
                
                <li>
                    <a class="drawer-menu-item right-menu" href="{{ URL::to('featuredbrands') }}">featured brands</a>
                </li>
            </ul>
            <!--navigation end-->

            <div class="share-options text-center">
                <ul>
                    <li><a href="https://www.facebook.com/naipala.au" target="_blank"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="https://www.instagram.com/naipala.au" target="_blank"><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <!-- Cart Menu -->
    @if(Request::segment(1)!='cart')
        <div id="cartapp" ng-app="cartapp" ng-controller="CartController" ng-init="current={{ $current }};init()"  class="drawer-cart drawer-right">
            <div class="drawer-nav-cart" role="navigation">
                <section>
                    <div class="clearfix drawer-nav-brand">
                        <a class="drawer-brand text-center" href="#">My Cart <i class="fa fa-shopping-cart"></i></a>
                        <span class="closecart">
                            <i class="fa fa-times"></i>
                        </span>
                    </div>
                    <hr class="custom-hr" style="width: 100%;">
                    <ul class="cart-list">
                        <li ng-repeat="c in cartitems">
                            <div class="cart-item">
                                <a class="removeFromCart" ng-click="removeItem(c.rowId)" style="cursor:pointer"><i class="fa fa-times"></i></a>
                                <div class="cart-img">
                                    <img ng-src="{{ URL::to('asset/uploads') }}/@{{ c.options['image'] }}/110/110"  alt="@{{c.name}}" nopin="nopin">
                                </div>
                                <div class="cart-details">
                                    <ul>
                                        <li>@{{c.name}}</li>
                                        <li>Price: @{{ currency_convert(c.price) }}</li>
                                        <li>Quantity: @{{c.qty}}</li>
                                        <li ng-hide ="c.options['size']==''">Size: @{{c.options['size']}}</li>
                                        <li ng-hide ="c.options['color']==''">Color: @{{c.options['color']}}</li>
                                    </ul>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <div class="clearfix drawer-cart-action">
                        <div class="text-center"><h5>Total : @{{ currency_convert(total) }}</h5></div>
                        <div class="text-center">
                            <a href="{{URL::to('/cart')}}" class="btn custom-btn checkoutBtnCart">Checkout</a>
                            <button ng-click="emptyCart()" class="btn custom-btn black-btn emptyCart_btn">Empty Cart &nbsp; <img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="emptycartloading" ></img></button>
                        </div>
                    </div>
                </section>
            </div>
            <input type="hidden" value="{{$cartitems}}" id="cartitems" ng-click="init()"/>

        </div> <!-- /.drawer cart -->
    @endif

</header>
