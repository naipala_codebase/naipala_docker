 <div class="main-panel">
        <nav class="navbar navbar-default navbar-fixed">
            <div class="container-fluid">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navigation-example-2">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="#">Admin Panel</a>
                </div>
                <div class="collapse navbar-collapse">

                    <ul class="nav navbar-nav navbar-right">

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="loadNotification()">
                                <i class="fa fa-globe"></i>
                                <b class="caret"></b>
                                <span class="notification" id='notification'>{{ $unseen }}</span>
                            </a>
                            <ul class="dropdown-menu" id="notis">
                                <div style="text-align: center;margin:10px;"><img src="{{ asset('img/loading.gif') }}" id='loader-admin' style='max-height:11px;max-width:16px;' ></div>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                                <i class="fa fa-user fa-fw"></i> {{ Auth::guard('admin_user')->user()->name }} <i class="fa fa-caret-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ route('admin.changepassword') }}">Change Password</a></li>
                                <li><a href="{{ url('/admin/logout') }}"
                                   onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                                      Logout
                                    </a>

                                    <form id="logout-form" action="{{ url('/admin/logout') }}" method="POST" style="display: none;">
                                      {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
      
    <script type="text/javascript">
        
        function getUnseen(){
            $.get("{{ route('getUnseen') }}",
                function(data,status){
                    var d = parseInt($('#notification').text());
                    $( "#notification" ).empty();
                    $( "#notification" ).html(data);
                    if(data > d){
                        var audio = new Audio('/ding.mp3');
                        audio.play(); 
                    }
                }).fail(function() {
                    location.reload();
                });
        }

        setInterval(function(){
            getUnseen();
        },5000);

        function loadNotification(){
            $.get("{{ route('admin-notification') }}",
                function(data,status){
                    $('#notis').empty();
                    for(var i=0;i<data.length; i++){
                        $('#notis').append("<li><a href='/admin/notifications/"+ data[i].id + "'>"+data[i].customerName+" purchased "+data[i].count+" items</li>");
                    }
                    if(data.length==0){
                        $('#notis').append("<div style='text-align: center;margin:20px;'>No New Sales</div>");
                    }
                    $('#notis').append("<li class='divider'></li><li><a href='/admin/notifications/all' class='text-center'>See All Transactions</a></li>");
                    
                }).fail(function() {
                    location.reload();
                });
        }
      
    </script>

  