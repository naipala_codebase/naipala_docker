<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<link rel="icon" type="image/png" href="{{asset('admin-assets/img/favicon.ico')}}"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />

	<title>Naipala Admin Panel</title>

	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <meta name="_token" content="{{ csrf_token() }}"/>

    <!-- Bootstrap core CSS     -->
    <link href="{{asset('/admin-assets/css/bootstrap.min.css')}}" rel="stylesheet" />

    <!-- Animation library for notifications   -->
    <link href="{{asset('/admin-assets/css/animate.min.css')}}" rel="stylesheet"/>

    <!--  Light Bootstrap Table core CSS    -->
    <link href="{{asset('/admin-assets/css/light-bootstrap-dashboard.css')}}" rel="stylesheet"/>


    <!--     Fonts and icons     -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet"/>
    <link href='https://fonts.googleapis.com/css?family=Roboto:400,700,300' rel='stylesheet' type='text/css'/>
    <link href="{{asset('/admin-assets/css/pe-icon-7-stroke.css')}}" rel="stylesheet" />
    <link rel="stylesheet" href="{{asset('admin-assets/css/sweetalert2.css')}}"/>
    

<!-- Include a polyfill for ES6 Promises (optional) for IE11 and Android browser -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/core-js/2.4.1/core.js"></script>
  <script src="{{asset('admin-assets/js/sweetalert2.js')}}"></script>
  <script src="{{asset('admin-assets/js/jquery-1.10.2.js')}}"></script>
  <script src="{{asset('admin-assets/js/angular.min.js')}}"></script>
<script src="https://cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
    @yield('admin-css')
</head>

<body>
<div class="wrapper">
    