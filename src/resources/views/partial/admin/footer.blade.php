
            <footer class="footer">
                <div class="container-fluid">

                    <p class="copyright pull-right">
                        &copy; 2016 <a href="http://www.creative-tim.com">Creative Tim</a>, made with love for a better web
                    </p>
                </div>
            </footer>

        </div> <!--main-pannel close -->
    </div> <!--wrapper close -->


</body>

    <!--   Core JS Files   -->
    <script src="{{asset('/admin-assets/js/jquery-1.10.2.js')}}" type="text/javascript"></script>
	<script src="{{asset('/admin-assets/js/bootstrap.min.js')}}" type="text/javascript"></script>

	<!--  Checkbox, Radio & Switch Plugins -->
	<script src="{{asset('/admin-assets/js/bootstrap-checkbox-radio-switch.js')}}"></script>

	<!--  Charts Plugin -->
	<script src="{{asset('/admin-assets/js/chartist.min.js')}}"></script>

    <!--  Notifications Plugin    -->
    <script src="{{asset('admin-assets/js/bootstrap-notify.js')}}"></script>

    <!-- Light Bootstrap Table Core javascript and methods for Demo purpose -->
	<script src="{{asset('/admin-assets/js/light-bootstrap-dashboard.js')}}"></script>

	@yield('admin-js')
    <script type="text/javascript">
        
        $(document).ready(function(){
            
            var url = $(location).attr('href').split("/").splice(0, 6);
            var action=url[4];
            if(action=='config')
            {
                if(url[5]=='landing')
                    $('#Configlanding').attr('class','active');
                else if(url[5]=='info')
                    $('#Configinfo').attr('class','active');
                else if(url[5]=='contact')
                    $('#Contact').attr('class','active');
            }
            else if(action=='review')
                $('#Review').attr('class','active');
            else if(action=='serviceCategories')
                $('#Service').attr('class','active');
            else if(action=='item')
                $('#Item').attr('class','active');
            else if(action=='social')
                $('#Social').attr('class','active');
            else if(action=='donations')
                $('#Donation').attr('class','active');
            else if(action=='giftCards')
                $('#Card').attr('class','active');
            else if(action=='eCards')
                $('#eCard').attr('class','active');
            else if(action=='newsletter')
                $('#Newsletter').attr('class','active');
            else if(action=='notifications')
                $('#Transactions').attr('class','active');
            else if(action=='featuredBrands')
                $('#FeaturedBrands').attr('class','active');
            else if(action=='footerBrands')
                $('#FooterBrands').attr('class','active');
            else if(action=='invitations')
                $('#Invitations').attr('class','active');
            else if(action=='agents')
                $('#Agents').attr('class','active');
            else if(action=='currencies')
                $('#Currencies').attr('class','active');
            else if(action=='countries')
                $('#Countries').attr('class','active');
            else
                $('#Dashboard').attr('class','active');
    	});

        $("[id*='delete']").click(function(e){
            var ele = this;
            e.preventDefault();
            
            swal({
                title: "Are you sure?",
                text: "You will not be able to recover this record!",
                type: "warning",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Delete',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-danger',
                cancelButtonClass: 'btn btn-primary'

            }).then(function(){
                ele.form.submit();
            }).catch(swal.noop);

        });
        
	</script>

</html>
