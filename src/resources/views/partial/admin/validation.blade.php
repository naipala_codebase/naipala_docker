@if(isset($errors))
	@if (count($errors) > 0)
	    @foreach ($errors->all() as $error)
	    	<div class="alert alert-danger" role="alert">
				<strong> {{ $error }} </strong>
			</div>	
	    @endforeach
	@endif
@endif
<!--  Notifications Plugin    -->

@if (Session::has('success'))
    <script type="text/javascript">
        $(document).ready(function() {
                $.notify({
                    icon: "fa fa-check",
                    message: "{{ Session::get('success') }}"
                    
                },{
                    type: 'success',
                    timer: 1000,
                    placement: {
                        from: 'bottom',
                        align: 'right'
                    }
                });
        });
    </script>
@endif

@if (Session::has('error'))
    <script type="text/javascript">
        $(document).ready(function() {
                $.notify({
                    icon: "pe-7s-close-circle",
                    message: "{{ Session::get('error') }}"
                    
                },{
                    type: 'danger',
                    timer: 1000,
                    placement: {
                        from: 'bottom',
                        align: 'right'
                    }
                });
        });
    </script>
@endif
