
 <div class="sidebar" data-color="blue" data-image="{{ asset('admin-assets/img/sidebar-5.jpg') }}">
    	<div class="sidebar-wrapper" style="max-height:100%;overflow-y:auto">
            <div class="logo">
                <a href="{{ route('admin_dashboard') }}" class="simple-text">
                    Naipala
                </a>
            </div>
            <ul class="nav" >
                <li id='Dashboard'>

                    <a href="{{URL::to('/admin')}}">
                        <i class="pe-7s-graph"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                <li id='Currencies'>
                    <a href="{{URL::to('/admin/currencies')}}">
                        <i class="fa fa-money"></i>
                        <p>Currencies</p>
                    </a>
                </li>
                <li id='Countries'>
                    <a href="{{URL::to('/admin/countries')}}">
                        <i class="fa fa-globe"></i>
                        <p>Countries</p>
                    </a>
                </li>
                <li id='Agents'>
                    <a href="{{URL::to('/admin/agents')}}">
                        <i class="pe-7s-cash"></i>
                        <p>Agents</p>
                    </a>
                </li>
                <li id='Transactions'>
                    <a href="{{URL::to('/admin/notifications/all')}}">
                        <i class="pe-7s-cash"></i>
                        <p>Transactions</p>
                    </a>
                </li>
                <li id='eCard'>
                    <a href="{{URL::to('/admin/eCards')}}">
                        <i class="fa fa-gift"></i>
                        <p>E-GiftCards</p>
                    </a>
                </li>
                <li id='Item'>
                    <a href="{{URL::to('/admin/item')}}">
                        <i class="pe-7s-cash"></i>
                        <p>Collection</p>
                    </a>
                </li>
                <li id='Service'>
                    <a href="{{route('serviceCategories.index')}}">
                        <i class="pe-7s-note2"></i>
                        <p>Services</p>
                    </a>
                </li>
                  <li id='Review'>
                    <a href="{{URL::to('/admin/review')}}">
                        <i class="pe-7s-like"></i>
                        <p>Review</p>
                    </a>
                </li>
                <li id='Contact'>
                    <a href="{{URL::to('/admin/config/contact')}}">
                        <i class="pe-7s-id"></i>
                        <p>Contact</p>
                    </a>
                </li>
                <li id='Configlanding'>
                    <a href="{{URL::to('/admin/config/landing')}}">
                        <i class="pe-7s-tools"></i>
                        <p>Home Config</p>
                    </a>
                </li>
               <li id='Configinfo'>
                    <a href="{{URL::to('/admin/config/info')}}">
                        <i class="pe-7s-tools"></i>
                        <p>Info Config</p>
                    </a>
                </li>
                <li id='Social'>
                    <a href="{{URL::to('/admin/social')}}">
                        <i class="pe-7s-tools"></i>
                        <p>Social Config</p>
                    </a>
                </li>

                <li id='Donation'>
                    <a href="{{URL::to('/admin/donations')}}">
                        <i class="pe-7s-tools"></i>
                        <p>Donation Config</p>
                    </a>
                </li>

                <li id='Card'>
                    <a href="{{URL::to('/admin/giftCards')}}">
                        <i class="pe-7s-gift"></i>
                        <p>Gift Cards</p>
                    </a>
                </li>

                <li id='FeaturedBrands'>
                    <a href="{{URL::to('/admin/featuredBrands')}}">
                        <i class="fa fa-star-o"></i>
                        <p>Featured Brands</p>
                    </a>
                </li>
                
                <li id='FooterBrands'>
                    <a href="{{URL::to('/admin/footerBrands')}}">
                        <i class="fa fa-star-o"></i>
                        <p>Footer Brands</p>
                    </a>
                </li>

                <li id='Newsletter'>
                    <a href="{{URL::to('/admin/newsletter')}}">
                        <i class="pe-7s-news-paper"></i>
                        <p>Newsletter</p>
                    </a>
                </li>
                
                <li id='Invitations'>
                    <a href="{{URL::to('/admin/invitations')}}">
                        <i class="pe-7s-mail"></i>
                        <p>Invitation Emails</p>
                    </a>
                </li>
               
            </ul>
    	</div>
    </div>

