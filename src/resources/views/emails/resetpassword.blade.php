<!DOCTYPE html>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<style type="text/css">
/* CLIENT-SPECIFIC STYLES */
body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
img { -ms-interpolation-mode: bicubic; }

/* RESET STYLES */
img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
table { border-collapse: collapse !important; }
body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

/* iOS BLUE LINKS */
a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

/* MEDIA QUERIES */
@media screen and (max-width: 480px) {
    .mobile-hide {
        display: none !important;
    }
    .mobile-center {
        text-align: center !important;
    }
    .full {
        display:block;
        text-align: center;
        width:100%;
    }
}


/* ANDROID CENTER FIX */
div[style*="margin: 16px 0;"] { margin: 0 !important; }

</style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">



<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;background-image:url({{ asset('frontend-assets/images/body.png') }});background-repeat: round;background-size:cover;">
            
            <tr style="background-image:url({{ asset('frontend-assets/images/header1.png') }});background-repeat: repeat-x;">
                <td align="center" valign="top" style="padding-bottom: 35px">
     
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style="font-size:0; font-family:brandon-grotesque; font-size: 36px; font-weight: 800; line-height: 48px;">
                    <a href="{{ route('home') }}"><img width="140" src="{{ asset('frontend-assets/images/naipala_logo.png') }}" alt="Naipala" height="59"></a>
                </td>
            </tr>
            
            <tr>
                <td align="center" style="padding:10px 35px;">
                <!--[if (gte mso 9)|(IE)]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                <td align="center" valign="top" width="600">
                <![endif]-->
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                    <tr>
                        <td align="center" style="font-family: brandon-grotesque; font-size: 16px; font-weight: 400; line-height: 24px;">
                            
                            <h2 style="font-size: 30px; font-weight: 400; line-height: 36px; color: #c72626; margin: 0;">
                                <i>Reset Password</i>
                            </h2>
                            
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-family: brandon-grotesque; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 15px;">
                            <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                Hello {{ $name }},
                            </p>
                            
                            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #333333;">
                                You are receiving this email because we received a password reset request for your account.
                                                    
                                <div style="text-align:center;margin:20px">
                                    <a href="{{ route('password.reset', $token) }}" style="background-color:#23272b;color:#ffffff;display:inline-block;font-family:brandon-grotesque;text-transform: uppercase;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Reset Password</a>
                                </div>
                                If you did not request a password reset, no further action is required.
                            </p>

                            <p style="font-size: 16px; font-weight: 600; line-height: 24px; color: #333333;">                              
                                Thanks,<br>
                                Naipala Team
                            </p>
            
                        </td>
                    </tr>
     
                </table>
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
                </td>
            </tr>

            <tr>
                <td align="center" style="padding-left: 35px;padding-right: 35px;">
                <!--[if (gte mso 9)|(IE)]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                <td align="center" valign="top" width="600">
                <![endif]-->
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                    
                    <tr class="row">
                        <td align="left" class="full" width="30%" style="font-family: brandon-grotesque; font-size: 10px; font-weight: 400; line-height: 24px; padding: 0;">
                            <p style="font-size: 10px; font-weight: 400; line-height: 18px; color: #333333;">
                                <img src="{{ asset('frontend-assets/images/pin.png') }}" style="width:10px;height:10px;"/> &nbsp;&nbsp;3/15 KING STREET<br>
                                <img src="{{ asset('frontend-assets/images/pin.png') }}" style="width:10px;height:10px;"/> &nbsp;&nbsp;ASHFIELD, 2131<br>
                                <span style="color: #cc1f45;font-weight: 600;font-size: 12px;">&#9990;</span> &nbsp;&nbsp;043 537 2002 | 045 041 9803
                            </p>
                        </td>
                        <td align="center" class="full" width="40%" style="font-family: brandon-grotesque; font-size: 14px; font-weight: 400; line-height: 24px; padding: 0;">
                            <p style="font-size: 20px; font-weight: 500; line-height: 18px; color: #cc1f45;margin-bottom: 3px;">
                                <b>NAIPALA PTY LTD</b>
                            </p>
                            <p style="font-size: 15px; font-weight: 400; line-height: 18px; color: #333333;margin-top: 0px;">
                                ABN: 46 617 717 398
                            </p>
                        </td>
                        <td align="right" class="full" width="30%" style="font-family: brandon-grotesque; font-size: 10px; font-weight: 400; line-height: 24px; padding: 0;">
                            <p style="font-size: 10px; font-weight: 400; line-height: 18px; color: #333333;">
                                <img src="{{ asset('frontend-assets/images/web.png') }}" style="width:9px;height:9px;"/>  &nbsp;&nbsp;www.naipala.com&nbsp;<br>
                                <span style="color: #cc1f45;font-weight: 600;font-size: 12px;">&#9993;</span> &nbsp;&nbsp;info@naipala.com<br>
                                <img src="{{ asset('frontend-assets/images/fb.png') }}" style="width: 10px;height: 10px;"/> <img src="{{ asset('frontend-assets/images/insta1.png') }}" style="width: 10px;height: 10px;"/>  &nbsp;/naipala.com.au&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </p>
                        </td>
                    </tr>
                    
                </table>
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
                </td>
            </tr>
            <tr style="background-image:url({{ asset('frontend-assets/images/footer1.png') }}); background-repeat: repeat-x;">
                <td align="center" valign="top" style="padding-bottom: 15px;">
                </td>
            </tr>
            {{--  <tr></tr>  --}}
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
        </td>
    </tr>
</table>

</body>
</html>
