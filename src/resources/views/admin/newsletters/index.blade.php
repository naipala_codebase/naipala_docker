@extends('layouts.admin')
@section('body')
	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
		
		
	tinymce.init({
			selector: "textarea#newsletter",
    theme: "modern",
    paste_data_images: true,
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
    toolbar2: "print preview | forecolor backcolor emoticons | template",
    image_advtab: true,
    images_upload_url: '/upload.php',
    convert_urls: false,
    // override default upload handler to simulate successful upload
    images_upload_handler: function (blobInfo, success, failure) {
        var xhr, formData;
      
        xhr = new XMLHttpRequest();
        xhr.withCredentials = false;
        xhr.open('POST', '/upload.php');
      
        xhr.onload = function() {
            var json;
        
            if (xhr.status != 200) {
                failure('HTTP Error: ' + xhr.status);
                return;
            }
        
            json = JSON.parse(xhr.responseText);
        
            if (!json || typeof json.location != 'string') {
                failure('Invalid JSON: ' + xhr.responseText);
                return;
            }
        
            success(json.location);
        };
      
        formData = new FormData();
        formData.append('file', blobInfo.blob(), blobInfo.filename());
      
        xhr.send(formData);
    },
    templates: [
			    {title: 'Newsletter1', description: 'Notice', url: "{{ asset('templates/newsletter.html') }}"}
			  ]
  });
</script>
	<div class="content">
		<div class="container-fluid">
			<div class="card">
				<div class="header">
					<h4 class="title">Create a new Newsletter</h4>
				</div>
				<div class="content">
					<form method='POST' action="{{ route('newsletters.send') }}">
						{{ csrf_field() }}
						<div class="form-group">
							<textarea id='newsletter' class="form-control" name="content" cols="50" rows="20"></textarea>
						</div>
						<input name="image" type="file" id="upload" class="hidden" onchange="">

						<br>
						<div class="form-group">
							<button type='submit' class='btn btn-primary btn-fill btn-md'>Send</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

@endsection