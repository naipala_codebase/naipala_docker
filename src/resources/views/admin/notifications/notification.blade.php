@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
		<div class="card ">
		    <div class="header">
				<a href="{{URL::to('/admin/notifications/all')}}" class="btn btn-info btn-fill pull-right" >Back</a>
		        <h4 class="title">Transaction No-{{$transaction->id}}</h4>
		    </div>


		    <div class="content">

	    		<p><b>Customer Name: </b>{{ $transaction->transactionable->name }}</p>
	    		<p><b>Customer Email: </b>{{ $transaction->transactionable->email }}</p>
	    		<p><b>Customer Contact: </b>{{ $transaction->shipping_contact }}</p>
	    		<p><b>Order Date: </b>{{ date('M j, Y h:i a', strtotime($transaction->updated_at)) }}</p>
					<p><b>Agent Id: </b>{{ $transaction->agent_code }}</p>
				
					<p><b>Shipping Address: </b>
						<span>{{ $transaction->shipping_address }}, {{ $transaction->apt_suit }} </span><br>
						<span style="margin-left:136px">{{ $transaction->suburb }}, {{ $transaction->city }}, {{ $transaction->state }} {{ $transaction->postcode }}</span><br>
						<span style="margin-left:136px"> {{ $transaction->country }}</span>
					</p>
	    		<span class="pull-right">
	    			<form  action="{{ route('confirmDelivery',$transaction->id) }}" method="POST">
	    				{{ csrf_field() }}
	    				<input onchange="this.form.submit()	" type="checkbox" style="transform:scale(2);" {{ $transaction->status == 'shipped' ?'checked':'' }}/>
	    				<label> &nbsp;&nbsp;Confirm Delivery</label>
	    			</form>
	    		</span>
	    		<br>
	    		<h2>Order(s)</h2>
	    		<table class="table">
	    			<thead>
	    				<th>#</th>
	    				<th width="45%">Item</th>
	    				<th width="25%">Details</th>
						<th width="10%">Quantity</th>
						<th width="15%">Price</th>

	    			</thead>
	    			<tbody>
	    				<?php $details = json_decode($transaction->details); $subtotal = 0; ?>
	    				@foreach ($details as $item)
	    				<tr>
	    					<th>{{ $loop->iteration }}</th>
	    					<td>{{ $item->item }}</td>
	    					<td>{{ ($item->details->color =='') ? $item->details->size : ($item->details->color . ($item->details->size == '' ? '':'('. $item->details->size .')')) }}</td>

							<td>{{ $item->quantity }} </td>
							<td>&nbsp;&nbsp;${{ $item->quantity * $item->price }} </td>
							<?php $subtotal += $item->price * $item->quantity ?>
	    				</tr>
						@endforeach
						<table cellspacing="0" cellpadding="0" border="0" width="100%" style="border-top: 1px solid #eeeeee;">
							@if($transaction->donation!=0 || $transaction->giftCard)
								<tr>
									<td width="85%" align="right" style="padding: 10px;">
										Sub Total&nbsp;&nbsp;
									</td>
									<td width="15%" align="left" style="padding: 10px;">
										&nbsp;&nbsp;${{ $subtotal }} 
									</td>
								</tr>
								@if($transaction->giftCard)
									<tr>
										<td width="85%" align="right" style="padding: 5px 10px;">
											Gift Card (${{ $transaction->giftCard->discount }})&nbsp;&nbsp;
										</td>
										<td width="15%" align="left" style="padding: 5px 10px;">
											- ${{ ($transaction->total - $transaction->donation == 0) ? $subtotal : $transaction->giftCard->discount }}
										</td>
									</tr>
								@endif
								@if($transaction->donation != 0)
									<tr>
										<td width="85%" align="right" style="padding: 5px 10px;">
											Donation&nbsp;&nbsp;
										</td>
										<td width="15%" align="left" style="padding: 5px 10px;">
											&nbsp;&nbsp;${{ $transaction->donation }}
										</td>
									</tr>
								@endif
							@endif
							<tr>
								<td width="85%" align="right" style="padding: 10px;border-top: 1px solid #eeeeee;">
									TOTAL&nbsp;&nbsp;
								</td>
								<td width="15%" align="left" style="padding: 10px;border-top: 1px solid #eeeeee;">
									&nbsp;&nbsp;${{ $transaction->total }}
								</td>
							</tr>
						</table>
	    			</tbody>
	    		</table>
	    	</div>
    	</div>
	</div>
</div>

@endsection