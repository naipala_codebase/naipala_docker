@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
		<div class="card ">
		    <div class="header">
		        <h4 class="title">Transactions</h4>
		    </div>


		    <div class="content">
	    		<h2>Order(s)</h2>
	    		<table class="table">
	    			<thead>
	    				<th>ID</th>
	    				<th>Details</th>
	    				<th>Purchase Date</th>
						<th>Agent Id</th>
	    				<th>Delivered</th>
	    				<th></th>

	    			</thead>
	    			<tbody>
	    				
	    				@foreach ($notifications as $n)
	    				<tr>
	    					<th>{{ $n['id'] }}</th>
	    					<td>{{ $n['customerName'] }} purchased {{ $n['count'] }} items </td>
	    				    <td>{{ date('M j, Y', strtotime($n['updated_at'])) }}</td>
							<td>{{$n['agent_code']}}</td>
							<td>
	    						@if($n['status'] == 'shipped')
	    							&nbsp;&nbsp;<i class='fa fa-check'></i>
	    						@endif
	    					</td>
	    					<td><a href="{{ route('notification',$n['id']) }}" class="btn btn-simple">View Full</a></td>
	    				</tr>
	    				@endforeach
	    			</tbody>
	    		</table>
	    		<div class="text-center">
	    			{{ $transactions->links() }}
	    		</div>
	    	</div>
    	</div>
	</div>
</div>

@endsection