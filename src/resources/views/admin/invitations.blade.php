@extends('layouts.admin')

@section('body')


    <div class="content">
        <div class="container-fluid">
            <div class="card ">
                <div class="header">
                    <h4 class="title">Emails for Invitation
                        <form method="POST" class="pull-right" action="{{ route('invitations.reset') }}">
                            {{ csrf_field() }}
                            <button type="button" class="btn btn-danger btn-fill btn-sm " id="deleteEmailAll" style="margin-left:  5px;">Reset</button>
                        </form>
                        <a href="{{ route('invitations.all') }}" class="btn btn-primary btn-fill btn-sm pull-right" target="_blank">View All</a>
                    </h4>
                    <p class="category">Total Emails: {{ $count }}</p>
                    
                </div>
                <div class="content">
                    <div class="table-full-width">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Email ID.</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($emails as $email)
                                    <tr>
                                        <td>{{ $loop->iteration + (($emails->currentPage()-1) * $emails->perPage()) }}</td>
                                        <td>{{$email->email}}</td>
                                        <td>
                                            <form method="POST" action="{{ route('invitations.destroy',$email->id) }}">
                                                <input name="_method" type="hidden" value="DELETE">
                                                {{ csrf_field() }}
                                                <button type="button" rel="tooltip" id="deleteEmail{{ $email->id }}" title="Remove" class="btn btn-danger btn-fill btn-simple btn-sm">
                                                    Delete
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                

                            </tbody>
                        </table>
                    </div>
        
                    <div class="text-center">
                        {{ $emails->links() }}    
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

