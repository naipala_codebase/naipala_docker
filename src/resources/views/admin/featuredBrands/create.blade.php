@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{URL::to('admin/featuredBrands')}}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Add new Featured Brand</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('featuredBrands.store') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Brand Name</label>
                        <input type="text" id="name" class="form-control" name="name" required/>
                        <br>
                        <label>Description</label>
                        <textarea id="description" rows="8" class="form-control" name="description" required></textarea>
                        <br>
                        <label>Website Link</label>
                        <input id="link" value="http://" class="form-control" name="link" required/>
                        <br>
                        <label>Brand Logo</label>
                        <input type="file" id="logo" class="form-control" name="logo" accept="image/*" required/>
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Add"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
