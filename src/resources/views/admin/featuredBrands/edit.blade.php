@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{URL::to('admin/featuredBrands')}}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Edit Featured Brand</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('featuredBrands.update',$featured_brand->id) }}"  enctype="multipart/form-data">                            
                    <input type="hidden" name="_method" value="PATCH" >
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Brand Name</label>
                        <input type="text" id="name" class="form-control" name="name" value="{{ $featured_brand->name  }}" required/>
                        <br>
                        <label>Description</label>
                        <textarea id="description" rows="6" class="form-control" name="description"  required>{{ $featured_brand->description  }}</textarea>
                        <br>
                        <label>Website Link</label>
                        <input id="link" value="{{ $featured_brand->link }}" class="form-control" name="link" required/>
                        <br>
                        <label>Brand Logo</label>
                        <input type="file" id="logo" class="form-control" name="logo" accept="image/*"/>
                        <div><br><b>Current : </b>
                            <img src="{{ asset('/featured_brands'.'/'.$featured_brand->logo) }}" alt="{{ $featured_brand->name }}" style="width:50px;height: 50px;" />
                        </div>
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Save"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
