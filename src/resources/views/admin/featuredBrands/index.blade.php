@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('featuredBrands.create') }}" class="btn btn-primary btn-fill btn-sm pull-right" style="margin:10px">Add New Brand</a>
                    
                    <div class="header">
                        <h4 class="title">Featured Brands</h4>
                        <p class="category">Add, edit or remove featured brands</p>
                    </div>
                    <div class="content">
                        <div class="row">
                            @forelse($featured_brands as $brand)
                                <div class="col-md-4">
                                    <div class="card card-user" style="background:#f1f1f1">
                                        <div class="" style="height: 70px">
                                        </div>
                                        <div class="content">
                                            <div class="author">
                                                <img class="avatar" src="{{ asset('/featured_brands'.'/'.$brand->logo) }}" alt="{{ $brand->name }}" style="border: 5px solid #929191; border-radius: 0%;"/>
                                                <h4 class="title">{{ $brand->name }} </h4>
                                            </div><br>
                                            <p class="description"> {{ strlen($brand->description)>140 ? substr($brand->description,0,140).'...' : $brand->description }}</p>
                                        </div>
                                        <hr>
                                        <div class="text-center">
                                            <a href="{{ $brand->link }}" class="btn btn-simple" target="_blank" title="{{ $brand->link }}"><i class="fa fa-link"></i></a>
                                            <a href="{{ route('featuredBrands.edit',$brand->id) }}" class="btn btn-simple"><i class="fa fa-pencil"></i></a>
                                            <form action="{{ route('featuredBrands.destroy',$brand->id) }}" method="POST" style="display:inline">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE" >
                                                <button id='deleteBrand{{$brand->id}}' type="button" class="btn btn-simple"><i class="fa fa-trash-o"></i></button>                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p class="text-center" style="margin:150px"> No Featured Brands Added Yet</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection