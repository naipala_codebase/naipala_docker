@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('agents.create') }}" class="btn btn-primary btn-fill btn-sm pull-right" style="margin:10px">Add New Agent</a>
                    
                    <div class="header">
                        <h4 class="title">Agents</h4>
                        <p class="category">Add, edit or remove agents</p>
                    </div>
                    <div class="content">
                            <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                      <thead>
                                        <th>Id</th>
                                          <th>Name</th>
                                          <th>Email</th>
                                          <th>Contact</th>
                                          <th>Buisness Id</th>
                                          <th>Status</th>
                                          <th>Actions</th>
                                      </thead>
                                      <tbody>
                                          @foreach($agents as $i)
                                          <tr>
                                              <td>{{$i->id}}</td>                  
                                              <td>{{$i->name}}</td>
                                              <td>{{$i->email}}</td>
                                             
                                              <td>{{$i->contact}}</td>
                                              <td>{{$i->agent_code}}</td>
                                             
                                                <td>
                                                  <a href="{{URL::to('admin/agents/changestatus'.'/'.$i->id)}}"><span class="badge">{{$i->agent_status}}</span></a>
                                              </td>
                                              <td>
                                                  <a href="{{URL::to('admin/agents'.'/'.$i->id.'/'.'edit')}}"><span class="badge">edit</span></a>
                                                  <a href="{{URL::to('admin/agents'.'/'.$i->id.'/'.'purchases')}}"><span class="badge">view purchases</span></a>
                                                  
                                              <form method="POST" action="{{ URL::to('admin/agents'.'/'.$i->id) }}">
                                                <input name="_method" type="hidden" value="DELETE">
                                                {{ csrf_field() }}
                                                <button class="badge" type="button" id="deleteItem" border="1px"> 
                                                  delete
                                                </button>
                                              </form>
                                              
                                          
                                              </td>
                                          </tr>
                                          @endforeach
                                      </tbody>
                                    </table>
                                  </div>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection