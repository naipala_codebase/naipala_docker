@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    
                    <div class="header">
                        <h4 class="title">{{$agent->name}} Transaction List</h4>
                    </div>
                    <div class="content">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <th>Id</th>
                                    <th>Name</th>
                                    <th>Actions</th>
                                </thead>
                                <tbody>
                                    @foreach ($purchases as $n)
                                    <tr>
                                        <th>{{ $n['id'] }}</th>
                                        <td>{{ $n['customerName'] }} purchased {{ $n['count'] }} items </td>
                                        
                                        <td><a href="{{ route('notification',$n['id']) }}" class="btn btn-simple">View Full</a></td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection