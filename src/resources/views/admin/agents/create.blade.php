@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{URL::to('admin/agents')}}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Add new Agent</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('agents.store') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Agent Name</label>
                        <input type="text" id="name" class="form-control" name="name" required/>
                        <br>
                        <label>Email</label>
                        <input type="email" id="email" class="form-control" name="email" required/>
                        <br>
                        <label>Contact</label>
                        <input type="text" id="name" class="form-control" name="contact" required/>
                        <br>
                        <label>Status</label>
                        <select class="form-control" name="agent_status">
                            <option value="active">active</option>
                            <option value="dormant">dormant</option>
                            
                        </select>
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Add"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
