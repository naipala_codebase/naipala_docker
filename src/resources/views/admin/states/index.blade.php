@extends('layouts.admin')

@section('body')
<div class="content" style="padding-top:0">
    <div class="container-fluid">
        <div style="text-align:right;margin: 10px;">
            <a href="{{ route('countries.index') }}" class="btn btn-sm btn-info btn-fill" >Back</a>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('states.create', $country->id) }}" class="btn btn-primary btn-fill btn-sm pull-right" style="margin:10px">Add New</a>
                    
                    <div class="header">
                        <h4 class="title">{{ $country->term_for_administrative_divisions }}</h4>
                    </div>
                    <div class="content">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                          <thead>
                            <th>S.N.</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                              @foreach($states as $i)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>                  
                                    <td>{{$i->name}}</td>
                                    <td>{{$i->code}}</td>
                                    <td>
                                        <a href="{{ route('states.edit',[$country->id,$i->id]) }}" class="btn btn-sm btn-warning" title="Update Details"><i class="fa fa-edit"></i></a>
                                        
                                        <form method="POST" action="{{ route('states.destroy',[$country->id,$i->id]) }}" style="display:inline">
                                          <input name="_method" type="hidden" value="DELETE">
                                          {{ csrf_field() }}
                                          <button type="button" class="btn btn-sm btn-danger" id="deleteItem" title="Delete"> 
                                            <i class="fa fa-trash"></i>
                                          </button>
                                        </form>
                                    
                                
                                    </td>
                                </tr>
                              @endforeach
                          </tbody>
                        </table>
                      </div>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection