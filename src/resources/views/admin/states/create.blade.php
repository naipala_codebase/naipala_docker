@extends('layouts.admin')
@section('body')

<div class="content" ng-app="states" style="padding-top:0">
    <div class="container-fluid">
        <div style="text-align:right;margin: 10px;">
            <a href="{{ route('states.index',$country_id) }}" class="btn btn-sm btn-info btn-fill" >Back</a>
        </div>
        <div class="card" ng-controller="StateController">

            <div class="header">
                <button class="btn btn-success btn-fill pull-right" ng-click="addState()" rel="tooltip" title="add new"><i class="fa fa-plus"></i></button>
                <h4 class="title">Add new State</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('states.store',$country_id) }}">
                    {{ csrf_field() }}   
                    <table class="table table-striped table-bordered table-hover" >
                        <thead>
                            <th width="20%">Code</th>
                            <th>Name</th>
                            <th width="10%"></th>
                        </thead>
                        <tbody>
                            <tr ng-repeat="s in states">
                                <td><input class="form-control" type="text" name="states[@{{ $index }}][code]" placeholder="Code" required /></td>
                                <td><input class="form-control" type="text" name="states[@{{ $index }}][name]" placeholder="Name" required /></td>
                                <td><span class="input-group-btn" ng-hide="states.length==1"><button type="button" ng-click="removeState($index)" class="btn btn-danger btn-simple btn-md" rel="tooltip" title="remove"><i class="fa fa-remove"></i></button></span></td>
                            </tr>
                        </tbody>
                    </table>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-info btn-fill" value="Save"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
@section('admin-js')
    <script src="{{asset('admin-assets/scripts/states.js')}}"></script>
@stop