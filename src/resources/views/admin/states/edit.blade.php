@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{ route('states.index',$state->country_id) }}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Edit</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('states.update',[$state->country_id,$state->id]) }}"  enctype="multipart/form-data">                            
                    <input type="hidden" name="_method" value="PATCH" >
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Code</label>
                        <input type="text" id="code" class="form-control" name="code" value="{{ $state->code }}" required/>
                            
                        <label>Name</label>
                        <input type="text" id="name" class="form-control" name="name" value="{{ $state->name }}" required/>
                        <br>
                      
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Save"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
