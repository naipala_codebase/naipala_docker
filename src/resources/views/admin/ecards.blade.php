@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="header">
                        <h4 class="title"><b>E-Gift Card Transactions</b></h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover">
                            <thead>
                                <th>Transaction Id</th>
                                <th>Amount $</th>
                                <th>Purchase Date</th>
                                <th>Sender Email</th>
                                <th>Receiver Email</th>
                                <th>Gift Card Id</th>
                                <th>Gift Card Status</th>
                            </thead>
                            <tbody>
                                @foreach($etransactions as $et)
                                    <tr>
                                        <td>{{ $et->id }}</td>
                                        <td>{{ $et->amount }}</td>
                                        <td>{{ date('M j, Y', strtotime($et->updated_at)) }}</td>
                                        <td>{{ $et->sender_email }}</td>
                                        <td>{{ $et->recipient_contact }}</td>
                                        <td>{{ $et->giftCard->id }}</td>
                                        <td>
                                            @if($et->giftCard->status != 'pending')
                                                {{ $et->giftCard->status }}
                                            @else
                                                <form method="POST" action="{{ route('gc.changeStatus',$et->giftCard->id) }}">
                                                    {{ csrf_field() }}
                                                    <select id="changeCard{{$et->giftCard->id}}">
                                                        <option>pending</option>
                                                        <option>unused</option>
                                                    </select>
                                                </form>
                                            @endif    
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @if(count($etransactions)==0)
                            <div class="text-center" style="margin:150px"> <h5>No E-gift cards purchased yet </h5></div>
                        @endif
                        <div class="text-center">
                            {!! $etransactions->links() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('admin-js')
<script type="text/javascript">

    $("[id*='changeCard']").change(function(e){
            var ele = this;
            e.preventDefault();
            
            swal({
                title: "Are you sure?",
                text: "The transaction asscociated with the giftcard will be deleted!",
                type: "warning",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Change Status to Unused',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-danger',
                cancelButtonClass: 'btn btn-primary'

            }).then(function(){
                ele.form.submit();
            }).catch(swal.noop);

        });
</script>
@endsection