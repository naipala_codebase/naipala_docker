@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{ route('currencies.index') }}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Add new Currency</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('currencies.store') }}">
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Currency Name</label>
                        <input type="text" id="name" class="form-control" name="name" value="{{ old('name') }}" required/>
                        <br>
                        <label>Code</label>
                        <input type="text" id="code" class="form-control" name="code" value="{{ old('code') }}" required/>
                        <br>
                        <label>Symbol</label>
                        <input type="text" id="symbol" class="form-control" name="symbol" value="{{ old('symbol') }}" required/>
                        <br>
                        <label>Exchange Rate</label>
                        <input type="number" id="exchange_rate" min="0" class="form-control" name="exchange_rate" step="0.000001" value="{{ old('exchange_rate') }}" required/>
                        <br>
                        <label>Status</label>
                        <select class="form-control" name="active" required>
                            <option value="1" selected>Active</option>
                            <option value="0">Dormant</option>
                            
                        </select>
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Add"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
