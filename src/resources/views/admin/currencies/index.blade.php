@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('currencies.create') }}" class="btn btn-primary btn-fill btn-sm pull-right" style="margin:10px">Add New Currency</a>
                    
                    <div class="header">
                        <h4 class="title">Currencies</h4>
                        <p class="category">Add, edit or remove currencies</p>
                    </div>
                    <div class="content">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                          <thead>
                            <th>S.N.</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Symbol</th>
                            <th>Exchange Rate</th>
                            <th>Status</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                              @foreach($currencies as $i)
                                <tr>
                                    <td>{{ $loop->iteration + (($currencies->currentPage()-1) * $currencies->perPage()) }}</td>                  
                                    <td>{{$i->name}}</td>
                                    <td>{{$i->code}}</td>
                                    <td>{{$i->symbol}}</td>
                                    <td>{{$i->exchange_rate}}</td>
                                    
                                    <td>
                                        @if($i->code == 'AUD')
                                          <span class="badge">{{$i->active ? 'active' : 'inactive'}}</span>
                                        @else
                                          <a href="{{URL::to('admin/currencies/changestatus'.'/'.$i->id)}}"><span class="badge">{{$i->active ? 'active' : 'inactive'}}</span></a>
                                        @endif
                                    </td>
                                    <td>
                                      @if($i->code != 'AUD')
                                        <a href="{{URL::to('admin/currencies'.'/'.$i->id.'/'.'edit')}}" class="btn btn-sm btn-warning" title="Update Details"><i class="fa fa-edit"></i></a>
                                        
                                        <form method="POST" action="{{ URL::to('admin/currencies'.'/'.$i->id) }}" style="display:inline">
                                          <input name="_method" type="hidden" value="DELETE">
                                          {{ csrf_field() }}
                                          <button type="button" class="btn btn-sm btn-danger" id="deleteItem" title="Delete"> 
                                            <i class="fa fa-trash"></i>
                                          </button>
                                        </form>
                                      @endif
                                
                                    </td>
                                </tr>
                              @endforeach
                          </tbody>
                        </table>

                        <div class="text-center">
                          {!! $currencies->links() !!}
                        </div>
                      </div>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection