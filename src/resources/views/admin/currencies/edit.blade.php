@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{URL::to('admin/currencies')}}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Edit Currency</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('currencies.update',$currency->id) }}"  enctype="multipart/form-data">                            
                    <input type="hidden" name="_method" value="PATCH" >
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Currency Name</label>
                        <input type="text" id="name" class="form-control" name="name" value="{{ $currency->name }}" required/>
                        <label>Code</label>
                        <input type="text" id="code" class="form-control" name="code" value="{{ $currency->code }}" required/>
                        <br>
                        <label>Symbol</label>
                        <input type="text" id="symbol" class="form-control" name="symbol" value="{{ $currency->symbol }}" required/>
                        <br>
                        <label>Exchange Rate</label>
                        <input type="number" id="exchange_rate" class="form-control" name="exchange_rate" min="0" step="0.000001" value="{{ $currency->exchange_rate }}" required/>
                        <br>
                        <label>Status</label>
                        <select class="form-control" name="active" required>
                            <option value="1" {{ $currency->active ? 'selected' : '' }}>Active</option>
                            <option value="0" {{ !$currency->active ? 'selected' : '' }}>Dormant</option>
                            
                        </select>
                        <br>
                      
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Save"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
