@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            
                <div class="card">
                    <div class="header">
                        <h4 class="title"><b> All Gift Cards</b></h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover">
                            <thead>
                                <th>Id.</th>
                                <th>Discount %</th>
                                <th>Promo-Code</th>
                                <th>Exipires On</th>
                                <th>Batch No. </th>
                                <th>Status</th>
                            </thead>
                            <tbody>
                                @foreach($cards as $card)
                                    <tr>
                                        <td>{{ $card['id'] }}</td>
                                        <td>{{ $card['discount'] }}</td>
                                        <td>
                                            {{$card['promocode']}}
                                        </td>
                                      
                                        <td>{{ date('M j, Y', strtotime($card['expires_on'])) }}</td>
                                        <td>{{$card['batch']}}</td>
                                        <td>{{ $card['status'] }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
           
            
        </div>
    </div>
</div>
@endsection