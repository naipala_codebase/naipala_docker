@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-9">
                <div class="card">
                    <div class="header">
                        <h4 class="title"><b>Gift Cards</b><button class="btn btn-primary btn-fill btn-sm pull-right" onclick='verify("all")'>View all</button></h4>
                    </div>
                    <div class="content table-responsive table-full-width">
                        <table class="table table-hover">
                            <thead>
                                <th>Id</th>
                                <th>Discount $</th>
                                <th>Promo-Code</th>
                                
                                <th>Exipires On</th>
                                <th>Batch No.</th>
                                <th>Status</th>
                                <th>Actions</th>
                            </thead>
                            <tbody>
                                @foreach($cards as $card)
                                    <tr>
                                        <td>{{ $card->id }}</td>
                                        <td>{{ $card->discount }}</td>
                                        <td>
                                            *******
                                            <button class="btn btn-primary btn-simple btn-md" style='margin-bottom:5px;' onclick='verify({{ $card->id }})'><i class="fa fa-eye"></i></button>
                                        </td>
                                
                                        <td>{{ date('M j, Y', strtotime($card->expires_on)) }}</td>
                                        <td>{{ $card->batch }}</td>
                                        <td>
                                            @if($card->status != 'pending')
                                                {{ $card->status }}
                                            @else
                                                <form method="POST" action="{{ route('gc.changeStatus',$card->id) }}">
                                                    {{ csrf_field() }}
                                                    <select id="changeCard{{$card->id}}">
                                                        <option>pending</option>
                                                        <option>unused</option>
                                                    </select>
                                                </form>
                                            @endif
                                        </td>
                                        <td>
                                            <form class="form-horizontal" role="form" method="POST" action="{{ route('giftCards.destroy',$card->id) }}" style='display:inline-block'>
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE">
                                                <button type="button" id="deleteCard{{$card->id}}" class="btn btn-danger btn-fill btn-sm">
                                                    <i class='pe-7s-trash'></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <div class="text-center">
                            {!! $cards->links() !!}
                        </div>

                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <div class="well">
                    <form method='POST' action="{{ route('giftCards.store') }}">
                       
                        {{ csrf_field() }}
                        <h3>Add Card</h3>
                        <hr>
                        <label for="discount">Batch:</label>
                        <input id="discount" type="number" min="1" class="form-control" name="batch" required/>
                        
                        <label for="discount">Discount:</label>
                        <input id="discount" type="number" class="form-control" name="discount" min='1'  required/>
                        
                        <label for="expires_on">Expires On:</label>
                        <input id="expires_on" type="date" class="form-control" name="expires_on"  required/>
                        <br>
                        <button type="submit" class="btn btn-primary btn-fill btn-block">
                            Add
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<form action="{{ route('viewAll') }}" method="POST"  hidden id="sucessViewAll">
    {{ csrf_field() }}
    <button type="submit" ></button>
</form>
@endsection
@section('admin-js')
<script type="text/javascript">
    function verify(id){
        swal({
          title: 'Verify Password',
          input: 'password',
          showCancelButton: true,
          confirmButtonText: 'Verify',
          showLoaderOnConfirm: true,
          preConfirm: function (password) {
            return new Promise(function (resolve, reject) {
              setTimeout(function() {
                if (password === '') {
                  reject('Enter the Password.')
                } else {
                  resolve()
                }
              }, 1000)
            })
          },
          allowOutsideClick: false
        }).then(function(password) {
                $.ajaxSetup({
                      headers: {
                          'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                      }
                  });
                $.ajax(
                {
                    type: "post",
                    url: "/admin/giftCards/showcode/"+id,
                    data: { password: password },
                    success: function(data){

                        if(id=='all' && data.type=="success"){
                            
                            
                                $('#sucessViewAll').submit();
                            
                        }
                        else{
                            swal({
                                type: data.type,
                                title: '',
                                html: 'Result: ' + '<b>'+data.code+'</b>'
                              });
                        }
                    }
                });
                
            });
    }

    $("[id*='changeCard']").change(function(e){
            var ele = this;
            e.preventDefault();
            
            swal({
                title: "Are you sure?",
                text: "The transaction asscociated with the giftcard will be deleted!",
                type: "warning",
                showCloseButton: true,
                showCancelButton: true,
                confirmButtonColor: '#d33',
                cancelButtonColor: '#3085d6',
                confirmButtonText: 'Change Status to Unused',
                cancelButtonText: 'Cancel',
                confirmButtonClass: 'btn btn-danger',
                cancelButtonClass: 'btn btn-primary'

            }).then(function(){
                ele.form.submit();
            }).catch(swal.noop);

        });
</script>
@endsection