@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
	<div class="row">
		
		<div class="col-md-9">
			<div class="card">
				<div class="header">
					<h4 class="title">Service Categories</h4>
					<p class="category">Add, edit or remove category</p>
				</div>
				<div class="content">
					<table class="table">
						<thead>
							<th>#</th>
							<th>Title</th>
							<th></th>
						</thead>
						<tbody ng-app='Edit' ng-controller='EditController' ng-init='ini({{ $service_categories }})'>
							@foreach ($service_categories as $category)
								<tr>
									<th>{{ $loop->iteration }}</th>
									<td contenteditable='false' id='data{{ $loop->iteration }}' style='' ng-model="asd[{{ $loop->iteration }}]" ng-value="{{ $category->title }}">{{ $category->title }} </td>
									<td>&nbsp;&nbsp;
										<a style="" id='edit{{$loop->iteration}}' class="btn btn-primary btn-fill btn-sm" ng-click='Edit({{$loop->iteration}})'>Edit </a>
										<a style="display:none" id='save{{$loop->iteration}}' class="btn btn-success btn-fill btn-sm" ng-click='Save({{$loop->iteration}},{{ $category->id}} )'>Save </a>
										<a style="display:none" id='cancel{{$loop->iteration}}' class="btn btn-primary btn-fill btn-sm" ng-click='Cancel({{$loop->iteration}})'>Cancel </a>
										<form class="form-horizontal" role="form" method="POST" action="{{ route('serviceCategories.destroy',$category->id) }}" style='display:inline-block'>
											{{ csrf_field() }}
											<input type="hidden" name="_method" value="DELETE">
											<button type="button" id="deleteService" class="btn btn-danger btn-fill btn-sm">
												Delete
											</button>
										</form>
									</td>
								</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-3">
			<div class="well">
				<form class="form-horizontal" role="form" method="POST" action="{{ route('serviceCategories.store') }}">
                        {{ csrf_field() }}
                        <h3>New Category</h3>
                        <hr>
                        <label for="title">Title:</label>
                        <input id="title" type="text" class="form-control" name="title" required>
                        <br>
                        <button type="submit" class="btn btn-primary btn-fill btn-block">
                            Add
                        </button>
                </form>
			</div>
		</div>

	</div>
</div>
</div>
	<script type="text/javascript">
		var app = angular.module('Edit',[]);
		app.controller('EditController',function($scope,$http){
			$scope.edited = '';
			$scope.asd = [];
			$scope.cat = [];
			$scope.ini=function(cat){
				$scope.cat = cat;
				var ed='';
				$scope.asd.push(ed);
				for(var i=0;i<cat.length;i++){
					ed = cat[i].title;
					$scope.asd.push(ed);
				}
			}

			$scope.revertToNormal = function(id){
				$("#data"+id).prop('contenteditable', false);
				$("#data"+id).attr('style',"");
				$("#edit"+id).attr('style',"");
				$("#save"+id).attr('style',"display:none");
				$("#cancel"+id).attr('style',"display:none");
			}
			
			$scope.Edit=function(id){
				$("#data"+id).prop('contenteditable', true);
				$("#data"+id).attr('style',"border:1px solid #ccc");
				$("#edit"+id).attr('style',"display:none");
				$("#save"+id).attr('style',"");
				$("#cancel"+id).attr('style',"");
				for(var i=1;i<=$scope.cat.length;i++){
					if(i!=id){
						$scope.revertToNormal(i);
						$scope.asd[i] = $scope.cat[i-1].title;
					}
				}
			}

			$scope.Save=function(loop,id){
				if($scope.asd[loop].length==0 || $scope.asd[loop].length>255) {
					$scope.asd[loop] = $scope.cat[loop-1].title;
					return;
				}
				$('#save'+loop).html("<img src='/img/loading.gif' id='loader-admin' style='max-height:11px;max-width:16px;' >");
				var params = {title:$scope.asd[loop]};
				$http.post("/admin/serviceCategories/"+id+"/edit", params).success(function (response) {
					
					$scope.cat[loop-1].title = response.data;					
					if($scope.asd[loop]==response.data){
						$scope.revertToNormal(loop);
					}
					else
						$scope.asd[loop]=response.data;
					$('#save'+loop).html("Save");
				});
			}

			$scope.Cancel=function(id){
				$scope.revertToNormal(id);
				$scope.asd[id] = $scope.cat[id-1].title;
			}
		});

		app.directive("contenteditable", function() {
		  return {
		    restrict: "A",
		    require: "ngModel",
		    link: function(scope, element, attrs, ngModel) {

		      function read() {
		        ngModel.$setViewValue(element.html());
		      }

		      ngModel.$render = function() {
		        element.html(ngModel.$viewValue || "");
		      };

		      element.bind("blur keyup change", function() {
		        scope.$apply(read);
		      });
		    }
		  };
		});
	</script>
@stop