@extends('layouts.admin')

@section('body')
<style type="text/css">
    .card .numbers {
        font-size: 2em;
        text-align: right; 
    }
    .card .numbers p {
      margin: 0; 
    }
    .card .icon-big {
        font-size: 3em;
        min-height: 64px; 
    }
    .icon-info {
      color: #68B3C8; 
    }

    .icon-success {
      color: #7AC29A; 
    }
    .icon-primary {
        color: #7A9E9F;
    }

</style>
<div class="content">
    <div class="container-fluid">
        <div class="header">
            <h3 class="title">Dashboard</h3><br>
        </div>
        
        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-4 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-primary text-center">
                                            <i class="fa fa-calendar"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Today's Visits</p>
                                            {{$todaysvisitors}}
                                            
                                        </div>
                                    </div>

                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-success text-center">
                                            <i class="fa fa-dollar"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Sales</p>
                                            ${{ $sales }}
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr />
                                    <div class="stats">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-sm-6">
                        <div class="card">
                            <div class="content">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="icon-big icon-info text-center">
                                            <i class="fa fa-eye"></i>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <div class="numbers">
                                            <p>Total Visits</p>
                                            {{ $totalvisitors }}
                                        </div>
                                    </div>
                                </div>
                                <div class="footer">
                                    <hr>
                                    <div class="stats">
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row" style="">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Active Users (Total: {{ $count }})
                                <div class="pull-right" style="display:inline">
                                    <a href='{{ route("admin.active_users")}}' target="_blank">View all <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>User Id</th>
                                                <th>Name</th>
                                                <th>Email ID.</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($active_users as $user)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{ $user->id }}</td>
                                                <td>{{ $user->name }}</td>
                                                <td>{{$user->email}}</td>
                                            </tr>
                                            @endforeach
                                            

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div class="row" style="">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Subscribers 
                                <div class="pull-right" style="display:inline">
                                    <a href='{{ route("admin.subscribers")}}' target="_blank">View all <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Email ID.</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($subscribers as $u)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td>{{$u->email}}</td>
                                            </tr>
                                            @endforeach
                                            

                                        </tbody>
                                    </table>
                                </div>
                                <div class="text-center">
                                    {{$subscribers->appends(['users' => $users->currentPage()])->links()}}    
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-md-6 col-sm-12 col-xs-12">

                        
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                Users
                                <div class="pull-right" style="display:inline">
                                    <a href='{{ route("admin.users")}}' target="_blank">View all <i class="fa fa-arrow-circle-right"></i></a>
                                </div>
                            </div> 
                            <div class="panel-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>User Name</th>
                                                <th>Email ID.</th>
                                                <th>Contact</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $u)
                                                <tr>
                                                    <td>{{ $loop->iteration }}</td>
                                                    <td>{{$u->name}}</td>
                                                    <td>{{$u->email}}</td>
                                                    <td>{{$u->phone}}</td>
                                                </tr>
                                            @endforeach
                                            

                                            </tbody>
                                        </table>
                                </div>
                                <div class="text-center">
                                    {{$users->appends(['subscribers' => $subscribers->currentPage()])->links()}}    
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

