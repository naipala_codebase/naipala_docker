@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{URL::to('admin/footerBrands')}}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Add new Brand</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('footerBrands.store') }}"  enctype="multipart/form-data">
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" name="category" required>
                            <option value="left">{{ $left}}</option>
                            <option value="right">{{ $right }}</option>
                        </select>
                        <br>
                        <label>Website Link</label>
                        <input id="link" value="http://" class="form-control" name="link" required/>
                        <br>
                        <label>Brand Logo</label>
                        <input type="file" id="logo" class="form-control" name="logo" accept="image/*" required/>
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Add"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
