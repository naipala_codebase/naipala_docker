@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{URL::to('admin/footerBrands')}}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Edit Brand</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('footerBrands.update',$footer_brand->id) }}"  enctype="multipart/form-data">                            
                    <input type="hidden" name="_method" value="PATCH" >
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Category</label>
                        <select class="form-control" name="category" required>
                            <option value="left" {{ $footer_brand->category == 'left' ? 'selected' : '' }}>{{ $left}}</option>
                            <option value="right" {{ $footer_brand->category == 'right' ? 'selected' : '' }}>{{ $right }}</option>
                        </select>
                        <br>
                        <label>Website Link</label>
                        <input id="link" value="{{ $footer_brand->link }}" class="form-control" name="link" required/>
                        <br>
                        <label>Brand Logo</label>
                        <input type="file" id="logo" class="form-control" name="logo" accept="image/*"/>
                        <div><br><b>Current : </b>
                            <img src="{{ asset('footer_brands') .'/'. $footer_brand->logo }}" alt="{{ $footer_brand->name }}" style="width:50px;height: 50px;" />
                        </div>
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Save"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
