@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card" id="categories">
                    <div class="header">
                        <h4 class="title">Category Titles</h4>
                    </div>
                    <div class="content">
                        <div class="table-full-width">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <td class="td-actions">
                                        <span id="titleleft">{{$left}}</span>
                                        <div id="ColupdateDivleft" hidden>
                                            <form class="form" method="POST" action="{{URL::to('admin/footerBrands/left/edit')}}">
                                                <div class="row form-body">
                                                    <div class="form-group col-sm-6">
                                                        {{csrf_field()}}

                                                        <input type="text" class="form-control" placeholder="Edit Title" name="title" value="{{ $left }}" required> 
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <button type="submit" class="btn btn-primary"> 
                                                            <i class="fa fa-check"></i> 
                                                        </button>

                                                        <button class="btn btn-danger" type="button" id="Colcancelleft">
                                                            <i class="fa fa-times"></i>
                                                        </button>          
                                                    </div>
                                                </div> 
                                            </form>
                                        </div>
                                        <a href="javascript:void(0)"  style="margin-left:10px" id="Coleditleft"> <i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>

                                <tr>
                                    <td class="td-actions">
                                        <span id="titleright">{{$right}}</span>
                                        <div id="ColupdateDivright" hidden>
                                            <form class="form" method="POST" action="{{URL::to('admin/footerBrands/right/edit')}}">
                                                <div class="row form-body">
                                                    <div class="form-group col-sm-6">
                                                        {{csrf_field()}}

                                                        <input type="text" class="form-control" placeholder="Edit Title" name="title" value="{{ $right }}" required> 
                                                    </div>
                                                    <div class="form-group col-sm-6">
                                                        <button type="submit" class="btn btn-primary"> 
                                                            <i class="fa fa-check"></i> 
                                                        </button>

                                                        <button class="btn btn-danger" type="button" id="Colcancelright">
                                                            <i class="fa fa-times"></i>
                                                        </button>          
                                                    </div>
                                                </div> 
                                            </form>
                                        </div>
                                        <a href="javascript:void(0)"  style="margin-left:10px" id="Coleditright"> <i class="fa fa-edit"></i></a>
                                    </td>
                                </tr>        
                            </tbody>
                        </table>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('footerBrands.create') }}" class="btn btn-primary btn-fill btn-sm pull-right" style="margin:10px">Add New Brand</a>
                    
                    <div class="header">
                        <h4 class="title">Brands</h4>
                        <p class="category">Add, edit or remove brands</p>
                    </div>
                    <div class="content">
                        <div class="row">
                            @forelse($footer_brands as $brand)
                                <div class="col-md-4">
                                    <div class="card" style="">
                                        <div class="content">
                                            <div style="text-align:center">
                                                <img class="" src="{{ asset('footer_brands').'/'. $brand->logo }}" style="width:80px;height: 80px;">
                                            </div><br>
                                        </div>
                                        <hr>
                                        <div class="text-center">
                                            <a href="{{ $brand->link }}" class="btn btn-simple" target="_blank" title="{{ $brand->link }}"><i class="fa fa-link"></i></a>
                                            <a href="{{ route('footerBrands.edit',$brand->id) }}" class="btn btn-simple"><i class="fa fa-pencil"></i></a>
                                            <form action="{{ route('footerBrands.destroy',$brand->id) }}" method="POST" style="display:inline">
                                                {{ csrf_field() }}
                                                <input type="hidden" name="_method" value="DELETE" >
                                                <button id='deleteBrand{{$brand->id}}' type="button" class="btn btn-simple"><i class="fa fa-trash-o"></i></button>                                                
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p class="text-center" style="margin:150px"> No Brands Added Yet</p>
                            @endforelse
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('admin-js')

<script type="text/javascript">
    $(document).ready(function() {
        $("[id*='Coledit']").click(function(){
            var id = $(this).attr("id").slice(7);
            $('#title'+id).attr("hidden","true");
            $(this).css("display","none");
            $("#ColupdateDiv"+id).removeAttr("hidden");
        });

        $("[id*='Colcancel']").click(function(){
            var id = $(this).attr("id").slice(9);
            $('#title'+id).removeAttr("hidden");
            $('#Coledit'+id).css("display","inline-block");
            $("#ColupdateDiv"+id).attr("hidden","true");
        });
    });
</script>
@endsection