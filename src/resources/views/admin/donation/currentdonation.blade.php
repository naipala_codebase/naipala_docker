@extends('layouts.admin')

@section('body')

<div class='content' >
    <div class="container-fluid">
        <h5 style="text-align: right;margin-top: -16px;"><a href="{{ route('alldonations') }}" class="btn btn-info btn-fill btn-sm" >Back</a></h5>

        <div class="card" id="donationdescription">
            <div class="header">
                <h4 class="title">Donation Page Content</h4>
                <p class="category">Add or edit Content for Donation page</p>
            </div>
            <div class="content">
                <form id="donationform" class="form" method="POST" enctype="multipart/form-data" action="{{ route('updateDonationPage') }}">
                    {{ csrf_field() }}
                    <div class="form-group">
                        <label>Title</label>
                        <input type="text" id="title" class="form-control" value="{{ $donation->title }}" name="title" required/>
                    </div>
                    <div class="form-group">
                        <label>Image</label>
                        <input type="file" id="image" class="form-control" name="image"/>
                        <div><br>Current:<img src="{{ asset('uploads'.'/'.$donation->image) }}" style="height:100px;width:100px" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Description</label>
                        <div class="editable">
                            {!! $donation->description !!}
                        </div>
                    </div>
                    <hr>
                    <div style="text-align:right">
                        <button type="button" id="donationsubmit" class="btn btn-info btn-fill" >Save</button>
                    </div>
                </form>   
            </div>
        </div>
    </div>
</div>
@endsection

@section('admin-js')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    
    $(document).ready(function(){
		tinymce.init({
            selector: 'div.editable',
            inline: true,
            theme: "modern",
            relative_urls: false,
            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
            toolbar2: "print preview | forecolor backcolor emoticons"
        });

        $('#donationsubmit').click(function(e){
            param = $('#mce_1').html();
            param = "<textarea name='description' hidden>" + param + "</textarea>";
            $('#donationform').append(param);
            this.form.submit();
        });
    });
</script>
@endsection