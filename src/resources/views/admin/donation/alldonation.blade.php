@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <h5 style="text-align: right;margin-top: -16px;"><a href="{{ route('newdonation') }}" class="btn btn-primary btn-fill btn-sm pull-right" style="margin:10px">Add New Donation</a></h5>

        <div class="row">
            <div class="col-md-12">
                <div class="card" id="donationtext">
                    <div class="header">
                        <h4 class="title">Shipping Donation Text</h4>
                        <p class="category">Add or edit Text for Purchase page</p>
                    </div>
                    <div class="content">
                
                        <form id="descriptionform" class="form" method="POST" action="{{ route('updateDonationText') }}">
                            {{ csrf_field() }}
                            <div class="editable">
                                {!! $shipping_donation_text !!}
                            </div>
                            <hr>
                            <div style="text-align:right">
                                <button type="button" id="descriptionsubmit" class="btn btn-info btn-fill" >Save</button>
                            </div>
                        </form>   
                    </div>
                </div>
                <div class="card">

                    <div class="header">
                        <h4 class="title">Donations</h4>
                        <p class="category">See All Donations</p>
                    </div>
                    <div class="content">
                        <div class="row">
                            @forelse($donations as $donation)
                                <div class="col-md-12">
                                    <div class="card card-user" style="background: {{ $donation->id == $latest ? '#eae6e6' : '#f1f1f1' }}">
                                        <div class="" style="height: 70px">
                                        </div>
                                        <div class="content">
                                            <div class="author">
                                                <img class="avatar" src="{{ asset('/uploads'.'/'.$donation->image) }}" alt="{{ $donation->title }}" style="border: 5px solid #929191; border-radius: 0%;"/>
                                                <h4 class="title">{{ $donation->title }} </h4>
                                            </div><br>
                                            <p class="description"> {!! $donation->description !!}</p>
                                        </div>
                                        <hr>
                                        <div class="text-center">
                                            @if($donation->id == $latest)
                                                <a href="{{ route('currentdonation') }}" class="btn btn-simple"><i class="fa fa-pencil"></i></a>
                                            @endif

                                            @if($donation->id != $latest)
                                                <form action="{{ route('destroy',$donation->id) }}" method="POST" style="display:inline">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="_method" value="DELETE" >
                                                    <button id='deletedonation{{$donation->id}}' type="button" class="btn btn-simple"><i class="fa fa-trash-o"></i></button>                                                
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            @empty
                                <p class="text-center" style="margin:150px"> No Donation Added Yet</p>
                            @endforelse

                            <div class="text-center">
                                {!! $donations->links() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('admin-js')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
    
    $(document).ready(function(){
		tinymce.init({
            selector: 'div.editable',
            inline: true,
            theme: "modern",
            relative_urls: false,
            plugins: [
                "advlist autolink lists link charmap print preview hr anchor pagebreak",
                "searchreplace wordcount visualblocks visualchars code",
                "insertdatetime media nonbreaking save table contextmenu directionality",
                "emoticons paste textcolor colorpicker textpattern"
            ],
            toolbar1: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link",
            toolbar2: "print preview | forecolor backcolor emoticons"
        });

        $('#descriptionsubmit').click(function(e){
            param = $('#mce_1').html();
            param = "<textarea name='donationText' hidden>" + param + "</textarea>";
            $('#descriptionform').append(param);
            this.form.submit();
        });
    });
</script>
@endsection
