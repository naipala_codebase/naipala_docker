@extends('layouts.admin')

@section('body')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="header">
                            <h4 class="title">Change Password</h4>
                        </div>
                        <div class="content">
                            <form action="{{ route('admin.changedpassword') }}" method="POST">
                                {{ csrf_field() }}
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-weight: 600">Old Password</label>
                                            <input type="password" class="form-control" placeholder="New Password" name="oldPassword" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-weight: 600">New Password</label>
                                            <input type="password" class="form-control" placeholder="New Password"  name="password" required>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-weight: 600">Confirm Password</label>
                                            <input type="password" class="form-control" placeholder="Confirm New Password"  name="password_confirmation" required>
                                        </div>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-info btn-fill">Update</button>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection