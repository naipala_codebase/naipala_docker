@extends('layouts.admin')

@section('body')
<div class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <a href="{{ route('countries.create') }}" class="btn btn-primary btn-fill btn-sm pull-right" style="margin:10px">Add New Country</a>
                    
                    <div class="header">
                        <h4 class="title">Countries</h4>
                        <p class="category">Add, edit or remove countries</p>
                    </div>
                    <div class="content">
                      <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                          <thead>
                            <th>S.N.</th>
                            <th>Name</th>
                            <th>Code</th>
                            <th>Actions</th>
                          </thead>
                          <tbody>
                              @foreach($countries as $i)
                                <tr>
                                    <td>{{ $loop->iteration + (($countries->currentPage()-1) * $countries->perPage()) }}</td>                  
                                    <td>{{$i->name}}</td>
                                    <td>{{$i->code}}</td>
                                    <td>
                                        @if($i->code != 'AU')
                                          <a href="{{URL::to('admin/countries'.'/'.$i->id.'/'.'edit')}}" class="btn btn-sm btn-warning" title="Update Details"><i class="fa fa-edit"></i></a>
                                        @endif
                                        <a href="{{URL::to('admin/countries'.'/'.$i->id.'/'.'states')}}" class="btn btn-sm btn-primary" title="Add {{ $i->term_for_administrative_divisions }}"><i class="fa fa-sitemap"></i></a>
                                        
                                        @if($i->code != 'AU')
                                          <form method="POST" action="{{ URL::to('admin/countries'.'/'.$i->id) }}" style="display:inline">
                                            <input name="_method" type="hidden" value="DELETE">
                                            {{ csrf_field() }}
                                            <button type="button" class="btn btn-sm btn-danger" id="deleteItem" title="Delete"> 
                                              <i class="fa fa-trash"></i>
                                            </button>
                                          </form>
                                        @endif
                                    
                                
                                    </td>
                                </tr>
                              @endforeach
                          </tbody>
                        </table>

                        <div class="text-center">
                          {!! $countries->links() !!}
                        </div>
                      </div>
                           
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection