@extends('layouts.admin')
@section('body')

<div class="content">
    <div class="container-fluid">
        <div class="card">

            <div class="header">
                <a href="{{ route('countries.index') }}" class="btn btn-info btn-fill pull-right" >Back</a>
                <h4 class="title">Add new Country</h4>
            </div>
            <div class="content">
                <form class="form" method="POST" action="{{ route('countries.store') }}">
                    {{ csrf_field() }}    
                    <div class="form-group">
                        <label>Country Name</label>
                        <input type="text" id="name" class="form-control" name="name" value="{{ old('name') }}" required/>
                        <br>
                        <label>Code</label>
                        <input type="text" id="code" class="form-control" name="code" value="{{ old('code') }}" required/>
                        <br>
                        <label>Term for administrative divisions</label>
                        <input type="text" id="symbol" class="form-control" name="term_for_administrative_divisions" value="{{ old('term_for_administrative_divisions') }}" required/>
                        
                        <br>
                    </div>
                    
                    <div class="form-group">
                        <input type="submit" class="btn btn-success btn-fill" value="Add"/>
                    </div>

                </form>
            </div>
        
        </div>
    </div>
</div>
@endsection
