@foreach($secatajx as $service)
        <div class="card services-info-list">
            <div class="card-header si-list-header">
                <section class="si-header-date">
                    <div>
                        <span>{{ date('M j, Y', strtotime($service->updated_at)) }}</span>
                    </div>

                </section>
                <section class="si-header-title">
                    <div>
                        <h3>{{ $service->title }}</h3>
                    </div>
                    <div>
                        <a href="javascript:void(0)" class="badge badge-dark">{{ $service->service_category->title }}</a>
                    </div>
                </section>
            </div>
            <div class="card-body si-list-body">
                <p class="" style="white-space: pre-wrap;">{{ $service->description }}</p>
            </div>
            <div class="si-list-footer">
                <div class="user-avatar-block">
                    <div class="user-avatar">
                        @if( $service->user->image!=null)
                            <img src=" {{ asset('profile_images/' . $service->user->image) }}" alt="{{ $service->user->name }}">
                        @else
                            <img src="{{asset('frontend-assets/images/profile.png')}}" alt="{{ $service->user->name }}">
                        @endif
                    </div>
                    <div class="user-info">
                        <h5>{{ $service->user->name }}</h5>
                    </div>
                </div>
                @if(Auth::check())
                    <button class="btn btn-secondary btn-sm" onclick='bookService({{ $service->id }})' {{ $service->user->id==Auth::user()->id ? 'hidden':'' }} >
                        Apply
                    </button>
                @else
                    <button class="btn btn-secondary btn-sm" onclick="guestInfoBook({{$service->id}})">
                        Apply
                    </button>
                @endif
            </div>
        </div>
        @if($loop->iteration==count($secatajx))

            <script type="text/javascript">
                var loop = '{{$id}}';
                $('#lastItem'+loop).val('{{ $service->updated_at }}');
            </script>
        @endif
@endforeach