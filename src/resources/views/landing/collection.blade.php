@extends('layouts.app')

@section('body')

<style>
        @media (min-width: 200px){
            .container {
                max-width: 480px;
            }
            #collection .collection-header > p{
                font-size: 1.25rem !important;
                font-family: 'Barlow Semi Condensed',sans-serif;
            }
        }
        @media (min-width: 376px){
            .container {
                max-width: 480px;
            }
            #collection .collection-header > p{
                font-size: 1.5rem !important;
                font-family: 'Barlow Semi Condensed',sans-serif;
            }
        }
    @media (min-width: 576px){
        .container {
            max-width: 650px;
        }
        #collection .collection-header > p{
            font-size: 1.75rem !important;
            font-family: 'Barlow Semi Condensed',sans-serif;
        }
    }
   
    @media (min-width: 768px){
        .container {
            max-width: 830px;   
        } 
        #collection .collection-header > p{
            font-size: 2.0rem !important;
            font-family: 'Barlow Semi Condensed',sans-serif;
        }
    }
   
    @media (min-width: 992px) {
        .container{
            max-width: 1100px;
        }
        #collection .collection-header > p{
            font-size: 2.25rem !important;
            font-family: 'Barlow Semi Condensed',sans-serif;
        }
    } 
    @media (min-width: 1200px) {
        .container{
            max-width: 1280px;
        }
        #collection .collection-header>p{
            font-size: 2.5rem !important; 
            font-family: 'Barlow Semi Condensed',sans-serif;
        }
    }
     
</style>
<style>
        @media (min-width: 200px){
   
            .product .product-overlay .product-title{
                    font-size:1rem;
                    font-weight:600;
                    color:#000;
                    margin-bottom:16px
                }
                .product-block .product .product-overlay .product-price{
                    color:#000;
                    font-size:.8rem;
                    text-align:center;
                    padding:0 0 10px;
                    font-family:'Barlow Semi Condensed',sans-serif
                }
                .product-block .product .product-overlay .product-options{
                    margin-bottom:10px
                    ;display:block
                }
                .product-block .product .product-overlay .product-options select{
                    display:block;
                    max-width:160px;
                    min-width:150px;
                    min-height:24px;
                    max-height:24px;
                    background:#ebebeb;
                    border:1px solid #bfbfbf;
                    padding:0 5px;
                    font-size:.8rem;
                    margin:0 auto 10px;
                    font-weight:700;
                    text-transform:uppercase;
                    -webkit-border-radius: 0;  /* Safari 3-4, iOS 1-3.2, Android 1.6- */    
                    -moz-border-radius: 0;
                }
                .product-block .product .product-overlay .product-options .addToCart{
                    display:block;
                    font-size:.8rem;
                    background:#444;
                    max-width:160px;
                    min-width:150px;
                    min-height:24px;
                    max-height:24px;
                    padding:4px 0;
                    font-weight:700
                }
                .product-block .product .product-overlay .product-options .addToCart:hover{
                    color:#000
                }
                .product-block .product .product-overlay .product-actions{
                    text-align:center;
                    display:-webkit-box;
                    display:-ms-flexbox;
                    display:flex;
                    -webkit-box-pack:center;
                    -ms-flex-pack:center;
                    justify-content:center;
                    width:100%;
                    padding:0 15px 7px}
                    .product-block .product .product-overlay .product-actions a{
                        display:block;
                        font-size:.65em;
                        font-weight:700;
                        text-decoration:none;
                        margin:5px 10px;
                        font-family:'Barlow Semi Condensed',sans-serif;color:#000
                    }
                    .product-block .product .product-overlay .product-actions a:focus,.product-block .product .product-overlay .product-actions a:hover{
                        outline:0;
                        text-decoration:none
                    }
                    .product-block .product .product-overlay .product-actions a.awishlist i{
                        font-size:.6rem;
                        color:#23272b;
                        margin-right:5px
                    }
                    .product-block .product .product-overlay .stock-details{
                        text-transform:uppercase;
                        margin:.5rem 0 0;
                        font-weight:700
                    }
                    .product-block  .product:active .product-overlay{
                        
                    }
                    .product-block  .product:active .product-img img{
                        -webkit-filter:blur(.03rem) opacity(.25);
                        filter:blur(.03rem) opacity(.25)
                    }
                }
        @media (min-width: 900px){
   
    .product .product-overlay .product-title{
            font-size:1rem;
            font-weight:600;
            color:#000;
            margin-bottom:16px
        }
        .product-block .product .product-overlay .product-price{
            color:#000;
            font-size:.8rem;
            text-align:center;
            padding:0 0 10px;
            font-family:'Barlow Semi Condensed',sans-serif
        }
        .product-block .product .product-overlay .product-options{
            margin-bottom:10px
            ;display:block
        }
        .product-block .product .product-overlay .product-options select{
            display:block;
            max-width:160px;
            min-width:150px;
            min-height:24px;
            max-height:24px;
            background:#ebebeb;
            border:1px solid #bfbfbf;
            padding:0 5px;
            font-size:.8rem;
            margin:0 auto 10px;
            font-weight:700;
            text-transform:uppercase;
            -webkit-border-radius: 0;  /* Safari 3-4, iOS 1-3.2, Android 1.6- */    
            -moz-border-radius: 0;
        }
        .product-block .product .product-overlay .product-options .addToCart{
            display:block;
            font-size:.8rem;
            background:#444;
            max-width:160px;
            min-width:150px;
            min-height:24px;
            max-height:24px;
            padding:4px 0;
            font-weight:700
        }
        .product-block .product .product-overlay .product-options .addToCart:hover{
            color:#000
        }
        .product-block .product .product-overlay .product-actions{
            text-align:center;
            display:-webkit-box;
            display:-ms-flexbox;
            display:flex;
            -webkit-box-pack:center;
            -ms-flex-pack:center;
            justify-content:center;
            width:100%;
            padding:0 15px 7px}
            .product-block .product .product-overlay .product-actions a{
                display:block;
                font-size:.65em;
                font-weight:700;
                text-decoration:none;
                margin:5px 10px;
                font-family:'Barlow Semi Condensed',sans-serif;color:#000
            }
            .product-block .product .product-overlay .product-actions a:focus,.product-block .product .product-overlay .product-actions a:hover{
                outline:0;
                text-decoration:none
            }
            .product-block .product .product-overlay .product-actions a.awishlist i{
                font-size:.6rem;
                color:#23272b;
                margin-right:5px
            }
            .product-block .product .product-overlay .stock-details{
                text-transform:uppercase;
                margin:.5rem 0 0;
                font-weight:700
            }
            .product-block .product:hover .product-overlay{
                visibility:visible;
                opacity:1
            }
            .product-block .product:hover .product-img img{
                -webkit-filter:blur(.03rem) opacity(.25);
                filter:blur(.03rem) opacity(.25)
            }
        }
        .badge {
            opacity: .8;
        }
</style>

<div id="collection">
    <section class="container">
        <div class="collection-header">
            <p id="collectiondesc">
               {!! $desc !!}
            </p>
        </div>
        <div class="row ads-box-container">
            @forelse($items as $i)
            <!--<div class="col-lg-3 col-md-4 col-6 product-list">-->
                <div class="col-lg-4 col-md-4 col-sm-6 product-list">
                    <div class="product-block">
                        <section class="product">
                            <a style="margin:0;" href="{{ route('collection.itemDetail',$i->item_alias) }}">
                        
                                <div class="product-img">
                                    
                                        @if ($i->item_images()->first()==null)
                                            <img src="{{asset('frontend-assets/images/cream.jpg')}}" id="itemimage{{$i->item_id}}" alt="{{$i->item_name}}" nopin="nopin"/>
                                        @else
                                            <img src="{{ route('optimize', ['uploads',$i->item_images->first()->image_link,700,526]) }}" id="itemimage{{$i->item_id}}" alt="{{$i->item_name}}" nopin="nopin"/>
                                        @endif
                                
                                </div>
                                <div class="product-overlay">
                                    <div class="product-actions">
                                        @if(Auth::guest())
                                            <a class="awishlist" data-toggle="modal" data-target="#loginModal" style="cursor:pointer">
                                                <span><i class="fa fa-heart"></i>Wishlist</span>
                                            </a>
                                        @else
                                            <a class="awishlist" id="awishlist{{$i->item_id}}" style="cursor:pointer">
                                                <span><i class="fa fa-heart"></i>Wishlist</span>
                                            </a>
                                        @endif
                                        <a class="viewDetails" id="quickViewItem{{ $i->item_id }}" item-description="{{ $i->item_description }}" item-name="{{$i->item_name}}" item-price="{{$i->display_price}}" item-alias="{{$i->item_alias}}" item-images="{{ $i->item_images ? $i->item_images->pluck('image_link'): [] }}">
                                            <span>Quick View</span>
                                        </a>
                                    </div>
                                
                                    <a href="{{ route('collection.itemDetail',$i->item_alias) }}" class="product-title" style="text-align: center;">{{$i->item_name}}</a>
                                    <div class="product-options">
                                        <?php $details=json_decode($i->details,true);?>
                                        <?php $c=0?>
                                        @foreach($details as $d)
                                            <?php $c=$c+$d['quantity']?>
                                        @endforeach
                                        @if($c>0)
                                            <select id="size{{ $i->item_id }}"  class=""  {{($details[0]['size']!='' || $details[0]['color']!='')?"":'hidden'}} >
                                                @foreach($details as $detail)
                                                    <?php $det = json_encode($detail); ?>
                                                    @if($detail['quantity']!=0)
                                                        <option value="{{ $det }}">{{ ($detail['color'] =='') ? $detail['size'] : ($detail['color'].($detail['size'] =='' ? '':'('. $detail['size'].')')) }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @endif
                                        @if($c==0)
                                            <button class="btn custom-btn addToCart">Sold Out</button>
                                        @else
                                            <button id="addToCart{{$i->item_id}}"  class="btn custom-btn addToCart" >Add to Cart &nbsp;  <img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="loading{{$i->item_id}}" nopin="nopin"/></button>
                                        @endif
                                    </div>
                                    <div class="product-price">{{ $i->display_price }}</div>
                                </div>
                            </a>
                        </section>
                    </div>
                </div>
                <?php $isNoItem = false; ?>
            @empty
                <p style="padding: 0 3rem;
                text-align: center;
                margin: 1rem auto 1rem;"> <strong>No Items to Display. </strong></p>
                
                <?php $isNoItem = true; ?>
            @endforelse
        </div>
        
        @if((isset($isSearch) && $isSearch) || $isNoItem)
            <!-- Google Ads Div-->
            <!-- CollectionSearch -->
            <div class="card google-ads" style="width:80%; height:auto;">
                <div class="card-header ads-box-header">
                    <section class="si-header-date">
                        <div></div>
                    </section>
                    <section class="ads-box-header-title">
                        <div>
                            <h3>Sponsored</h3>
                        </div>
                        <div>
                            <a href="javascript:void(0)" class="badge badge-dark">Ads</a>
                        </div>
                    </section>
                </div>
                <div class="card-body ads-body">
                    <!-- Google Ads Div - ServicePageAds -->
                    <ins class="adsbygoogle"
                    style="display:inline-block; width:100%; overflow:hidden;"
                    data-ad-client="ca-pub-9044710729904359"
                    data-ad-slot="5054916444"
                    data-ad-format="auto"
                    data-full-width-responsive="true"></ins>
                    <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        @endif
    </section>
</div>
<div class="modal fade" id="quickViewModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" style="bottom:5%;top:2%;" role="document">
        <div class="modal-content">
            <div class="modal-header" style="flex-direction: row-reverse; border: none; padding-top:0px;padding-bottom:0px;">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="padding-top:0px;padding-bottom:0px;">
                <div class="sg-product-section text-center">
                    
                    <div class="sg-product-details" style="background: rgba(255, 255, 255, 0.15);">
                        <div class="sg-main-image flex-container p-0">
                            <div id="itemhomeslider" style="height:330px; width:435px;" class="carousel slide carousel-fade" data-ride="carousel">
                                <ol class="carousel-indicators" id="imagesliderli">
                                </ol>
                                <div class="carousel-inner" id="imagesliderdiv">
                                    
                                </div>
                                <a class="carousel-control-prev" href="#itemhomeslider" role="button" data-slide="prev">
                                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Previous</span>
                                </a>
                                <a class="carousel-control-next" href="#itemhomeslider" role="button" data-slide="next">
                                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                                    <span class="sr-only">Next</span>
                                </a>
                            </div>
                        </div>
                        <br>
                        <div class="sg-product-title">
                            <div class="sg-header">
                                <h1 id="itemName"></h1>
                            </div>
                            <hr>
                        </div>
                        <div class="sg-product-info">
                            <div id="accordion" role="tablist" aria-multiselectable="true">
                                <div class="sg-product-accordion">
                                    <div id="collapseOne" class="collapse show" role="tabpanel"
                                        aria-labelledby="productInfo">
                                        <div class="acc-body"  id="itemDescription">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="sg-price">
                            <span id="itemPrice"></span>
                        </div>
                        <a href="" class="btn custom-btn"  id="itemDetail">View Details</a>
                        
                        <div class="sg-product-actions">
                            <div class="share-options">
                                <h6>share</h6>
                                <ul>
                                    <li><a href="" id="itemShare" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="https://www.pinterest.com/pin/create/button/" id="itemPin" data-pin-custom="true" data-pin-do="buttonBookmark" onmouseover="this.style.color='black'" onmouseout="this.style.color='white'" style="color:white;cursor:pointer" title="Share on pinterest"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(document).ready(function(e){
        $('[id*="addToCart"]').click(function(e){
            var id=$(this).attr('id').slice(9);
            var _attr= JSON.parse($('#size'+id).val());
            var attr = JSON.stringify({"size":_attr.size,"color":_attr.color});
            $('#loading'+id).removeAttr('hidden');
            var image=$('#itemimage'+id).attr('src');
            $.ajax({
                type: "POST",
                url: "{{URL::to('/cart/additem')}}",
                data: {id:id, _token:'{{csrf_token()}}',details:attr,image:image}, // serializes the form's elements.
                success: function(data)
                {
                        $('#cartitems').val(data);
                        $('#loading'+id).attr('hidden','true');
                        angular.element($('#cartapp')).scope().$apply("init()");
                        $('#carticon').trigger('click');
                },
                error:function(data)
                {
                    if(data.responseJSON)
                        aalert(data.responseJSON.errors,'','Oops..');
                    else
                        aalert('Something Went Wrong','','Oops..');
                    $('#loading'+id).attr('hidden','true');
                }
            });

            // $(this).removeAttr('hidden');
        });

        $('[id*="quickViewItem"]').click(function(){
            var images = JSON.parse($(this).attr('item-images'));
            var htmlli = '';
            var htmldiv = '';
            for (var i=0;i<images.length;i++){
                htmlli += '<li data-target="#itemhomeslider" data-slide-to="'+i+'" class="'+ ((i==0) ? 'active' : '') +'"></li>';
                
                htmldiv += '<div class="carousel-item '+ ((i==0) ? 'active' : '') +'"> <img class="d-block w-100" style="max-height:330px; max-width:435px;"  src="{{ asset("uploads")."/" }}'+images[i]+'" alt="'+$(this).attr('item-name')+" "+(i+1)+'" data-pin-url="{{url()->current()}}/item/'+$(this).attr("item-alias")+'"></div>';    
            }
            $('#imagesliderli').html(htmlli);
            $('#imagesliderdiv').html(htmldiv);
            $('#itemName').html($(this).attr('item-name'));
            $('#itemDescription').html($(this).attr('item-description'));
            $('#itemDetail').attr('href','{{ url()->current() }}/item/'+$(this).attr('item-alias'));
            $('#itemPrice').html($(this).attr('item-price'));
            $('#itemShare').attr('href','https://www.facebook.com/sharer/sharer.php?app_id=1576279952450591&kid_directed_site=0&u={{urlencode(url()->current())}}/item/'+$(this).attr('item-alias')+'&display=popup&ref=plugin&src=share_button');
            $('#quickViewModal').modal('show');
        });

        $('[id*="awishlist"]').click(function(e){
            var id=$(this).attr('id').slice(9);
            var attr=$('#size'+id).val();
            $('#wloading').removeAttr('hidden');
            $('#wishlisttrigger').trigger('click');
            var image=$('#itemimage'+id).attr('src');
            $.ajax({
                type: "POST",
                url: "{{URL::to('/wishlist/additem')}}",
                data: {id:id, _token:'{{csrf_token()}}',details:attr,image:image}, // serializes the form's elements.
                success: function(data)
                {
                    $('#wishlistitems').val(data);
                    $('#wloading').attr('hidden','true');
                    angular.element($('#wishlist')).scope().$apply("init()");
                    
                },
                error:function(data)
                {
                    if(data.responseJSON)
                        aalert(data.responseJSON.errors,'','Oops..');
                    else
                        aalert('Something Went Wrong','','Oops..');
                    $('#wloading').attr('hidden','true');
                }
            });
        });
    });
</script>
<script type="text/javascript">
    (function(d){
        var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
        p.type = 'text/javascript';
        p.async = true;
        p.src = '//assets.pinterest.com/js/pinit.js';
        f.parentNode.insertBefore(p, f);
    }(document));
</script>
@stop
















