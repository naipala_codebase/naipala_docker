@extends('layouts.app')

@section('body')
<div id="about-us">
    <section class="container info-pg-container">
        <div class="row">
            <div class="col-12 aboutUs-wrapper">
                <div class="info-pg-heading">
                    <h1>Unsubscribe?</h1>
                </div>
                <hr class="info-page-hr">
                <div class="aboutUs-details">
                    <form method="POST" action="{{route('unsubscribed',$token)}}">
                        {{ csrf_field() }}
                        <div style="text-align: center;">
                            <p style="margin:50px">You will no longer be updated.</p>
                            <a href="{{ route('home') }}" class="btn custom-btn">Cancel</a>
                            <button class="btn custom-btn" type="submit">Unsubscribe</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

@endsection
