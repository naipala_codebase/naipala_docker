@extends('layouts.app')

@section('body')
<div id="eGift-card">
    <section class="container info-pg-container">
        <div class="row">
            <div class="col-12 eGift-wrapper">
                <div class="info-pg-heading">
                    <h1>NAIPALA E-GIFT CARD</h1>
                </div>
                <hr class="info-page-hr">
                <div class="eGift-details">
                    <div class="row info-img-container">
                        <div class="col-sm-4 info-img"><img src="{{ asset('frontend-assets/images/gc1.png') }}" alt=""></div>
                        <div class="col-sm-4 info-img"><img src="{{ asset('frontend-assets/images/gc2.png') }}" alt=""></div>
                        <div class="col-sm-4 info-img"><img src="{{ asset('frontend-assets/images/gc3.png') }}" alt=""></div>
                    </div>
                    <div class="eGift-terms">
                        <ul class="info-pg-terms">
                            <li>
                                <p>Treat your loved ones in a simple <a target="_blank" href="{{ route('ecards.index') }}" style="color:#007bff">click!</a></p>
                            </li>
                            <li><p>Give the gift that not just makes receiver happy but also the sender, producer and us. Share happiness with the Naipala eGift Card!
                                </p>
                            </li>

                            <li>
                                <p>Whether you're looking to celebrate birthdays, seasonal festivals or just because they’re worth it, you can add an engraved message to convoy your gift.</p>
                            </li>
                            <li>
                                <p>You can write your preferred message and then any amount between $25 and $200 to send via email, straight to their mailbox or select ‘Print at home’ and give it them personally in an envelope.</p>
                            </li>
                            <li>
                                <p>The eGift Card is valid for 6 months so if they already own our latest stuffs and styles, there’s always time to wait for the next big trend to hit the cloud.</p>
                            <li>
                                <p>And if you’re a ‘whoops Facebook just reminded me it's your birthday and I clearly forgot’ kinda person.. don’t worry, our eGift Card can be sent instantly so your secret can stay safe with us.</p>
                            </li>
                            <li>
                                <p><strong>HOW IT WORKS</strong></p>
                                <ol>
                                    <li>Select the value of the e-Gift card.</li>
                                    <li>Tell us who to deliver it to.</li>
                                    <li>You have the option to enter a personalised message.</li>
                                    <li>Tell us the recipient's email address.</li>
                                    <li>Schedule the delivery of the e-Gift card.</li>
                                    <li>Complete payment.</li>
                                    <li>You are done. </li>
                                </ol>
                            </li>
                            <li>
                                <p><strong>HOW TO USE YOUR E-GIFT CARD</strong></p>
                                <p>It's easy! Enter your code in the Payment section of the <a target="_blank" href="{{ route('collection') }}" style="color:#007bff">checkout</a>.
                                </p>
                                <ol>
                                    <li>e-Gift cards: Check your email for the code.</li>
                                    <li>Physical gift cards: scratch the back of the card to reveal the code.</li>
                                    <li>Gift can be redeemed at the checkout.</li>
                                    <li>Once redeemed, the full amount needs to be spent.</li>
                                </ol>
                            </li>                                    
                            <li>
                                <p>You can view terms and conditions below.</p>
                                <ol>
                                    <li>e-Gift Cards are valid for 6 months from issue date.</li>
                                    <li>e-Gift Cards can only be redeemed online.</li>
                                    <li>Naipala cannot be held responsible for lost or stolen e-Gift Cards. eGift Cards cannot be re-issued.</li>
                                    <li>e-Gift Cards can be used in full however it cannot be used with left over balance. Thus, spend it on one go. </li>
                                    <li>The purchaser of the eGift Card will also receive a confirmation email – On request instructional use can be mailed to the recipient with the eGift Card</li>
                                    <li>Once the eGift Cards has been processed there can be no changes made.</li>
                                    <li>e-Gift Cards are non-refundable.</li>
                                </ol>
                            </li>
                            <li>
                                <p>Any questions?<br>
                                    For more information see our FAQ page or contact us.
                                </p>
                            </li>
                            <li><p>
                                    <strong>ALREADY HAVE A GIFT CARD?</strong><br>
                                    <a target="_blank" href="{{ route('collection') }}" style="color:#007bff"><strong>SHOP NOW</strong></a>
                                </p>
                            </li>
                                    
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@stop