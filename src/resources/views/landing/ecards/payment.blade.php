@extends('layouts.app')

@section('body')

<link rel="stylesheet" href="{{asset('frontend-assets/datepicker/bootstrap-datepicker.css') }}">
<style>

    .step {
        margin: 0 2px;
        display: inline-block;
        opacity: 0.5;
    }

    .step.active {
        opacity: 1;
        font-size: 1.5rem;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
        color: #4CAF50;
        opacity: 1;
    }
</style>

<div id="checkout">
    <section class="container mt-5 mb-5">
        <div style="text-align:center;" class="mb-4">
            <span class="step active">Review Purchase</span>
            <hr>
        </div>
        
        <div class="row" style="margin-top:  10px;">
            <div class="col-12">
                
                <div class="form-row">
                    <div class="form-group col-12">
                        
                        <label><strong>Amount: </strong>{{ \App\Currency::currency($etransaction->amount) }}</label><br>
                        <label><strong>Sender Name: </strong>{{ $etransaction->sender_name }}</label><br>
                        <label><strong>Recipient Name: </strong>{{ $etransaction->recipient_name }}</label><br>
                        <label><strong>Recipient Email: </strong>{{ $etransaction->recipient_contact }}</label><br>
                        <label><strong>Delivery Date: </strong>{{ date('M j, Y', strtotime($etransaction->delivery_date)) }}</label><br>
                        <label><strong>Message: </strong></label>
                        <div style="border: 2px solid #a09d9d;background: #e0e0e0;padding: 1rem;">
                            {!!  $etransaction->message !!}
                        </div>
                        <br>
                        
                    </div>
                </div>
                
                <div class="form-row pull-left">
                    <div class="form-group col-12" style="position: relative;">
                        <a href="{{ route('ecards.checkout',$etransaction->token) }}" class="btn btn-custom-outline">Back</a>
                    </div>
                </div>
                <div class="form-row pull-right">
                    <div class="form-group col-12" style="position: relative;">
                        <button class="btn custom-btn">Proceed</button>
                        <div id="paypal-button" style="margin-top:6px;z-index:3;position: absolute;top: 0;left: 0;opacity:0.01"></div>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection

@section('js')
<script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
  
    <script>
        paypal.Button.render({
        
            env: "{{ env('PAYPAL_CLIENT_MODE') }}",
            
            style: {
                size: 'small',
                color: 'black',
                shape: 'pill',
                label: 'pay'
            },

            payment: function(resolve, reject) {
                //This is your own API's endpoint
                var CREATE_PAYMENT_URL = "{{ route('postecardPaymentWithpaypal',$etransaction->token) }}";
                //Hit the endpoint with a request
                paypal.request.post(CREATE_PAYMENT_URL,{ _token: "{{ csrf_token() }}" })
                        .then(function(data) {
                                resolve(data.id)
                        })
                        .catch(function(err) {
                            reject(err);                         
                            window.location.href = "{{ route('ecards.payment',$etransaction->token) }}"
                        });    
            },
    
            onAuthorize: function(data) {
                //Your own API endpoint for executing an authorized payment
                var EXECUTE_PAYMENT_URL ="{{ route('ecards.status',$etransaction->token) }}";

                return paypal.request.post(EXECUTE_PAYMENT_URL,
                        { PaymentID: data.paymentID, PayerID: data.payerID,token: data.paymentToken, _token: '{{ csrf_token() }}' })
                        .then(function(data) {
                            window.location.href = "{{ route('ecards.portallogin') }}";
                        })
                        .catch(function(err) {
                            console.log(err)
                            // window.location.href = "{{ route('ecards.payment',$etransaction->token) }}";
                        });

                
            },
            onCancel:function(){
                window.location.href = "{{ route('ecards.payment',$etransaction->token) }}";
            }
                
        }, '#paypal-button');
    </script>
@endsection
