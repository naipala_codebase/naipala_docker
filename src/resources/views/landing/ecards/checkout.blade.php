@extends('layouts.app')

@section('body')

<link rel="stylesheet" href="{{asset('frontend-assets/datepicker/bootstrap-datepicker.css') }}">
<style>

    .step {
        margin: 0 2px;
        display: inline-block;
        opacity: 0.5;
    }

    .step.active {
        opacity: 1;
        font-size: 1.5rem;
    }

    /* Mark the steps that are finished and valid: */
    .step.finish {
        color: #4CAF50;
        opacity: 1;
    }
</style>

<div id="checkout">
    <section class="container mt-5 mb-5">
        <div style="text-align:center;" class="mb-4">
            <span class="step active">Contact Information</span>
            <hr>
        </div>
        @if(count($errors)>0)
            <div class="flex-container">
                <p style="color:#c61616">Please Provide all the Information</p>
            </div>
        @endif
        
        <div class="row">
            <form class="col-12" method="POST" action="{{ route('ecards.checkout',$etransaction->token) }}">
                {{ csrf_field() }}
                <div class="row">
                    <div class="col-12 flex-container">
                        <h5>Sender Email</h5>
                        @if(Auth::guest())
                            <span>Have an account with us?
                                <a data-toggle="modal" data-target="#loginModal">Login</a>
                            </span>
                        @else
                            <span>Not you?  <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></span>
                        @endif
                    </div>
                </div>
    
                @if(Auth::guest())
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <input type="email" oninput="this.className = 'form-control'" class="form-control {{ $errors->has('senderEmail') ? 'border-error' : '' }}" name="senderEmail" value="{{ $etransaction->sender_email }}">
                            </div>
                        </div>
                    </div>
                @else
                    <div class="row">
                        <div class="col-12">
                            <span><strong>{{ Auth::user()->name }}  </strong>({{ Auth::user()->email }})</span>
                            <input type="email" hidden value="{{ Auth::user()->email }}" name="senderEmail">
                        </div>
                    </div>
                @endif
            
                <br>

                <div class="form-row">
                    <div class="form-group col-12">
                        <h5>Recipient Email</h5>
                        <input type="email" oninput="this.className = 'form-control'" class="form-control {{ $errors->has('recipientEmail') ? 'border-error' : '' }}" name="recipientEmail" value="{{ $etransaction->recipient_contact ? $etransaction->recipient_contact : old('recipientEmail') }}">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-12">
                        <h5>Confirm Recipient Email</h5>
                        <input type="email" oninput="this.className = 'form-control'" class="form-control {{ $errors->has('recipientEmail_confirmation') ? 'border-error' : '' }}" name="recipientEmail_confirmation" value="{{ $etransaction->recipient_contact ? $etransaction->recipient_contact : old('recipientEmail_confirmation') }}">
                    </div>
                </div>
                
                <div class="form-row">
                    <div class="form-group col-12">
                        <h5>Delivery Date</h5>
                        <input type="text" oninput="this.className = 'form-control'" class="form-control {{ $errors->has('deliveryDate') ? 'border-error' : '' }}" name="deliveryDate" value="{{ $etransaction->delivery_date ? $etransaction->delivery_date : old('deliveryDate') }}" id="deliveryDate" readonly="readonly">
                    </div>
                    
                </div>

                <div class="form-row">
                    <div class="form-group col-12">
                        <button type="submit" class="btn custom-btn" style="float:right;">Next</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
</div>
@endsection

@section('js')
<script src="{{asset('/frontend-assets/datepicker/bootstrap-datepicker.js')}}"></script>
<script>
    
    $('#deliveryDate').datepicker({
        startDate: new Date(),
        format: 'yyyy-mm-dd',
        todayHighlight: true
    });
</script>
@endsection
