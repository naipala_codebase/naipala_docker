@extends('layouts.app')

@section('body')
    <div id="featured-brands">
        <section class="container">
            <div class="row mb-4 py-4">
                <div class="col-12 text-center">
                    <h2 class="text-uppercase">Featured Brands</h2>
                </div>
            </div>

            <div class="row" style="padding-bottom: 5.5em; justify-content: center;">
                @forelse($featured_brands as $brand)
                    <div class="col-lg-2 col-md-3 col-4 fbrand-list">
                        <div class="fbrand">
                            <a class="fbrand-img" data-toggle="modal" data-target="#brandViewModal{{ $brand->id }}">
                                <img src="{{ URL::to('asset/featured_brands/'. $brand->logo .'/137/137') }}" alt="{{ $brand->name }}">
                            </a>
                        </div>
                    </div>
                    <div class="modal fade" style="top:10%;" id="brandViewModal{{ $brand->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="flex-direction: row-reverse; border: none;">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <div class="row sg-product-section">
                                        <div class="col-lg-6 col-md-6 sg-main-image flex-container">
                                            <div class="">
                                                <img id="brandimage" src="{{ asset('/featured_brands'.'/'.$brand->logo) }}" alt="{{ $brand->name }}">
                                            </div>
                                        </div>
                                        <div class="col-lg-6 col-md-6 sg-product-details" style="background: rgba(255, 255, 255, 0.15);">
                                            <div class="sg-product-title">
                                                <div class="sg-header">
                                                    <h1 id="brandname">{{ $brand->name }}</h1>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="sg-product-info">
                                                <div id="accordion" role="tablist" aria-multiselectable="true">
                                                    <div class="sg-product-accordion">
                                                        <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="productInfo">
                                                            <div class="acc-body" id="branddescription" style="text-align : justify">
                                                                {{ $brand->description }}
                                                            </div>
                                                            <br>
                                                            <a href="{{ $brand->link }}" class="btn custom-btn sg-addToCart" target="_blank">Visit Site</a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @empty
                    <div class='col-12 text-center'>
                        <h4 style="margin:60px">No featured brands</h4>
                    </div>
                @endforelse
            </div>
            <div class="Google Ads">
                <div style="margin:0px; min-width:250px; width:100%; height:auto; padding-top: 15px;">
                    <ins class="adsbygoogle"
                         style="display:inline-block;width:100%; height:150px;"
                         data-ad-client="ca-pub-9044710729904359"
                         data-ad-slot="5869407926"
                         data-ad-format="auto"
                         data-full-width-responsive="true"></ins>
                    <script>
                         (adsbygoogle = window.adsbygoogle || []).push({});
                    </script>
                </div>
            </div>
        </section>
    </div>
    
@stop