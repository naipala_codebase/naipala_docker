@extends('layouts.app')

@section('body')
    <div id="serviceSearch">
        <section class="container">
            <section class="services-pg-container">
                <div class="mt-5 services-header">
                    <h2 class="text-center">Search Result for '{{$key}}'</h2>
                    <section class="pull-left">
                        <a href="{{ route('services') }}" style="padding-left:15px; padding-right:15px; background:#81302e;" class="btn custom-btn"><i class="fa fa-arrow-left"></i></a>
                    </section>
                    <section>
                        <form method="GET" action="{{URL::to('services/servicesearch')}}" id='servicesearch' class="sg-search-box">
                            <input type="search" class="form-control" placeholder="Search Service..." aria-label="Search" aria-describedby="search-addon2" name="key" required>
                            <button type="submit" class="btn custom-btn"><i class="fa fa-search"></i></button>
                        </form>
                    </section>
                    <hr>
                </div>
                <div class="row">
                    <div class="col-12">
                        @foreach($services as $service)
                            <div class="card services-info-list">
                                <div class="card-header si-list-header">
                                    <section class="si-header-date">
                                        <div>
                                            <span>{{ date('M j, Y', strtotime($service->updated_at)) }}</span>
                                        </div>

                                    </section>
                                    <section class="si-header-title">
                                        <div>
                                            <h3>{{ $service->title }}</h3>
                                        </div>
                                        <div>
                                            <a href="javascript:void(0)" class="badge badge-dark">{{ $service->service_category->title }}</a>
                                        </div>
                                    </section>
                                </div>
                                <div class="card-body si-list-body">
                                    <p class="" style="white-space: pre-wrap;">{{ $service->description }}</p>
                                </div>
                                <div class="si-list-footer">
                                    <div class="user-avatar-block">
                                        <div class="user-avatar">
                                            @if( $service->user->image!=null)
                                                <img src=" {{ asset('profile_images/' . $service->user->image) }}" alt="{{ $service->user->name }}">
                                            @else
                                                <img src="{{asset('frontend-assets/images/profile.png')}}" alt="{{ $service->user->name }}">
                                            @endif
                                        </div>
                                        <div class="user-info">
                                            <h5>{{ $service->user->name }}</h5>
                                        </div>
                                    </div>
                                    @if(Auth::check())
                                        <button class="btn btn-secondary btn-sm" onclick='bookService({{ $service->id }})' {{ $service->user->id==Auth::user()->id ? 'hidden':'' }} >
                                            Apply
                                        </button>
                                    @else
                                        <button class="btn btn-secondary btn-sm" onclick="guestInfoBook({{$service->id}})">
                                            Apply
                                        </button>
                                    @endif
                                </div>
                            </div>
                            
                        @endforeach

                        @if (count($services) == 0)
                            <div class='text-center'>
                                <h4 style="margin:100px">No services found</h4>
                            </div>
                        @endif
                    </div>
                </div>
            </section>
            <div class="row">
                <div class="col-12">
                    <nav aria-label="Page navigation example">
                        {!! $services->appends($_GET)->links('partial.user.pagination') !!}
                    </nav>
                </div>
            </div>
            
            <!-- Google Ads Div-->
            <!-- ServiceSearch -->
            <div class="row" style="margin: 0px; width:100%; height:auto;">
                <ins class="adsbygoogle"
                 style="display:inline-block;width:100%;"
                 data-ad-client="ca-pub-9044710729904359"
                 data-ad-slot="3699551912"
                 data-ad-format="auto"
                 data-full-width-responsive="true"></ins>
                <script>
                    (adsbygoogle = window.adsbygoogle || []).push({});
                </script>
            </div>
            
        </section>
    </div>
    @include('landing.service.serviceModals')
@endsection

@section('js')
    <script>
        function bookService(service_id) {
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            
            $.ajax(
            {
                url: '/services/book/' + service_id,
                type: "get",
                beforeSend: function()
                {
                    $('#bookload'+service_id).text('Processing').attr('disabled',true);
                }
            })
            .done(function(data)
            {
                data = JSON.parse(data);
                if(data.type=='success'){
                    $('#book_id').val(data.id);
                    $('#serviceExtraInfoModal').modal('show');
                }
                else
                    aalert(data.msg,'','Oops...');
                $('#bookload'+service_id).text('Apply').attr('disabled',false);
                
            })
            .fail(function(){
                $('#bookload'+service_id).text('Apply').attr('disabled',false);
                
            });

        }
        
        $('#nomessage').click(function(){
            $('#serviceExtraInfoModal').modal('hide');
            aalert('Applied Succesfully','','Success');
        });
            
        
        function guestInfoBook(id){
            $('#GuestInfo_serviceId').val(id);
            $('#serviceApplyModal').modal('show');
        }
        
        function loginToBook(){
            var service_id = $('#GuestInfo_serviceId').val();
            var next="{{URL::to('services/book') }}"+ "/" + service_id;
            window.location.href="{{URL::to('login')}}"+"?next="+next;
        }

        $('#cv').on('change', function() {
            myfile= $(this).val();
            var ext = myfile.split('.').pop();
            if(ext=="pdf" || ext=="doc" || ext == 'docx' || ext == 'rtf'){
                
            } else{
                $(this).val(null);
                aalert('Please Choose a Valid File','','Oops');
                
            }
        });

        $(document).on('show.bs.modal', '.modal', function () {
            var zIndex = 1040 + (10 * $('.modal:visible').length);
            $(this).css('z-index', zIndex);
            setTimeout(function() {
                $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
            }, 0);
        });
    </script>
@endsection