@extends('layouts.app')

@section('pin')
    <?php $details=json_decode($item->details,true);?>
    <?php $c=0?>
    @foreach($details as $d)
        <?php $c=$c+$d['quantity']?>
    @endforeach
    <meta property="fb:app_id" content="1576279952450591" />
    <meta property="og:type" content="product" />
    <meta property="og:title" content="{{ $item->item_name }}" />
    <meta property="og:description" content="{{ $item->item_description }}" />
    <meta property="og:image" content="{{ asset('uploads').'/'.$img }}" />
    <meta property="og:image:width" content="200" />
    <meta property="og:image:height" content="200" />
    <meta property="og:url" content="{{ url()->current() }}"/>
    <meta property="og:site_name" content="Naipala" />
    <meta property="product:price:amount" content="{{ $item->item_price }}" />
    <meta property="product:price:currency" content="AUD" />
    <meta property="og:availability" content="{{ $c>0 ? 'instock':'out of stock' }}" />
@endsection

@section('body')
    <style>
        .srating {
            direction: rtl;
        }
        .srating > span {
            display: inline-block;
            position: relative;
            width: 1.1em;
        }

        .srating > span#star1:hover:before,
        .srating > span#star1:hover ~ span#star1:before {
            content:url({{asset('/frontend-assets/images/ratings/1.png')}});
            position: absolute;
        }

        .srating > span#star2:hover:before,
        .srating > span#star2:hover ~ span#star1:before {
            content:url({{asset('/frontend-assets/images/ratings/1.png')}});
            position: absolute;
        }
        .srating > span#star2:hover:before,
        .srating > span#star2:hover~ span#star2:before {
            content:url({{asset('/frontend-assets/images/ratings/2.png')}});
            position: absolute;
        }

        .srating > span#star3:hover:before,
        .srating > span#star3:hover ~ span#star1:before {
            content:url({{asset('/frontend-assets/images/ratings/1.png')}});
            position: absolute;
        }
        .srating > span#star3:hover:before,
        .srating > span#star3:hover ~ span#star2:before {
            content:url({{asset('/frontend-assets/images/ratings/2.png')}});
            
            position: absolute;
        }
        .srating > span#star3:hover:before,
        .srating > span#star3:hover ~ span#star3:before {
            content:url({{asset('/frontend-assets/images/ratings/3.png')}});
            position: absolute;
        }

        .srating > span#star4:hover:before,
        .srating > span#star4:hover ~ span#star1:before {
            content:url({{asset('/frontend-assets/images/ratings/1.png')}});
            position: absolute;
        }
        .srating > span#star4:hover:before,
        .srating > span#star4:hover ~ span#star2:before {
            content:url({{asset('/frontend-assets/images/ratings/2.png')}});
            position: absolute;
        }
        .srating > span#star4:hover:before,
        .srating > span#star4:hover ~ span#star3:before {
            content:url({{asset('/frontend-assets/images/ratings/3.png')}});
            
            position: absolute;
        }
        .srating > span#star4:hover:before,
        .srating > span#star4:hover ~ span#star4:before {
            content:url({{asset('/frontend-assets/images/ratings/4.png')}});
            position: absolute;
        }

        .srating > span#star5:hover:before,
        .srating > span#star5:hover ~ span#star1:before {
            content:url({{asset('/frontend-assets/images/ratings/1.png')}});
            position: absolute;
        }
        
        .srating > span#star5:hover:before,
        .srating > span#star5:hover ~ span#star2:before {
            content:url({{asset('/frontend-assets/images/ratings/2.png')}});
            position: absolute;
        }
        .srating > span#star5:hover:before,
        .srating > span#star5:hover ~ span#star3:before {
            content:url({{asset('/frontend-assets/images/ratings/3.png')}});
            position: absolute;
        }
        .srating > span#star5:hover:before,
        .srating > span#star5:hover ~ span#star4:before {
            content:url({{asset('/frontend-assets/images/ratings/4.png')}});
            position: absolute;
        }
        .srating > span#star5:hover:before,
        .srating > span#star5:hover ~ span#star5:before {
            content:url({{asset('/frontend-assets/images/ratings/5.png')}});
            position: absolute;
        }
        
        /* The Modal (background) */
        .modal1 {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1000000; /* Sit on top */
            left: 0;
            top: 0;
            padding-top:5%;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.9); /* Black w/ opacity */
        }
        
        /* modal1 Content (image) */
        .modal1-content {
            margin: auto;
            display: block;
            width: 80%;
            
            max-height: 531px;
            max-width: 700px;
        }
        
        /* Caption of modal1 Image */
        #caption {
            margin: auto;
            display: block;
            width: 80%;
            max-width: 700px;
            text-align: center;
            color: #ccc;
            padding: 10px 0;
            height: 150px;
        }
        
        /* Add Animation */
        .modal1-content, #caption {    
            -webkit-animation-name: zoom;
            -webkit-animation-duration: 0.6s;
            animation-name: zoom;
            animation-duration: 0.6s;
        }
        
        @-webkit-keyframes zoom {
            from {-webkit-transform:scale(0)} 
            to {-webkit-transform:scale(1)}
        }
        
        @keyframes zoom {
            from {transform:scale(0)} 
            to {transform:scale(1)}
        }
        
        /* The close1 Button */
        .close1 {
            position: absolute;
            top: 15px;
            right: 35px;
            color: #f1f1f1;
            font-size: 40px;
            font-weight: bold;
            transition: 0.3s;
        }
        
        .close1:hover,
        .close1:focus {
            color: #bbb;
            text-decoration: none;
            cursor: pointer;
        }
        
        /* 100% Image Width on Smaller Screens */
        @media only screen and (max-width: 700px){
            .modal1-content {
                width: 100%;
            }
            .flex-control-thumbs{
                margin: 5px 2.4px 2px !important;
            }
        }

        .custom-input-option{
            width: 60px;
            border: 1px solid #444;
            background-color: #ebebeb;
            padding: 0 12px;
            height: 32px;
            border-radius: 0px;
            font-weight: 400;
            font-family: 'Barlow Semi Condensed', sans-serif;
        }
        .custom-input {
            display: inline-block;
            height: calc(2.25rem + 2px);
            padding: .375rem .375rem .375rem .75rem;
            line-height: 1.25;
            color: #495057;
            vertical-align: middle;
            border: 1px solid rgba(0,0,0,.15);
            border-radius: 0;
            -webkit-appearance: none;
            -moz-appearance: none;
            appearance: none;
        }
    </style>
    <div id="single-product">
        <section class="container">
            <div class="row sg-product-section">
                <div class="col-lg-7 col-md-6 sg-main-image" id="flexdiv">

                    <!--flex-slider one-->
                    <div class="flexslider" id="sliderImages">
                    </div>
                </div>
                <div class="col-lg-5 col-md-6 sg-product-details">
                    <div class="sg-product-title">
                        <div class="sg-header">
                            <h1>{{ $item->item_name }}</h1>
                            <img src="{{ asset('/frontend-assets/images/min_logo.png') }}" alt="made in Nepal">
                        </div>
                        <hr>
                    </div>
                    
                    <div class="sg-price">
                        <span>{{$item->display_price}}</span>
                        @if($item->item_old_price)
                            <span class="sg-price-older">{{ $item->display_old_price }}</span>
                        @endif
                    </div>
                    
                    <div id="accordion" role="tablist" aria-multiselectable="true" style="padding: 5px 0;">
                        <div class="sg-product-accordion">
                            <div id="collapseOne" class="collapse show" role="tabpanel" aria-labelledby="productInfo">
                                <div class="acc-body">
                                    @if(strlen($item->item_description)<=300)
                                        <div style="text-align:justify;;margin-bottom:0"> {!! $item->item_description !!} </div>
                                    @else
                                        <div id="desclesstext" style="text-align:justify;margin-bottom:0">{!! substr($item->item_description,0,200) !!}<text id="dotmore"> ...&nbsp;</text><b><a id="descmore" style="color:#8a6d3b;cursor:pointer;">SEE MORE</a></b></div>
                                        <div hidden id="descmoretext" style="text-align:justify;margin-bottom:0">{!! $item->item_description !!} <b><a id="descless" style="color:#8a6d3b;cursor:pointer">HIDE</a></b></div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                   
                    <div class="sg-product-options">
                        @if($details[0]['size']!='')
                            <div class="sg-select" style="padding-bottom: 8px;">
                                <!--<h6>Sizes</h6>-->
                                <select id="size" class="custom-select custom-select-option"  style="-webkit-appearance:menulist; -moz-appearance:menulist; appearance:menulist;">
                                    <option selected disabled>Size</option>
                                </select>
                            </div>
                        @endif

                        @if($details[0]['color']!='')
                            <div class="sg-colors" id="colors">
                                <h6>Colors</h6>
                                @foreach($colors as $color=>$details)
                                    <div class="sg-color-options" data-color="{{ $color }}">
                                        <a style="cursor:pointer" onclick="changeImage('{{$color}}',event)">
                                            @if(count($details['images'])==0)
                                                <img src="" alt="{{ $color }}" title="{{ $color }}">
                                            @else  
                                                <img src="{{ route('optimize', ['uploads',$details['images'][0]->image_link,48,48]) }}" alt="{{ $color }}" title="{{ $color }}">
                                            @endif
                                        </a>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                        <div style="padding: 10px 0;">  
                            <h6 style="margin-bottom:0px">Availability:  <text id="pro_status" style="text-transform: capitalize;font-weight: 500;font-size: 1rem;"></text></h6>
                        </div>                    
                        
                        @if($rated=="false")
                            @if(Auth::check())
                                <input type="hidden" id="authcheck" value="true">
                            @else
                                <input type="hidden" id="authcheck" value="false">
                            @endif
                            <div class="sg-product-info">
                                
                                <div class="row" id="ratediv">
                                    <div class="col-xl-6 col-lg-7 col-md-8 col-xs-8" >
                                        <div class="srating pull-left" id="rating" style="cursor:pointer">
                                                
                                            <span id="star5"><img src="{{asset('/frontend-assets/images/ratings/5b.png')}}" nopin="nopin"/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span id="star4"><img src="{{asset('/frontend-assets/images/ratings/4b.png')}}" nopin="nopin"/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span id="star3"><img src="{{asset('/frontend-assets/images/ratings/3b.png')}}" nopin="nopin"/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span id="star2"><img src="{{asset('/frontend-assets/images/ratings/2b.png')}}" nopin="nopin"/></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <span id="star1"><img src="{{asset('/frontend-assets/images/ratings/1b.png')}}" nopin="nopin"/></span>&nbsp;&nbsp;&nbsp;&nbsp;
                                            
                                        </div>
                                    </div>
                                    <div class="col-xl-6 col-lg-4 col-md-4 col-xs-4" >
                                        <img src="{{ asset('/img/ratehere.png') }}" style="height:30px;width:90px;cursor:default" id="ratehere" alt="Rate Here" nopin="nopin"/>
                                    </div>
                                </div>
                                <text id="feedback" hidden>Thank you for your feedback</text>                                
                            </div>
                        @endif
                        
                        <div style="padding: 12px 0;">
                            <input class="custom-input custom-input-option" type="number" min="1" placeholder="QTY" id="quantity" style="margin: 0px 3px;margin-left:0"/>
                            <div class="btn custom-btn sg-addToCart" id="addCart123" style="padding: 8px 12px;margin: 0px 3px;">Add to cart &nbsp; <img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="loading" ></img></div>
                            @if(Auth::guest())
                                <div data-toggle="modal" data-target="#loginModal" class="btn custom-btn sg-addToCart" style="padding: 8px 12px;margin: 0px 3px;" title="Add to Wishlist"><i class="fa fa-heart" style="margin-right: 0px;"></i><img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="awloading" ></img></div>
                            @else
                                <div class="btn custom-btn sg-addToCart" id="addWishlist123" style="padding: 8px 12px;margin: 0px 3px;" title="Add to Wishlist"><i class="fa fa-heart" style="margin-right: 0px;"></i><img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="awloading" ></img></div>
                            @endif
                        </div>
                    </div>
                      
                    <div class="sg-product-actions">
                        <div class="share-options">
                            <h6>share</h6>
                            <ul>
                                <li><a href="https://www.facebook.com/sharer/sharer.php?app_id=1576279952450591&kid_directed_site=0&u={{urlencode(url()->current())}}&display=popup&ref=plugin&src=share_button" target="_blank" title="Share on fb"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="https://www.pinterest.com/pin/create/button/" data-pin-custom="true" data-pin-do="buttonBookmark" onmouseover="this.style.color='black'" onmouseout="this.style.color='white'" style="color:white;cursor:pointer" title="Share on pinterest"><i class="fa fa-pinterest"></i></a></li>
                                <!-- Load Facebook SDK for JavaScript -->
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row sg-story-section">
                <div class="col-12">
                    <h3>What's the Story</h3>
                    <div class="sg-review-block">
                        <div class="card sg-review-card">
                            <div class="card-body">
                                <p>{!! $item->whats_the_story !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row sg-sets-apart-section">
                <div class="col-12">
                    <h3>What sets it Apart</h3>
                    <div class="sg-review-block">
                        <div class="card sg-review-card">
                            <div class="card-body">
                                <p>{!! $item->what_sets_it_apart !!}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row sg-review-section">
                <div class="col-12">
                    <h3>Reviews

                        @if($item->item_raters!=0)
                        <div style="display:inline" class="pull-right">
                            <?php $rate = round($item->item_ratings/$item->item_raters); ?>
                            <span title="{{$rate}}/5">
                                @for($i=1;$i<=$rate;$i++)
                                    <img src="{{asset('/frontend-assets/images/ratings'.'/'.$i)}}.png" style="width:20px;height:25px" nopin="nopin"/>
                                @endfor
                                @while($i<=5)
                                    <img src="{{asset('/frontend-assets/images/ratings'.'/'.$i)}}b.png" style="width:20px;height:25px" nopin="nopin"/>
                                    <?php $i++ ?>
                                @endwhile
                            </span>
                        </div>
                        @endif
                    </h3>
                    @if($item->item_raters!=0)<br>@endif
                    <div class="sg-review-block">
                        <div class="tab-content" id="Reviews">
                            @foreach($reviews as $r)
                                <div class="card sg-review-card">
                                    <div class="card-header">
                                        <div class="sg-card-img">
                                            @if($r->review_image=="Anonymous")
                                                <img src="{{asset('frontend-assets/images/profile.png')}}" alt="Anonymous" nopin="nopin"/>
                                            @else
                                                <img src="{{asset('profile_images/'.$r->review_image)}}" alt="{{ $r->review_name }}" nopin="nopin"/>
                                            @endif
                                        </div>
                                        <div class="sg-card-title">
                                            <h3>{{$r->review_name}}
                                                <span class="sg-date">{{ date('M j, Y', strtotime($r->created_at)) }}</span>
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <p>{{ $r->review_description }}</p>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
    
                    <div class="text-center" {{ count($reviews)==5 ?'':'hidden'}}>
                        <button id="allreviews" class="btn custom-btn black-btn">See all reviews</button>
                    </div>
    
                    <div class="sg-user-review">
                        @if(Auth::user())
                            <input type="hidden" name="username" id="username" value="{{ Auth::user()->name }}"/>
                            <input type="hidden" name="userimage" id="userimage" value="{{ Auth::user()->image }}"/>
                            <input type="hidden" id="user_id" value="{{Auth::user()->id}}" />
                        @else
                            <input type="hidden" name="username" id="username" value="Anonymous"/>
                            <input type="hidden" name="userimage" id="userimage" value="Anonymous"/>
                
                        @endif
                        <input type="hidden" value="{{$item->item_id}}" id="item_id"/>
                  
                        <textarea name="" id="userComment" cols="30" rows="5" placeholder="What's on your mind ?"></textarea>
                        <button id="addComment" class="btn btn-custom-outline">Add Review</button>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div id="mymodal" class="modal1">
        <span class="close1">&times;</span>
        <img class="modal1-content" src ="" id="img01">
        <div id="caption"></div>
    </div>
@endsection

@section('js')
    <script>
        var colors = JSON.parse('<?php echo json_encode($colors) ?>');
        
        var selected = '';
        for(var c in colors){
            selected = c;
            $("[data-color='" + selected + "']").css('border-color','black');
            
            var html = '<ul class="slides" >';
            
            for(var i=0;i<colors[c]['images'].length;i++){
                html += "<li data-thumb='{{ asset('uploads').'/' }}"+colors[c]['images'][i].image_link+"'><div class='thumb-image'><img src='{{ asset('uploads').'/' }}"+colors[c]['images'][i].image_link+"' class='img-responsive test' style='cursor:pointer'></div></li>";
            }

            html += "</ul>";                                  
            $("#sliderImages").html(html);
            
            html = '<option selected disabled>Size</option>';
            for(var i=0;i<colors[c]['sizes'].length;i++){
                if(colors[c]['sizes'][i]['availability'])
                    html += "<option value='"+colors[c]['sizes'][i]['size']+"'>"+colors[c]['sizes'][i]['size']+"</option>";
            }
            $('#size').empty();
            $('#size').html(html);

            $('#pro_status').text((colors[c]['availability']?'In Stock':'Out Of Stock'));
            break;
        }

        $(document).ready(function(){
            // set defualt quantity 1
            $('#quantity').val(1);

            $('#descmore').click(function(){
                $('#desclesstext').attr('hidden','true');
                $('#descmoretext').removeAttr('hidden');
            });

            $('#descless').click(function(){
                $('#descmoretext').attr('hidden','true');
                $('#desclesstext').removeAttr('hidden');
                
            });

            $('#addCart123').click(function(e){
                if($('#quantity').val()<1){
                    aalert('Quantity cannot be less than 1','','');                    
                    return;
                }

                if($('#size').length>0){
                    if(!$('#size').val()){
                        aalert('Please select a size','','');
                        return;
                    }
                }
                
                var id='{{ $item->item_id }}';
                var s = $('#size').val() ? $('#size').val() : '';
                var c = selected ? selected : '';
                var attr = JSON.stringify({"size":s,"color":c});
                
                $('#loading').removeAttr('hidden');
                var image = $("[data-color='" + selected + "']").find('img').attr('src');
                $.ajax({
                    type: "POST",
                    url: "{{URL::to('/cart/additem')}}",
                    data: {id:id, _token:'{{csrf_token()}}',details:attr,image:image,quantity:$('#quantity').val()}, // serializes the form's elements.
                    success: function(data)
                    {
                        $('#cartitems').val(data);                        
                        $('#loading').attr('hidden','true');
                        angular.element($('#cartapp')).scope().$apply("init()");
                        $('#carticon').trigger('click');
                    },
                    error:function(data)
                    {
                        if(data.responseJSON)
                            aalert(data.responseJSON.errors,'','Oops..');
                        else
                            aalert('Something Went Wrong','','Oops..');
                        $('#loading').attr('hidden','true');
                    }
                });
            });
            $('#addWishlist123').click(function(e){
                
                var id='{{ $item->item_id }}';
                        
                $('#awloading').removeAttr('hidden');
                $.ajax({
                    type: "POST",
                    url: "{{URL::to('/wishlist/additem')}}",
                    data: {id:id, _token:'{{csrf_token()}}'}, // serializes the form's elements.
                    success: function(data)
                    {
                        $('#wishlistitems').val(data);                        
                        $('#awloading').attr('hidden','true');
                        angular.element($('#wishlist')).scope().$apply("init()");
                        $('.wishlist__trigger').trigger('click');
                    },
                    error:function(data)
                    {
                        if(data.responseJSON)
                            aalert(data.responseJSON.errors,'','Oops..');
                        else
                            aalert('Something Went Wrong','','Oops..');
                        $('#awloading').attr('hidden','true');
                    }
                });
            });

            $('[id *= "star"]').click(function(){
                var rate=$(this).attr('id').substr(4,1);
                var item_id=$('#item_id').val();
                var user_id=$('#user_id').val();
                var auth=$('#authcheck').val();
                var i;
                var html = '';
                for(i=1;i<=rate;i++){
                    if(i == 1)
                        html = '<span id="star'+i+'"><img src="' + "{{asset('/frontend-assets/images/ratings')}}"+'/'+i+'.png" /></span>&nbsp;&nbsp;&nbsp;&nbsp;'+html;
                    else
                        html = '<span id="star'+i+'"><img src="'+"{{asset('/frontend-assets/images/ratings')}}"+'/'+i+'.png" /></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+html;
                }
                while(i<=5){
                    html = '<span id="star'+i+'"><img src="'+"{{asset('/frontend-assets/images/ratings')}}"+'/'+i+'b.png" /></span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+html;
                    i++;
                }
                $('#rating').empty();
                $('#rating').html(html);
                if(auth == "false")
                {
                    var next="{{URL::to('collection'.'/'.$item->item_id.'/rating')}}"+"/"+rate;
                    setTimeout(function(){
                        window.location.href="{{URL::to('login')}}"+"?next="+next;
                    }, 1000);
                }
                else{
                    $('[id *= "star"]').attr('disabled','true');
                    setTimeout(function(){
                        $.get("{{URL::to('collection/rating')}}",{rate:rate, item_id:item_id},function(data,status){
                            $('#ratediv').remove();
                            $('#feedback').removeAttr('hidden');
                        }).fail(function() {
                            location.reload();
                        });
                    }, 1000);
                }
                
            });

            $('#allreviews').click(function(){
                var id=$('#item_id').val();
                $('#allreviews').prop('disabled',true);
                $.get("{{ URL::to('collection/getreviews') .'/' }}"+id,function(data,status){
                    $('#Reviews').empty();
                    for(var i=0;i<data.length;i++)
                    {
                
                        var s1='<div class="card sg-review-card"><div class="card-header"><div class="sg-card-img">';
                        var s2=''
                        if(data[i].review_image=='Anonymous')
                            s2= '<img src="'+"{{asset('frontend-assets/images/profile.png')}}"+'" alt="Anonymous" nopin="nopin"/>';
                        else
                            s2=  '<img src="'+"{{asset('profile_images')}}"+'/'+data[i].review_image+'" alt="'+data[i].review_name+'" nopin="nopin"/>';
                        var s3='</div><div class="sg-card-title"><h3>'+data[i].review_name+'<span class="sg-date">';
                        var s4=  data[i].created_at.substr(0,10);
                        var s5= '</span></h3></div></div><div class="card-body"><p>'+data[i].review_description+'</p></div></div>';

                        $('#Reviews').append(s1+s2+s3+s4+s5);
                        $('#allreviews').remove();
                    
                        $('#userComment').val('');  
                    }
                }).fail(function(){
                    $('#allreviews').prop('disabled',false);
                });
            });

            $('#addComment').click(function(){
                var id=$('#item_id').val();
                var post= $('#userComment').val();
                var username= $('#username').val();
                var image=$('#userimage').val();
                if(image=='')
                    image = 'Anonymous';
                if(post=='')
                    return;
                $(this).text("Adding..").prop('disabled',true);
                $.post("{{ URL::to('collection/addreview') }}",
                {
                    _token: "{{ csrf_token() }}",
                    item_id: id,
                    post: post,
                    username: username,
                    image: image
                },
                function(data, status){
                    $('#addComment').text('Add Comment').prop('disabled',false);
            
                    var s1='<div class="card sg-review-card"><div class="card-header"><div class="sg-card-img">';
                    var s2=''
                    if(image=='Anonymous')
                        s2= '<img src="'+"{{asset('frontend-assets/images/profile.png')}}"+'" alt="Anonymous" nopin="nopin"/>';
                    else
                        s2=  '<img src="'+"{{asset('profile_images')}}"+'/'+image+'" alt="'+username+'" nopin="nopin"/>';
                    var s3='</div><div class="sg-card-title"><h3>'+username+'<span class="sg-date">';
                    var s4=  'now';
                    var s5= '</span></h3></div></div><div class="card-body"><p>'+post+'</p></div></div>';
                            
                    $('#Reviews').append(s1+s2+s3+s4+s5);
                    var l;

                    if(image=='Anonymous')
                        l ="{{asset('frontend-assets/images/profile.png')}}";
                    else
                        l ="{{ asset('uploads').'/' }}"+image;
                    $('#userimgid').attr('src',l);
                    $('#userComment').val('');

                }).fail(function() {
                    $('#addComment').text('Add Comment').prop('disabled',false);
                });

            });

            $('.flexslider').on('click','img.test',function(){
                var image = $(this).attr('src');
                $('#img01').attr('src',image);
                
                $('.modal1').attr('style','display:block');
            });

            $('.close1').click(function(){
                $('#img01').attr('src','');
                $('.modal1').attr('style','display:none');
            });

        });

        function changeImage(color,e){
            e.preventDefault();
            $("[data-color='" + selected + "']").css('border-color','');
            selected = color;
            $("[data-color='" + selected + "']").css('border-color','black');
            var html = '<ul class="slides" >';
            
            for(var i=0;i<colors[color]['images'].length;i++){
                html += "<li data-thumb='{{ asset('uploads').'/' }}"+colors[color]['images'][i].image_link+"'><div class='thumb-image'><img src='{{ asset('uploads').'/' }}"+colors[color]['images'][i].image_link+"' class='img-responsive test' style='cursor:pointer'></div></li>";
            }
            html += "</ul>";
            height = $('#flexdiv').outerHeight();
            
            $('.flexslider').attr('hidden',true);            
            $('#flexdiv').attr('style','opacity:0;height:'+height+'px');
            $('.flexslider').removeData("flexslider");
            $("#sliderImages").html(html);
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails",
                slideshow: false,
                start: function(){
                    $('.flexslider').removeAttr('hidden');
                    $('#flexnav').attr('style','opacity:1');
                    $('#flexdiv').attr('style','opacity:1');
                    $('.flexslider').resize(); 
                }
            });
            
            html = '';
            for(var i=0;i<colors[color]['sizes'].length;i++){
                html += "<option value='"+colors[color]['sizes'][i]['size']+"'"+(colors[color]['sizes'][i]['availability']?"":" disabled") +">"+colors[color]['sizes'][i]['size']+"</option>";
            }
            $('#size').empty();
            $('#size').html(html);
            $('#pro_status').text((colors[color]['availability']?'In Stock':'Out Of Stock'));
            //return false;
            
        }

        
    </script>
    <script>
        $(document).ready(function () {
            $('.flexslider').flexslider({
                animation: "slide",
                controlNav: "thumbnails",
                slideshow: false,
                start: function(){
                    $('#flexnav').attr('style','opacity:1');
                }
            });
        });
        
    </script>
    
    <script type="text/javascript">
        (function(d){
            var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
            p.type = 'text/javascript';
            p.async = true;
            p.src = '//assets.pinterest.com/js/pinit.js';
            f.parentNode.insertBefore(p, f);
        }(document));
    </script>
    
@endsection