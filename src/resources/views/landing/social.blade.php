@extends('layouts.app')

@section('body')
<style>
    .liked_social{
        color:#81302e;
    }
    .liked_social:hover{
        color:#81302e;

    }
    .like_social:hover{
        color:#81302e;

    }
    .social_action_info{
        font-size: 15px;
        color: #a7a5a3;
    }
    .comment_link:hover{
        color:#81302e;
    }
    .comment-card{
        background:unset;
        border:none;
    }
    .comment-textarea{
        background-color: #ebebeb;
        border: none;
        width: 100%;
        min-height: 45px;
        margin-bottom: 10px;
        padding: 10px 20px;
        font-family: 'Barlow Semi Condensed', sans-serif;
    }
    .comment-textarea:focus{
        outline: none ! important;

    }
    </style>
<div id="social-page">
    <section class="container">
        <div class="row">
            <div class="col-12 social-wrapper pg-top">
                <div class="info-pg-heading text-center">
                    <br>
                    <h2 style="font-family:Courier New;"><b>Social</b></h2>
                    <nav aria-label="breadcrumb" role="navigation">
                        @if(count($sociallinks) > 0)
                            <ol class="breadcrumb" style="margin-bottom: 0px;">
                                @foreach($sociallinks as $s)
                                    <li class="breadcrumb-item" aria-current="page"><a href="{{$s->config_value}}" target="_blank">{{$s->config_key}}</a></li>
                                @endforeach
                               
                            </ol>
                        @endif
                    </nav>
                </div>
            </div>
        </div>
        @foreach($socials as $s)

        <div class="row info-pg-container">
            <div class="col-12 social-wrapper pg-bottom">
                <div class="info-pg-heading">
                    <h1>{{$s->social_title}}</h1>
                </div>
                <div class="social-pg-details">
                    <div class="row info-img-container ">
                    	@foreach($s->socialimages as $i)
	                        <div class="col-sm-4 info-img"><img src="{{asset('/uploads'.'/'.$i->image_link)}}" alt=""></div>
                     	@endforeach
                     </div>
                    <div class="social-terms">
                        {!! $s->social_description !!}
                        <!-- Google Ads Div - SocialInsideArticle -->
                        <ins class="adsbygoogle"
                             style="display:block; text-align:center;"
                             data-ad-layout="in-article"
                             data-ad-format="fluid"
                             data-ad-client="ca-pub-9044710729904359"
                             data-ad-slot="6313218917"></ins>
                        <script>
                             (adsbygoogle = window.adsbygoogle || []).push({});
                        </script>
                    </div>
                </div>
                <div class="social-footer">
                <a href="#!" id="like_btn{{$s->social_id}}" sid="{{$s->social_id}}" class="like_social" style="text-decoration:none"><i class="fa fa-heart"></i> Like</a>
                <a href="#!" hidden id="liked_btn{{$s->social_id}}" class="liked_social" style="text-decoration:none"><i class="fa fa-heart"></i> Liked</a>
                   
                    <div class="pull-right">
                        <span class="social_action_info">
                            <span style="color:#81302e" id="likeCount{{$s->social_id}}">{{$s->like_count}}</span> Likes &nbsp;
                        </span> 
                    <span class="social_action_info"><a href="#!" id="showComments{{$s->social_id}}" sid="{{$s->social_id}}" class="comment_link"><span style="color:#81302e">{{$s->comments->count()}}</span> <i class="fa fa-spinner fa-spin" hidden></i> Comments</a></span>
                    </div>

                </div>
                <br>
                <div class="row sg-story-section">
                <div class="col-12" id="comments_block{{$s->social_id}}" hidden>
                    <h5 >Comments:</h5>
                    <div class="sg-review-block">
                        <div class="card comment-card" id="comments_block_div{{$s->social_id}}">
                                {{-- @foreach($s->comments as $comment)
                                <div class="card-body" style="background:rgba(0, 0, 0, .035)">
                                    <p><img src="{{$comment->photo}}" style="height:3rem"> {{$comment->name}}</p>
                                    
                                    <p>{{$comment->body}}</p>  </div>
                                <hr style="margin:0; color:black">
                                @endforeach
                                 --}}
                        </div>
                    <span class="social_action_info"><a href="#!" {{$s->comments->count()>5?'':'hidden'}} id="showAllComments{{$s->social_id}}" sid="{{$s->social_id}}" class="comment_link">
                        <i class="fa fa-spinner fa-spin" hidden></i> Show all comments</a>
                    </span>
                        <br>
                    </div>

                </div>
                <div class="col-12">

                    <div class="row sg-review-section">
                        <div class="col-12">
                            
                                                <div class="sg-review-block">
                                <div class="tab-content" id="Reviews">
                                                            </div>
                            </div>
            
            
                            <div class="sg-user-review">
                                                            <input type="hidden" name="username" id="username" value="Anonymous">
                                    <input type="hidden" name="userimage" id="userimage" value="Anonymous">
                        
                                                        <input type="hidden" value="64" id="item_id">
                            
                            <textarea name="" class="comment-textarea" style="width:100%" id="userComment{{$s->social_id}}" cols="30" rows="1" placeholder="What's on your mind ?"></textarea>
                                <button id="addComment{{$s->social_id}}" sid="{{$s->social_id}}" class="btn btn-custom-outline pull-right"><i class="fa fa-spinner fa-spin" hidden></i> Comment</button>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
        @endforeach

    </section>

</div>

@endsection


@section('js')
<script>
    $(document).ready(function(){
        $('[id*="like_btn"').click(function(){
            var id=$(this).attr('sid');
            $.ajax(
            {
                url: "{{ URL::to('/social/like') }}" ,
                type: "POST",
                data:{
                    id:id,
                    _token:'{{csrf_token()}}'
                },
                beforeSend: function()
                {

                }
            })
            .done(function(data)
            {
                $('#likeCount'+id).text(data);
                $('#like_btn'+id).attr('hidden','true');
                $('#liked_btn'+id).removeAttr('hidden');

                
            })
            .fail(function(){
                
            });
        });
        $('[id*="addComment"').click(function(){
            var id=$(this).attr('sid');
            var body=$('#userComment'+id).val();
            $.ajax(
            {
                url: "{{ URL::to('/social/comment/') }}" ,
                type: "POST",
                data:{
                    id:id,
                    body:body,
                    _token:'{{csrf_token()}}'
                },
                beforeSend: function()
                {
                    $('#addComment'+id).find('i').removeAttr('hidden');
                }
            })
            .done(function(data)
            {   
                $('#comments_block_div'+id).append(data);
                $('#userComment'+id).val('');
                $('#comments_block'+id).removeAttr('hidden');

                $('#addComment'+id).find('i').attr('hidden','true');
                
            })
            .fail(function(){
                $('#addComment'+id).find('i').attr('hidden');
                
            });
        });
        $('[id*="showComments"').click(function(){
            var id=$(this).attr('sid');
            $.ajax(
            {
                url: "{{ URL::to('/social/gettopcomments/') }}" ,
                type: "GET",
                data:{
                    id:id,
                },
                beforeSend: function()
                {
                    $('#showComments'+id).find('i').removeAttr('hidden');
                }
            })
            .done(function(data)
            {   
                $('#comments_block_div'+id).html(data);
                $('#comments_block'+id).removeAttr('hidden');

                $('#showComments'+id).find('i').attr('hidden','true');
                
                
            })
            .fail(function(){
                $('#showComments'+id).find('i').attr('hidden','true');
            });
        });
        $('[id*="showAllComments"').click(function(){
            var id=$(this).attr('sid');
            $.ajax(
            {
                url: "{{ URL::to('/social/getallcomments/') }}" ,
                type: "GET",
                data:{
                    id:id,
                },
                beforeSend: function()
                {
                    $('#showAllComments'+id).find('i').removeAttr('hidden');
                }
            })
            .done(function(data)
            {   
                $('#comments_block_div'+id).html(data);
                $('#comments_block'+id).removeAttr('hidden');

                $('#showAllComments'+id).attr('hidden','true');
                
                
            })
            .fail(function(){
                $('#showAllComments'+id).attr('hidden','true');
                
            });
        });
    });
</script>
@endsection