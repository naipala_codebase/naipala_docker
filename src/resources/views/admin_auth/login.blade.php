@extends('layouts.app')

@section('body')

    <div id="login">
        <section class="container my-5">
            <div class="row p-4">
                <div class="col-12">
                    <h4 class="mb-4" style="text-align:center">Admin Login</h4><br>
                    <form method="POST" action="{{ route('admin_login') }}">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                                <input type="text" class="form-control {{ $errors->has('email') ? 'border-error' : '' }}" placeholder="email@example.com" name="email" value="{{ old('email') }}" required autofocus>
                                @if ($errors->has('email'))
                                    <p style="margin-top: 10px;">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            <label  class="col-sm-3 col-form-label">Password</label>
                            <div class="col-sm-9">
                                <input type="password" class="form-control {{ $errors->has('password') ? 'border-error' : '' }}"  placeholder="Password" name="password" required>
                                @if ($errors->has('password'))
                                    <p style="margin-top: 10px;">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </p>
                                @endif
                            </div>
                        </div>
                        <div class="form-group flex-container justify-content-between align-items-stretch my-4">
                            <div class="form-check login-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="None" id="rememberme" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                                </label>
                            </div>
                            <div class="login-forgot">
                                <a href="{{ route('admin_password.request') }}">Forgot Password?</a>
                            </div>
                        </div>
                        <div class="text-center">
                            <button type="submit" class="btn custom-btn btn-block">Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>

@endsection
