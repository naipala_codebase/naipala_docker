@extends('layouts.app')

@section('body')
    <div id="register">
        <section class="container my-5">
            <div class="row p-4 text-center">
                <div class="col-12">
                    <h4 class="mb-4"><b>Admin Reset Password</b></h4>
                    <br>
                    @if (session('status'))
                        <p>
                            <strong>{{ session('status') }}</strong>
                        </p>
                    @endif
                    <form style="padding: 40px;" method="POST" action="{{ route('admin_password.email') }}">
                        {{ csrf_field() }}
                        
                        <div class="form-group">
                            <input type="email" class="form-control {{ $errors->has('email') ? 'border-error' : '' }}" style="background:#e8e8e8;" id="" placeholder="email@example.com" name="email" value="{{ old('email') }}" required>
                            @if ($errors->has('email'))
                                <p style="margin-top: 10px;">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </p>
                            @endif
                        </div>
                        
                        <br>
                        <div class="text-center" style="padding-top: 10px;">
                            <button class="btn custom-btn" type="submit">Send Password Reset Link</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    
@endsection