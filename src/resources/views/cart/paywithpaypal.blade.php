@extends('layouts.app')

@section('body')

    <?php $items = json_decode($transaction->details); ?>
    <div id="cartPage">
        <div class="container">
            <section class="container cart-pg-container my-5 p-0">
                <div class="row">
                    <div class="col-md-12">
                        <!--Shopping Cart table-->
                        <div class="">
                            <table class="table table-responsive table-hover cart-table my-0 pb-4 mb-4">
                                <!--Table head-->
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Details</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <!--/Table head-->

                                <!--Table body-->
                                <tbody id="purchaselist">
                                <?php $subtotal = 0 ?>
                                @foreach($items as $item)
                                    <tr>
                                        <td>
                                            <h5>
                                                <strong>{{ $item->item }}</strong> ({{ $item->quantity}}pcs)
                                            </h5>
                                        </td>
                                        <td>{{ ($item->details->color =='') ? $item->details->size : ($item->details->color.($item->details->size =='' ? '':'('. $item->details->size.')')) }}</td>

                                        <td>&nbsp;&nbsp;{{ \App\Currency::currency($item->price * $item->quantity) }}</td>
                                    </tr>
                                    <?php $subtotal += $item->price * $item->quantity ?>
                                @endforeach

                                @if($transaction->giftCard)
                                    <tr>
                                        <td>
                                            <h5>
                                                <strong>Gift Card Discount</strong> ({{ \App\Currency::currency($transaction->giftCard->discount) }})
                                            </h5>
                                        </td>
                                        <td></td>

                                        <td>- {{ ($transaction->total - $transaction->donation == 0) ? \App\Currency::currency($subtotal) : \App\Currency::currency($transaction->giftCard->discount) }}</td>
                                    </tr>
                                @endif

                                @if($transaction->donation != 0)
                                    <tr>
                                        <td>
                                            <h5>
                                                <strong>Donation</strong>
                                            </h5>
                                        </td>
                                        <td></td>

                                        <td>&nbsp;&nbsp;{{ \App\Currency::currency($transaction->donation) }}</td>
                                    </tr>
                                @endif
                                

                                <!--/Table body-->
                            </table>
                        </div>
                        <!--/Shopping Cart table-->
                    </div>
                </div>
            </section>
            <?php $itemCat = Request::get('itemCat'); ?>
            <span style="color:brown;">(The total amount is inclusive of 10% G.S.T. <b>{{ $itemCat!="Tickets" ? '&amp; Free Shipping':' ' }}</b>)</span>
            <section class="container my-4 p-3">
                <div class="row justify-content-between marginZero">
                    <div class="flex-container">
                        <h5><strong>{{ $itemCat=="Tickets" ? 'Billing':'Shipping' }} Address:</strong></h5> 
                        <h5><span style="margin-left:13px">{{ $transaction->shipping_address }}, {{ $transaction->apt_suit }} </span>
                        <span style="margin-left:13px">{{ $transaction->suburb }}, {{ $transaction->city }}, {{ $transaction->state }} {{ $transaction->postcode }}</span>
                        <span style="margin-left:13px"> {{ $transaction->country }}</span><h5>
                    </div>
                    <div class="flex-container total-block">
                        <h5><strong>Total</strong></h5>
                        <h5 class=""><span id="total">{{ \App\Currency::currency($transaction->total) }}</span></h5>
                    </div>
                </div>
            </section>
            @if($transaction->donation == 0)
                <section class="container my-4 p-3">
                    <div class="row justify-content-between marginZero">
                        <div>
                            <h5><strong>Make a Difference</strong><span style="font-size:15px !important;"> (Optional)</span></h5>
                            <div id="donationDiv">
                                
                                <h6>{!! $shipping_donation_text !!}</h6>
                                
                                <!-- <h6>Please select amount <select id="donationAmount"><option value="5" selected>$5</option><option value="10">$10</option><option value="15">$15</option><option value="20">$20</option><option value="25">$25</option><option value="30">$30</option></select>  to support</h6> -->
                                <h6>Please enter any amount {{ Session::get('currency')->symbol }} <input id="donationAmount" class="custom-input custom-input-option" type="number" min="1" value="1" style="width: 50px;"/> to support the needy.</h6>
                                <button class="btn custom-btn" id='donate' style="background-color: brown !important;">DONATE</button>
                            
                            </div>
                        </div>
                    </div>
                </section>
            @endif
            <section class="container my-4 p-3">
                <div class="row justify-content-between marginZero">
                    <a href="{{ route('cart.payment',$transaction->code) }}" class="btn btn-custom-outline">Edit Information</a>
                    
                    <div style="position: relative;" id="paypal" {{ $transaction->total==0 ? 'hidden':'' }}>
                        <button class="btn custom-btn">Proceed</button>
                        <div id="paypal-button" style="margin-top:6px;z-index:3;position: absolute;top: 0;left: 0;opacity:0.01"></div>
                    </div>
                    @if($transaction->total==0)
                        <div id="nopaypal">
                            <form method="POST" action="{{ route('paymentSuccess',$transaction->code) }}">
                                {{ csrf_field() }}
                                <button class="btn custom-btn" type="submit">Proceed</button>
                            </form>
                        </div>
                    @endif
                </div>
            </section>
        </div>
    </div>
@endsection

@section('js')

	<script type="text/javascript">
		$('.drawer-toggle-cart').attr('hidden','true');
    </script>

    <script type="text/javascript">
		$(document).ready(function(){

			$('#donate').click(function(){
				var donated= $('#donationAmount').val();
				
                var url = "/cart/"+'{{ $transaction->code }}'+"/donate";
                $('#donate').attr('disabled','true').html('Processing..');
				$.post(url,
				{
					_token: "{{ csrf_token() }}",
					donated : donated
				},
				function(data, status){  
                    $('#donationDiv').empty(); 	                    
                    $('#donationDiv').html('<span style="color:green;margin-top: 10px;">Thank you for making a difference!</span>');
                    
                    if(data.old == 0){
                        $('#nopaypal').remove();
                        $('#paypal').removeAttr('hidden');
                    }
                    $('#purchaselist').append('<tr><td><h5><strong>Donation</strong></h5></td><td></td><td>&nbsp;&nbsp;{{ Session::get('currency')->symbol }} '+donated+'</td></tr>');
                    $('#total').text(data.total);
				}).fail(function() {
                    aalert('Please enter amount at least $1.','','Oops');
                    $('#donate').removeAttr('disabled').html('Donate');
                });
			});
        });
    </script>
    <script src="https://www.paypalobjects.com/api/checkout.js" data-version-4></script>
  
    <script>
        paypal.Button.render({
        
            env: "{{ env('PAYPAL_CLIENT_MODE') }}",
            
            style: {
                size: 'small',
                color: 'black',
                shape: 'pill',
                label: 'pay'
            },

            payment: function(resolve, reject) {
                //This is your own API's endpoint
                var CREATE_PAYMENT_URL = "{{ route('postPaymentWithpaypal',$transaction->code) }}";
                //Hit the endpoint with a request
                paypal.request.post(CREATE_PAYMENT_URL,{ _token: "{{ csrf_token() }}" })
                        .then(function(data) { 
                            if(data.url)
                                window.location.href = data.url;
                            else
                                resolve(data.id)
                        })
                        .catch(function(err) {
                            reject(err);  
                            window.location.href = "{{ route('billingform',$transaction->code) }}"
                        });
                
            },
    
            onAuthorize: function(data) {
                //Your own API endpoint for executing an authorized payment
                var EXECUTE_PAYMENT_URL ="{{ route('payment.status',$transaction->code) }}";
                
                return paypal.request.post(EXECUTE_PAYMENT_URL,
                        { PaymentID: data.paymentID, PayerID: data.payerID,token: data.paymentToken, _token: '{{ csrf_token() }}' })
                        .then(function(data) { 
                            window.location.href = "{{ route('collection') }}";
                        })
                        .catch(function(err) { 
                            window.location.href = "{{ route('billingform',$transaction->code) }}";
                        });
            },
            onCancel:function(){
                window.location.href = "{{ route('billingform',$transaction->code) }}";
            }
                
        }, '#paypal-button');
    </script>
@endsection
