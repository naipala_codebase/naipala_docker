@extends('layouts.app')

@section('body')
<?php $items = json_decode($transaction->details);
$itemCat = $items[0]->details->itemCat;?>
	<form id="checkout" method="POST" action="{{ route('shippingInformation',$transaction->code) }}">
		{{ csrf_field() }}
		<input type="text" name="itemCat" value="{{$itemCat}}" hidden /> <!-- // to handle tickets -->
		@if(count($errors)>0)
			<div class="mt-5 flex-container">
				<p style="color:#c61616">Please Provide all the Information</p>
			</div>
		@endif
		<section class="container mt-5 mb-5">
			<div class="row">
				<div class="col-12">
					<div class="row">
						<div class="col-12 flex-container">
							<h5>Customer Information</h5>
							@if(Auth::guest())
								<span>Have an account with us?
									<a data-toggle="modal" data-target="#loginModal">Login</a>
								</span>
							@else
								<span>Not you?  <a href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a></span>
							@endif
						</div>
					</div>
		
					@if(Auth::guest())
						<div class="row">

							<div class="col-12">
								<div class="form-group">
									<input type="email" class="form-control {{ $errors->has('email') ? 'border-error' : '' }}" id="inputEmail" placeholder="Email" name="email" value="{{ $transaction->transactionable ? $transaction->transactionable->email : '' }}" required>
								</div>
								<div class="form-group">
									<div class="form-check">
										<label class="form-check-label">
											<input class="form-check-input" type="checkbox" name="subscribeme" checked> Subscribe to our newsletter
										</label>
									</div>
								</div>
							</div>
						</div>
					@else
						<div class="row">
							<div class="col-12">
								<span>Logged In as <strong>{{ Auth::user()->name }}</strong></span>
							</div>
						</div>
					@endif
				</div>
			</div>
		</section>
		<section class="container mt-5 mb-5">
			<div class="row">
				<div class="col-12">
					<div class="row mb-2">
						<div class="col-12">
							<h5>Shipping Address</h5>
						</div>
					</div>
	
					<div class="row">
						<div class="col-12">
							@if(Auth::guest())
								<div class="form-row">
									<div class="form-group col-md-6">
										<input type="text" class="form-control {{ $errors->has('fname') ? 'border-error' : '' }}" id="fName" placeholder="First name" name="fname" value="{{ $transaction->transactionable ? $transaction->transactionable->name : '' }}" required>
									</div>
									<div class="form-group col-md-6">
										<input type="text" class="form-control {{ $errors->has('lname') ? 'border-error' : '' }}" id="lName" placeholder="Last name" name="lname" required>
									</div>
								</div>
							@endif
							<div class="form-row">
								<div class="form-group col-12">
									<input type="text" class="form-control" id="companyName" name="companyName" placeholder="Company name (Optional)" value="{{ $transaction->companyName }}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-md-6">
									<input type="text" class="form-control  {{ $errors->has('shipping_address') ? 'border-error' : '' }}" id="Address" placeholder="Address Line 1" name="shipping_address" value="{{ $transaction->shipping_address ? $transaction->shipping_address : (Auth::check() ? Auth::user()->address :'') }}" required>
								</div>
								<div class="form-group col-md-6">
									<input type="text" class="form-control {{ $errors->has('apt_suit') ? 'border-error' : '' }}" id="apt" placeholder="Address Line 2 (Optional)" name="apt_suit" value="{{ $transaction->apt_suit ? $transaction->apt_suit : (Auth::check() ? Auth::user()->apt_suit :'') }}">
								</div>
							</div>
							<div class="form-row">
								<div class="form-group col-6">
									<input type="text" class="form-control {{ $errors->has('suburb') ? 'border-error' : '' }}" id="suburb" placeholder="Suburb" name="suburb" value="{{ $transaction->suburb ? $transaction->suburb : (Auth::check() ? Auth::user()->suburb :'') }}" required>
								</div>
								<div class="form-group col-6">
									<input type="text" class="form-control {{ $errors->has('city') ? 'border-error' : '' }}" id="city" placeholder="City" name="city" value="{{ $transaction->city ? $transaction->city : (Auth::check() ? Auth::user()->city :'') }}" required>
								</div>
							</div>
							<?php 
								$defaultCountry = $transaction->country ? $transaction->country : (Auth::check() ? (Auth::user()->country ? Auth::user()->country : Session::get('country')) : Session::get('country'));
								$defaultState = $transaction->state ? $transaction->state : (Auth::check() ? Auth::user()->state : '');
							?>
							<div class="form-row">
								<div class="form-group col-md-5">

									<select id="inputCountry" class="form-control {{ $errors->has('country') ? 'border-error' : '' }}" name="country" required>
										<option value="" selected disabled="">Choose Country</option>
										@foreach ($countries as $country)
											<option value="{{ $country->name }}" data-code="{{ $country->code }}" {{ $defaultCountry == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-5">
									<select id="inputState" class="form-control {{ $errors->has('state') ? 'border-error' : '' }}" name="state" required>
										@foreach ($countries as $country)
											<option value="" id="placeholder{{ $country->code }}" disabled country="{{ $country->code }}" {{ $defaultCountry == $country->name ? 'selected' : '' }} {{ old('country') == $country->name ? '' : ( old('country') == $defaultCountry ? '' : 'hidden') }}>Choose {{ $country->term_for_administrative_divisions }}</option>
											@foreach ($country->states->sortBy('name') as $state)
												<option value="{{ $state->code }}"  {{ $defaultState == $state->code ? 'selected' : '' }} country="{{ $country->code }}" {{ old('country') == $country->name ? '' : ( $defaultCountry == $country->name ? '' : 'hidden') }}>{{ $state->name }}</option>
											@endforeach
										@endforeach
									</select>
								</div>
								<div class="form-group col-md-2">
									<input type="text" class="form-control {{ $errors->has('postcode') ? 'border-error' : '' }}" id="inputZip" placeholder="Postcode" name="postcode" value="{{ $transaction->postcode ? $transaction->postcode : (Auth::check() ? Auth::user()->postcode :'') }}" required>
								</div>
							</div>
	
							<div class="form-group">
								<input type="text" class="form-control {{ $errors->has('shipping_contact') ? 'border-error' : '' }}" id="inputPhone" placeholder="Phone" name="shipping_contact" value="{{ $transaction->shipping_contact ? $transaction->shipping_contact : (Auth::check() ? Auth::user()->phone :'') }}" required>
							</div>
						</div>
					</div>
					<br>
					<div class="row mb-2">
						<div class="col-12">
							<h5>Help us get the word out !</h5>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="form-row">
								<div class="form-group col-12">
									<input type="text" class="form-control" id="agentId" name="agentId" placeholder="Referrer ID (Optional)" value="{{ $transaction->agent_code}}">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="container mt-5 mb-5">
			<p id="validateResponse" style="margin-bottom:5px"></p>			
			
			<div class="row justify-content-between marginZero">
				<div class="flex-container">
					<input type="text" id="discountCode" class="form-control" placeholder="Gift Code">
					<button class="btn custom-btn" type="button" id="validateCode">Redeem</button>
				</div>
				<div class="flex-container total-block">
					<h5><strong>Total &nbsp;</strong></h5>
					<h5 class=""><span id="total">{{ \App\Currency::currency($transaction->total - $transaction->donation) }}</span></h5>
				</div>
			</div>
		</section>
		<section class="container mt-5 mb-5">
			<div class="row justify-content-between marginZero">
				<div class="flex-container">
				</div>
				<div class="flex-container total-block">
					<button type="submit" class="btn custom-btn">Continue to payment method</button>
				</div>
			</div>
		</section>
	</form>

	<form method="POST" action="{{ route('cart.discard',$transaction->code) }}" id="cart-discard" hidden> 
		{{ csrf_field() }}
		<button id="confirmCartDiscard" type="button" class="btn btn-custom-outline">Discard</button>
	</form>
	
@endsection

@section('js')

	<script type="text/javascript">
		$('.drawer-toggle-cart').attr('hidden','true');
	</script>

	<script type="text/javascript">
		$(document).ready(function(){

			$('#validateCode').click(function(){
				
				var code= $('#discountCode').val();
				if(code.length==0) return;
				var url = "/cart/"+'{{ $transaction->code }}'+"/validateCode";
				$('#validateCode').attr('disabled','true').html('Validating..');
				$('#validateResponse').html('');
				$.post(url,
				{
					_token: "{{ csrf_token() }}",
					code:code
				},
				function(data, status){
					if(data.type=='success'){
						$('#validateResponse').css('color','green').html('Discount: '+data.msg);
						$('#total').text(data.total);
						$('#discountCode').val('');
					}
					else if(data.type=='error'){
						$('#validateResponse').css('color','red').html(data.msg);
					}
					else
						aalert('Try Again','','Oops');
					$('#validateCode').removeAttr('disabled').html('Redeem');
					
				});
			});

			var country = $('#inputCountry').find('option:selected').attr('data-code');
			$("#inputState option").attr('hidden','hidden');
			$("#inputState option[country='"+country+"']").removeAttr('hidden');
			if(!$("#inputState"))
				$("#placeholder"+country).attr('selected','selected');

		});

		$("#confirmCartDiscard").click(function(){
			$('#doIt').removeAttr('hidden');
			aalert('You will not be able to undo it','',"Are you sure?");
		});

		$('#doIt').click(function(){
			$('#cart-discard').submit();
		});

		$('#inputCountry').on('change',function(){
		  var country = $('#inputCountry').find('option:selected').attr('data-code');
          $("#inputState option").attr('hidden','hidden').removeAttr('selected');
          $("#inputState option[country='"+country+"']").removeAttr('hidden');
          $("#placeholder"+country).attr('selected','selected');
        });

	</script>
	

@endsection