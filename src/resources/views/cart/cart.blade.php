@extends('layouts.app')

@section('css')
    <link rel="stylesheet" href="{{asset('/frontend-assets/stackonly/tablesaw.stackonly.css')}}">
    <script src="{{asset('/frontend-assets/stackonly/tablesaw.stackonly.js')}}"></script>
    <script src="{{asset('/frontend-assets/stackonly/tablesaw-init.js')}}"></script>
    <style>
        [ng\:cloak], [ng-cloak], .ng-cloak {
          display: none !important;
        }
    </style>
@endsection

@section('body')

<div id="cartPage" ng-app="cartapp" ng-controller="CartController" ng-init="current={{ Session::get('currency') }};init()">
    <div class="container">
        <section class="container cart-pg-container my-4 p-0">
            <div class="row">
                <div class="col-md-12">
                    <!--Shopping Cart table-->
                    <div class="">
                        <h4 style="margin:25px" class="text-center"><strong>My Purchases</strong></h4>
                        <table class="table tablesaw tablesaw-stack table-hover cart-table my-0 pb-4" data-tablesaw-mode="stack" ng-hide="cartitems.length==0">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Product</th>
                                    <th>QTY</th>
                                    <th>Amount</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <!--Table body-->
                            <tbody>

                            <!--First row-->
                            <tr ng-repeat="c in cartitems">
                                <th scope="row" width="10%">
                                    <img ng-cloak ng-src="{{ URL::to('asset/uploads') }}/@{{ c.options['image'] }}/150/100" alt="@{{c.name}}" class="img-fluid-cart">
                                </th>
                                <td>
                                    <h5>
                                        <strong ng-cloak>@{{c.name}}</strong>
                                    </h5>
                                    <p class="text-muted" style="font-weight: 600;height: 0;">
                                        <span ng-cloak ng-hide="c.options['color']==''">@{{c.options['color']}}<span ng-hide="c.options['size']==''">, </span></span><span ng-cloak ng-hide="c.options['size']==''">@{{c.options['size']}}</span></td>
                                    </p>
                                    
                                </td>
                                
                                <td>
                                    <input ng-cloak ng-model="c.qty" ng-change="qtyChange(c.rowId)" class="form-control" type="number" id="qty@{{c.rowId}}" value="@{{c.qty}}" min="0" max="10">
                                    <span hidden id="upd@{{c.rowId}}" style="color:brown;"><a href="javascript:void(0)" ng-click="updateQuantity(c.rowId)"> Update<img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="updloading@{{c.rowId}}" ></img></a></span>
                                </td>
                                <td ng-cloak>@{{ currency_convert(c.price*c.qty) }}</td>
                                <td>
                                    <button ng-cloak type="button" ng-click="removeItem(c.rowId)" class="btn btn-sm btn-dark" title="Remove">X<img hidden src="{{asset('/img/loading.gif')}}" style="text-align:center" id="xloading@{{c.rowId}}"></img></button>
                                </td>
                                
                            </tr>
                            
                            <!--/Table body-->
                        </table>
                        <h5 class="text-center" style="margin:60px"><strong ng-show="cartitems.length==0" ng-cloak>Your Cart is Empty</strong></h5>
                    </div>
                    <!--/Shopping Cart table-->
                </div>
            </div>
        </section>
        <section class="container my-4 p-3">
            <div class="row justify-content-between marginZero">
                <div class="flex-container">
                    
                </div>
                <div class="flex-container total-block">
                    <h5><strong>Total</strong></h5>
                    <h5 class="" ng-cloak>@{{ currency_convert(total) }}</h5>
                </div>
            </div>
        </section>

        <section class="container my-4 p-3">
            <div class="row justify-content-between marginZero">
                <a href="{{ route('collection') }}" class="btn btn-custom-outline">Back</a>
                <form method="POST" action="{{ route('cart.purchase') }}"> 
                    {{ csrf_field() }}              
                    <button type="submit" class="btn custom-btn" ng-disabled="cartitems.length==0">Purchase</button>
                </form>
            </div>
        </section>
    </div>
    <input type="hidden" value="{{$cartitems}}" id="cartitems" name="purchases" ng-click="init()"/>    
</div>

@endsection
