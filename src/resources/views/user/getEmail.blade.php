@extends('layouts.app')

@section('body')
    <div id="register">
        <section class="container my-5">
            <div class="row p-4 text-center">
                <div class="col-12">
                    <h4 class="mb-4"><b>Register</b></h4>
                    <br>
                    <form style="padding: 40px;" method="POST" action="{{ route('setEmail') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" style="background:#e8e8e8;" class="form-control" name="name" value="{{ $user->name }}" disabled>
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control {{ array_has($errors,'email') ? 'border-error' : '' }}" style="background:#e8e8e8;" id="" placeholder="email@example.com" name="email" value="{{ old('email') }}" required>
                            @if (array_has($errors,'email'))
                                <p style="margin-top: 10px;">
                                    <strong>{{ $errors['email'] }}</strong>
                                </p>
                            @endif
                        </div>
                        <input type="hidden" name="user" value="{{json_encode($user)}}"/>
                        <br>
                        <div class="text-center" style="padding-top: 10px;">
                            <button class="btn custom-btn" type="submit">Create</button>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>
    
@endsection