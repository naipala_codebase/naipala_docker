@extends('layouts.app')

@section('body')
<link rel="stylesheet" href="https://code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.css" />
<style>
    .ui-datepicker {
        background: rgb(226, 224, 224);
    }
</style>
<div id="profile-page">
    <section class="container">
        <div class="row">
            <div class="col-md-3 profile-img">
                <div class="user-avatar-block">
                    <div class="user-avatar">
                        @if( Auth::user()->image!=null)
                            <img src="{{ asset('profile_images/' . Auth::user()->image) }}" alt="{{ Auth::user()->name }}">
                        @else
                            <img src="{{asset('frontend-assets/images/profile.png')}}" alt="{{ Auth::user()->name }}"/>
                        @endif
                    </div>
                </div>
                <div class="profile-actions" style="margin-bottom:20px">
                    <a class="my-3" data-toggle="modal" data-target="#editProfileModal">Edit profile</a>
                    <a class="btn custom-btn black-btn" href="{{ url('/logout') }}" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Log out</a>
                </div>
            </div>
            <div class="col-md-9 profile-details">
                <div class="custom-tabs">
                    <ul class="nav nav-tabs" id="profilepg-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link" id="info" data-toggle="tab" href="#infoBlock" role="tab"
                                aria-controls="info" aria-selected="true">Info</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="history" data-toggle="tab" href="#historyBlock" role="tab"
                                aria-controls="history" aria-selected="false">My History</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="post" data-toggle="tab" href="#postBlock" role="tab"
                                aria-controls="service" aria-selected="true">Post</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="applicants" data-toggle="tab" href="#applicantsBlock" role="tab"
                                aria-controls="applicants" aria-selected="false">Applicants</a>
                        </li>

                    </ul>

                    <div class="tab-content" id="profileTabContent">
                        <!--my info block-->
                        <div class="tab-pane fade" id="infoBlock" role="tabpanel"
                                aria-labelledby="info">
                            <section class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3>My Info</h3>
                                            <a data-toggle="modal" data-target="#editProfileModal"><i
                                                class="fa fa-pencil"></i></a>
                                        </div>
                                        <div class="card-body">
                                            <ul>
                                                <li>Name: <span style="margin-left:20px">{{ Auth::user()->name }}</span></li>
                                                <li>Email: <span style="margin-left:23px">{{ Auth::user()->email }}</span></li>
                                                
                                                <li {{ Auth::user()->address ==''?'hidden':'' }}>Address: 
                                                    <span>{{ Auth::user()->address }}, {{ Auth::user()->apt_suit }} </span><br>
                                                    <span style="margin-left:75px">{{ Auth::user()->suburb }}, {{ Auth::user()->city }}, {{ Auth::user()->state }} {{ Auth::user()->postcode }}</span><br>
                                                    <span style="margin-left:75px"> {{ Auth::user()->country }}</span>
                                                </li>
                                                <li {{ Auth::user()->dob ==''?'hidden':'' }}>DOB: <span style="margin-left:30px">{{ date('M j, Y',strtotime(Auth::user()->dob)) }}</span></li>
                                                
                                                <li {{ Auth::user()->phone ==''?'hidden':'' }}>Contact: <span>{{ Auth::user()->phone }}</span></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>

                        <!--purchase history block-->
                        <div class="tab-pane fade" id="historyBlock" role="tabpanel" aria-labelledby="history" ng-controller='HistoryController'>
                            <section class="row">
                                @if (count($histories) == 0)
                                    <div class='col-12 text-center'>
                                        <h4 style="margin-top:70px">No histories</h4>
                                    </div>
                                @else
                                    <div class="col-12" id="style-2-h">
                                    
                                        <div class="list-group" id="history-data">
                                            @include('user.partials.history')
                                        </div>
                                        <div class="ajax-load text-center" style="display:none;margin-top: 10px;" id='ajax-loadHistory'>
                                            <p>Loading More &nbsp;&nbsp;<img src="{{ asset('img/loading.gif') }}"></p>
                                        </div>
                                    </div>
                                @endif
                            </section>
                        </div>

                        <!--posts block-->
                        <div class="tab-pane fade" id="postBlock" role="tabpanel" aria-labelledby="service" ng-controller='ServiceController'>
                            <section class="add-posts">
                                <h4>My Posts</h4>
                                <button data-toggle="modal" data-target="#addPostModal" class="btn btn-custom-outline">Add <i class="fa fa-plus"></i></button>
                            </section>
                            <section class="row">
                                @if (count($services) == 0)
                                    <div class='col-12 services-pg-container text-center'>
                                        <h4 id='noservice' style="margin-top:30px">No services</h4>
                                    </div>
                                @endif
                                <div class="col-12 services-pg-container" id="style-2-s">
                                
                                    <div class="list-group" id="service-data">
                                        @include('user.partials.service')
                                    </div>
                                    <div class="ajax-load text-center" style="display:none;margin-top: 10px;" id='ajax-loadService'>
                                        <p>Loading More &nbsp;&nbsp;<img src="{{ asset('img/loading.gif') }}"></p>
                                    </div>
                                </div>
                            </section>
                            <!-- Modal AddPost -->
                            <div class="modal fade" id="addPostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Add Post Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form ng-submit='add()'>                                        
                                            <div class="modal-body">
                                                <div class="flex-container">
                                                    <p style="color:#d80707" ng-show="addError" >Please Provide all the Information</p>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="title" class="col-sm-3 col-form-label">Title</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="title" style="" name="title" ng-model='_service.title' placeholder="Enter Title" required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="service_cat" class="col-sm-3 col-form-label">Category</label>
                                                    <div class="col-sm-9">
                                                        <select id="service_cat" class="form-control" placeholder="Select a Category" ng-model="_service.service_category_id" required>
                                                            <option value=""  disabled selected>Please select a category</option>
                
                                                            @foreach($service_categories as $category)
                                                                <option value='{{ $category->id }}'>{{ $category->title }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="description" class="col-sm-3 col-form-label">Description</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" id="description" rows='10' placeholder="Enter Description of your service" ng-model="_service.description" required></textarea>
                                                    </div>
                                                </div>
                                            
                                            </div>
                                            <div class="modal-footer">
                                                <button id="addingPost" type='submit' class="btn btn-dark">Add Post</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>

                            <!-- Modal EditPost -->
                            <div class="modal fade" id="editPostModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Edit Post Details</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <form ng-submit='edit()'>                                        
                                            <div class="modal-body">
                                                <div class="flex-container">
                                                    <p style="color:#d80707" ng-show="editError" >Please Provide all the Information</p>
                                                </div>
                                                <input id="editid" type="hidden" ng-model="e_service.id"/>
                                                <div class="form-group row">
                                                    <label for="edittitle" class="col-sm-3 col-form-label">Title</label>
                                                    <div class="col-sm-9">
                                                        <input type="text" class="form-control" id="edittitle" style="" name="title" ng-model='e_service.title' placeholder="Enter Title"  required>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="eservice_cat" class="col-sm-3 col-form-label">Category</label>
                                                    <div class="col-sm-9">
                                                        <select id="eservice_cat" class="form-control" placeholder="Select a Category" ng-model="e_service.service_category_id" required>
                                                            @foreach($service_categories as $category)
                                                                <option value='{{ $category->id }}'>{{ $category->title }} </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                
                                                <div class="form-group row">
                                                    <label for="editdescription" class="col-sm-3 col-form-label">Description</label>
                                                    <div class="col-sm-9">
                                                        <textarea class="form-control" rows="10" id="editdescription" placeholder="Enter Description of your service" ng-model="e_service.description" required></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button id="editingPost" type='submit' class="btn btn-dark">Edit Post</button>
                                            </div>
                                        </form>                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!--application-block-->
                        <div class="tab-pane fade" id="applicantsBlock" role="tabpanel" aria-labelledby="applicants">
                                
                            @if(count($servicetitles)!=0)
                                <div class="row mb-4">
                                    <div class="col-12">
                                        <select class="custom-select-option" id="titleselect">
                                            @foreach($servicetitles as $key =>$value)
                                                <option value="{{$key}}">{{$value}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            @endif

                            @if (count($notifications) == 0)
                                <div class='col-12 text-center'>
                                    <h4 style="margin-top:100px">No applicants</h4>
                                </div>
                            @endif
                            
                            <section class="row">
                                <div class="col-12">
                                    @foreach($servicetitles as $key => $value)
                                        <div class="list-group" id="apct{{$key}}" hidden>   
                                            @foreach($notifications as $n)
                                                @if($n->service_id==$key)
                                                    <div id='card{{$n->id}}' class="list-group-item list-group-item-action flex-column align-items-start">
                                                        <div class="d-flex w-100 justify-content-between">
                                                            <small>
                                                                <?php 
                                                                    $interval = \Carbon\Carbon::createFromTimeStamp(strtotime($n->created_at))->diffForHumans();
                                                                ?>
                                                                {{ $interval }}
                                                            </small>
                                                        </div>
                                                        <div class="mb-1 applicant-details">
                                                            @if($n->bookerImage=='')
                                                                <img src="{{asset('frontend-assets/images/profile.png')}}" alt="{{ $n->bookerName }}"/>
                                                            @else
                                                                <img src="{{ asset('profile_images/' . $n->bookerImage) }}" alt="{{ $n->bookerName }}"/>
                                                            @endif 
                                                            
                                                            <ul>
                                                                <li><b>Name:</b> {{ $n->bookerName }}</li>
                                                                <li><b>Title:</b> {{ $n->title }}</li>
                                                                <li><b>Category:</b> {{ $n->category }}</li>
                                                            </ul>
                                                        </div>
                                                        <small class="pull-right" style="margin-top:-10px;color: #285caf;"><a href= 'javascript:void(0)' id="viewdetail{{$n->id}}" onclick='viewdetails({{$n->id}},{{$n->seen}})'>View details</a></small>
                                                        
                                                        @if($n->seen==0)
                                                            <section class="application-badge" id="newbadge{{$n->id}}">
                                                                
                                                            </section>
                                                        @endif
                                                        <div id="detail{{$n->id}}" hidden>

                                                            <p><b>Email: </b>{{ $n->bookerEmail }}</p>
                                                            <p {{ $n->bookerPhone==''?'hidden':'' }}><b>Phone: </b>{{ $n->bookerPhone }}</p>
                                                            <p {{ $n->bookerMessage==''?'hidden':'' }}><b>Message: </b>{{ $n->bookerMessage }}</p>
                                                            <p {{ $n->bookerCV==''?'hidden':'' }}><b>CV: </b><a href="{{ asset('cvs/'.$n->bookerCV)}}" style="color: #285caf;" target=_blank>View</a></p>
                                                            <small class="pull-right" style="margin-top:-10px;color: #285caf;"><a href= 'javascript:void(0)' id="hidedetail{{$n->id}}" onclick='hidedetails({{$n->id}})'>Hide details</a></small>
                                            
                                                        </div>
                                                    </div>
                                                @endif
                                            @endforeach
                                            <br>
                                            <a id="viewall{{$key}}" sid="{{$key}}" style="color: #285caf;" href= 'javascript:void(0)'><text style="text-align: center;">View all applicants &nbsp;&nbsp;</text><img hidden id="loading{{$key}}" src="{{asset('/img/loading-11.gif')}}"/></a>
                                        </div>
                                    @endforeach         
                                </div>
                            </section>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </section>
</div>

<!-- Modal Edit Profile-->
<div class="modal fade" id="editProfileModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top:-15px">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title"> Edit Profile</h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body custom-tabs">
                <div class="flex-container profile-img mb-4">
                    <div class="user-avatar-block">
                        <a href="javascript:void(0)" onclick='uploadImage()' title="Edit Image" class="user-avatar">
                            @if( Auth::user()->image!=null)
                                <img src="{{ asset('profile_images/' . Auth::user()->image) }}" alt="{{ Auth::user()->name }}">
                            @else
                                <img src="{{asset('frontend-assets/images/profile.png')}}" alt="{{ Auth::user()->name }}"/>
                            @endif
                        </a>
                        <form id='imguploadForm1' enctype="multipart/form-data" method='POST' action="{{ route('user.image') }}" hidden>
                            {{  csrf_field() }}
                            <input type="file" name="image" id='imgupload' accept="image/*">
                        </form>
                    </div>
                </div>
                <form method="POST" id="test" action="{{ route('profile.edit') }}">
                    {{  csrf_field() }}
                    @if(count($errors->profileedit)>0)
                        <div class="text-center">
                            <p style="color:#c61616">Please Provide all the Information</p>
                        </div>
                    @endif
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <input type="text" class="form-control {{ $errors->profileedit->has('name') ? 'border-error' : '' }}" placeholder="User Name" name="name" value='{{ Auth::user()->name }}' required>
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control  {{ $errors->profileedit->has('address') ? 'border-error' : '' }}" id="Address" placeholder="Address Line 1" name="address" value="{{ Auth::user()->address }}" required>
                        </div>
                        <div class="form-group col-md-6">
                            <input type="text" class="form-control {{ $errors->profileedit->has('apt_suit') ? 'border-error' : '' }}" id="apt" placeholder="Address Line 2 (Optional)" name="apt_suit" value="{{ Auth::user()->apt_suit }}">
                        </div>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-6">
                            <input type="text" class="form-control {{ $errors->profileedit->has('suburb') ? 'border-error' : '' }}" id="suburb" placeholder="Suburb" name="suburb" value="{{ Auth::user()->suburb }}" required>
                        </div>
                        <div class="form-group col-6">
                            <input type="text" class="form-control {{ $errors->profileedit->has('city') ? 'border-error' : '' }}" id="city" placeholder="City" name="city" value="{{ Auth::user()->city }}" required>
                        </div>
                    </div>

                    <?php 
                        $defaultCountry = Auth::user()->country ? Auth::user()->country : Session::get('country');
                        $defaultState = Auth::user()->state;
                    ?>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <select id="inputCountry" class="form-control {{ $errors->profileedit->has('country') ? 'border-error' : '' }}" name="country" required>
                                <option value="" selected disabled="">Choose Country</option>
                                @foreach ($countries as $country)
                                    <option value="{{ $country->name }}" data-code="{{ $country->code }}" {{ $defaultCountry == $country->name ? 'selected' : '' }}>{{ $country->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-5">
                            <select id="inputState" class="form-control {{ $errors->profileedit->has('state') ? 'border-error' : '' }}" name="state" required>
                                @foreach ($countries as $country)
                                    <option value="" id="placeholder{{ $country->code }}" disabled country="{{ $country->code }}" {{ $defaultCountry == $country->name ? 'selected' : '' }} {{ old('country') == $country->name ? '' : ( old('country') == $defaultCountry ? '' : 'hidden') }}>Choose {{ $country->term_for_administrative_divisions }}</option>
                                    @foreach ($country->states->sortBy('name') as $state)
                                        <option value="{{ $state->code }}"  {{ $defaultState == $state->code ? 'selected' : '' }} country="{{ $country->code }}" {{ old('country') == $country->name ? '' : ( $defaultCountry == $country->name ? '' : 'hidden') }}>{{ $state->name }}</option>
                                    @endforeach
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="text" class="form-control {{ $errors->profileedit->has('postcode') ? 'border-error' : '' }}" id="inputZip" placeholder="Postcode" name="postcode" value="{{ Auth::user()->postcode }}" required>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" class="form-control {{ $errors->profileedit->has('dob') ? 'border-error' : '' }}" id="inputDOB" placeholder="Date of Birth" name="dob" value="{{ Auth::user()->dob }}" required autocomplete="off" readonly>
                    </div>
                    
                    <div class="form-group">
                        <input type="text" class="form-control {{ $errors->profileedit->has('contact') ? 'border-error' : '' }}" id="inputPhone" placeholder="Phone" name="phone" value="{{ Auth::user()->phone }}" required>
                    </div>
                    
                    <div class="flex-container justify-content-between">
                        <button type="button" class="btn custom-btn" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn custom-btn">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
    <script type="text/javascript">
        if({{ count($errors->profileedit)}} > 0){
            $('#editProfileModal').modal('show');
        }

        var tselect=$('#titleselect').val();
        $('#apct'+tselect).removeAttr('hidden');
        
        if((window.location.hash.split('#')[1]+'s')=='applicants'){
            $('#applicants').attr('class',"nav-link active");
            $('#applicantsBlock').attr('class',"tab-pane fade show active");
            window.location.hash = '';
        }
        else{
            $('#info').attr('class',"nav-link active");
            $('#infoBlock').attr('class',"tab-pane fade show active");
        }

        $(window).on('hashchange', function() {
            var active = window.location.hash.split('#')[1];
            if((active+'s')=='applicants'){
                window.location.reload();
            }
        });

        $('#titleselect').change(function(){
            var k = $('#titleselect').val();
            $('[id*="apct"]').attr('hidden','true');
            $('#apct'+k).removeAttr('hidden');
        });

        $('[id*="viewall"]').click(function(){
            sid=$(this).attr('sid');
            $('#loading'+sid).removeAttr('hidden');
            $.get("{{URL::to('profile/viewallapplicants')}}",{service_id:sid},function(data,success){
                $('#loading'+sid).attr('hidden','true');
                $('#apct'+sid).empty();  
                $('#apct'+sid).html(data);
            }); 
        });

        function viewdetails(id,seen){
            $("#viewdetail"+id).attr('hidden','true');
            $("#detail"+id).removeAttr('hidden');
            if(seen==0)
            {
                $.ajax(
                {
                    url: '/notification/' + id +'/read',
                    type: "post",
                    data:{'_token':"{{csrf_token()}}"}
                })
                .done(function(response)
                {
                    if(response==0){
                        var d = parseInt($('#notification').text())-1;
                        $( "#notification" ).html(d);
                        if(d==0){
                            $( "#notification" ).attr('hidden','true');
                            document.title = 'Naipala';
                        }
                        else{
                            document.title = "(" + d + ") " + 'Naipala';
                        }

                        $('#newbadge'+id).attr('hidden',true);
                    }
                    
                })
                .fail(function(){
                    window.location.reload();
                });
            }
        }

        function hidedetails(id){
            $("#detail"+id).attr('hidden','true');
            $("#viewdetail"+id).removeAttr('hidden');
        }

        function editP(post){
            $('#edittitle').val(post.title);
            $('#editdescription').val(post.description);
            $('#editid').val(post.id);
            $('#eservice_cat').val(post.service_category_id);
            $('#editPostModal').modal('show');
        }

        function uploadImage(){
            $('#imgupload').trigger('click');
        }
    
        $("#imgupload").on("change", function() {
            $("#imguploadForm1").submit();
        });
    </script>

    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $( function() {
            $( "#inputDOB" ).datepicker({
                dateFormat: 'yy-mm-dd',
                yearRange: "-100:+0",
                changeMonth: true,
                changeYear: true,
                beforeShow: function(input, inst) {
                    $(document).off('focusin.bs.modal');
                },
                onClose:function(){
                    $(document).on('focusin.bs.modal');
                }
            });
        } );

        $(document).ready(function(){

            var country = $('#inputCountry').find('option:selected').attr('data-code');
            $("#inputState option").attr('hidden','hidden');
            $("#inputState option[country='"+country+"']").removeAttr('hidden');
            if(!$("#inputState"))
                $("#placeholder"+country).attr('selected','selected');
        });

        $('#inputCountry').on('change',function(){
		  var country = $('#inputCountry').find('option:selected').attr('data-code');
          $("#inputState option").attr('hidden','hidden').removeAttr('selected');
          $("#inputState option[country='"+country+"']").removeAttr('hidden');
          $("#placeholder"+country).attr('selected','selected');
        });
    </script>
    
    <script src="{{asset('frontend-assets/scripts/profile.js')}}"></script>
@endsection