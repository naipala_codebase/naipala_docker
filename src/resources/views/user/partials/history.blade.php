@foreach($histories as $h)
    <div class="list-group-item list-group-item-action flex-column align-items-start">
        <div class="d-flex w-100">
            <div class="cart-item">
                <div class="cart-img">
                    <a href="{{ URL::to('collection/item').'/'. $h['slug'] }}" target='_blank'>
                        <img src="{{ URL::to('asset/uploads/'. $h['item']->details->image .'/110/110') }}" alt="{{ $h['item']->item }}">
                    </a>
                </div>
                <div class="cart-details">
                    <ul>
                        <li>{{ $h['item']->item }}</li>
                        <li>{{ \App\Currency::currency($h['item']->quantity*$h['item']->price) }} ({{ $h['item']->quantity }} pcs)</li>
                        <li>{{ date('M j, Y ', strtotime($h['purchased_date'])) }}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endforeach    

