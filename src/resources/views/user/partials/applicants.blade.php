@foreach($applicants as $n)
  <div id='card{{$n->id}}' class="list-group-item list-group-item-action flex-column align-items-start">
    <div class="d-flex w-100 justify-content-between">
        <small>
            <?php 
                $interval = \Carbon\Carbon::createFromTimeStamp(strtotime($n->created_at))->diffForHumans();
            ?>
            {{ $interval }}
        </small>
    </div>
    <div class="mb-1 applicant-details">
        @if($n->bookerImage=='')
            <img src="{{asset('frontend-assets/images/profile.png')}}" alt="{{ $n->bookerName }}"/>
        @else
            <img src="{{ asset('profile_images/' . $n->bookerImage) }}" alt="{{ $n->bookerName }}"/>
        @endif 
        
        <ul>
          <li><b>Name:</b> {{ $n->bookerName }}</li>
          <li><b>Title:</b> {{ $n->title }}</li>
          <li><b>Category:</b> {{ $n->category }}</li>
        </ul>
    </div>
    <small class="pull-right" style="margin-top:-10px;color: #285caf;"><a href= 'javascript:void(0)' id="viewdetail{{$n->id}}" onclick='viewdetails({{$n->id}},{{$n->seen}})'>View details</a></small>
    
    @if($n->seen==0)
        <section class="application-badge" id="newbadge{{$n->id}}">
            
        </section>
    @endif
    <div id="detail{{$n->id}}" hidden>

        <p><b>Email: </b>{{ $n->bookerEmail }}</p>
        <p {{ $n->bookerPhone==''?'hidden':'' }}><b>Phone: </b>{{ $n->bookerPhone }}</p>
        <p {{ $n->bookerMessage==''?'hidden':'' }}><b>Message: </b>{{ $n->bookerMessage }}</p>
        <p {{ $n->bookerCV==''?'hidden':'' }}><b>CV: </b><a href="{{ asset('cvs/'.$n->bookerCV.'/'.$n->bookerCV)}}" style="color: #285caf;">View</a></p>
        <small class="pull-right" style="margin-top:-10px;color: #285caf;"><a href= 'javascript:void(0)' id="hidedetail{{$n->id}}" onclick='hidedetails({{$n->id}})'>Hide details</a></small>

    </div>
  </div>

@endforeach