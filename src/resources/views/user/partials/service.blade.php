@foreach($services as $s)
    <section class="card services-info-list">
        <div class="card-header si-list-header">
            <section class="si-header-date">
                <div>
                    <span>{{ date('M j, Y ', strtotime($s->updated_at)) }}</span>
                </div>

            </section>
            <section class="si-header-title">
                <div>
                    <h3>{{ $s->title }}</h3>
                </div>
                <div>
                    <a href="#" class="badge badge-dark">{{ $s->service_category->title }}</a>
                </div>
            </section>
        </div>
        <div class="card-body si-list-body">
            <p class="" style="white-space: pre-line;">
                {{ $s->description }}
            </p>
        </div>
        <div class="si-list-footer">
            <button onclick="editP({{$s}})" class="btn btn-secondary small-btn"> Edit Post</button>
            <button class="btn btn-dark small-btn" id='cross{{$s->id}}'  title="delete post" ng-click='delete({{$s->id}})'>Remove Post</button>
        </div>
    </section>

@endforeach