<!DOCTYPE html>
<html>
<head>
<title></title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
<style type="text/css">
/* CLIENT-SPECIFIC STYLES */
body, table, td, a { -webkit-text-size-adjust: 100%; -ms-text-size-adjust: 100%; }
table, td { mso-table-lspace: 0pt; mso-table-rspace: 0pt; }
img { -ms-interpolation-mode: bicubic; }

/* RESET STYLES */
img { border: 0; height: auto; line-height: 100%; outline: none; text-decoration: none; }
table { border-collapse: collapse !important; }
body { height: 100% !important; margin: 0 !important; padding: 0 !important; width: 100% !important; }

/* iOS BLUE LINKS */
a[x-apple-data-detectors] {
    color: inherit !important;
    text-decoration: none !important;
    font-size: inherit !important;
    font-family: inherit !important;
    font-weight: inherit !important;
    line-height: inherit !important;
}

/* MEDIA QUERIES */
@media screen and (max-width: 480px) {
    .mobile-hide {
        display: none !important;
    }
    .mobile-center {
        text-align: center !important;
    }
}


/* ANDROID CENTER FIX */
div[style*="margin: 16px 0;"] { margin: 0 !important; }

#blockquote {
  background: #f9f9f9;
  border-left: 10px solid #ccc;
  margin: 1.5em 10px;
  padding: 0.5em 10px;
  quotes: "\201C""\201D""\2018""\2019";
}

#blockquote p {
  display: inline;
}
</style>
</head>
<body style="margin: 0 !important; padding: 0 !important;">



<table border="0" cellpadding="0" cellspacing="0" width="100%">
    <tr>
        <td align="center">
        <!--[if (gte mso 9)|(IE)]>
        <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
        <tr>
        <td align="center" valign="top" width="600">
        <![endif]-->
        <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;background-image:url({{ asset('frontend-assets/images/body.png') }});">
            
            <tr style="background-image:url({{ asset('frontend-assets/images/header1.png') }});background-repeat:no-repeat;">
                <td align="center" valign="top" style="padding-bottom: 35px">
     
                </td>
            </tr>
            <tr>
                <td align="center" valign="top" style="font-size:0; font-family:brandon-grotesque; font-size: 36px; font-weight: 800; line-height: 48px;">
                    <a href="{{ route('home') }}"><img width="137" src="{{ asset('frontend-assets/images/naipala_logo.png') }}" alt="Naipala" height="47"></a>
                            
                </td>
            </tr>
            
            <tr>
                <td align="center" style="padding: 35px;">
                <!--[if (gte mso 9)|(IE)]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                <td align="center" valign="top" width="600">
                <![endif]-->
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                    <tr>
                        <td align="center" style="font-family: brandon-grotesque; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 25px;">
                            
                            <h2 style="font-size: 30px; font-weight: 800; line-height: 36px; color: #333333; margin: 0;">
                                Naipala E-Gift Card Gifted
                            </h2>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="font-family: brandon-grotesque; font-size: 16px; font-weight: 400; line-height: 24px; padding-top: 15px;">
                            <p style="font-size: 18px; font-weight: 800; line-height: 24px; color: #333333;">
                                Hello Anisha,
                            </p>
                            <div style="background: #f9f9f9;
                            border-left: 10px solid #ccc;
                            margin: 1.5em 10px;
                            padding: 0.5em 10px;"><p style="display: inline">
                                I believe every human has a finite number of heartbeats. I don't intend to waste any of mine.
                                </p><br>
                                <div style="text-align: right">-Alisha</div>
                            </div>
                            <p style="text-align:center;font-size: 16px; font-weight: 400; line-height: 24px; color: #333333;">
                                Gift Card Discount : $50<br>
                                Gift Code: htsvgsb
                                <div style="text-align:center;margin:20px">
                                    <a href="{{ route('collection') }}" style="background-color:#23272b;color:#ffffff;display:inline-block;font-family:brandon-grotesque;text-transform: uppercase;font-size:14px;font-weight:regular;line-height:45px;text-align:center;text-decoration:none;width:155px;-webkit-text-size-adjust:none;mso-hide:all;">Shop Now</a>
                                </div>
                            </p>
                            <p style="font-size: 16px; font-weight: 400; line-height: 24px; color: #333333;">                              
                                Thanks,<br>
                                Naipala Team
                            </p>
                        </td>
                    </tr>
     
                </table>
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
                </td>
            </tr>

            <tr>
                <td align="center" style="padding-left: 35px;padding-right: 35px;">
                <!--[if (gte mso 9)|(IE)]>
                <table align="center" border="0" cellspacing="0" cellpadding="0" width="600">
                <tr>
                <td align="center" valign="top" width="600">
                <![endif]-->
                <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:600px;">
                    
                    <tr class="row">
                        <td align="left" width="30%" style="font-family: brandon-grotesque; font-size: 10px; font-weight: 400; line-height: 24px; padding: 5px 0 10px 0;">
                            <p style="font-size: 10px; font-weight: 400; line-height: 18px; color: #333333;">
                                <i class="fa fa-map-marker" style="color: #cc1f45"></i> &nbsp;&nbsp;3/15 KING STREET<br>
                                <i class="fa fa-map-marker" style="color: #cc1f45"></i>  &nbsp;&nbsp;ASHFIELD, 2131<br>
                                <i class="fa fa-phone" style="color: #cc1f45"></i>  &nbsp;&nbsp;043 537 2002 | 045 041 9803
                            </p>
                        </td>
                        <td align="center" width="40%" style="font-family: brandon-grotesque; font-size: 14px; font-weight: 400; line-height: 24px; padding: 5px 0 10px 0;">
                            <p style="font-size: 20px; font-weight: 500; line-height: 18px; color: #cc1f45;margin-bottom: 3px;">
                                <b>NAIPALA PTY LTD</b>
                            </p>
                            <p style="font-size: 15px; font-weight: 400; line-height: 18px; color: #333333;margin-top: 0px;">
                                ABN: 46 617 717 398
                            </p>
                        </td>
                        <td align="right" width="30%" style="font-family: brandon-grotesque; font-size: 10px; font-weight: 400; line-height: 24px; padding: 5px 0 10px 0;">
                            <p style="font-size: 10px; font-weight: 400; line-height: 18px; color: #333333;">
                                <i class="fa fa-globe" style="color: #cc1f45"></i> &nbsp;&nbsp;www.naipala.com<br>
                                <i class="fa fa-envelope" style="color: #cc1f45"></i> &nbsp;&nbsp;info@naipala.com<br>
                                <i class="fa fa-facebook-square" style="color: #cc1f45"></i> <i class="fa fa-instagram" style="color: #cc1f45"></i> &nbsp;&nbsp;/naipala.com.au
                            </p>
                        </td>
                    </tr>
                    
                </table>
                <!--[if (gte mso 9)|(IE)]>
                </td>
                </tr>
                </table>
                <![endif]-->
                </td>
            </tr>
            <tr style="background-image:url({{ asset('frontend-assets/images/footer1.png') }});background-repeat:no-repeat;">
                <td align="center" valign="top" style="padding-bottom: 15px;">
                </td>
            </tr>
            {{--  <tr></tr>  --}}
        </table>
        <!--[if (gte mso 9)|(IE)]>
        </td>
        </tr>
        </table>
        <![endif]-->
        </td>
    </tr>
</table>

</body>
</html>
