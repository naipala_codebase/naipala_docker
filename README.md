# Naipala Project Docker Setup
A pretty simplified docker-compose workflow that sets up a LEMP network of containers for local Laravel development for Naipala.


## Prerequisites
- **Important Points to be noted**
  - In Ubuntu or any linux server, make sure you create a non-root user and install docker or docker-compose in that user.
    This will prevent root permission issues.
  - Allow https protocol (google it for how to do it)

- **How To Install and Use Docker on Ubuntu 18.04?**
  - `https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04`

- **How To Install Docker Compose on Ubuntu 18.04?**
  - `https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04`

- **Refer the youtube video tutorial to setup the project in Ubuntu**
  - `https://www.youtube.com/watch?v=G5Nk4VykcUw`

- **How To Install Docker/Docker Compose on MacOS?**
  - `https://docs.docker.com/docker-for-mac/install/`


**The acutal Naipala Laravel app is in the `src` directory for this docker configuration and should must be there before bringing the containers up, otherwise the artisan container will not build, as it's missing the appropriate file. So please do not mess up with the default configs.** 

## Usage

Lets get started!

Open a terminal and clone this repository. cd into the project's root folder.

## Run application in Local/Dev envs
1. Local environment wihtout SSL
  - Copy contents of .env.local settings to .env
  - Run command below to spin up the containers
    - sudo docker-compose -f docker-compose.local.yml up -d
  - Run all the command listed in "Run docker commands" down below**
  - Open up your browser of choice to http://localhost:8088
2. Local Dev enironment with SSL
  - Open /etc/host and put aliases `naipalalocal.dev` for 127.0.0.1 and `naipalalocal.prod` for 127.0.0.1
  - Run either of the commands below to spin up the containers
    - sudo docker-compose -f docker-compose.yml up -d
    - sudo docker-compose -f docker-compose.dev.yml up -d
  - Copy contents of .env.dev settings to .env


## Run application in Production env
- Copy contents of .env.production settings to .env
- Run command below to spin up the containers
  - sudo docker-compose -f docker-compose.prod.yml up -d
- Run all the command listed in "Run docker commands" down below**
- Open up your browser of choice to https://naipala.com


## For More info on Productin Env, contact with Admins
  - Sarthak Joshi
  - Safal Joshi


## Run docker commands**

Open a terminal and from the cloned respository's root, use the following command templates from your project root, modifiying them to fit your particular use case:

- `docker-compose run --rm composer install`
- `docker-compose run --rm composer update`
- `docker-compose run --rm npm install --save-dev cross-env`
- `docker-compose run --rm npm install`
- `docker-compose run --rm npm run dev`
- `docker-compose run --rm artisan migrate:refresh` 
- `docker-compose run --rm artisan db:seed` 


## Containers created and their ports (if used) are as follows:

- **nginx** - `:80`
- **mysql** - `:4306`
- **php** - `:9000`
- **npm**
- **composer**
- **artisan**
- **redis** - `:6379`


## Migration issue (if you face it):
(Please skip step 1, 2 and 3 if the container/service name i.e. "mysql" works fine for DB_HOST.)

1. Fire this command to check the IP addresses of each services in the network:
  - `docker inspect -f '{{.Name}} - {{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(docker ps -aq)`
2. You will see output something like this:
  `/composer - /nginx - 172.18.0.6 /artisan - /php - 172.18.0.2 /npm - /mysql - 172.18.0.3 `
3. Grab the IP address of mysql from Step #2 and update in DB_HOST's value in .env file.
(Please skip step 1, 2 and 3 if the container/service name i.e. "mysql" works fine for DB_HOST.)
4. Fire config clear
  - `docker-compose run --rm artisan config:clear`
5. Fire cache clear
  - `docker-compose run --rm artisan cache:clear`
6. Fire migrate refresh
  - `docker-compose run --rm artisan migrate:refresh`
